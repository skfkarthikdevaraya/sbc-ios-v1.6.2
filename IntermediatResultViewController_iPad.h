//
//  IntermediatResultViewController_iPad.h
//  fiveinone
//
//  Created by Admin on 21/08/14.
//  Copyright (c) 2014 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculationDataManager.h"


@interface IntermediatResultViewController_iPad : UIViewController  <CalculationDataManagerDelegate, UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate>{
    
    
    UITableView *intermediatResultTableView;
    NSMutableArray *resultArray;
    NSMutableArray *intermediatresultsParameterArray;
    
    NSString *cssString;

    
}

@property (nonatomic, retain) IBOutlet UITableView *intermediatResultTableView;
@property (nonatomic, retain) NSMutableArray *intermediatresultsParameterArray;
@property (nonatomic, retain) NSMutableArray *resultArray;
@property (nonatomic, retain)     NSString *cssString;


@end
