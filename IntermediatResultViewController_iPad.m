//
//  IntermediatResultViewController_iPad.m
//  fiveinone
//
//  Created by Admin on 21/08/14.
//  Copyright (c) 2014 HiQ. All rights reserved.
//

#import "IntermediatResultViewController_iPad.h"
#import "ParameterResult.h"
#import "ParameterDatabase.h"
#import "ParameterAvailable.h"
#import "NSString+StripHTML.h"
#import "TitleManager.h"
#import "HyperlinkViewController.h"




@interface IntermediatResultViewController_iPad ()

@end

@implementation IntermediatResultViewController_iPad


@synthesize intermediatResultTableView;
@synthesize intermediatresultsParameterArray;
@synthesize resultArray;
@synthesize cssString;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.rightBarButtonItem = [TitleManager getSKFLogo];
    
    NSString *navigationBarTintColor = NAVIGATION_TEXT_COLOR;
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithHexString:navigationBarTintColor]];
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    self.title = [calculationDataManager getTranslationString:@"Results"];
    
    
    intermediatresultsParameterArray = [[NSMutableArray alloc]init];
    
    cssString = @"<html> \n"
    "<head> \n"
    "<style type=\"text/css\"> \n"
    "body {font-family: \"SKF Chevin Medium\"; font-size: 12; margin:4px 0px;background-color:transparent; color:#333333; word-wrap:break-word;}\n"
    "</style> \n"
    "</head> \n";

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(float) calculateHeightOfTextFromWidth:(NSString*)text: (UIFont*)withFont: (float)width :(UILineBreakMode)lineBreakMode
{
    
    NSString *textAdd = [text stringByAppendingString:@"MM"];
    [text retain];
    [withFont retain];
    CGSize suggestedSize = [textAdd sizeWithFont:withFont constrainedToSize:CGSizeMake(width, FLT_MAX) lineBreakMode:lineBreakMode];
    CGSize lineHeightSize = [textAdd sizeWithFont:withFont];
    
    float height = 0;
    if (![text isEqualToString:@""])
    {
        CGFloat rows = suggestedSize.height / lineHeightSize.height;
        height = rows * (lineHeightSize.height + 5);
        height += 5;
    }
    [text release];
    [withFont release];
    
    return height;
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 85.0;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return resultArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UIWebView *nameWebView;
    UIWebView *descriptionLabel;
    UILabel *resultLabel;

    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        nameWebView = [[UIWebView alloc] initWithFrame:CGRectMake(20, 5, 200, 60)];
        nameWebView.backgroundColor = [UIColor clearColor];
        nameWebView.opaque = NO;
        nameWebView.dataDetectorTypes = UIDataDetectorTypeLink;
        nameWebView.delegate = self;
        nameWebView.tag = 10;
        [cell addSubview:nameWebView];
        [nameWebView release];
        
        
        descriptionLabel= [[UIWebView alloc] initWithFrame:CGRectMake(20, 28, 230, 12)];
        descriptionLabel.backgroundColor = [UIColor clearColor];
        descriptionLabel.userInteractionEnabled = false;
        descriptionLabel.opaque = NO;
        descriptionLabel.dataDetectorTypes = UIDataDetectorTypeNone;
        descriptionLabel.tag = 20;
        [cell addSubview:descriptionLabel];
        [descriptionLabel release];
        
        resultLabel = [[UILabel alloc] initWithFrame:CGRectMake(200, 7, 100, 35)];
        resultLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
        resultLabel.textAlignment = NSTextAlignmentCenter;
        resultLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
        resultLabel.backgroundColor = [UIColor clearColor];
        resultLabel.tag = 100;
        [cell addSubview:resultLabel];
        [resultLabel release];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    
    nameWebView = (UIWebView *)[cell viewWithTag:10];
    descriptionLabel = (UIWebView *)[cell viewWithTag:20];
    resultLabel = (UILabel *)[cell viewWithTag:100];
    // Set up the cell...
   // if(indexPath.row > 0)
    {
    
    NSLog(@" ..... %d" , indexPath.row);
    ParameterResult *parameterResult = [resultArray objectAtIndex:indexPath.row];
    
   
    
    
    NSString *nameString;
    
    if (!parameterResult.uom)
    {
        nameString = parameterResult.presentation;
        [nameWebView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, parameterResult.presentation] baseURL:[NSURL URLWithString:@""]];
    }
    else
    {
        nameString = [NSString stringWithFormat:@"%@ [%@]", parameterResult.presentation, parameterResult.uom];
        [nameWebView loadHTMLString:[NSString stringWithFormat:@"%@%@ [%@]", cssString, parameterResult.presentation, parameterResult.uom] baseURL:[NSURL URLWithString:@""]];
    }
    
    [descriptionLabel loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, parameterResult.description] baseURL:[NSURL URLWithString:@""]];
    
    resultLabel.text = parameterResult.value;
    
    UIFont *nameFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
    UIFont *resultFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
    UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
    
    float nameHeight = [self calculateHeightOfTextFromWidth:[NSString  stringByStrippingHTML:nameString] :nameFont :200 :NSLineBreakByWordWrapping];
    float resultHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:parameterResult.value] :resultFont :100 :NSLineBreakByWordWrapping];
    float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:parameterResult.description] :descriptionFont :210 :NSLineBreakByWordWrapping];
    
    float totalHeight = MAX(nameHeight+descriptionHeight+5, resultHeight);
    
    descriptionLabel.frame = CGRectMake(descriptionLabel.frame.origin.x, descriptionLabel.frame.origin.y, descriptionLabel.frame.size.width, descriptionHeight+10);
    resultLabel.frame = CGRectMake(resultLabel.frame.origin.x, resultLabel.frame.origin.y, resultLabel.frame.size.width, resultHeight+10);
    

    
    
    }
        return cell;
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [intermediatresultsParameterArray removeAllObjects];
    intermediatresultsParameterArray = nil;
    
    
}


-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;
{
    NSURL *requestURL =[ [ request URL ] retain ];
    NSString *urlString = [requestURL absoluteString];
    NSURL *hyperlinkURL = nil;
    
    if ([urlString isEqualToString:@"file:///"] == false) {
        
        CalculationDataManager *calculateDataManager = [CalculationDataManager sharedManager];
        urlString = [urlString stringByReplacingOccurrencesOfString:@"file:///" withString:[NSString stringWithFormat:@"%@/",[calculateDataManager baseURL]]];
        NSLog(@"URL Path: %@",urlString);
        hyperlinkURL = [NSURL URLWithString:urlString];
        [self showHyperlinkView:hyperlinkURL];
    }
    [requestURL release ];
    return YES;
}

-(void) showHyperlinkView:(NSURL *) url{
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    UIWebView *webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    [webView loadRequest:request];
    webView.scalesPageToFit = YES;
    
    HyperlinkViewController *hyperlinkView = [[[HyperlinkViewController alloc]
                                               initWithNibName:@"HyperlinkViewController" bundle:nil]autorelease];
    [hyperlinkView.view addSubview:webView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
    [self.navigationController pushViewController:hyperlinkView animated:YES];
    [UIView commitAnimations];
}




@end
