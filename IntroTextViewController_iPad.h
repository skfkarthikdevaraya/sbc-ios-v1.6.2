//
//  IntroTextViewController_iPad.h
//  fiveinone
//
//  Created by Admin on 22/11/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IntroTextViewController.h"

@interface IntroTextViewController_iPad : IntroTextViewController {
    
    UIButton *checkbox;
    BOOL checkboxSelected;
    
    BOOL showingAlert;
    
    
}
@property (nonatomic, assign) BOOL showingAlert;
@property (nonatomic, assign) BOOL connected;


@property (nonatomic, retain) UIButton *checkbox;
@property (nonatomic, assign) BOOL checkboxSelected;


-(IBAction)buttonNextClicked:(id)sender;
-(void)ButtonCheckboxSelected:(id)sender;
@end
