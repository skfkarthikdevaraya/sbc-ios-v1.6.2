//
//  IntroTextViewController_iPad.m
//  fiveinone
//
//  Created by Admin on 22/11/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import "IntroTextViewController_iPad.h"
#import "MainViewController_iPad.h"
#import "LegalInformation_iPad.h"

@implementation IntroTextViewController_iPad

@synthesize checkbox;
@synthesize checkboxSelected;
@synthesize connected;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setSearchingModeStart];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:RKReachabilityDidChangeNotification
                                               object:nil];
    
    showingAlert = false;
    connected = true;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}


- (void)reachabilityChanged:(NSNotification*)notification {
    RKReachabilityObserver* observer = (RKReachabilityObserver*)[notification object];
    
    if (![observer isNetworkReachable])
    {
      //  [self setSearchingModeEnd];
        connected = false;
        [self showingAlert];
        DLog(@"Disconnected");
    }
    if ([observer isNetworkReachable]) {
        connected = true;
        DLog(@"Connected");
    }
}



-(void)ButtonCheckboxSelected:(id)sender {
    
    [checkbox setBackgroundImage:[UIImage imageNamed:@"checked2.png"] forState:UIControlStateSelected];
    [checkbox setBackgroundImage:[UIImage imageNamed:@"checked2.png"] forState:UIControlStateHighlighted];

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults boolForKey:@"DONOTSHOW"] == NO) {
        
        [userDefaults setBool:YES forKey:@"DONOTSHOW"];
        
    } else {
        
        [userDefaults setBool:NO forKey:@"DONOTSHOW"];
    }
    
    [userDefaults synchronize];
    
    checkboxSelected = !checkboxSelected;
    [checkbox setSelected:checkboxSelected];
    
}


-(IBAction)buttonNextClicked:(id)sender {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];

    calculationDataManager.stopIntroText = 1;

    
    if (!connected) {
        
        [calculationDataManager showAlertForNetworkConnection];
        return;
        
    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if (![[prefs valueForKey:@"firstrun"] isEqualToString:@"NO"]) {
        
        LegalInformation_iPad *legalInformationController = [[LegalInformation_iPad alloc]initWithNibName:@"LegalInformation" bundle:nil];
        
        legalInformationController.modalPresentationStyle = UIModalPresentationFormSheet;
        legalInformationController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal; //transition shouldn't matter
       // [self presentModalViewController:legalInformationController animated:YES];
        [self presentViewController:legalInformationController animated:YES completion:nil];
        legalInformationController.view.superview.frame = CGRectMake(0, 0, 320, 462);//it's important to do this after
    
        if( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait ||  [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown){
            legalInformationController.view.superview.center = self.view.center;
            
        } else if ( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight) {
            
            legalInformationController.view.superview.center = self.view.center;

        }
 

    }
    
    [self dismissModalViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:NO completion:nil];

    
    
}

- (void)setSearchingModeStart {
    
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    
    HUD = [[MBProgressHUD alloc] initWithFrame:CGRectMake(35, 100, 250, 250)];
    HUD.labelText = [calculationDataManager getTranslationString:@"Searching"];
    HUD.graceTime = 0.5f;
    HUD.taskInProgress = YES;
    HUD.allowsCancelation = YES;
    HUD.delegate = self;
    HUD.labelFont = [UIFont fontWithName:CHEVIN_BOLD size:20.f];
    [self.view addSubview:HUD];
    [HUD show:YES];
}

//- (void)setSearchingModeEnd {
//    
//    [HUD hide:YES];
//    HUD.taskInProgress = NO;
//    [HUD removeFromSuperview];
//    [self.view setUserInteractionEnabled:YES];
//    self.navigationItem.hidesBackButton = NO;
//    [HUD release];
//    HUD = nil;
//    
//}


@end
