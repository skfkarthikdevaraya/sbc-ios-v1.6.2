//
//  MainViewController_iPad.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/13/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "MainViewController_iPad.h"
#import "FilterBearingsViewController_iPad.h"
#import "Product.h"
#import "Property.h"
#import "ParameterMissing.h"
#import "ParameterResult.h"
#import "ResultsViewController.h"
#import "SelectCalculationViewController_iPad.h"
#import "OptionSelectViewController_iPad.h"
#import "FamilySelectViewController_iPad.h"
#import "Warning.h"
#import "Error.h"
#import "Message.h"
#import "ParameterDatabase.h"
#import "LegalInformation_iPad.h"
#import "fiveinoneAppDelegate_iPad.h"
#import "Option.h"
#import "Family.h"
#import "ParameterAvailable.h"
#import "NSString+StripHTML.h"
#import "SettingsViewController.h"
#import "CalculationDataManager.h"
#import "Product.h"
#import "IntroTextViewController_iPad.h"
#import "HyperlinkViewController.h"
#import "ReportsViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import <QuartzCore/QuartzCore.h>

NSString *lastString;
NSInteger lastnumber;


@implementation MainViewController_iPad

@synthesize helpIllustrationBearing;
@synthesize helpIllustrationCalculationType;
@synthesize noParametersAvailableError;
@synthesize bearingContainerRectPortrait;
@synthesize inputParametersContainerRectPortrait;
@synthesize inputParametersTableRectPortrait;
@synthesize resultItemRectPortrait;
@synthesize resultsContainerScrollViewRectPortrait;
@synthesize resultItemTableViewRectPortrait;
@synthesize doCalculationButtonRectPortrait;
@synthesize doResetButtonRectPortrait;
@synthesize doReportButtonRectPortrait;
@synthesize lineViewRectPortrait;
@synthesize removeCalculationTypeButtonRectPortrait;
@synthesize bearingContainerRectLandscape;
@synthesize inputParametersContainerRectLandscape;
@synthesize inputParametersTableRectLandscape;
@synthesize resultItemRectLandscape;
@synthesize resultsContainerScrollViewRectLandscape;
@synthesize resultItemTableViewRectLandscape;
@synthesize doCalculationButtonRectLandscape;
@synthesize doResetButtonRectLandscape;
@synthesize doReportButtonRectLandscape;
@synthesize lineViewRectLandscape;
@synthesize removeCalculationTypeButtonRectLandscape;
@synthesize skfChevinTitleLabel;
@synthesize removedView;
@synthesize changeBearingButton;
@synthesize changeBearingPopover;
@synthesize settingsPopover;
@synthesize selectedProduct;
@synthesize calculatedProduct;
@synthesize indexPathsForTextFields;
@synthesize cssString;
@synthesize cssStringGray;
@synthesize cssStringBlue;
@synthesize cssStringRed;
@synthesize cssStringBrgParam;
@synthesize inputParametersTableiPad;
@synthesize doCalculationButton;
@synthesize doResetButton;
@synthesize doReportButton;
@synthesize lineView;
@synthesize resultsViewContainer;
@synthesize bearingViewContainer;
@synthesize parametersViewContainer;
@synthesize bearingTitleLabel;
@synthesize bearingTypeTextTitle;
@synthesize bearingTypeTextContent;
@synthesize topNavigationBar;
@synthesize topBarNavigationItem;
@synthesize topToolbar;
@synthesize topToolbarLabel;
@synthesize addCalculationButton;
@synthesize addCalculationPopover;
@synthesize optionSelectPopover;
@synthesize intermediatResultPopover;
@synthesize calculationResultsCount;
@synthesize resultsContainerScrollView;
@synthesize resultsContainerScrollViewContent;
@synthesize currTextField;
@synthesize backgroundImage;
@synthesize currentDeviceOrientation;
@synthesize keyboardToolBar;
@synthesize activeField;
@synthesize storedReturnedDictionaries;
@synthesize infoButton;
@synthesize logoImageView;
@synthesize tabsOverlapping;
@synthesize keyboardIsShowing;
@synthesize keyboardIsShowingOption;
@synthesize removeCalculationTypeButton;
@synthesize resultArray;
@synthesize resultItemView;
@synthesize resultsTable;
@synthesize arrayWithCalcID;
@synthesize calculationsCount;
@synthesize lastvalue;
@synthesize HUD;
@synthesize parameterYbearingMinLoad;
@synthesize parameterOilJet;
@synthesize noFormulaErrorArray;
@synthesize objectsToDiscard;
@synthesize params;
@synthesize selectedCalsView;
@synthesize hyperlinkPopover;
@synthesize reportId;
@synthesize lastReportFileName;
@synthesize isLanscape;
@synthesize reportViewPopover;
@synthesize selectCalculationViewController;
@synthesize parameterSwitch;
@synthesize textFieldFrequencyiPad;
@synthesize textFieldTimeiPad;
@synthesize lubClean_grease_iPad;
@synthesize lubClean_oil_iPad;
@synthesize lubClean_parameter_iPad;
@synthesize selectedCalculationId;
@synthesize freqVal;
@synthesize timeVal;
@synthesize whereInter;
@synthesize hideIntermediate;
@synthesize intermediate_result;
@synthesize intermediate_result_composite ;
@synthesize what_plain_brearing ;
@synthesize  resultViewProductImage;
//@synthesize ProdDataImage;

#define TBL_SEC_ERRORS 0
#define TBL_SEC_RESULTS 1
#define TBL_SEC_WARNINGS 2
#define TBL_SEC_MESSAGES 3
#define TBL_SEC_INPUT_PARAMETERS 4
#define TBL_SEC_PRODUCT_DATA 5
#define TAG_CELL_NAME_LBL  6710010
#define TAG_CELL_DESCR_LBL 6710020
#define TAG_CELL_PARAM_LBL 6710030
#define TAG_CELL_PARAM_SWITCH 6710040
#define TAG_CELL_PARAM_FIELD 6710100
#define TAG_CELL_SEPARATOR_VIEW 6710050
#define ID_MULTIPLE_CALCULATIONS 100


-(void)dealloc{
    CalculationDataManager* calculatorDataManager = [CalculationDataManager sharedManager];
    [calculatorDataManager removeObserver:self];
    
    [noFormulaErrorArray removeAllObjects];
    [noFormulaErrorArray release];
    noFormulaErrorArray=nil;
    
    [params removeAllObjects];
    [params release];
    params = nil;
    
    self.settingsPopover = nil;
    //REFACT    self.missingInputValues = nil;
    
    [_calculationControl release];
    _calculationControl = nil;
    
    //[selectedCalsLabel release];
    // selectedCalsLabel = nil;
    [super dealloc];
}

-(void)reorganizeTabs {
    int totalSpaceAvailable = parametersViewContainer.frame.size.width;
    //int freeSpace = totalSpaceAvailable - addCalculationButton.frame.size.width - buttonTextSize.width;
    int totalTabsWidth = 0;
    int numberOfTabs = 0;
    int newPos = 30;
    
    int viewTag=0;
    /*if(calculationDataManager.mutlipleCalculation == true) {
        
        viewTag = ID_MULTIPLE_CALCULATIONS;
    }
    else {
        
        viewTag = [calculationDataManager.selectedCalculation.calculationId intValue];
    }*/
    
    viewTag = selectedCalculationId;

    
    for(UIButton *tabs in [self.view subviews]) {
        if ([tabs isKindOfClass:[UIButton class]] && tabs.tag > 9999) {
            tabs.titleEdgeInsets=UIEdgeInsetsMake(4, 20, 0, 20);
            [[tabs viewWithTag:50] setHidden:YES];
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.3];
            [UIView setAnimationDelay:0];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            CGSize textSize = [tabs.titleLabel.text sizeWithFont:[tabs.titleLabel font]];
            totalTabsWidth += textSize.width+40;
            numberOfTabs++;
            
            
            if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {
                
                tabs.frame = CGRectMake(newPos, 235, textSize.width+40, 40);
                
            }else {
                
                 tabs.frame = CGRectMake(newPos, 275, textSize.width+40, 40);
            }


            
            UIImage *calcButtonImage = [UIImage imageNamed:@"ipad_tab_button_inactive.png"];
            UIImage *calcButtonImageStrechable = [calcButtonImage stretchableImageWithLeftCapWidth:12 topCapHeight:0];
            
            [tabs setBackgroundImage:calcButtonImageStrechable forState:UIControlStateNormal];
            [tabs setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateNormal];
            
            newPos += textSize.width+40;
            [UIView commitAnimations];
        }
        
        if((viewTag+9999) == tabs.tag) {
            
            //CGSize activeTab = [tabs.titleLabel.text sizeWithFont:[tabs.titleLabel font]];
            //freeSpace -= activeTab.width;
            UIImage *calcButtonImage = [UIImage imageNamed:@"ipad_tab_button.png"];
            UIImage *calcButtonImageStrechable = [calcButtonImage stretchableImageWithLeftCapWidth:12 topCapHeight:0];
            
            [tabs setBackgroundImage:calcButtonImageStrechable forState:UIControlStateNormal];
            [tabs setTitleColor:[UIColor colorWithRed:0.f/0.f green:0.f/0.f blue:0.f/0.f alpha:1.f] forState:UIControlStateNormal];
            tabs.titleEdgeInsets=UIEdgeInsetsMake(4, 35, 0, 20);
            CGSize textSize = [tabs.titleLabel.text sizeWithFont:[tabs.titleLabel font]];
            
            if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {
                
                tabs.frame = CGRectMake(tabs.frame.origin.x, 235, textSize.width+55, 40);
                
            }else {
                
                tabs.frame = CGRectMake(tabs.frame.origin.x, 275, textSize.width+55, 40);
            }
            
            

            [[tabs viewWithTag:50] setHidden:NO];
            
            newPos+=15;
            
        }
    }
    
    CGRect addCalcFrame = addCalculationButton.frame;
    addCalcFrame.origin.x = newPos;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelay:0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    addCalculationButton.frame = addCalcFrame;
    
    [UIView commitAnimations];
    
    newPos += addCalculationButton.frame.size.width;
    
    if(newPos > totalSpaceAvailable) {
        
        newPos = 30;
        int sharedSpace = totalSpaceAvailable - addCalculationButton.frame.size.width - [self.view viewWithTag:(viewTag+9999)].frame.size.width - 30;
        
        for(UIButton *tabs in [self.view subviews]) {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.3];
            [UIView setAnimationDelay:0];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            if ([tabs isKindOfClass:[UIButton class]] && tabs.tag > 9999 && (viewTag+9999) != tabs.tag) {
                
                tabs.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
                
                CGFloat width = numberOfTabs <= 1 ? sharedSpace : sharedSpace / (numberOfTabs-1);
                
                if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {
                    
                   tabs.frame = CGRectMake(newPos, 235, width, 40);
                    
                }else {
                    
                   tabs.frame = CGRectMake(newPos, 275, width, 40);
                }
                
                newPos += tabs.frame.size.width;
            } else if ([tabs isKindOfClass:[UIButton class]] && tabs.tag > 9999 && (viewTag+9999) == tabs.tag) {
                
                CGSize textSize = [tabs.titleLabel.text sizeWithFont:[tabs.titleLabel font]];
                
                if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {
                    
                    tabs.frame = CGRectMake(newPos, 235, textSize.width+55, 40);
                    
                }else {
                    
                    tabs.frame = CGRectMake(newPos, 275, textSize.width+55, 40);
                }
                
                newPos += tabs.frame.size.width;
            }
            
            [UIView commitAnimations];
            
        }
        
        CGRect addCalcFrame2 = addCalculationButton.frame;
        addCalcFrame2.origin.x = newPos;
        
        addCalculationButton.frame = addCalcFrame2;
        
    }
    
}

/** WARNINGS **/
- (void)dataManagerReturnedParametersWarnings:(NSArray *)returnedArray
{
    [_calculationControl updateWarnings:returnedArray];
    Warning* noFormulaWarning= [_calculationControl getNoFormulaWarning];
    
    //REFACT     warningsArray = [returnedArray copy];
    //REFACT     DLog(@"Warnings Array:%@", warningsArray);
    //REFACT     Warning *noFormulaError = nil;
    //REFACT     for (Warning *warning in warningsArray) {
    //REFACT         if ([warning.key isEqualToString:@"NoFormula"]) {
    //REFACT             noFormulaError = warning;
    //REFACT             DLog(@"warning:%@", warning);
    //REFACT             break;
    //REFACT         }
    //REFACT     }
    
    
    if (noFormulaWarning != nil) {
        [noParametersAvailableError removeFromSuperview];
        noParametersAvailableError = [[DisplayParametersErrorIllustration_iPad alloc] initWithFrame:CGRectMake(60, 20, 539, 223)];
        noParametersAvailableError.helpString = noFormulaWarning.description;
        noParametersAvailableError.opaque = NO;
        noParametersAvailableError.backgroundColor = [UIColor clearColor];
        [parametersViewContainer addSubview:noParametersAvailableError];
        //REFACT         [warningsArray release];
        //REFACT         warningsArray = nil;
    }
}

/** ERRORS **/
- (void)dataManagerReturnedParametersErrors:(NSArray *)returnedArray
{
    [_calculationControl updateErrors:returnedArray];
    //Error* noFormulaError= [_calculationControl getNoFormulaError];
    
    //REFACT    errorsArray = [returnedArray copy];
    //REFACT    DLog(@"Errors Array:%@", errorsArray);
    //REFACT    Error *noFormulaError = nil;
    //REFACT    for (Error *error in errorsArray) {
    //REFACT        if ([error.key isEqualToString:@"NoFormula"]) {
    //REFACT            noFormulaError = error;
    //REFACT            DLog(@"error:%@", error);
    //REFACT            break;
    //REFACT        }
    //REFACT    }
    
    Error* noFormulaError;
    
    for (int i = 0 ; i <[_calculationControl getNoFormulaErrors].count ; i++) {
        noFormulaError = [[_calculationControl getNoFormulaErrors] objectAtIndex:i];
        [noFormulaErrorArray addObject:noFormulaError];
    }
    isHeaderShown = YES;
    [doResetButton setHidden:YES];
    
}
-(IBAction)showLegalInfo:(id)sender {
    
    if (!connected) {
        
        [self showAlertForNetworkConnection];
        return;
    }
    
    
    
    LegalInformation_iPad *legalInformationController = [[LegalInformation_iPad alloc]initWithNibName:@"LegalInformation" bundle:nil];
    
    legalInformationController.modalPresentationStyle = UIModalPresentationFormSheet;
    legalInformationController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal; //transition shouldn't matter
    
    [self presentModalViewController:legalInformationController animated:YES];
    
    legalInformationController.view.superview.frame = CGRectMake(0, 0, 320, 462);//it's important to do this after
    
    if( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait ||  [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown){
        legalInformationController.view.superview.center = self.view.center;
        
    } else if ( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
        legalInformationController.view.superview.center = CGPointMake(500, 390);
    }
    
    [legalInformationController release];
}


-(float) calculateHeightOfTextFromWidth:(NSString*)text: (UIFont*)withFont: (float)width :(UILineBreakMode)lineBreakMode
{
    NSString *textAdd = [text stringByAppendingString:@"MM"];
    [text retain];
    [withFont retain];
    CGSize suggestedSize = [textAdd sizeWithFont:withFont constrainedToSize:CGSizeMake(width, FLT_MAX) lineBreakMode:lineBreakMode];
    CGSize lineHeightSize = [textAdd sizeWithFont:withFont];
    
    float height = 0;
    if (![text isEqualToString:@""])
    {
        CGFloat rows = suggestedSize.height / lineHeightSize.height;
        height = rows * (lineHeightSize.height + 8);
        height += 5;
    }
    [text release];
    [withFont release];
    
    return height;
}


-(UITableViewCell*)getCellFromTableView:(UITableView*)tableView Sender:(id)sender {
    CGPoint pos = [sender convertPoint:CGPointZero toView:tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:pos];
    return [tableView cellForRowAtIndexPath:indexPath];
}

- (void)prevButton:(id)sender {
    
    UITableViewCell *cell = [self getCellFromTableView:inputParametersTableiPad Sender:activeField];
    if(cell== nil) {
        
        return;
    }
    
    NSLog(@"Cell tag:%ld", (long)cell.tag);
    
    //UITableViewCell *cell = (UITableViewCell*) [activeField superview];
    NSIndexPath *currentPath = [inputParametersTableiPad indexPathForCell:cell];
    
    int currentPosition = [indexPathsForTextFields indexOfObject:currentPath];
    int nextFieldIndex;
    
    if(currentPosition==0) {
        
        [activeField becomeFirstResponder];
        return;
    }
    
    if (currentPosition > 0 && currentPosition < ([indexPathsForTextFields count])) {
        
        nextFieldIndex = currentPosition-1;
    } else {
        nextFieldIndex = [indexPathsForTextFields count]-1;
    }
    
    NSLog(@"nextFieldIndex:%d", nextFieldIndex);
    NSIndexPath *nextIndexPath = [indexPathsForTextFields objectAtIndex:nextFieldIndex];
    NSLog(@"nextIndexPath.row:%d", nextIndexPath.row);
    
    UITableViewCell *nextCell = [inputParametersTableiPad cellForRowAtIndexPath:nextIndexPath];
    
    if(nextCell == nil) {
        
        NSArray *indexPaths = [NSArray arrayWithObject:nextIndexPath];
        
        [inputParametersTableiPad scrollToRowAtIndexPath:nextIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:UITableViewRowAnimationNone];
        [inputParametersTableiPad reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        nextCell = [inputParametersTableiPad cellForRowAtIndexPath:nextIndexPath];
    }
    
    activeField = (UITextField *)[nextCell viewWithTag:TAG_CELL_PARAM_FIELD];
    [activeField becomeFirstResponder];
    
    
}


- (void)nextButton:(id)sender {
    
    UITableViewCell *cell = [self getCellFromTableView:inputParametersTableiPad Sender:activeField];
    
    if(cell== nil) {
        return;
    }
    
    NSLog(@"Cell tag:%ld", (long)cell.tag);
    
    //UITableViewCell *cell = (UITableViewCell*) [activeField superview];
    NSIndexPath *currentPath = [inputParametersTableiPad indexPathForCell:cell];
    
    int currentPosition = [indexPathsForTextFields indexOfObject:currentPath];
    int nextFieldIndex;
    
    if(currentPosition == ([indexPathsForTextFields count]-1)) {
        [activeField becomeFirstResponder];
        return;
    }
    
    if (currentPosition+1 < ([indexPathsForTextFields count])) {
        nextFieldIndex = currentPosition+1;
    } else {
        nextFieldIndex = 0;
        [inputParametersTableiPad setContentOffset:CGPointMake(0, 0)];
    }
    
    NSLog(@"nextFieldIndex:%d", nextFieldIndex);
    NSIndexPath *nextIndexPath = [indexPathsForTextFields objectAtIndex:nextFieldIndex];
    UITableViewCell *nextCell = [inputParametersTableiPad cellForRowAtIndexPath:nextIndexPath];
    
    if(nextCell != nil) {
        activeField = (UITextField *)[nextCell viewWithTag:TAG_CELL_PARAM_FIELD];
        [activeField becomeFirstResponder];
    }
    
}
/*************** NOTIFICATIONS ***************/


-(void)ipadLegalInfoAgreed: (NSNotification*)notification {
    
    //[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];

}

/***** Dismiss Popover ****/

-(void)dismissChangeBearingPopover: (NSNotification*)notification {
    
    if ([changeBearingPopover isPopoverVisible]) {
        [changeBearingPopover dismissPopoverAnimated:YES];
        [changeBearingPopover release];
    }
}

-(void)paintBearingParams:(int)numberOfParameters
{
    changeBearingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [changeBearingButton addTarget:self
                            action:@selector(changeBearing:)
                  forControlEvents:UIControlEventTouchUpInside];
    [changeBearingButton setTitle:GetLocalizedString(@"Select_bearing") forState:UIControlStateNormal];
    changeBearingButton.frame = CGRectMake(20, 19, 128, 37);
    changeBearingButton.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON];
    changeBearingButton.titleLabel.textColor = [UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f];
    
    UIImage *calcButtonImage = [UIImage imageNamed:@"ipad_blue_button.png"];
    UIImage *calcButtonImageStrechable = [calcButtonImage stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [changeBearingButton setBackgroundImage:calcButtonImageStrechable forState:UIControlStateNormal];
    [changeBearingButton setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateSelected];
    [changeBearingButton setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateNormal];
    [bearingViewContainer addSubview:changeBearingButton];
    
    bearingTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(180, 12, 450, 50)];
    bearingTitleLabel.font = [UIFont fontWithName:CHEVIN_BOLD size:IPAD_FONT_SIZE_FOR_BEARING_TITLE];
    bearingTitleLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    [bearingViewContainer addSubview:bearingTitleLabel];
    
    /** BEARING DIAMETER IMAGE */
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    NSLog(@"type: %@", selectedProduct.type);
    
    if ((![selectedProduct.type isEqualToString:@"3_1"]) && (![selectedProduct.type isEqualToString:@"3_3a"]) &&  (![selectedProduct.type isEqualToString:@"3_3b"]) &&  (![selectedProduct.type isEqualToString:@"3_4"]) &&  (![selectedProduct.type isEqualToString:@"3_3A"])) {
        
        UIImageView *prodDataImage_iPad = [[[UIImageView alloc] initWithFrame:CGRectMake(420, -10, 220, 150)] autorelease];
        
        float scale1 = [UIScreen mainScreen].scale;
        CGSize diaImageViewSize = CGSizeMake(320, 280);
        CGSize diaImageSize = CGSizeMake(scale1 * (diaImageViewSize.width - 1), scale1 * (diaImageViewSize.height - 1));
        
        NSMutableString* diaImagePath =
        [NSMutableString stringWithFormat:@"/Image?bearingType=%@&productId=%@&withDia=true&measurement=%@&width=%d&height=%d",
         selectedProduct.type, selectedProduct.productId,[calculationDataManager selectedMeasurement],
         (int)diaImageSize.width, (int)diaImageSize.height];
        
        NSMutableString* diaProductImageUrl = [NSMutableString stringWithFormat:@"%@%@", calculationDataManager.baseURL,diaImagePath];
        prodDataImage_iPad.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:diaProductImageUrl]]];
        prodDataImage_iPad.contentMode = UIViewContentModeScaleAspectFit;
        
        /** Setting X & Y position values in Portriat 7 Landscape Orientations */
        
        if([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait ||  [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown){
            
            prodDataImage_iPad.center = CGPointMake(prodDataImage_iPad.center.x +40, prodDataImage_iPad.center.y);
            
        } else if ( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
            
            prodDataImage_iPad.center = CGPointMake(prodDataImage_iPad.center.x+10, prodDataImage_iPad.center.y-5);
        }
        
        [bearingViewContainer addSubview:prodDataImage_iPad];
        
        //Displaying image desclaimer text for Product image at Bearing Container
        
    
        UIWebView *imageDesclaimer = [[[UIWebView alloc]init]autorelease];
        
        if( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait ||  [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown) {
            
            imageDesclaimer.frame = CGRectMake(435, prodDataImage_iPad.frame.size.height+prodDataImage_iPad.frame.origin.y -20, 300, 40);
            
            
        } else if ( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
            
            imageDesclaimer.frame = CGRectMake(440, prodDataImage_iPad.frame.size.height+prodDataImage_iPad.frame.origin.y -22, 220, 50);
            
        }
        
        [imageDesclaimer loadHTMLString:[NSString stringWithFormat:@"%@%@", cssStringRed, [self fixBR:[calculationDataManager getTranslationString:@"ImageDisclaimer"]]] baseURL:[NSURL URLWithString:@""]];
        
        imageDesclaimer.scrollView.scrollEnabled = NO;
        [imageDesclaimer setBackgroundColor:[UIColor clearColor]];
        [imageDesclaimer setOpaque:NO];
        
        [bearingViewContainer addSubview:imageDesclaimer];
        
    }
    
    //UIImage *patternImage = [UIImage imageNamed:@"diagonal_stripe_background.png"];
    int fullWidth = 300;
    if( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait ||  [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown){
        fullWidth=670;
        
    } else if ( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
        fullWidth=630;
    }
    
    int space = 20;
    
    //float boxWidthFloat = numberOfParameters == 0 ? 1 : (fullWidth-(numberOfParameters-1)*space)/numberOfParameters;
    float boxWidthFloat = numberOfParameters == 0 ? 1 : (400-(numberOfParameters-1)*space)/numberOfParameters;
    
    int boxWidth = boxWidthFloat;
    
    for (int i=0; i<numberOfParameters; i++) {
        
        //UIView *bearingParam = [[UIView alloc] initWithFrame:CGRectMake(20+(i*(boxWidth+space)), 70, boxWidth, 35)];
        UIView *bearingParam = [[UIView alloc] initWithFrame:CGRectMake(20+(i*(boxWidth+space)), 70, 85, 35)];
        
        bearingParam.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        
        //bearingParam.backgroundColor = [UIColor colorWithPatternImage:patternImage];
        bearingParam.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
        [bearingParam setOpaque:NO];
        [[bearingParam layer] setOpaque:NO];
        
        [bearingViewContainer addSubview:bearingParam];
        
        UILabel *bearingParamValue = [[UILabel alloc] initWithFrame:CGRectMake(8, 10, 110, 30)];
        bearingParamValue.backgroundColor = [UIColor clearColor];
        bearingParamValue.font = [UIFont fontWithName:CHEVIN_BOLD size:IPAD_FONT_SIZE_FOR_BEARING_VALUES];
        bearingParamValue.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
        
        Property *property;
        property = [selectedProduct.properties objectAtIndex:i];
        bearingParamValue.text = [NSString stringWithFormat:@"%@ %@ ", property.value, property.uom];
        [bearingParamValue sizeToFit];
        
        [bearingParam addSubview:bearingParamValue];
        
        UILabel *bearingParamTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, 110, 30)];
        bearingParamTitle.backgroundColor = [UIColor clearColor];
        bearingParamTitle.font = [UIFont fontWithName:CHEVIN_MEDIUM size:IPAD_FONT_SIZE_FOR_BEARING_DESCRIPTIONS];
        bearingParamTitle.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
        
        NSMutableString *string = [NSMutableString stringWithFormat:@"%@: ", [[selectedProduct.properties objectAtIndex:i] presentation]];
        
        if (([string rangeOfString:@"0"].location == NSNotFound) && ([string rangeOfString:@"2max"].location == NSNotFound) && ([string rangeOfString:@"k"].location == NSNotFound) ){
            
            bearingParamTitle.text = [NSString stringWithFormat:@"%@: ", [[selectedProduct.properties objectAtIndex:i] presentation]];
            [bearingParamTitle sizeToFit];
            [bearingParam addSubview:bearingParamTitle];
            
            bearingParamValue.frame = CGRectMake(bearingParamTitle.frame.origin.x+bearingParamTitle.frame.size.width, bearingParamValue.frame.origin.y, bearingParamValue.frame.size.width, bearingParamValue.frame.size.height);
            
        } else {
            
            //UIWebView *descriptionLabelView= [[[UIWebView alloc] initWithFrame:CGRectMake(10, 4, 110, 30)]autorelease];
            UIWebView *descriptionLabelView= [[[UIWebView alloc] initWithFrame:CGRectMake(0, 2, 110, 30)]autorelease];
            
            descriptionLabelView.backgroundColor = [UIColor clearColor];
            descriptionLabelView.userInteractionEnabled = false;
            descriptionLabelView.opaque = NO;
            descriptionLabelView.dataDetectorTypes = UIDataDetectorTypeNone;
            
            [descriptionLabelView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, [NSString stringWithFormat:@"%@: ", [[selectedProduct.properties objectAtIndex:i] presentation]]] baseURL:[NSURL URLWithString:@""]];
            [descriptionLabelView sizeToFit];
            
            bearingParamValue.frame = CGRectMake(descriptionLabelView.frame.origin.x + descriptionLabelView.frame.size.width-90, bearingParamValue.frame.origin.y, bearingParamValue.frame.size.width, bearingParamValue.frame.size.height);
            
            [bearingParam addSubview:descriptionLabelView];
            
        }
        
        [bearingParamValue release];
        [bearingParamTitle release];
        [bearingParam release];
    }
    
    NSString *designationTitle = [NSString stringWithFormat:@"%@: %@",GetLocalizedString(@"Bearing"), selectedProduct.designation];
    
    bearingTitleLabel.text = designationTitle;
    selectedDesignationTitle.text = designationTitle;
    
    //UIView *bearingType = [[UIView alloc] initWithFrame:CGRectMake(20, 115, fullWidth, 35)];
    UIView *bearingType = [[UIView alloc] initWithFrame:CGRectMake(20, 120, 400, 35)];
    
    //bearingType.backgroundColor = [UIColor colorWithPatternImage:patternImage];
    bearingType.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
    [bearingType setOpaque:NO];
    [[bearingType layer] setOpaque:NO];
    
    bearingType.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [bearingViewContainer addSubview:bearingType];
    
    bearingTypeTextContent = [[UILabel alloc] initWithFrame:CGRectMake(8, 10, 110, 50)];
    bearingTypeTextContent.backgroundColor = [UIColor clearColor];
    bearingTypeTextContent.font = [UIFont fontWithName:CHEVIN_BOLD size:IPAD_FONT_SIZE_FOR_BEARING_VALUES];
    bearingTypeTextContent.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    bearingTypeTextContent.text = selectedProduct.typestext;
    [bearingTypeTextContent sizeToFit];
    
    [bearingType addSubview:bearingTypeTextContent];
    
    bearingTypeTextTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, 110, 30)];
    bearingTypeTextTitle.backgroundColor = [UIColor clearColor];
    bearingTypeTextTitle.font = [UIFont fontWithName:CHEVIN_MEDIUM size:IPAD_FONT_SIZE_FOR_BEARING_DESCRIPTIONS];
    bearingTypeTextTitle.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    bearingTypeTextTitle.text = [NSString stringWithFormat:@"%@: ", GetLocalizedString(@"Type")];
    [bearingTypeTextTitle sizeToFit];
    
    bearingTypeTextContent.frame = CGRectMake(bearingTypeTextTitle.frame.origin.x+bearingTypeTextTitle.frame.size.width, bearingTypeTextContent.frame.origin.y, bearingTypeTextContent.frame.size.width, bearingTypeTextContent.frame.size.height);
    
    [bearingType addSubview:bearingTypeTextTitle];
    [bearingType release];
}


-(void)	:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
    
    
    
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    //[self resetObjects];
    
    //alertShown = NO;
    //isHeaderShown = NO;
}

/***** Bearing Changed *****/
-(void)ipadBearingChanged: (NSNotification*)notification {
    
    [self resetObjects];
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    selectedProduct = calculationDataManager.selectedBearing;
    
    [[bearingViewContainer subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    [self paintBearingParams:[selectedProduct.properties count]];
    
    if ([changeBearingPopover isPopoverVisible]) {
        [changeBearingPopover dismissPopoverAnimated:YES];
        [changeBearingPopover release];
    }
    
    //indexPathsForTextFields = [[NSMutableArray alloc] init];
    [indexPathsForTextFields removeAllObjects];
    
    /*if (calculationDataManager.selectedCalculation.calculationId != nil || calculationDataManager.mutlipleCalculation) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ipadUpdateCalculationType" object:self];
    }*/
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ipadUpdateCalculationType" object:self];
    
    //[self.selectCalculationViewController reloadCalculations];
    bearingChanged = true;

    
    
}

-(void)ipadAddCalculationType: (NSNotification*)notification {
    
    [self resetObjects];
    
    [helpIllustrationBearing setHidden:YES];
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    selectedProduct = calculationDataManager.selectedBearing;
    
    selectedCalculationId = [calculationDataManager.selectedCalculation.calculationId intValue];
    
    if ([addCalculationPopover isPopoverVisible]) {
        [addCalculationPopover dismissPopoverAnimated:YES];
        [addCalculationPopover release];
        [addCalculationButton setUserInteractionEnabled:YES];
        
    }
    
    int viewTag=0;
    NSString *tabName = nil;
    
    if(calculationDataManager.mutlipleCalculation == true) {
        
        //viewTag = ID_MULTIPLE_CALCULATIONS;
        
        selectedCalculationId = ID_MULTIPLE_CALCULATIONS;
        viewTag = selectedCalculationId;

        tabName = GetLocalizedString(@"Multiple_calculations");
        
    } else {
        //viewTag = [calculationDataManager.selectedCalculation.calculationId intValue] ;
        viewTag = selectedCalculationId;
        tabName = calculationDataManager.selectedCalculation.presentation;
        
        if ([tabName length] >= 35) {
            
            NSRange stringRange = {0, MIN([tabName length], 35)};
            stringRange = [tabName rangeOfComposedCharacterSequencesForRange:stringRange];
            NSString *shortString = [tabName substringWithRange:stringRange];
            tabName = [shortString stringByAppendingString:@"..."];
            
        }
    }
    
    if([self.view viewWithTag:viewTag+9999] == nil) {
        
        UIButton *removeCalculationButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *removeCalculationButtonImage = [UIImage imageNamed:@"round_cross.png"];
        [removeCalculationButton setImage:removeCalculationButtonImage forState:UIControlStateNormal];
        removeCalculationButton.frame = CGRectMake(5, 8, 30, 30);
        removeCalculationButton.tag = 50;
        [removeCalculationButton addTarget:self
                                    action:@selector(removeCalulationType:)
                          forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *newCalculationTypeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [newCalculationTypeButton addTarget:self
                                     action:@selector(didSelectSavedCalculation:)
                           forControlEvents:UIControlEventTouchUpInside];
        
        UIImage *calcButtonImage = [UIImage imageNamed:@"ipad_tab_button.png"];
        UIImage *calcButtonImageStrechable = [calcButtonImage stretchableImageWithLeftCapWidth:12 topCapHeight:0];
        
        [newCalculationTypeButton setBackgroundImage:calcButtonImageStrechable forState:UIControlStateNormal];
        
        [newCalculationTypeButton setTitle: tabName forState:UIControlStateNormal];
        [newCalculationTypeButton setTitle: tabName forState:UIControlStateSelected];
        [newCalculationTypeButton setTitle: tabName forState:UIControlStateHighlighted];
        [newCalculationTypeButton setTitle: tabName forState:UIControlStateApplication];
        [newCalculationTypeButton setTitle: tabName forState:UIControlEventTouchDown];
        
        newCalculationTypeButton.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON];
        newCalculationTypeButton.titleEdgeInsets=UIEdgeInsetsMake(4, 35, 0, 20);
        newCalculationTypeButton.frame = CGRectMake(0, 235, 40, 40);
        newCalculationTypeButton.titleLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
        
        newCalculationTypeButton.tag = viewTag+9999;
        removeCalculationTypeButton.tag = viewTag+99999;
        
        [self.view addSubview:newCalculationTypeButton];
        [newCalculationTypeButton addSubview:removeCalculationButton];
        
        [self setSearchingModeStart];
        [self sendNewRequest];
        
        [self reorganizeTabs];
        
    } else {
        
        [self performSelector:@selector(didSelectSavedCalculation:) withObject:[self.view viewWithTag:viewTag+9999]];
    }
    
    

}


-(IBAction)removeCalulationType:(id)sender{
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    [self resetObjects];
    
    [doResetButton setHidden:YES];
    
    // To avoid searching bar after removing all  calculation tabs at a time
  //  [removeCalculationTypeButton setUserInteractionEnabled:NO];
    
    
    if (calculationDataManager.mutlipleCalculation == true) {
        
        [calculationDataManager.selectedCalculationsArray removeAllObjects];
        
        calculationDataManager.mutlipleCalculation = false;
    }
    
    UIButton* button = sender;
    [[self.view viewWithTag:button.superview.tag] removeFromSuperview];
    
    int numberOfTabs = 0;
    
    UIButton *lastTabItem = [[[UIButton alloc] init] autorelease];
    
    for(UIButton *tabs in [self.view subviews]) {
        if ([tabs isKindOfClass:[UIButton class]] && tabs.tag > 9999) {
            
            numberOfTabs++;
            lastTabItem = tabs;
        }
    }
    
    if (numberOfTabs > 0) {
        
        [self performSelector:@selector(didSelectSavedCalculation:) withObject:lastTabItem];
        
    } else {
        
        [removeCalculationTypeButton setHidden:YES];
        [inputParametersTableiPad setHidden:YES];
        //[helpIllustrationCalculationType setHidden:NO];
        [noParametersAvailableError removeFromSuperview];
        
        helpIllustrationCalculationType = [[DisplayHelpIllustrationCalculationType_iPad alloc] initWithFrame:CGRectMake(60, 20, 539, 223)];
        helpIllustrationCalculationType.opaque = NO;
        helpIllustrationCalculationType.backgroundColor = [UIColor clearColor];
        helpIllustrationCalculationType.delay = 0;
        helpIllustrationCalculationType.tag = 123123;
        [parametersViewContainer addSubview:helpIllustrationCalculationType];
        
        CGRect addCalcFrame = addCalculationButton.frame;
        addCalcFrame.origin.x = 30;
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelay:0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        
        addCalculationButton.frame = addCalcFrame;
        [UIView commitAnimations];
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        calculationDataManager.selectedCalculation = nil;
    }
       // [removeCalculationTypeButton setUserInteractionEnabled:YES];
    
}

-(void)ipadUpdateCalculationType: (NSNotification*)notification {
    
    [noFormulaErrorArray removeAllObjects];
    [self resetObjects];
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    selectedProduct = calculationDataManager.selectedBearing;
    
    if(calculationDataManager.selectedCalculation != nil || calculationDataManager.mutlipleCalculation) {
    
    [self setSearchingModeStart];
    [self sendNewRequest];
        
    }
    
    if(calculationDataManager.mutlipleCalculation) {
        
        [self handleErrors];
    }
}


-(void)didSelectSavedCalculation:(UIButton*)selected  {
    
    //indexPathsForTextFields = [[NSMutableArray alloc] init];
    [indexPathsForTextFields removeAllObjects];
    
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    //calculationDataManager.selectedCalculation.calculationId = [NSString stringWithFormat:@"%d",selected.tag-9999];
    //calculationDataManager.selectedCalculation.presentation = selected.titleLabel.text;
    
    selectedCalculationId = selected.tag-9999;

    
    if(selected.tag-9999 == ID_MULTIPLE_CALCULATIONS) {
        calculationDataManager.mutlipleCalculation = true;
        
    }else{
        calculationDataManager.mutlipleCalculation = false;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ipadUpdateCalculationType" object:self];
    
    UIImage *calcButtonImage = [UIImage imageNamed:@"ipad_tab_button.png"];
    UIImage *calcButtonImageStrechable = [calcButtonImage stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    
    [selected setBackgroundImage:calcButtonImageStrechable forState:UIControlStateNormal];
    [selected setTitleColor:[UIColor colorWithRed:0.f/0.f green:0.f/0.f blue:0.f/0.f alpha:1.f] forState:UIControlStateNormal];
    
    removeCalculationTypeButton.tag = (selected.tag-9999)+99999;
    
    for(UIButton *tabs in [self.view subviews]) {
        if ([tabs isKindOfClass:[UIButton class]] && tabs.tag > 9999) {
            if(selected.tag != tabs.tag) {
                
                UIImage *calcButtonImage = [UIImage imageNamed:@"ipad_tab_button_inactive.png"];
                UIImage *calcButtonImageStrechable = [calcButtonImage stretchableImageWithLeftCapWidth:12 topCapHeight:0];
                
                [tabs setBackgroundImage:calcButtonImageStrechable forState:UIControlStateNormal];
                [tabs setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateNormal];
                
            } else {
                
            }
        }
    }
    
    [self reorganizeTabs];
    
    // To avoid searching bar after removing all  calculation tabs at a time
    //[removeCalculationTypeButton setUserInteractionEnabled:YES];
   // [self setSearchingModeEnd];
    
}



/***** Remove Calculation Type *****/
-(void)ipadRemoveCalculationType: (NSNotification*)notification {
    
}

/*************** ACTIONS ***************/

/***** SELECT BEARING *****/
-(IBAction)changeBearing:(id)sender {
    
    if (!connected) {
        
        [self showAlertForNetworkConnection];
        return;
    }
    
    FilterBearingsViewController_iPad *filterBearingsViewController = [[FilterBearingsViewController_iPad alloc]initWithNibName:@"FilterBearingsViewController" bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:filterBearingsViewController];
    changeBearingPopover = [[[UIPopoverController alloc] initWithContentViewController:navigationController] retain];

    
    // iOS 7 - Navigation background & title color change
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        
    }else {
    
        NSString *navigationBarTintColor = NAVIGATION_BACKGROUND_COLOR;
        //set bar color
        navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:navigationBarTintColor];
        //optional, i don't want my bar to be translucent
        navigationController.navigationBar.barStyle = UIBarStyleBlack;
        navigationController.navigationBar.translucent = NO;
        //set back button color
        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
        //set back button arrow color
        [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
        filterBearingsViewController.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        [changeBearingPopover setBackgroundColor:[UIColor colorWithHexString:NAVIGATION_BACKGROUND_COLOR]];

    }

    CGRect popoverFrame = CGRectMake(changeBearingButton.frame.origin.x,changeBearingButton.frame.origin.y,changeBearingButton.frame.size.width,changeBearingButton.frame.size.height);
    changeBearingPopover.popoverContentSize=CGSizeMake(320, 453);
	//filterBearingsViewController.contentSizeForViewInPopover=CGSizeMake(320, 453);
    filterBearingsViewController.parentNavigationController = navigationController;
    [changeBearingPopover presentPopoverFromRect:popoverFrame inView:bearingViewContainer permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    
}

-(void) showAlertForNetworkConnection {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[calculationDataManager getTranslationString:@"Network_error!"]
                                                    message:[calculationDataManager getTranslationString:@"This_application_need_a_network_connection."]
                                                   delegate:nil
                                          cancelButtonTitle:[calculationDataManager getTranslationString:@"OK"]
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
}


-(IBAction)addCalculationType:(id)sender {
    
    if (!connected) {
        
        [self showAlertForNetworkConnection];
        return;
    }
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    if(calculationDataManager.selectedBearing != nil) {
        //SelectCalculationViewController_iPad *selectCalculationType = [[SelectCalculationViewController_iPad alloc]initWithNibName:@"SelectCalculationViewController_iPad" bundle:nil];
        
        if(bearingChanged == true) {
            [selectCalculationViewController release];
            selectCalculationViewController = nil;
            selectCalculationViewController = [[SelectCalculationViewController_iPad alloc]initWithNibName:@"SelectCalculationViewController_iPad" bundle:nil];
            bearingChanged = false;
        }
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:selectCalculationViewController];
        
        addCalculationPopover = [[[UIPopoverController alloc] initWithContentViewController:navigationController] retain];

        // iOS 7 - Navigation background & title color change
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
            
        }else {
            
            NSString *navigationBarTintColor = NAVIGATION_BACKGROUND_COLOR;
            //set bar color
            navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:navigationBarTintColor];
            //optional, i don't want my bar to be translucent
            navigationController.navigationBar.barStyle = UIBarStyleBlack;
            navigationController.navigationBar.translucent = NO;
            //set back button color
            [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
            //set back button arrow color
            [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
            [addCalculationPopover setBackgroundColor:[UIColor colorWithHexString:NAVIGATION_BACKGROUND_COLOR]];

        }
        
        CGRect popoverFrame = CGRectMake(addCalculationButton.frame.origin.x,addCalculationButton.frame.origin.y,addCalculationButton.frame.size.width,addCalculationButton.frame.size.height);
        //m.CGRect popoverFrame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.width/2, 1, 1);
        addCalculationPopover.popoverContentSize=CGSizeMake(350, 480);
        
        [addCalculationPopover presentPopoverFromRect:popoverFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        //[addCalculationButton setUserInteractionEnabled:NO];
        addCalculationPopover.delegate = self;
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:GetLocalizedString(@"Please_select_a_bearing.")
                                                       delegate:self
                                              cancelButtonTitle:GetLocalizedString(@"OK")
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}


-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    
    [addCalculationButton setUserInteractionEnabled:YES];
}

////---called when the user clicks outside the popover view---
//- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
//
//    NSLog(@"popover about to be dismissed");
//    return YES;
//}


- (void)switchDidChangeValue:(id)sender
{
    UISwitch* aSwitchCtrl = (UISwitch*)sender;
    
    // To avoid crash when select the switch element
    UITableViewCell* aCell = (UITableViewCell*) aSwitchCtrl.superview;

    
    /*UITableViewCell* aCell;
    if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {
        
        aCell = (UITableViewCell*) aSwitchCtrl.superview;
    }else {
        
        aCell = (UITableViewCell*) aSwitchCtrl.superview.superview;
    }*/
    

    NSString* parameterValue = aSwitchCtrl.on ? @"true" : @"false";
    [_calculationControl setValue:parameterValue forId:aCell.tag];
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    NSString* parameterName = [_calculationControl nameForParameterId:aCell.tag];
    [calculationDataManager setPreviousInput:parameterName withValue:parameterValue];
    
}

- (IBAction)doReset:(id)sender {
    
    if (! [_calculationControl CheckParameterValues]) {
        
        return;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:GetLocalizedString(@"Reset_input_parameters")
                                                   delegate:self
                                          cancelButtonTitle:GetLocalizedString(@"No")
                                          otherButtonTitles:GetLocalizedString(@"Reset"), nil];
    alert.tag = 11;
    [alert show];
    [alert release];
    
}

-(IBAction)doCalculation:(id) sender {
    
    if (!connected) {
        
        [self showAlertForNetworkConnection];
        return;
    }
    
    
     CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
   
    
    
    calculationsCount=0;
    [CalculationControl setNum:0];
    [params removeAllObjects];
    
    [self.inputParametersTableiPad endEditing:YES];
   
    
    hideIntermediate = true;
    what_plain_brearing = 0;
    
    intermediate_result = false;
    intermediate_result_composite = false;
    //NSLog(@"......... %d ..%d" ,selectedCalculationId ,calculationDataManager.selectedCalculationsArray.count);
    
//    if(selectedCalculationId == 23)
//    {
//        intermediate_result_composite = true;
//        what_plain_brearing = 2;
//      
//    }
//
//    
//    for (Calculation *calculation in calculationDataManager.selectedCalculationsArray) {
//        if ([calculation.calculationId isEqualToString:@"17"]){
//            intermediate_result = true;
//            what_plain_brearing = 1;
//            break;
//            
//        }
//        
//         }
    
    NSLog(@"Error count: %lu , %lu", (unsigned long)noFormulaErrorArray.count , (unsigned long)calculationDataManager.selectedCalculationsArray.count);
    NSLog(calculationDataManager.mutlipleCalculation ? @"Yes" : @"No");
    
    
    if(((calculationDataManager.selectedBearing != nil) && (calculationDataManager.selectedCalculation != nil)) ||
       ((calculationDataManager.selectedBearing != nil) && ((calculationDataManager.mutlipleCalculation) && calculationDataManager.selectedCalculationsArray.count) ))
    {
        
        BOOL inputMissing = [_calculationControl updateMissingInputValues];
        BOOL inputInvalid = [_calculationControl updateInvalidInputValues];
        BOOL inputIncorrect = [_calculationControl updateIncorrectInputValues];

        
        
		if (inputMissing) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
			                                                message:GetLocalizedString(@"Please_Fill_in_All_Required_Fields.")
			                                               delegate:self
			                                      cancelButtonTitle:GetLocalizedString(@"OK")
			                                      otherButtonTitles:nil];
			alert.tag = 1;
			[alert show];
			[alert release];
		}else if (inputInvalid) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
			                                                message:[_calculationControl getInvalidInputWarnings]
			                                               delegate:self
			                                      cancelButtonTitle:GetLocalizedString(@"OK")
			                                      otherButtonTitles:nil];
			alert.tag = 1;
			[alert show];
			[alert release];
		}else if (inputIncorrect) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
			                                                message:@"Please fill valid input for highlighted fields."
			                                               delegate:self
			                                      cancelButtonTitle:GetLocalizedString(@"OK")
			                                      otherButtonTitles:nil];
			alert.tag = 35;
			[alert show];
			[alert release];
		}
        
        
        /*else if ((!calculationDataManager.mutlipleCalculation && !isHeaderShown) || (calculationDataManager.mutlipleCalculation &&  (noFormulaErrorArray.count == calculationDataManager.selectedCalculationsArray.count))) {
            
            [self showAlertForUnsupportedCalculation];
            isHeaderShown = NO;*/
        
     
            
        else if ((noFormulaErrorArray.count == calculationDataManager.selectedCalculationsArray.count) && calculationDataManager.mutlipleCalculation && !isHeaderShown) {
            
            [self showAlertForUnsupportedCalculation];
            isHeaderShown = NO;
            
        }  else {
            
            [params addEntriesFromDictionary:[_calculationControl getParameterValuesByType:VISIBLE_PARAM_BY_NAME]];
            [params setValue:[selectedProduct productId] forKey:@"productId"];
            
            if ([parameterYbearingMinLoad.parameterId isEqualToString:@"165"]) {
                [params setValue:@"0" forKey:@"YB_MinLoad_Message"];
            }
            
            
            /*NSLog(@"typeid: %@",calculationDataManager.selectedBearing.typeId);

            if ((!calculationDataManager.mutlipleCalculation) && ([calculationDataManager.selectedBearing.typeId isEqualToString:@"103021"] || [calculationDataManager.selectedBearing.typeId isEqualToString:@"158008"]  || [calculationDataManager.selectedBearing.typeId isEqualToString:@"103002"] || [calculationDataManager.selectedBearing.typeId isEqualToString:@"146232"] )) {
                
                [params setValue:@"0" forKey:@"YB_MinLoad_Message"];
            }*/

            
            [self setSearchingModeStart];
            [self sendNewRequestForResult];
        }
        
        

        // To avoid crash issue for CARB bearing in second time &&
        // sending 0 as input param for minimum load calc for "No input required" input parameter 
        /*else {
            
            [params addEntriesFromDictionary:[_calculationControl getParameterValuesByType:VISIBLE_PARAM_BY_NAME]];
            [params setValue:[selectedProduct productId] forKey:@"productId"];
            
            if ([parameterYbearingMinLoad.parameterId isEqualToString:@"165"]) {
                [params setValue:@"0" forKey:@"YB_MinLoad_Message"];
            }
            
            [self setSearchingModeStart];
            [self sendNewRequestForResult];
        }*/
        
        [inputParametersTableiPad reloadData];
        
    }
    else if ((calculationDataManager.selectedBearing == nil) && (calculationDataManager.selectedCalculation == nil)) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:GetLocalizedString(@"Please_select_a_bearing_and_calculation_type.")
                                                       delegate:self
                                              cancelButtonTitle:GetLocalizedString(@"OK")
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }else if ((calculationDataManager.selectedBearing !=nil) && (calculationDataManager.selectedCalculation == nil) ) {
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Please select a calculation type."
                                                       delegate:self
                                              cancelButtonTitle:GetLocalizedString(@"OK")
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    
   
    
    
}

- (void)dataManagerReturnedParametersForCalculation:(NSArray *)dataManagerReturnedParametersForCalculation {
    
    [helpIllustrationCalculationType removeFromSuperview];
    
    if ([dataManagerReturnedParametersForCalculation count] > 0) {
        [noParametersAvailableError removeFromSuperview];
    }
    
    [inputParametersTableiPad setHidden:NO];
    //indexPathsForTextFields = [[NSMutableArray alloc] init];
    [indexPathsForTextFields removeAllObjects];
    
    [_calculationControl updateMissingParameters:dataManagerReturnedParametersForCalculation];
    [_calculationControl prefillValuesWithPreviousInputsForParameters];
    [_calculationControl prefillValuesWithDefaultsForParameters];
    
    [inputParametersTableiPad reloadData];
    [self setSearchingModeEnd];
    [self handleErrors];
    [doResetButton setHidden:NO];
    
}


- (void)dataManagerReturnedTranslation:(BOOL)success {
    
    if (success) {
        
        skfChevinTitleLabel.text = GetLocalizedString(@"Application_name");
        [addCalculationButton setTitle:GetLocalizedString(@"Calculation_type") forState:UIControlStateNormal];
        [changeBearingButton setTitle:GetLocalizedString(@"Select_bearing") forState:UIControlStateNormal];
        [doCalculationButton setTitle:GetLocalizedString(@"Calculate") forState:UIControlStateNormal];
        [doResetButton setTitle:@"Reset" forState:UIControlStateNormal];
        [doReportButton setTitle:@"Reports" forState:UIControlStateNormal];
        
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        int delay = 0;
        
        if (calculationDataManager.selectedBearing == nil) {
            
            for (UIView *subview in bearingViewContainer.subviews) {
                
                if (subview.tag == 123123) {
                    [subview removeFromSuperview];
                }
            }
            
            helpIllustrationBearing = [[DisplayHelpIllustrationBearing_iPad alloc] initWithFrame:CGRectMake(70, 20, 458, 112)];
            helpIllustrationBearing.opaque = NO;
            helpIllustrationBearing.backgroundColor = [UIColor clearColor];
            helpIllustrationBearing.tag = 123123;
            
            [bearingViewContainer addSubview:helpIllustrationBearing];
            delay=1;
        }
        
        if (calculationDataManager.selectedCalculation == nil)  {
            
            for (UIView *subview in parametersViewContainer.subviews) {
                
                if (subview.tag == 123123) {
                    [subview removeFromSuperview];
                }
            }
            
            helpIllustrationCalculationType = [[DisplayHelpIllustrationCalculationType_iPad alloc] initWithFrame:CGRectMake(60, 20, 539, 223)];
            helpIllustrationCalculationType.opaque = NO;
            helpIllustrationCalculationType.backgroundColor = [UIColor clearColor];
            helpIllustrationCalculationType.delay = delay;
            helpIllustrationCalculationType.tag = 123123;
            [parametersViewContainer addSubview:helpIllustrationCalculationType];
        }
        
        [bearingViewContainer bringSubviewToFront:changeBearingButton];
    }
    
    
    [self setSearchingModeEnd];
}


- (void) updateResultData:(NSDictionary *)storedDict
{
    [resultArray removeAllObjects];
    [warningsArray removeAllObjects];
    [errorsArray removeAllObjects];
    [messagesArray removeAllObjects];
    [parameterAvailableArray removeAllObjects];
    [parameterDatabaseArray removeAllObjects];
    
    calculatedProduct = [[storedDict objectForKey:@"selectedProduct"] copy];
    
    NSArray *calculationResults = [storedDict objectForKey:@"results"];
    if(calculationResults != nil && calculationResults.count) {
        
        for(Calculation *calc in calculationResults)
        {
            reportId = calc.reportId;
            [resultArray addObject:calc.name];
            [resultArray addObjectsFromArray:calc.resultParams];
            
            if(calc.warnings)
            {
                [self copyUniqueWarnings:[calc.warnings copy] ];
                [_calculationControl updateWarnings:calc.warnings];
            }
            
            if(calc.errors)
            {
                [self copyUniqueErrors:[calc.errors copy ]];
                [_calculationControl updateErrors:calc.errors];
            }
            
            if(calc.messages)
            {
                [self copyUniqueMessages:[calc.messages copy]];
            }
            
            if(calc.availableParams)
            {
                [self copyUniqueParameters:[calc.availableParams copy ]];
            }
            
            if(calc.resultDatabaseParams)
            {
                [self copyUniqueDatabaseParameters:[calc.resultDatabaseParams copy ]];
                
            }
            
        }
    }
    
    for(int i = 1 ; i < resultArray.count-1 ; i++)
    {
        ParameterResult *newresult = [resultArray objectAtIndex:i];
        
        if(hideIntermediate && [newresult.description rangeOfString:@"rating life"].location != NSNotFound)
        {
            whereInter = newresult.parameterId;
        }
        
    }
}


// Modified for multiple calculations

- (void)loadResultsForTag:(int)tag {
    
    [self updateResultData:[storedReturnedDictionaries objectForKey:[NSString stringWithFormat:@"%d",tag]]];
    
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)activeScrollView {
    
    if(activeScrollView.tag == 9920) {
        [self loadResultsForTag:activeScrollView.superview.tag];
    }
}


- (void)setupResultsView:(NSDictionary*)returnedDictionary {
    
    
    
    
    
    
    //UIView *resultItemView = nil;
    resultItemView = nil;
    
    if( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown ){
        
        /*** Portrait Orientation **/
        
        resultItemView = [[UIView alloc]initWithFrame:resultItemRectPortrait];
        
        UIImageView *resultBackgroundImage = [[UIImageView alloc] init];
        UIImage *image = [UIImage imageNamed:@"kvitto_bg_portrait_big.png"];
        
        [resultBackgroundImage setImage:image];
        
        resultBackgroundImage.frame = CGRectMake(0, 0, image.size.width, image.size.height);
        resultBackgroundImage.opaque = NO;
        
        [resultItemView addSubview:resultBackgroundImage];
        [resultBackgroundImage release];
        
        
    } else if ( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
        
        /*** Landscape Orientation **/
        
        resultItemView = [[UIView alloc]initWithFrame:resultItemRectLandscape];
        
        UIImageView *resultBackgroundImage = [[UIImageView alloc] init];
        
        [resultItemView addSubview:resultBackgroundImage];
        [resultBackgroundImage release];
    }
    
    [resultItemView setOpaque:NO];
    
    resultItemView.layer.shadowOffset = CGSizeMake(1, 1);
    resultItemView.layer.shadowRadius = 4;
    resultItemView.layer.shadowOpacity = 0.5;
    resultItemView.tag = calculationResultsCount;
    //resultItemView.tag = [reportId integerValue];
    
    [storedReturnedDictionaries setObject:[returnedDictionary mutableCopy] forKey:[NSString stringWithFormat:@"%d",calculationResultsCount]];
    //[storedReturnedDictionaries setObject:[returnedDictionary mutableCopy] forKey:[NSString stringWithFormat:@"%ld",(long)[reportId integerValue]]];
    resultItemView.clipsToBounds = NO;
    //UITableView *resultsTable = [[UITableView alloc] initWithFrame:CGRectMake(10, 50, 280, 240) style:UITableViewStyleGrouped];
    resultsTable = [[UITableView alloc] initWithFrame:CGRectMake(10, 50, 280, 240) style:UITableViewStyleGrouped];
    
    
    resultsTable.backgroundColor = [UIColor clearColor];
    resultsTable.opaque = NO;
    resultsTable.backgroundView = nil;
    resultsTable.delegate = self;
    resultsTable.dataSource = self;
    resultsTable.tag = 9920;
    
    UILabel *resultBearingTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 264, 20)];
    resultBearingTitle.text = [NSString stringWithFormat:@"%@", selectedProduct.designation];
    resultBearingTitle.textAlignment = NSTextAlignmentRight;
    resultBearingTitle.backgroundColor = [UIColor clearColor];
    resultBearingTitle.font = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_BUTTON_LARGE];
    resultBearingTitle.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    
    [resultItemView addSubview:resultBearingTitle];
    [resultItemView addSubview:resultsTable];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(removeCalcResult:)
     forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *buttonImage = [UIImage imageNamed:@"round_cross.png"];
    [button setImage:buttonImage forState:UIControlStateNormal];
    
    button.frame = CGRectMake(15.0, 15.0, 30.0, 30.0);
    button.tag = calculationResultsCount;
    //button.tag = [reportId integerValue];
    
    button.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON];
    button.titleLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    
    [resultItemView addSubview:button];
    
    
    /*** Adding Save, print, Email icons***/
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveButton addTarget:self action:@selector(saveReport:) forControlEvents:UIControlEventTouchUpInside];
    UIImage *saveImage = [UIImage imageNamed:@"save_new.png"];
    [saveButton setImage:saveImage forState:UIControlStateNormal];
    saveButton.frame = CGRectMake(70.0, 15.0, 30.0, 30.0);
    saveButton.tag = [reportId integerValue];
        
    [resultItemView addSubview:saveButton];
    
    NSMutableArray *temp = [[NSMutableArray alloc] initWithArray:resultArray];
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];

    
    [resultItemView.layer setValue:temp forKey:@"result"];
    
    if((selectedCalculationId == 17 || selectedCalculationId == 23) && !calculationDataManager.mutlipleCalculation)
    {
       [resultItemView.layer setValue:@"YES" forKey:@"isIntermediate"];
    }
    
    
    for (Calculation *calculation in calculationDataManager.selectedCalculationsArray) {
        
        if ([calculation.calculationId isEqualToString:@"17"] && calculationDataManager.mutlipleCalculation){
            [resultItemView.layer setValue:@"YES" forKey:@"isIntermediate"];
            break;
            
        }
        
    }
    
    
    
    
    /*** END ***/
    
    
    
    [resultsContainerScrollView addSubview:resultItemView];
    
    if( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown ){
        
        int newWidth;
        newWidth = -915;
        
        CGPoint leftOffset = CGPointMake(0,0);
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelay:0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        
        [resultsContainerScrollView setContentOffset: leftOffset];
        
        [UIView commitAnimations];
        
        for (UIView *subview in resultsContainerScrollView.subviews) {
            
            CGRect resultContainerFrame = subview.frame;
            resultContainerFrame.origin.x += 302;
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.5];
            [UIView setAnimationDelay:0];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            
            subview.frame = resultContainerFrame;
            [UIView commitAnimations];
            newWidth += 302;
        }
        [resultsContainerScrollView setContentSize:CGSizeMake(newWidth, 300)];
        
    } else if ( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
        
        [resultsTable reloadData];
        
        resultItemView.frame = CGRectMake(540, -resultsTable.contentSize.height-60, 302, resultsTable.contentSize.height+80);
        
//        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
//            
//            resultItemView.frame = CGRectMake(540, -resultsTable.contentSize.height-60, 302, resultsTable.contentSize.height+80);
//        }else {
//            
//            resultItemView.frame = CGRectMake(540, -resultsTable.contentSize.height-80, 302, resultsTable.contentSize.height+80);
//            
//        }
        
        NSMutableArray *calculationResultViews = [[NSMutableArray alloc] init];
        
        for(UIView *view in [resultsContainerScrollView subviews]) {
            
            if (view.tag > 999) {
                [calculationResultViews addObject:view];
            }
        }
        
        int currentItemHeight = 0;
        int currPos = 0;
        
        for(int i = [calculationResultViews count]-1; i>=0; i--) {
            
            UIView *view = [calculationResultViews objectAtIndex:i];
            
            for(UITableView *resultTableView in [view subviews]) {
                
                if ([resultTableView isKindOfClass:[UITableView class]]) {
                    
                    resultTableView.frame = CGRectMake(10, 50, 280, resultTableView.contentSize.height);
                    currentItemHeight = resultTableView.contentSize.height+80;
                    break;
                }
            }
            
            for(UIImageView *resultBackgroundImage in [view subviews]) {
                
                if ([resultBackgroundImage isKindOfClass:[UIImageView class]]) {
                    
                    UIImage *image = [[UIImage imageNamed:@"kvitto_bg_landscape_big.png"] stretchableImageWithLeftCapWidth:0 topCapHeight:40];
                    [resultBackgroundImage setImage:image];
                    resultBackgroundImage.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
                    resultBackgroundImage.opaque = NO;
                }
            }
            
            CGRect resultContainerFrame = view.frame;
            resultContainerFrame.origin.y +=  resultsTable.contentSize.height+80;
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.5];
            [UIView setAnimationDelay:0];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            
            view.frame = resultContainerFrame;
            [UIView commitAnimations];
            currPos += currentItemHeight;
        }
        [resultsContainerScrollView setContentSize:CGSizeMake(280, currPos)];
    }
    
    resultsContainerScrollView.delaysContentTouches = NO;
    resultsContainerScrollView.canCancelContentTouches = NO;
    
    [inputParametersTableiPad reloadData];
}


/*** Modified for multiple calculations */

- (void)dataManagerReturnedCalculationDictionary:(NSDictionary *)returnedDictionary {
    
    
    [self updateResultData:returnedDictionary];
    
    calculationResultsCount += 1;
    [self setupResultsView:returnedDictionary];
    
    [self setSearchingModeEnd];
    
    [doResetButton setHidden:NO];
    
}


-(void)updateMissingParameters:(NSArray *)nextParameterAvailableArray {
    
    bool found=false;
    for (ParameterAvailable *newParam in nextParameterAvailableArray) {
        
        found=false;
        
        for (ParameterAvailable *paramAvail in parameterAvailableArray) {
            
            if([[paramAvail parameterAvailableId] isEqualToString:[newParam parameterAvailableId]]) {
                
                found=true;
                break;
            }
        }
        
        if(!found) {
            
            [parameterAvailableArray addObject:newParam];
        }
    }
}

- (void)copyUniqueParameters: (NSArray *)newParameterAvailableArray {
    
    
    NSMutableArray *newParamAddedArray = [[NSMutableArray alloc]init];
    bool isAvailable = false;
    
    for (ParameterAvailable *newParam in newParameterAvailableArray) {
        
        isAvailable = false;
        
        for (ParameterAvailable *oldParam in parameterAvailableArray) {
            
            if([newParam.parameterAvailableId isEqualToString:oldParam.parameterAvailableId]) {
                isAvailable = true;
                break;
            }
            
        }
        if(isAvailable == false) {
            
            [newParamAddedArray addObject:newParam];
        }
    }
    
    
    parameterAvailableArray = [[parameterAvailableArray arrayByAddingObjectsFromArray:newParamAddedArray] mutableCopy];
    
    
    [newParamAddedArray release];
}


- (void)copyUniqueDatabaseParameters: (NSArray *)newDatabaseParamsArray {
    
    NSMutableArray *newParamAddedArray = [[NSMutableArray alloc]init];
    bool isAvailable = false;
    
    for (ParameterDatabase *newParam in newDatabaseParamsArray) {
        
        isAvailable = false;
        
        for (ParameterDatabase *oldParam in parameterDatabaseArray) {
            
            if([newParam.parameterDatabaseId isEqualToString:oldParam.parameterDatabaseId]) {
                isAvailable = true;
                break;
            }
            
        }
        if(isAvailable == false) {
            
            [newParamAddedArray addObject:newParam];
        }
    }
    
    parameterDatabaseArray = [[parameterDatabaseArray arrayByAddingObjectsFromArray:newParamAddedArray] mutableCopy];
    
    [newParamAddedArray release];
}


- (void)copyUniqueWarnings: (NSArray *)newWarningsAvailableArray {
    
    NSMutableArray *newWarningAddedArray = [[NSMutableArray alloc]init];
    bool isAvailable = true;
    
    for (Warning *newWarning in newWarningsAvailableArray) {
        
        isAvailable = false;
        
        for (Warning *oldWarning in warningsArray) {
            
            if([newWarning.description isEqualToString:oldWarning.description]) {
                isAvailable = true;
                break;
            }
        }
        if(isAvailable == false) {
            
            [newWarningAddedArray addObject:newWarning];
        }
    }
    
    
    warningsArray = [[warningsArray arrayByAddingObjectsFromArray:newWarningAddedArray] mutableCopy];
    
    
    [newWarningAddedArray release];
}


- (void)copyUniqueErrors: (NSArray *)newErrorsAvailableArray {
    
    NSMutableArray *newErrorAddedArray = [[NSMutableArray alloc]init];
    bool isAvailable = true;
    
    for (Error *newError in newErrorsAvailableArray) {
        
        isAvailable = false;
        
        for (Error *oldError in errorsArray) {
            
            if([newError.description isEqualToString:oldError.description]) {
                isAvailable = true;
                break;
            }
        }
        if(isAvailable == false) {
            
            [newErrorAddedArray addObject:newError];
        }
    }
    
    
    errorsArray = [[errorsArray arrayByAddingObjectsFromArray:newErrorAddedArray] mutableCopy];
    
    
    [newErrorAddedArray release];
}

- (void)copyUniqueMessages: (NSArray *)newMessagesAvailableArray {
    
    
    NSMutableArray *newMessagesAddedArray = [[NSMutableArray alloc]init];
    bool isAvailable = true;
    
    for (Warning *newWarning in newMessagesAvailableArray) {
        
        isAvailable = false;
        
        for (Warning *oldWarning in messagesArray) {
            
            if([newWarning.description isEqualToString:oldWarning.description]) {
                isAvailable = true;
                break;
            }
        }
        if(isAvailable == false) {
            
            [newMessagesAddedArray addObject:newWarning];
        }
    }
    
    messagesArray = [[messagesArray arrayByAddingObjectsFromArray:newMessagesAddedArray] mutableCopy];
    
    
    [newMessagesAddedArray release];
}


-(IBAction)removeCalcResult:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    
    [self.view bringSubviewToFront:resultsContainerScrollView];
    
    UIView *viewToRemove = [[self.view viewWithTag:button.tag] viewWithTag:9920];
    UIView *viewToRemoveSuper = viewToRemove.superview;
    
    removedView = viewToRemove;
    CGRect viewFrame = viewToRemoveSuper.frame;
    viewFrame.origin.y -= 200;
    
    [viewToRemoveSuper.superview bringSubviewToFront:viewToRemoveSuper];
    viewToRemoveSuper.alpha = 0.0;
    
    [UIView beginAnimations:@"removeWithEffect" context:nil];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:viewToRemove.superview cache:YES];
    [viewToRemove removeFromSuperview];
    
    [UIView commitAnimations];
    [viewToRemoveSuper performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:1.0f];
    
    [self performSelector:@selector(sendResultsContainerScrollViewBack:) withObject:nil afterDelay:1.0f];
    [self performSelector:@selector(reorganizeCalcResults:) withObject:button afterDelay:1.0f];
    
}

-(IBAction)sendResultsContainerScrollViewBack:(id)sender {
    
    [self.view sendSubviewToBack:resultsContainerScrollView];
    [self.view sendSubviewToBack:backgroundImage];
}


-(void)redrawResults {
    
    if( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait ){
        
        int newWidth;
        newWidth = 0;
        
        for (UIView *subview in resultsContainerScrollViewContent.subviews) {
            
            subview.frame = CGRectMake(newWidth, 350, 300, 300);
            newWidth += 302;
        }
        
        [resultsContainerScrollView setContentSize:CGSizeMake(newWidth, 300)];
        resultsContainerScrollViewContent.frame = CGRectMake(0,0,newWidth,resultsContainerScrollViewContent.frame.size.height);
        
    } else if ( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
        
        int newHeight;
        newHeight = 0;
        
        for (UIView *subview in resultsContainerScrollViewContent.subviews) {
            
            subview.frame = CGRectMake(200, newHeight, 300, 300);
            newHeight += 305;
            
        }
        
        [resultsContainerScrollView setContentSize:CGSizeMake(300, newHeight)];
        resultsContainerScrollViewContent.frame = CGRectMake(0,0,resultsContainerScrollViewContent.frame.size.width,newHeight);
        
    }
}

-(IBAction)reorganizeCalcResults:(UIButton*)selected {
    
    if( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown ) {
        
        for(int i = 1000; i<selected.tag; i++) {
            
            CGRect resultContainerFrame = [self.view viewWithTag:i].frame;
            resultContainerFrame.origin.x -= 302;
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.5];
            [UIView setAnimationDelay:0];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            
            [self.view viewWithTag:i].frame = resultContainerFrame;
            [UIView commitAnimations];
        }
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelay:0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        
        [resultsContainerScrollView setContentSize:CGSizeMake(resultsContainerScrollView.contentSize.width-305, 300)];
        resultsContainerScrollViewContent.frame = CGRectMake(0,0,resultsContainerScrollViewContent.frame.size.width-305,resultsContainerScrollViewContent.frame.size.height);
        
        [UIView commitAnimations];
        
    } else if ( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
        
        for(int i = 1000; i<selected.tag; i++) {
            
            CGRect resultContainerFrame = [self.view viewWithTag:i].frame;
            resultContainerFrame.origin.y -= (removedView.frame.size.height+80);
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.5];
            [UIView setAnimationDelay:0];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            
            [self.view viewWithTag:i].frame = resultContainerFrame;
            
            [UIView commitAnimations];
            
        }
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelay:0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        
        [resultsContainerScrollView setContentSize:CGSizeMake(300,resultsContainerScrollView.contentSize.height-(removedView.frame.size.height+80))];
        
        [UIView commitAnimations];
    }
}


- (NSString *)acceptsEntryInTextField:(UITextField *)textField {
    
    //    UITableViewCell *parentCell = (UITableViewCell *)[textField superview];
    //    ParameterMissing *currentParameter =[_calculationControl getParameterByIdAsInt:parentCell.tag];
    //    return [_calculationControl validateParameterValue:currentParameter value:textField.text];
    //
    return nil;
}

/*- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *NUMBERSPERIOD = @"1234567890.,-";
    NSString *NUMBERS = @"1234567890";
    NSString *NUMBERSPERIODNEW = @"1234567890.,";
    NSCharacterSet *cs;
    NSString *filtered;

    if(textField.placeholder != nil){
        
        if ((([textField.text rangeOfString:@"."].location == NSNotFound)) || (([textField.text rangeOfString:@","].location == NSNotFound)) ||
            (([textField.text rangeOfString:@"-"].location == NSNotFound)) ) {
            
            cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERSPERIOD] invertedSet];
            filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            return [string isEqualToString:filtered];
            
        }else {
            
        cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
        filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
        }
        
    }else{
        
        if ((([textField.text rangeOfString:@"."].location == NSNotFound)) || (([textField.text rangeOfString:@","].location == NSNotFound))) {
            
            cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERSPERIODNEW] invertedSet];
            filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            return [string isEqualToString:filtered];
            
        }else {
            cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
            filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            return [string isEqualToString:filtered];
        }
    }
    
    // Period is in use
    cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
    filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [string isEqualToString:filtered];
}*/

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if([string length]==0){
        return YES;
    }
    
    //Validate Character Entry
    NSCharacterSet *myCharSet;
    if (textField.placeholder != nil) {
        myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789,.-"];
    }else{
        myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789,."];
    }
    
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        
        if ([myCharSet characterIsMember:c]) {
            //now check if string already has 1 decimal mark
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            // NSArray *sep = [newString componentsSeparatedByString:@"."];
            
            NSArray  *arrayOfStringDot = [newString componentsSeparatedByString:@"."];
            NSArray  *arrayOfStringComma = [newString componentsSeparatedByString:@","];
            NSArray  *arrayOfStringMinus = [newString componentsSeparatedByString:@"-"];
            
            if ([arrayOfStringDot count] > 2 || [arrayOfStringComma count] > 2 ||[arrayOfStringMinus count] > 2 ) return NO;
            return YES;
        }
    }
    
    return NO;
    
    
}




- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    
}

- (void)keyboardWillBeShown:(NSNotification*)aNotification {
    
}

- (BOOL)disablesAutomaticKeyboardDismissal {
    
    return NO;
}

-(void)moveInputTable {
    
    keyboardIsShowing = YES;
    
    [addCalculationButton setHidden:YES];
    [bearingViewContainer setHidden:YES];
    //[doCalculationButton setHidden:YES];
    [removeCalculationTypeButton setHidden:YES];
    
    for(UIButton *tabs in [self.view subviews]) {
        if ([tabs isKindOfClass:[UIButton class]] && tabs.tag > 9999) {
            [tabs setHidden:YES];
        }
        
    }
    
    if( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait ||  [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        parametersViewContainer.frame = CGRectMake(20, 64, parametersViewContainer.frame.size.width, 600);
        
        inputParametersTableiPad.frame = CGRectMake(inputParametersTableiPad.frame.origin.x, inputParametersTableiPad.frame.origin.y, inputParametersTableiPad.frame.size.width, 500);
        doCalculationButton.frame = CGRectMake(doCalculationButton.frame.origin.x, 543, doCalculationButton.frame.size.width, doCalculationButton.frame.size.height );
        doResetButton.frame = CGRectMake(doResetButton.frame.origin.x, 543, doResetButton.frame.size.width, doResetButton.frame.size.height );
        doReportButton.frame = CGRectMake(doReportButton.frame.origin.x, 543, doReportButton.frame.size.width, doReportButton.frame.size.height );
        
        
    } else if ( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
        
        parametersViewContainer.frame = CGRectMake(20, 64, parametersViewContainer.frame.size.width, 265);
        inputParametersTableiPad.frame = CGRectMake(inputParametersTableiPad.frame.origin.x, inputParametersTableiPad.frame.origin.y, inputParametersTableiPad.frame.size.width, 165);
        doCalculationButton.frame = CGRectMake(doCalculationButton.frame.origin.x, 215, doCalculationButton.frame.size.width, doCalculationButton.frame.size.height);
        doResetButton.frame = CGRectMake(doResetButton.frame.origin.x, 215, doResetButton.frame.size.width, doResetButton.frame.size.height);
        doReportButton.frame = CGRectMake(doReportButton.frame.origin.x, 215, doReportButton.frame.size.width, doReportButton.frame.size.height);

    }
}

-(void)resetInputTable {
    
    keyboardIsShowing = NO;
    
    [addCalculationButton setHidden:NO];
    [bearingViewContainer setHidden:NO];
    //[doCalculationButton setHidden:NO];
    [removeCalculationTypeButton setHidden:NO];
    
    for(UIButton *tabs in [self.view subviews]) {
        if ([tabs isKindOfClass:[UIButton class]] && tabs.tag > 9999) {
            [tabs setHidden:NO];
        }
        
    }
    
    if( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait ||  [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        parametersViewContainer.frame = inputParametersContainerRectPortrait;
        inputParametersTableiPad.frame = inputParametersTableRectPortrait;
        doCalculationButton.frame = doCalculationButtonRectPortrait;
        doResetButton.frame = doResetButtonRectPortrait;
        doReportButton.frame = doReportButtonRectPortrait;
        
    } else if ( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
        
        parametersViewContainer.frame = inputParametersContainerRectLandscape;
        inputParametersTableiPad.frame = inputParametersTableRectLandscape;
        doCalculationButton.frame = doCalculationButtonRectLandscape;
        doResetButton.frame = doResetButtonRectLandscape;
        doReportButton.frame = doReportButtonRectLandscape;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    //lastString = textField.text;
    //lastnumber = textField.tag;
    
    activeField = textField;
    CGPoint point = [textField.superview convertPoint:textField.frame.origin toView:inputParametersTableiPad];
    CGPoint contentOffset = inputParametersTableiPad.contentOffset;
    contentOffset.y = point.y-15; // Adjust this value as you need
    [inputParametersTableiPad setContentOffset:contentOffset animated:NO];
    if(!keyboardIsShowing) {
      //  [self moveInputTable];
       if ( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
             [self moveInputTable];
       }
    }
    
    if(keyboardIsShowing && ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight)) {
        lineView.frame = CGRectMake(20, 184, 628, 1);
    }
    
    /*** For iOS 7 use below code for crashing issue while entering data in text field ***/
    UITableViewCell *cell= nil;
//    if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {
//     cell = (UITableViewCell*) [textField superview];
//    }else {
//         cell = (UITableViewCell *) textField.superview.superview;
//    }
    
    
    
    if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {
        cell = (UITableViewCell*) [textField superview];
    }else if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_7_1) {
        cell = (UITableViewCell *) textField.superview.superview;
        
    }else {
        
        cell = (UITableViewCell*) [textField superview];
        
        
    }

    
    
    //UITableViewCell *cell = [self getCellFromTableView:inputParametersTableiPad Sender:textField];
    //cell.backgroundView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"diagonal_stripe_background_tablecell.png"]];
    //[cell setBackgroundColor:[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"diagonal_stripe_background_tablecell.png"]]];
    [cell setBackgroundColor:[UIColor colorWithHexString:STRIPES_BACKGROUND_COLOR]];
    [_calculationControl resetMissingValueForParameterId:cell.tag];
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    
    NSLog(@"%ld",(long)textField.tag);
    
    [self resetInputTable];
    
   if ( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
        [self resetInputTable];
    }
    
    if(!keyboardIsShowing && ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight)) {
        lineView.frame = CGRectMake(20, 378, 628, 1);
    }
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    NSString *message = [self acceptsEntryInTextField:textField];
    //UITableViewCell *parentCell = [self getCellFromTableView:inputParametersTableiPad Sender:textField];
    UITableViewCell *parentCell= nil;
    
    if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {
        parentCell = (UITableViewCell*) [textField superview];
    }else if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_7_1) {
        parentCell = (UITableViewCell *) textField.superview.superview;

    }else {
        
        parentCell = (UITableViewCell*) [textField superview];

        
    }
    
    NSInteger tag = parentCell.tag;
    int state_change = -1;
    if (tag == 207)
    {
        if (textField != nil) {
            NSString *string = textField.text;
            if ([string rangeOfString:@","].location != NSNotFound) {
                NSString * newString = [string stringByReplacingOccurrencesOfString:@"," withString:@"."];
                //NSLog(@"........ %@ ... %@", newString,string);
                freqVal = [newString doubleValue];
            } else {
                freqVal   = [textField.text doubleValue];
            }
            
        }
        double freq;
        if (freqVal <= 0) {
                        UIAlertView *freqValue_iPad = [[UIAlertView alloc] initWithTitle:GetLocalizedString(@"Error")
                                                                 message:@"Please enter frequency value above zero."
                                                                delegate:self
                                                       cancelButtonTitle:GetLocalizedString(@"Ok")
                                                       otherButtonTitles:nil];
            [freqValue_iPad show];
            freqValue_iPad.tag = 50;
            [freqValue_iPad release];
            
        }
        if(freqVal > 0)
        {
            freq  = 60.0/freqVal;
            // To roundup the decimal value
            NSNumberFormatter *format = [[NSNumberFormatter alloc]init];
            [format setNumberStyle:NSNumberFormatterDecimalStyle];
            [format setRoundingMode:NSNumberFormatterRoundHalfUp];
            [format setMaximumFractionDigits:3];
            NSString *newFreqDecimalValue_ipad = [format stringFromNumber:[NSNumber numberWithFloat:freq]];
           // NSLog(@"%@",newFreqDecimalValue_ipad);
            
            [_calculationControl setValue:newFreqDecimalValue_ipad forId:226];
            NSString* parameterName = [_calculationControl nameForParameterId:226];
            CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
            [calculationDataManager setPreviousInput:parameterName withValue:newFreqDecimalValue_ipad];
            state_change = 1;
        }else{
            NSString * f_7 = [_calculationControl valueForId:@"207"];
            [textField setText:f_7];
            
        }
    }
    
    if (tag == 226) { // time param
        
        if (textField !=nil) {
            NSString *string = textField.text;
            if ([string rangeOfString:@","].location != NSNotFound) {
                NSString * newString = [string stringByReplacingOccurrencesOfString:@"," withString:@"."];
                timeVal = [newString doubleValue];
            }
            else
                timeVal = [textField.text doubleValue];
            
        }
        
        double time;
        if (timeVal <= 0) {
            
            UIAlertView *timeValue_iPad = [[UIAlertView alloc] initWithTitle:GetLocalizedString(@"Error")
                                                                 message:@"Please enter oscillation time value above zero."
                                                                delegate:self
                                                       cancelButtonTitle:GetLocalizedString(@"Ok")
                                                       otherButtonTitles:nil];
            [timeValue_iPad show];
            timeValue_iPad.tag = 51;
            [timeValue_iPad release];
            
        }
        if (timeVal >0)
        {
            time  = 60.0/timeVal;
            // To roundup the decimal value
            NSNumberFormatter *format = [[NSNumberFormatter alloc]init];
            [format setNumberStyle:NSNumberFormatterDecimalStyle];
            [format setRoundingMode:NSNumberFormatterRoundHalfUp];
            [format setMaximumFractionDigits:3];
            NSString *newTimeDecimalValue_ipad = [format stringFromNumber:[NSNumber numberWithFloat:time]];
            //NSLog(@"%@",newTimeDecimalValue_ipad);

            [_calculationControl setValue:newTimeDecimalValue_ipad forId:207];
            NSString* parameterName = [_calculationControl nameForParameterId:207];
            CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
            [calculationDataManager setPreviousInput:parameterName withValue:newTimeDecimalValue_ipad];
            state_change = 1;
        }else{
            NSString * f_7 = [_calculationControl valueForId:@"226"];
            [textField setText:f_7];
            [textField becomeFirstResponder];
        }
    }
    

    
    if ([message length] == 0)
    {   // this checks if the text is valid
        // We're done here
        
        //iOS 6
       // UITableViewCell *parentCell = (UITableViewCell *)[textField superview];
        [_calculationControl setValue:textField.text forId:parentCell.tag];
        NSString *parameterName = [_calculationControl nameForParameterId:parentCell.tag];
        [calculationDataManager setPreviousInput:parameterName withValue:textField.text];
        
        if(state_change > 0 )
        {
            [inputParametersTableiPad beginUpdates];
            [inputParametersTableiPad reloadData];
            [inputParametersTableiPad endUpdates];
            state_change = -1;
            NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:5 inSection:0];
            [inputParametersTableiPad cellForRowAtIndexPath:rowToReload];
            
        }
    }
    else
    {
        // Setting the delegate to nil will prevent the textField to listen for Return key events.
        textField.delegate = nil;
        
        //iOS 6
         UITableViewCell *parentCell = (UITableViewCell *)[textField superview];
        [_calculationControl removeValueForId:parentCell.tag];
        [textField setText:@""];
        [textField resignFirstResponder];
        currTextField = textField;
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:GetLocalizedString(@"Error")
                                                     message:message
                                                    delegate:self
                                           cancelButtonTitle:GetLocalizedString(@"Ok")
                                           otherButtonTitles:nil];
        [av show];
        [av release];
    }
    
}

- (void)dismissKeyboard
{
    UITextField *tempTextField = [[[UITextField alloc] initWithFrame:CGRectZero] autorelease];
    tempTextField.enabled = NO;
    [selectCalculationViewController.view addSubview:tempTextField];
    [tempTextField becomeFirstResponder];
    [self textFieldDidBeginEditing:tempTextField];
    [tempTextField resignFirstResponder];
    [tempTextField removeFromSuperview];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1001 && buttonIndex ==1){
        
        // To change navigation bgcolor, title color, back button color
        NSString *navigationBarTintColoriOS7 = NAVIGATION_BACKGROUND_COLOR;
        [[UINavigationBar appearanceWhenContainedIn:[QLPreviewController class], nil] setTintColor:[UIColor whiteColor]];
        [[UINavigationBar appearanceWhenContainedIn:[QLPreviewController class], nil] setBarTintColor:[UIColor colorWithHexString:navigationBarTintColoriOS7]];
        
        
        QLPreviewController *ql = [[QLPreviewController alloc] init];
        ql.dataSource = self;
        ql.delegate = self;
        ql.currentPreviewItemIndex = 0; //0 because of the assumption that there is only 1 file
        
        //[self presentViewController:ql animated:NO completion:nil];
        //self.view.translatesAutoresizingMaskIntoConstraints = NO;
        //[ql retain];
       
//        [[self.navigationController topViewController].navigationController pushViewController:ql animated:YES];
//        [self.navigationController popToRootViewControllerAnimated:NO];
        //[self.navigationController pushViewController:ql animated:YES];
       
        
             [self presentViewController:ql animated:NO completion:nil];
        
       
        
//        QLPreviewController *preview = [[[QLPreviewController alloc] init]autorelease];
//        
//        preview.dataSource = self;
//        [self presentViewController:preview
//                           animated:NO
//         
//                         completion:^(){
//                           
//                           //  UIViewController *vc = [[UIViewController alloc] initWithNibName:nil bundle:nil];
//                            // vc.someData = data;
//                            // [self.navigationController pushViewController:vc animated:NO];
//                           //  [self shouldAutorotate:UIInterfaceOrientationPortrait];
//                             UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
//                             [mainWindow setNeedsDisplay];
//                         }];
        
        
//        [preview.navigationItem setRightBarButtonItem:nil];
        
        return;
    }
    
    if (alertView.tag == 2) {
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        
        if(calculationDataManager.selectedCalculationsArray.count == noFormulaErrorArray.count) {
            
            [self.navigationController popViewControllerAnimated:YES];
            
        } else {
            
            [calculationDataManager.selectedCalculationsArray removeObjectsInArray:objectsToDiscard];
            [inputParametersTableiPad reloadData];
        }
        
    }else if (alertView.tag == 11) {
        if (buttonIndex == 1) {
            [activeField resignFirstResponder];
            CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
            [calculationDataManager resetInputParameters];
            [_calculationControl resetInputParameters];
            [inputParametersTableiPad reloadData];
        }
    }else {
        
        // gets called when the user dismisses the alert view
        currTextField.text = @""; // erase bad entry
        currTextField.delegate = self; // We want to listen for return events again
        [currTextField becomeFirstResponder];
        currTextField = nil;
    }
   }

/*
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration
{
    [UIView setAnimationsEnabled:NO];
    
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [UIView setAnimationsEnabled:NO];
}*/


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _calculationControl = [[CalculationControl alloc] init];
    }
    return self;
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _calculationControl = [[CalculationControl alloc] init];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

//int stopIntroText =-1;

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    /** App IntroText for iPad */
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];

    

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL dontShow= [userDefaults boolForKey:@"DONOTSHOW"];
    
    
    if(dontShow == NO && calculationDataManager.stopIntroText < 0)
    {
        IntroTextViewController_iPad *introTextViewController_iPad = [[ IntroTextViewController_iPad alloc] initWithNibName:@"IntroTextViewController_iPhone" bundle:nil];
        
        introTextViewController_iPad.modalPresentationStyle = UIModalPresentationFormSheet;
        introTextViewController_iPad.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal; //transition shouldn't matter
        [self presentModalViewController:introTextViewController_iPad animated:YES];
        introTextViewController_iPad.view.superview.frame = CGRectMake(0, 0, 320, 462);//it's important to do this after
        
        
        if( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait ||  [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown){
            
             introTextViewController_iPad.view.superview.frame = CGRectMake(0, 0, 320, 462);
            introTextViewController_iPad.view.superview.center = self.view.center;

            
        } else if ( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
            
            introTextViewController_iPad.view.superview.center = CGPointMake(500, 390);
        }
    }
    
    
   }



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"SBC_iPad - Main screen";
    
    [self setSearchingModeStart];
    
    
    
    
    
    //self.view.superview.layer.cornerRadius = 0;

    
//    [self.view.superview.layer setCornerRadius:4.0f];
//    [self.view.superview.layer setBorderColor:[UIColor colorWithHexString:NAVIGATION_BACKGROUND_COLOR].CGColor];
//    [self.view.superview.layer setBorderWidth:1.5f];
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    calculationDataManager.stopIntroText = -1;
    [doResetButton setHidden:YES];
    
    noFormulaErrorArray = [[NSMutableArray alloc] init];
    isHeaderShown = NO;
    
    params = [[NSMutableDictionary alloc] init];
    resultArray = [[NSMutableArray alloc] init];
    messagesArray = [[NSMutableArray alloc] init];
    warningsArray = [[NSMutableArray alloc] init];
    errorsArray = [[NSMutableArray alloc] init];
    parameterDatabaseArray = [[NSMutableArray alloc] init];
    parameterAvailableArray = [[NSMutableArray alloc] init];
    
    [calculationDataManager initTranslation:self];
    [calculationDataManager addObserver:self];
    
    tabsOverlapping = NO;
    keyboardIsShowing = NO;
    connected = true;
    
    
    /*** SIZES AND POSITIONS ***/
    
    if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {

    bearingContainerRectPortrait = CGRectMake(20, 61, 728, 166);
    inputParametersContainerRectPortrait = CGRectMake(20, 275, 728, 372);
    inputParametersTableRectPortrait = CGRectMake(20, 20, 688, 278);
    resultItemRectPortrait = CGRectMake(-302, 305, 302, 321);
    resultItemTableViewRectPortrait = CGRectMake(10, 50, 280, 240);
    resultsContainerScrollViewRectPortrait = CGRectMake(0, 363, 768, 641);
    doResetButtonRectPortrait = CGRectMake(360, 316, 150, 44);
    doReportButtonRectPortrait = CGRectMake(480, 316, 150, 44);
    doCalculationButtonRectPortrait = CGRectMake(600, 316, 150, 44);
    removeCalculationTypeButtonRectPortrait = CGRectMake(12, 316, 141, 46);
    
    bearingContainerRectLandscape = CGRectMake(20, 61, 670, 166);
    inputParametersContainerRectLandscape = CGRectMake(20, 275, 670, 450);
    inputParametersTableRectLandscape = CGRectMake(20, 20, 630, 358);
    resultItemRectLandscape = CGRectMake(200, -302, 302, 321);
    resultItemTableViewRectLandscape = CGRectMake(10, 50, 280, 240);
    resultsContainerScrollViewRectLandscape = CGRectMake(170, 44, 853, 704);
    doResetButtonRectLandscape = CGRectMake(190, 393, 150, 44);
    doReportButtonRectLandscape = CGRectMake(347, 393, 150, 44);
    doCalculationButtonRectLandscape = CGRectMake(502, 393, 150, 44);
    removeCalculationTypeButtonRectLandscape = CGRectMake(12, 393, 141, 46);
    
    }else {
        
        bearingContainerRectPortrait = CGRectMake(20, 81, 728, 166);
        inputParametersContainerRectPortrait = CGRectMake(20, 315, 728, 372);
        inputParametersTableRectPortrait = CGRectMake(20, 20, 688, 278);
        resultItemRectPortrait = CGRectMake(-302, 345, 302, 281);
        resultItemTableViewRectPortrait = CGRectMake(10, 50, 280, 240);
        resultsContainerScrollViewRectPortrait = CGRectMake(0, 363, 768, 641);
        doResetButtonRectPortrait = CGRectMake(360, 316, 150, 44);
        doReportButtonRectPortrait = CGRectMake(480, 316, 150, 44);
        doCalculationButtonRectPortrait = CGRectMake(600, 316, 150, 44);
        removeCalculationTypeButtonRectPortrait = CGRectMake(12, 316, 141, 46);
        
        bearingContainerRectLandscape = CGRectMake(20, 81, 670, 166);
        inputParametersContainerRectLandscape = CGRectMake(20, 315, 670, 450);
        inputParametersTableRectLandscape = CGRectMake(20, 20, 630, 358);
        resultItemRectLandscape = CGRectMake(200, -302, 302, 321);
        resultItemTableViewRectLandscape = CGRectMake(10, 50, 280, 240);
        resultsContainerScrollViewRectLandscape = CGRectMake(170, 44, 853, 704);
        doResetButtonRectLandscape = CGRectMake(190, 393, 150, 44);
        doReportButtonRectLandscape = CGRectMake(347, 393, 150, 44);
        doCalculationButtonRectLandscape = CGRectMake(502, 393, 150, 44);
        removeCalculationTypeButtonRectLandscape = CGRectMake(12, 393, 141, 46);
    }
    
    
    

    
    /***************************/
    
    storedReturnedDictionaries = [[NSMutableDictionary alloc] init];
    inputParametersTableiPad.separatorColor = [UIColor clearColor];
    
    indexPathsForTextFields = [[NSMutableArray alloc] init];
    
    calculationResultsCount = 999;
    inputParametersTableiPad.tag = 9910;
    
    cssString = @"<html> \n"
    "<head> \n"
    "<style type=\"text/css\"> \n"
    "body {font-family: \"SKF Chevin Medium\"; font-size: 14; margin:4px 0px;background-color:transparent; color:#333333; word-wrap:break-word;}\n"
    "</style> \n"
    "</head> \n";
    
    // For input parameter field description
    cssStringGray = @"<html> \n"
    "<head> \n"
    "<style type=\"text/css\"> \n"
    "body {font-family: \"SKF Chevin Medium\"; font-size: 12; margin:4px 0px;background-color:transparent; color:#808080; word-wrap:break-word;}\n"
    "</style> \n"
    "</head> \n";
    
    cssStringBlue = @"<html> \n"
    "<head> \n"
    "<style type=\"text/css\"> \n"
    "body {font-family: \"SKF Chevin Medium Blue\"; font-size: 14; margin:4px 0px;background-color:transparent; color:#da1e3c; word-wrap:break-word;}\n"
    "</style> \n"
    "</head> \n";
    
    cssStringRed = @"<html> \n"
    "<head> \n"
    "<style type=\"text/css\"> \n"
    "body {font-family: \"SKF Chevin Medium\"; font-size: 10; margin:4px 0px;background-color:transparent; color:#333333; word-wrap:break-word;}\n"
    "</style> \n"
    "</head> \n";

    cssStringBrgParam = @"<html> \n"
    "<head> \n"
    "<style type=\"text/css\"> \n"
    "body {font-family: \"SKF Chevin Medium\"; font-size: 10; margin:4px 0px;background-color:transparent; color:#333333; word-wrap:break-word;}\n"
    "</style> \n"
    "</head> \n";

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissChangeBearingPopover:) name:@"dismissChangeBearingPopover" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ipadBearingChanged:) name:@"ipadBearingChanged" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ipadDoCalculation:) name:@"ipadDoCalculation" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ipadRemoveResult:) name:@"ipadRemoveResult" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ipadAddCalculationType:) name:@"ipadAddCalculationType" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ipadUpdateCalculationType:) name:@"ipadUpdateCalculationType" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ipadRemoveCalculationType:) name:@"ipadRemoveCalculationType" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ipadOptionSelected:) name:@"ipadOptionSelected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ipadLegalInfoAgreed:) name:@"ipadLegalInfoAgreed" object:nil];
    
    bearingViewContainer.layer.backgroundColor = [[UIColor whiteColor] CGColor];
    bearingViewContainer.layer.masksToBounds = NO;
    bearingViewContainer.layer.cornerRadius = 8; // if you like rounded corners
    bearingViewContainer.layer.shadowOffset = CGSizeMake(1, 1);
    bearingViewContainer.layer.shadowRadius = 4;
    bearingViewContainer.layer.shadowOpacity = 0.3;
    bearingViewContainer.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    
    parametersViewContainer.layer.backgroundColor = [[UIColor whiteColor] CGColor];
    parametersViewContainer.layer.masksToBounds = NO;
    parametersViewContainer.layer.cornerRadius = 8; // if you like rounded corners
    parametersViewContainer.layer.shadowOffset = CGSizeMake(1, 1);
    parametersViewContainer.layer.shadowRadius = 4;
    parametersViewContainer.layer.shadowOpacity = 0.3;
    
    resultsViewContainer.layer.backgroundColor = [[UIColor whiteColor] CGColor];
    resultsViewContainer.layer.masksToBounds = NO;
    resultsViewContainer.layer.cornerRadius = 8; // if you like rounded corners
    resultsViewContainer.layer.shadowOffset = CGSizeMake(1, 1);
    resultsViewContainer.layer.shadowRadius = 4;
    resultsViewContainer.layer.shadowOpacity = 0.3;
    
    skfChevinTitleLabel = topToolbarLabel;
    skfChevinTitleLabel.backgroundColor = [UIColor clearColor];
    skfChevinTitleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:IPAD_FONT_SIZE_FOR_NAVIGATION_TITLE];
    skfChevinTitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    skfChevinTitleLabel.textAlignment = NSTextAlignmentCenter;
    skfChevinTitleLabel.textColor = [UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f];
    
    //UIImage *image = [UIImage imageNamed:@"ipad_logo_topbar.png"];
    UIImage *image = [UIImage imageNamed:@"SKF_logo_white_94x48.png"];
    logoImageView = [[UIImageView alloc] initWithImage:image];
    //logoImageView.frame = CGRectMake(700, 9, 50, 25);
    
    /*if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {

        logoImageView.frame = CGRectMake(700, 9, 50, 25);
        
    }else {
        
        logoImageView.frame = CGRectMake(700, 29, 50, 25);
    }*/
    


    
    [topNavigationBar addSubview:logoImageView];
    [topToolbar addSubview:logoImageView];
    
    /*CGRect rect = CGRectMake(
                             showLegalInfoButton.frame.origin.x - 8,
                             showLegalInfoButton.frame.origin.y - 8,
                             showLegalInfoButton.frame.size.width + 16,
                             showLegalInfoButton.frame.size.height + 16);
    [showLegalInfoButton setFrame:rect];*/
    
    
    /*** iOS 7 - To change the toptoolbar background and info button color ***/
    
    //Old code
     /*NSString *navigationBarTintColor = TITLE_TINT_COLOR;
     [topNavigationBar setTintColor:[UIColor colorWithHexString:navigationBarTintColor]];
     [topToolbar setTintColor:[UIColor colorWithHexString:navigationBarTintColor]];*/

    
    /*NSString *navigationBarTintColoriOS7 = NAVIGATION_BACKGROUND_COLOR;
    
    if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {
        
        [topNavigationBar setTintColor:[UIColor colorWithHexString:navigationBarTintColoriOS7]];
        [topToolbar setTintColor:[UIColor colorWithHexString:navigationBarTintColoriOS7]];
        
        logoImageView.frame = CGRectMake(700, 9, 50, 25);

        
    }else {
        
        topToolbar.frame = CGRectMake(0, 0, 768, 64);
        [topToolbar setBarTintColor:[UIColor colorWithHexString:navigationBarTintColoriOS7]];
        [topToolbar setTranslucent:NO];
        
        topToolbarLabel.frame = CGRectMake(127, 20, 514, 53);
        logoImageView.frame = CGRectMake(700, 29, 50, 25);
        
       // showLegalInfoButton.frame = CGRectMake(28, 150, 22, 22);
        //showLegalInfoButton.frame = CGRectMake(-20, -20, 22, 22);
        
        showLegalInfoButton.frame = CGRectMake(0, 0, 22, 22);
        showLegalInfoButton.tintColor = [UIColor whiteColor];
    }*/
    
    
    infoButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
    [infoButton addTarget:self action:@selector(showSettings:) forControlEvents:UIControlEventTouchUpInside];
   // infoIcon.frame = CGRectMake(20,37,25,25);
    UIImage * buttonImage = [UIImage imageNamed:@"infoButton.png"];
    [infoButton setImage:buttonImage forState:UIControlStateNormal];
    infoButton.tintColor = [UIColor whiteColor];
    [topToolbar addSubview:infoButton];

    
    
    NSString *navigationBarTintColoriOS7 = NAVIGATION_BACKGROUND_COLOR;
    
    if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {
        
        [topNavigationBar setTintColor:[UIColor colorWithHexString:navigationBarTintColoriOS7]];
        [topToolbar setTintColor:[UIColor colorWithHexString:navigationBarTintColoriOS7]];
        
        infoButton.frame = CGRectMake(20, 9, 22, 22);
        logoImageView.frame = CGRectMake(700, 9, 50, 25);
        
        skfChevinTitleLabel.frame = CGRectMake(127, 0, 514, 44);
        
        
        
    }else {
        
        //topToolbar.frame = CGRectMake(0, 0, 768, 64);
        topToolbar.frame = CGRectMake(0, 0, 1024, 64);
        [topToolbar setBarTintColor:[UIColor colorWithHexString:navigationBarTintColoriOS7]];
        [topToolbar setTranslucent:NO];
        
        infoButton.frame = CGRectMake(20, 29, 22, 22);
        topToolbarLabel.frame = CGRectMake(127, 20, 514, 53);
        logoImageView.frame = CGRectMake(700, 29, 50, 25);
        
        skfChevinTitleLabel.frame = CGRectMake(127, 15, 514, 44);
        
        
        /*showLegalInfoButton.frame = CGRectMake(28, 150, 22, 22);
         showLegalInfoButton.frame = CGRectMake(-20, -20, 22, 22);
         showLegalInfoButton.tintColor = [UIColor whiteColor];*/
    }

    
    
/*** END ***/
    
#pragma mark - Buttons
    
    // Change bearing button
    UIImage *calcButtonImage = [UIImage imageNamed:@"ipad_blue_button.png"];
    UIImage *calcButtonImageStrechable = [calcButtonImage stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [changeBearingButton setBackgroundImage:calcButtonImageStrechable forState:UIControlStateNormal];
    [changeBearingButton setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateSelected];
    [changeBearingButton setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateNormal];
    changeBearingButton.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON];
    changeBearingButton.titleLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];

    
    // Add calculation button
    UIImage *plusSymbol = [UIImage imageNamed:@"ipad_plus_symbol.png"];
    UIImageView *plusSymbolImageView = [[UIImageView alloc] initWithImage:plusSymbol];
    plusSymbolImageView.frame = CGRectMake(14, 12, 11, 11);

    addCalculationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [addCalculationButton setBackgroundImage:calcButtonImageStrechable forState:UIControlStateNormal];
    [addCalculationButton addSubview:plusSymbolImageView];
    [addCalculationButton addTarget:self
                             action:@selector(addCalculationType:)
                   forControlEvents:UIControlEventTouchDown];
    if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {

        addCalculationButton.frame = CGRectMake(30, 238, 140, 35);
        
    }else {
        addCalculationButton.frame = CGRectMake(30, 278, 140, 35);
    }
    
    [addCalculationButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 15.0, 0.0, 0.0)];
    addCalculationButton.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON];
    addCalculationButton.titleLabel.textColor = [UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f];
    [self.view addSubview:addCalculationButton];
    
    /*** Calculate button ***/
    UIImage *doCalcButtonImage = [UIImage imageNamed:@"ipad_calculate_button.png"];
    UIImage *doCalcButtonImageStrechable = [doCalcButtonImage stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    UIImage *doCalcSymbol = [UIImage imageNamed:@"ipad_calculate_symbol.png"];
    UIImageView *doCalcSymbolView = [[UIImageView alloc] initWithImage:doCalcSymbol];
    doCalcSymbolView.frame = CGRectMake(125, 10, 17, 23);
    
    
    /*** BUTTONS - RESET | CALCULATE |  REPORT  ***/
    
    [doResetButton setTitleEdgeInsets:UIEdgeInsetsMake(3.0, 10.0, 0.0, 15.0)];
    [doCalculationButton setTitleEdgeInsets:UIEdgeInsetsMake(3.0, 0.0, 0.0, 15.0)];
    [doReportButton setTitleEdgeInsets:UIEdgeInsetsMake(3.0, 10.0, 0.0, 15.0)];

    // For iOS 6 style

    /*[doResetButton setBackgroundImage:doCalcButtonImageStrechable forState:UIControlStateNormal];
    [doResetButton setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateSelected];
    [doResetButton setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateNormal];
    doResetButton.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON_LARGE];
    doResetButton.titleLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_WHITE];
    
    [doCalculationButton setBackgroundImage:doCalcButtonImageStrechable forState:UIControlStateNormal];
    [doCalculationButton addSubview:doCalcSymbolView];
    [doCalculationButton setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateSelected];
    [doCalculationButton setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateNormal];
    doCalculationButton.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON_LARGE];
    doCalculationButton.titleLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_WHITE];
    
    [doReportButton setBackgroundImage:doCalcButtonImageStrechable forState:UIControlStateNormal];
    [doReportButton setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateSelected];
    [doReportButton setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateNormal];
    doReportButton.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON_LARGE];
    doReportButton.titleLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_WHITE];*/

    // For iOS 7 style
    [doResetButton setTitleColor:[UIColor colorWithHexString:SKF_BLUE_COLOR] forState:UIControlStateSelected];
    [doResetButton setTitleColor:[UIColor colorWithHexString:SKF_BLUE_COLOR] forState:UIControlStateNormal];
    doResetButton.titleLabel.font = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_BUTTON_MEDIUM];
    doResetButton.titleLabel.textColor = [UIColor colorWithHexString:SKF_BLUE_COLOR];
    
    [doCalculationButton setTitleColor:[UIColor colorWithHexString:SKF_BLUE_COLOR] forState:UIControlStateSelected];
    [doCalculationButton setTitleColor:[UIColor colorWithHexString:SKF_BLUE_COLOR] forState:UIControlStateNormal];
    doCalculationButton.titleLabel.font = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_BUTTON_MEDIUM];
    doCalculationButton.titleLabel.textColor = [UIColor colorWithHexString:SKF_BLUE_COLOR];
    
    [doReportButton setTitleColor:[UIColor colorWithHexString:SKF_BLUE_COLOR] forState:UIControlStateSelected];
    [doReportButton setTitleColor:[UIColor colorWithHexString:SKF_BLUE_COLOR] forState:UIControlStateNormal];
    doReportButton.titleLabel.font = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_BUTTON_MEDIUM];
    doReportButton.titleLabel.textColor = [UIColor colorWithHexString:SKF_BLUE_COLOR];

    
    // Remove calculation button
    [removeCalculationTypeButton setBackgroundImage:doCalcButtonImageStrechable forState:UIControlStateNormal];
    [removeCalculationTypeButton setTitleEdgeInsets:UIEdgeInsetsMake(3.0, 15.0, 0.0, 15.0)];
    removeCalculationTypeButton.titleLabel.font = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_BUTTON_LARGE];
    [removeCalculationTypeButton setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateSelected];
    [removeCalculationTypeButton setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateNormal];
    [removeCalculationTypeButton setHidden:YES];
    
    
    /*** Removing Previous & Next button keyboard toolbar for SBC_V1.5.0 **/
   /* keyboardToolBar = [[UIToolbar alloc] init];
    [keyboardToolBar setBarStyle:UIBarStyleBlackTranslucent];
    [keyboardToolBar sizeToFit];
    
    UIBarButtonItem *prevButton =[[UIBarButtonItem alloc] initWithTitle:[calculationDataManager getTranslationString:@"Prev"] style:UIBarButtonItemStyleBordered target:self action:@selector(prevButton:)];
    UIBarButtonItem *nextButton =[[UIBarButtonItem alloc] initWithTitle:[calculationDataManager getTranslationString:@"Next"] style:UIBarButtonItemStyleBordered target:self action:@selector(nextButton:)];
    UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    NSArray *itemsArray = [NSArray arrayWithObjects:prevButton, nextButton, flexButton, nil];
    
    [keyboardToolBar setItems:itemsArray];
    [prevButton release];
    [nextButton release];
    [flexButton release];*/
    /*** END ***/
    
    
    
    inputParametersTableiPad.backgroundColor = [UIColor clearColor];
    inputParametersTableiPad.separatorColor = [UIColor clearColor];
    inputParametersTableiPad.opaque = NO;
    
    inputParametersTableiPad.backgroundView = nil;
    
   
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:RKReachabilityDidChangeNotification
                                               object:nil];
    
    
   }


- (void)reachabilityChanged:(NSNotification*)notification {
    RKReachabilityObserver* observer = (RKReachabilityObserver*)[notification object];
    
    if (![observer isNetworkReachable])
    {
        [self setSearchingModeEnd];
        connected = false;
        DLog(@"Disconnected");
    }
    else {
        connected = true;
        DLog(@"Connected");
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager removeObserver:self];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    [resultArray removeAllObjects];
    [warningsArray removeAllObjects];
    [errorsArray removeAllObjects];
    [messagesArray removeAllObjects];
    [parameterAvailableArray removeAllObjects];
    [parameterDatabaseArray removeAllObjects];
    resultArray = nil;
    warningsArray = nil;
    errorsArray = nil;
    messagesArray = nil;
    parameterAvailableArray = nil;
    parameterDatabaseArray = nil;
    
    [indexPathsForTextFields removeAllObjects];
    indexPathsForTextFields = nil;
    
    [reportsViewController release];
    reportsViewController = nil;
//    [textLabel_intr release];
//    [imgView_intr release];
//    [imgView_tintr release];
//    [myview_intr release];

}


/** iOS 5*/
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
    // Return YES for supported orientations
	//return YES;
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


/** For iOS 6*/   // For splash screen - with intro screen it was coming only in one orientaion

-(BOOL)shouldAutorotate {
    
//    double delayInSeconds = 20.0;
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        UIViewController *vc = [self presentedViewController];
        if([vc isKindOfClass : [QLPreviewController class]]){
        NSLog(@"Do some work");
            lastvalue += 10;
////        [self viewWillAppear:YES];
        }
        else
        {
            if(lastvalue > 10)
            {
//               UIDeviceOrientation *defaultvalue =  [[UIDevice currentDevice] orientation];
//                NSLog(@"%ld........... %@", (long)UIInterfaceOrientationLandscapeLeft,defaultvalue);
                NSNumber *value = 0;//&defaultvalue != UIInterfaceOrientationLandscapeLeft ? [NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft]: [NSNumber numberWithInt:UIInterfaceOrientationLandscapeRight];
                if([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft)
                {
                    value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
                }else if([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight)
                {
                    value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
                }
                else{
                   value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeRight];
                }
                [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
                lastvalue = 0;
                return YES;
            }
            lastvalue = 0;
        }
//     //   [self viewWillAppear:Ye];
//    });
    
    return YES;
}

BOOL StopOnce = TRUE;

-(NSUInteger)supportedInterfaceOrientations {
    
    if (StopOnce) {
        
        StopOnce = FALSE;
        return UIInterfaceOrientationMaskPortrait;
        
    } else {
        
        return UIInterfaceOrientationMaskAll;
    }
}

/*- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    
    return UIInterfaceOrientationPortrait;
    
}*/

/************************* TABLE VIEW *****************************/
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    
    
    if(tableView.tag == 9920)
    {
        if (section == TBL_SEC_ERRORS && [errorsArray count] == 0)
            return [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
        if (section == TBL_SEC_WARNINGS && [warningsArray count] == 0)
            return [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
        if (section == TBL_SEC_MESSAGES && [messagesArray count] == 0)
            return [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
        
        UIView* customView = [[[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 44.0)] autorelease];
        NSString *headerLabelColor = COLOR_SKF_RED;
        UIImageView *ProdDataImage = [[[UIImageView alloc]initWithFrame:CGRectMake(10, 0, 300, 44)] autorelease];

    
        //[customView.layer setValue: selectedProduct.type forKey: @"selectedProduct"];
 
        // create the button object
        UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        headerLabel.backgroundColor = [UIColor clearColor];
        headerLabel.opaque = NO;
        headerLabel.textColor = [UIColor colorWithHexString:headerLabelColor];
        headerLabel.highlightedTextColor = [UIColor colorWithHexString:headerLabelColor];
        headerLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_GROUPED_TABLE_HEADER];
        headerLabel.frame = CGRectMake(10.0, 0.0, 300.0, 44.0);
        
        switch (section)
        {
            case TBL_SEC_RESULTS:
                if(calculationDataManager.mutlipleCalculation == true) {
                    headerLabel.text = GetLocalizedString(@"Results");
                }else if (calculationDataManager.mutlipleCalculation == false){
                    headerLabel.text = @"Result";
                }
                break;
            case TBL_SEC_ERRORS:
                headerLabel.text = GetLocalizedString(@"Errors");
                break;
            case TBL_SEC_WARNINGS:
                headerLabel.text = GetLocalizedString(@"Warnings");
                break;
            case TBL_SEC_MESSAGES:
                headerLabel.text = GetLocalizedString(@"Messages");
                break;
            case TBL_SEC_PRODUCT_DATA:
                headerLabel.text = GetLocalizedString(@"Bearing_information");
                [customView setFrame:CGRectMake(10.0, 10.0, 300, 30)];
                
                /*if ((![selectedProduct.type isEqualToString:@"3_1"]) || (![selectedProduct.type isEqualToString:@"3_3a"]) ||  (![selectedProduct.type isEqualToString:@"3_3b"]) ||  (![selectedProduct.type isEqualToString:@"3_4"])) {
                    UIImageView *ProdDataImage= [[[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 270, 250)] autorelease];
                    ProdDataImage.backgroundColor = [UIColor clearColor];
                    float scale1 = [UIScreen mainScreen].scale;
                    CGSize diaImageViewSize = CGSizeMake(320, 280);
                    CGSize diaImageSize = CGSizeMake(scale1 * (diaImageViewSize.width - 1), scale1 * (diaImageViewSize.height - 1));
                    NSMutableString* diaImagePath =
                    [NSMutableString stringWithFormat:@"/Image?bearingType=%@&productId=%@&measurement=%@&withDia=true&width=%d&height=%d",
                     selectedProduct.type, selectedProduct.productId,[calculationDataManager selectedMeasurement],
                     (int)diaImageSize.width, (int)diaImageSize.height];
                    NSMutableString* diaProductImageUrl = [NSMutableString stringWithFormat:@"%@%@", calculationDataManager.baseURL,diaImagePath];
                    ProdDataImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:diaProductImageUrl]]];
                    ProdDataImage.contentMode = UIViewContentModeScaleAspectFit;
                    [customView addSubview:ProdDataImage];
                }*/
                
                NSLog(@"Selected type: %@", selectedProduct.type);
                NSLog(@"Calculated type: %@", calculatedProduct.type);
                
                if (selectedProduct.type == calculatedProduct.type) {
                    if (![calculatedProduct.type isEqualToString:@"3_1"]) {
                        ProdDataImage= [[[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 270, 250)] autorelease];
                        ProdDataImage.backgroundColor = [UIColor clearColor];
                        float scale1 = [UIScreen mainScreen].scale;
                        CGSize diaImageViewSize = CGSizeMake(320, 280);
                        CGSize diaImageSize = CGSizeMake(scale1 * (diaImageViewSize.width - 1), scale1 * (diaImageViewSize.height - 1));
                        NSMutableString* diaImagePath =
                        [NSMutableString stringWithFormat:@"/Image?bearingType=%@&productId=%@&measurement=%@&withDia=true&width=%d&height=%d",
                         selectedProduct.type, selectedProduct.productId,[calculationDataManager selectedMeasurement],
                         (int)diaImageSize.width, (int)diaImageSize.height];
                        NSMutableString* diaProductImageUrl = [NSMutableString stringWithFormat:@"%@%@", calculationDataManager.baseURL,diaImagePath];
                        ProdDataImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:diaProductImageUrl]]];
                        ProdDataImage.contentMode = UIViewContentModeScaleAspectFit;
                        [customView addSubview:ProdDataImage];
                    }else{
                        break;
                    }
                }
                
                if (selectedProduct.type != calculatedProduct.type){
                    
                    if (![calculatedProduct.type isEqualToString:@"3_1"]) {
                        ProdDataImage= [[[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 270, 250)] autorelease];
                        ProdDataImage.backgroundColor = [UIColor clearColor];
                        float scale1 = [UIScreen mainScreen].scale;
                        CGSize diaImageViewSize = CGSizeMake(320, 280);
                        CGSize diaImageSize = CGSizeMake(scale1 * (diaImageViewSize.width - 1), scale1 * (diaImageViewSize.height - 1));
                        NSMutableString* diaImagePath =
                        [NSMutableString stringWithFormat:@"/Image?bearingType=%@&productId=%@&measurement=%@&withDia=true&width=%d&height=%d",
                         calculatedProduct.type, selectedProduct.productId,[calculationDataManager selectedMeasurement],
                         (int)diaImageSize.width, (int)diaImageSize.height];
                        NSMutableString* diaProductImageUrl = [NSMutableString stringWithFormat:@"%@%@", calculationDataManager.baseURL,diaImagePath];
                        ProdDataImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:diaProductImageUrl]]];
                        ProdDataImage.contentMode = UIViewContentModeScaleAspectFit;
                        [customView addSubview:ProdDataImage];
                    }else{
                        break;
                    }
                    
                }
                
                
                
                
                
                break;
            case TBL_SEC_INPUT_PARAMETERS:
                headerLabel.text = GetLocalizedString(@"Input_parameters");
                break;
            default:
                break;
        }
        [customView addSubview:headerLabel];
        [headerLabel release];
        return customView;
    } else {
        return nil;
    }
}

- (void) productLinkButtonClicked:(id)sender {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    NSMutableString *productLinkPath = [NSMutableString stringWithFormat:@"http://www.skf.com/productcatalogue/prodlink.html?prodid=%@&lang=en&imperial=false",calculationDataManager.selectedBearing.productId];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: productLinkPath]];
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if(tableView.tag == 9920) {
        if (section == TBL_SEC_ERRORS && [errorsArray count] == 0)
            return 1.0;
        if (section == TBL_SEC_WARNINGS && [warningsArray count] == 0)
            return 1.0;
        if (section == TBL_SEC_MESSAGES && [messagesArray count] == 0)
            return 1.0;
        if (section == TBL_SEC_PRODUCT_DATA ){
            
            if (([selectedProduct.type isEqualToString:@"3_1"]) || ([selectedProduct.type isEqualToString:@"3_3a"]) ||  ([selectedProduct.type isEqualToString:@"3_3b"]) ||  ([selectedProduct.type isEqualToString:@"3_4"])) {
                
                
                return 44.0;
            }else {
                
                return 230.0;
            }
            
        }
        else
            return 44.0;
    } else {
        return 0;
    }
}

//-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
//{
//    if(tableView.tag == 9920) {
//        return 5.0;
//    } else {
//        return 0;
//    }
//}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    if(tableView.tag == 9920) {
        
        if (section == TBL_SEC_PRODUCT_DATA) {
            
            // return  100.0;
            
            
            if (([selectedProduct.type isEqualToString:@"3_1"]) || ([selectedProduct.type isEqualToString:@"3_3a"]) ||  ([selectedProduct.type isEqualToString:@"3_3b"]) ||  ([selectedProduct.type isEqualToString:@"3_4"])) {
                
                
                return 44.0;
            }else {
                
                return 100.0;
            }
            
            
        }else
            return 10.0;
    } else {
        return 0;
    }
}


//-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
//{
//    if(tableView.tag == 9920) {
//        return [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
//    } else {
//        return [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
//        //return nil;
//    }
//}


-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[[UIView alloc]init]autorelease];
    UILabel *explorerLabel;
    NSString *selectedColor = COLOR_SKF_BLUE;
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    if(tableView.tag == 9920) {
        
        if (section == TBL_SEC_PRODUCT_DATA ){
            
            /**SKF Explorer Bearing */
            
            UIButton *productLinkButton = [[[UIButton alloc]initWithFrame:CGRectMake(10, 10, 300, 30)]autorelease];
            
            
            if ([selectedProduct.explorer isEqualToString:@"true"]) {
                
                explorerLabel = [[[UILabel alloc]initWithFrame:CGRectMake(10, 10, 280, 20)]autorelease];
                explorerLabel.userInteractionEnabled = false;
                explorerLabel.backgroundColor = [UIColor clearColor];
                explorerLabel.text =GetLocalizedString(@"Explorer_bearing");
                explorerLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_SUBTEXT2];
                explorerLabel.textAlignment = NSTextAlignmentLeft;
                explorerLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
                [footerView addSubview:explorerLabel];
                
                productLinkButton.frame = CGRectMake(10, explorerLabel.frame.size.height+explorerLabel.frame.origin.y, 300, 30);
                
            }
            
            /** View product details link */
            
            //            UIButton *productLinkButton = [[UIButton alloc]initWithFrame:CGRectMake(10, explorerLabel.frame.size.height+explorerLabel.frame.origin.y, 300, 44)];
            [productLinkButton setTitle:GetLocalizedString(@"View_product_details") forState:UIControlStateNormal];
            productLinkButton.titleLabel.textColor = [UIColor colorWithHexString:selectedColor];
            productLinkButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            productLinkButton.backgroundColor = [UIColor clearColor];
            [productLinkButton setTitleColor:[UIColor colorWithHexString:selectedColor] forState:UIControlStateNormal];
            [productLinkButton setTitleColor:[UIColor colorWithHexString:selectedColor] forState:(UIControlStateSelected | UIControlStateHighlighted)];
            productLinkButton.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:IPAD_FONT_SIZE_FOR_BEARING_VALUES];
            [productLinkButton addTarget:self
                                  action:@selector(productLinkButtonClicked:)
                        forControlEvents:UIControlEventTouchUpInside];
            [footerView addSubview:productLinkButton];
            
            /** Displaying image desclaimer text for Product image at result screen */
            
            NSLog(@"Select product details: %@, %@, %@",selectedProduct.type, selectedProduct.typeId, selectedProduct.typestext);
            
           // NSLog(@"Typeid: %@, %@, %@", calculationDataManager.selectedBearing.typeId, calculationDataManager.selectedBearing.typestext, calculationDataManager.selectedBearing.type);

            
            if ((![selectedProduct.type isEqualToString:@"3_1"]) && (![selectedProduct.type isEqualToString:@"3_3a"]) &&  (![selectedProduct.type isEqualToString:@"3_3b"]) &&  (![selectedProduct.type isEqualToString:@"3_4"])) {
                
                
                
                UIWebView *imageDesclaimerViewAtResult = [[[UIWebView alloc] initWithFrame:CGRectMake(10, productLinkButton.frame.size.height+productLinkButton.frame.origin.y, 270, 40)]autorelease];
                [imageDesclaimerViewAtResult loadHTMLString:[NSString stringWithFormat:@"%@%@", cssStringRed, [self fixBR:[calculationDataManager getTranslationString:@"ImageDisclaimer"]]] baseURL:[NSURL URLWithString:@""]];
                imageDesclaimerViewAtResult.scrollView.scrollEnabled = NO;
                [imageDesclaimerViewAtResult setBackgroundColor:[UIColor clearColor]];
                [imageDesclaimerViewAtResult setOpaque:NO];
                [footerView addSubview:imageDesclaimerViewAtResult];
                
                footerView.frame = CGRectMake(10, 10, 280, imageDesclaimerViewAtResult.frame.size.height+imageDesclaimerViewAtResult.frame.origin.y);
                [tableView.tableFooterView addSubview:footerView];
                
                
            }
            
            
        }
        
    }
    
    else {
        return [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
    }
    
    
    
    
    return footerView;
    
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 9920) {
        
        UIFont *myFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
       
        if( indexPath.row < resultArray.count && [[tableView.superview.layer valueForKey:@"isIntermediate" ]isEqualToString:@"YES"])//(intermediate_result || intermediate_result_composite))
        {
            ParameterResult *newresult = [resultArray objectAtIndex:indexPath.row];
            if (hideIntermediate && [newresult.description rangeOfString:@"rating life"].location == NSNotFound && [newresult.description rangeOfString:@"Frictional"].location == NSNotFound)
            {
                return 0.0;
            }
            
            //        if([newresult.presentation isEqualToString:@"G<sub>Nmax</sub>"])// &&  [newresult.parameterId isEqualToString:@"193"])
            //            return 100.0;
        }
        
        
                
        switch (indexPath.section) {
            case TBL_SEC_RESULTS:
            {
                /** MultipleCalc - for calc title */
                
                if (![[[[resultArray objectAtIndex:indexPath.row] class] description] isEqualToString:@"ParameterResult"]) {
                    
                    return 44.0;
                }
                
                ParameterResult *parameterResult = [resultArray objectAtIndex:indexPath.row];
                
                UIFont *nameFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
                UIFont *resultFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
                UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
                NSString *nameString;
                
                if (!parameterResult.uom)
                {
                    nameString = parameterResult.presentation;
                }
                else
                {
                    nameString = [NSString stringWithFormat:@"%@ [%@]", parameterResult.presentation, parameterResult.uom];
                }
                
                float nameHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:nameString] :nameFont :200 :NSLineBreakByWordWrapping];
                float resultHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:parameterResult.value] :resultFont :100 :NSLineBreakByWordWrapping];
                float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:parameterResult.description] :descriptionFont :210 :NSLineBreakByWordWrapping];
                
                //return MAX(nameHeight+descriptionHeight+25, resultHeight);
                
                ParameterResult *newresult = [resultArray objectAtIndex:indexPath.row];
                if([newresult.parameterId isEqualToString:whereInter] && [[tableView.superview.layer valueForKey:@"isIntermediate" ]isEqualToString:@"YES"] )
                    return MAX(nameHeight+descriptionHeight+45, resultHeight);
                else
                    return MAX(nameHeight+descriptionHeight+5, resultHeight);
                

                break;
                
            }
            case TBL_SEC_ERRORS:
            {
                Error *error = [errorsArray objectAtIndex:indexPath.row];
                float height = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:error.description]  :myFont :250 :NSLineBreakByWordWrapping];
                return height+10;
                break;
            }
            case TBL_SEC_WARNINGS:
            {
                Warning *warning = [warningsArray objectAtIndex:indexPath.row];
                float height = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:warning.description] :myFont :250 :NSLineBreakByWordWrapping];
                return height+10;
                break;
            }
            case TBL_SEC_MESSAGES:
            {
                Message *message = [messagesArray objectAtIndex:indexPath.row];
                float height = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:message.description] :myFont :250 :NSLineBreakByWordWrapping];
                return height+10;
                break;
            }
            case TBL_SEC_PRODUCT_DATA:
            {
                UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
                
                
                //CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
                //Product *product = [calculationDataManager selectedBearing];

                
                NSMutableString *databaseString = [NSMutableString stringWithFormat:@"%@ - ", calculatedProduct.designation];
                
                for (ParameterDatabase *paramDb in parameterDatabaseArray) {
                    
                   // NSLog(@"Product Data: %lu, %@, %@", (unsigned long)parameterDatabaseArray.count,parameterDatabaseArray.description,parameterDatabaseArray.debugDescription);
                    
                    if (paramDb.uom)
                    {
                        [databaseString appendString:[NSMutableString stringWithFormat:@"%@: %@ %@, ", paramDb.presentation, paramDb.value, paramDb.uom]];
                    }
                    else
                    {
                        [databaseString appendString:[NSMutableString stringWithFormat:@"%@: %@, ", paramDb.presentation, paramDb.value]];
                    }
                }
                
                
                /*for (Property *property in product.properties) {
                    if (property.uom)
                    {
                        [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@ %@, ", property.presentation, property.value, property.uom]];
                    }
                    else
                    {
                        [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@, ", property.presentation, property.value]];
                    }
                }*/
                
                if ([databaseString length] != 0)
                {
                    [databaseString deleteCharactersInRange:NSMakeRange([databaseString length]-2, 2)];
                }
                
                float height = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:databaseString] :descriptionFont :240 :NSLineBreakByWordWrapping];
                return height+20;
                break;
            }
            case TBL_SEC_INPUT_PARAMETERS:
            {
                UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:13];
                NSMutableString *availableString = [NSMutableString stringWithString:@""];
                
                for (ParameterAvailable *paramAvail in parameterAvailableArray) {
                    
                    if (![paramAvail.parameterAvailableId isEqualToString:@"-1"])
                    {
                        if (paramAvail.uom)
                        {
                            if (paramAvail.families)
                            {
                                NSString *optionName = nil;
                                for (Family *family in paramAvail.families) {
                                    for (Option *option in family.options) {
                                        if ([option.optionId isEqualToString:paramAvail.value]) {
                                            optionName = option.presentation;
                                        }
                                    }
                                }
                                [availableString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@ %@, ", paramAvail.presentation, optionName, paramAvail.uom]];
                            }
                            else
                            {
                                [availableString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@ %@, ", paramAvail.presentation, paramAvail.value, paramAvail.uom]];
                            }
                        }
                        else
                        {
                            if (paramAvail.families)
                            {
                                {
                                    NSString *optionName = nil;
                                    for (Family *family in paramAvail.families) {
                                        for (Option *option in family.options) {
                                            if ([option.optionId isEqualToString:paramAvail.value]) {
                                                optionName = option.presentation;
                                            }
                                        }
                                    }
                                    
                                    [availableString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@, ", paramAvail.presentation, optionName]];
                                }
                                
                            }
                            else
                            {
                                [availableString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@, ", paramAvail.presentation, paramAvail.value]];
                            }
                        }
                    }
                }
                
                if ([availableString length] != 0)
                {
                    [availableString deleteCharactersInRange:NSMakeRange([availableString length]-2, 2)];
                }
                
                float height = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:availableString] :descriptionFont :250 :NSLineBreakByWordWrapping];
                
                // adjusting height for input parameters as for 1 cal it was going out of the box and for multi cals some 20px more height/space was exist.
                CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
                
                if (calculationDataManager.selectedCalculationsArray.count == 1) {
                    
                    return height+25;
                    
                }else {
                    return height+0;
                }
                
                //return height+15;
                break;
            }
            default:
                return 44.0;
                break;
        }
        
    } else     {
        ParameterMissing *parameterMissing = [_calculationControl getVisibleParameterAtIndex:indexPath.row];
        
        
        UIFont *nameFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
        UIFont *resultFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_SUBTEXT];
        UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_SUBTEXT];
        NSString *nameString;
        
        if (!parameterMissing.uom)
        {
            nameString = parameterMissing.presentation;
        }
        else
        {
            nameString = [NSString stringWithFormat:@"%@ [%@]", parameterMissing.presentation, parameterMissing.uom];
        }
        
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        NSString *lastInput = [calculationDataManager getPreviousInput:parameterMissing.name];
        
        float optionHeight;
        if (parameterMissing.families)
        {
            optionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:lastInput] :resultFont :150 :NSLineBreakByWordWrapping];
        }
        else
        {
            optionHeight = 44;
        }
        
        NSString *descriptionString;
        if (![parameterMissing.presentation isEqualToString:parameterMissing.description])
        {
            descriptionString = parameterMissing.description;
        }
        else
        {
            descriptionString = @"";
        }
        
        float nameHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:nameString] :nameFont :200 :NSLineBreakByWordWrapping];
        float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:descriptionString] :descriptionFont :200 :NSLineBreakByWordWrapping];
        
        return MAX(nameHeight+descriptionHeight+5, optionHeight);
        
    }
    return 44.0;
}

-(void)didtaplabel:(UITapGestureRecognizer *) tapgesture
{
    
    //hideIntermediate = !hideIntermediate;
    
    if(what_plain_brearing == 1)
        intermediate_result = true;
    else if (what_plain_brearing == 2)
        intermediate_result_composite = true;
    UIView *myview = (UIView *)tapgesture.view.superview;
    //UIView *view = field;
    while (myview && ![myview isKindOfClass:[UITableView class]]){
        myview = myview.superview;
    }
    
 // [myview reloadData];
    NSMutableArray *tempresultarray = [myview.superview.layer valueForKey:@"result" ];
    
//    NSLog(@" array length ... %d",tempresultarray.count);
    for(int i = 0 ; i < tempresultarray.count ; i++)
    {
        ParameterResult *parameterResult = [tempresultarray objectAtIndex:i];
        if([parameterResult.description rangeOfString:@"Frictional"].location != NSNotFound || [parameterResult.description rangeOfString:@"rating life"].location != NSNotFound)
        {
//            NSLog(@" prest ... %@",parameterResult.presentation);
           [tempresultarray removeObjectAtIndex:i];
             i--;
        }
       
    }
 
    
    
    intermediatResultViewController_iPad = [[[IntermediatResultViewController_iPad alloc] initWithNibName:@"IntermediatResultViewController_iPad" bundle:[NSBundle mainBundle]] autorelease];
    reportsViewController.baseViewController = self;
    intermediatResultViewController_iPad.resultArray = tempresultarray;
    
UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:intermediatResultViewController_iPad];
    
    if (floor(NSFoundationVersionNumber)> NSFoundationVersionNumber_iOS_6_1) {
        navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:NAVIGATION_BACKGROUND_COLOR];
        navigationController.navigationBar.barStyle = UIBarStyleBlack;
        navigationController.navigationBar.translucent = NO;
    }
    
    intermediatResultPopover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
    intermediatResultPopover.popoverContentSize=CGSizeMake(320, 480);
    intermediatResultPopover.delegate = self;
    [intermediatResultPopover presentPopoverFromRect:doReportButton.frame inView:parametersViewContainer permittedArrowDirections:0 animated:YES];

    


    
    
    
   }


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView.tag == 9920) {
        switch (section)
        {
            case TBL_SEC_RESULTS:
                NSLog(@"%d", resultArray.count);
                return resultArray.count;
                break;
            case TBL_SEC_ERRORS:
                return [errorsArray count];
                break;
            case TBL_SEC_WARNINGS:
                return [warningsArray count];
                break;
            case TBL_SEC_MESSAGES:
                return [messagesArray count];
                break;
            case TBL_SEC_PRODUCT_DATA:
                return 1;
            case TBL_SEC_INPUT_PARAMETERS:
                return 1;
            default:
                break;
        }
        return 0;
        
    } else {
        [_calculationControl resetVisibleParameters];
        
        if (
            [[_calculationControl getVisibleParameters] count]>1) {
            inputParametersTableiPad.tableHeaderView.hidden = FALSE;
            
        }
        return [[_calculationControl getVisibleParameters] count];    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([tableView cellForRowAtIndexPath:indexPath].selectionStyle != UITableViewCellSelectionStyleNone) {
        
        
        if(keyboardIsShowing) {
            [activeField resignFirstResponder];
            keyboardIsShowing = NO;
        }
        
        keyboardIsShowingOption = keyboardIsShowing;
        
        ParameterMissing *parameterMissing = [_calculationControl getVisibleParameterAtIndex:indexPath.row];
        
        //parameterMissing = [_calculationControl getVisibleParameterAtIndex:indexPath.row];
        
        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
        //cell.backgroundView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"diagonal_stripe_background_tablecell.png"]];
        cell.backgroundView.backgroundColor = [UIColor colorWithHexString:STRIPES_BACKGROUND_COLOR];
        [_calculationControl resetMissingValueForParameterId:cell.tag];
        
        if ([parameterMissing.families count] == 1)
        {
            OptionSelectViewController_iPad *optionController = [[OptionSelectViewController_iPad alloc] initWithNibName:@"OptionSelectViewController" bundle:[NSBundle mainBundle]];
            
            optionController.optionsArray = parameterMissing.families;
            optionController.parameterName = parameterMissing.name;
            optionController.parameterPresentation = parameterMissing.presentation;
            optionController.parentIndexPath = indexPath;
            optionController.delegate = self;
            optionController.familyNr = 0;
            
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:optionController];
            optionSelectPopover = [[[UIPopoverController alloc] initWithContentViewController:navigationController] retain];
            
            CGRect rect = [tableView convertRect:[tableView rectForRowAtIndexPath:indexPath]
                                          toView:tableView];
            optionSelectPopover.popoverContentSize=CGSizeMake(320, 280);
            [optionSelectPopover presentPopoverFromRect:rect inView:tableView permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
        }
        else
        {
            FamilySelectViewController_iPad *optionController = [[FamilySelectViewController_iPad alloc] initWithNibName:@"OptionSelectViewController" bundle:[NSBundle mainBundle]];
            
            optionController.optionsArray = parameterMissing.families;
            optionController.parameterName = parameterMissing.name;
            optionController.parameterPresentation = parameterMissing.presentation;
            optionController.parentIndexPath = indexPath;
            optionController.delegate = self;
            
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:optionController];
            optionSelectPopover = [[[UIPopoverController alloc] initWithContentViewController:navigationController] retain];
            
            CGRect rect = [tableView convertRect:[tableView rectForRowAtIndexPath:indexPath]
                                          toView:tableView];
            optionSelectPopover.popoverContentSize=CGSizeMake(320, 280);
            [optionSelectPopover presentPopoverFromRect:rect inView:tableView permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
        }
        
    }
}

- (void)selectedOption:(NSString *)selectedOption withIndex:(NSInteger)selectedIndex andName:(NSString *)name andIndexPath:(NSIndexPath *)indexPath {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager setPreviousInput:name withValue:selectedOption];
    [_calculationControl setValue:selectedOption forName:name];
    
    [inputParametersTableiPad reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView.tag == 9920) {
        
        [self loadResultsForTag:tableView.superview.tag];
        
        /*** As in iOS 7 we are not reusing the cell, we are using single Cellindentifier ***/
        /*static NSString *CellIdentifier1 = @"Cell1";
         static NSString *CellIdentifier2 = @"Cell2";
         static NSString *CellIdentifier3 = @"Cell3";*/
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"%d_%d",indexPath.section,indexPath.row];
        
        UIWebView *nameWebView;
        UIWebView *descriptionLabel;
        UILabel *resultLabel;
        UIWebView *descriptionView;
        UILabel *textLabel;
        UIView *myview;
        UITableViewCell *cell;
        
        tableView.separatorColor = [UIColor clearColor];
        
        
        
        if (indexPath.section == TBL_SEC_RESULTS)
        {
            // Modified for MultipleCalc Result screen - Selected Calculation Title
            
            if (![[[[resultArray objectAtIndex:indexPath.row] class] description] isEqualToString:@"ParameterResult"]) {
               // CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
                // NSLog(@".........  ..%d"  ,calculationDataManager.selectedCalculationsArray.count);
                // cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil) {
                    
                    cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                    //cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier3] autorelease];
                    
                }
                
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                cell.textLabel.text = [resultArray objectAtIndex:indexPath.row];
                cell.textLabel.textColor = [UIColor colorWithHexString:COLOR_SKF_RED];
                cell.textLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:17];
                
                return cell;
            }
            
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            //cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
            
            
            if (cell == nil ) {
                
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                //cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] autorelease];
                
                nameWebView = [[UIWebView alloc] initWithFrame:CGRectMake(20, 5, 200, 50)];
                nameWebView.userInteractionEnabled = TRUE;
                nameWebView.backgroundColor = [UIColor clearColor];
                nameWebView.opaque = NO;
                nameWebView.delegate = self;
                nameWebView.dataDetectorTypes = UIDataDetectorTypeLink;
                nameWebView.tag = 10;
                [cell addSubview:nameWebView];
                [nameWebView release];
                
                descriptionLabel= [[UIWebView alloc] initWithFrame:CGRectMake(20, 30, 210, 12)];
                descriptionLabel.backgroundColor = [UIColor clearColor];
                descriptionLabel.userInteractionEnabled = false;
                descriptionLabel.opaque = NO;
                descriptionLabel.dataDetectorTypes = UIDataDetectorTypeNone;
                descriptionLabel.tag = 20;
                [cell addSubview:descriptionLabel];
                [descriptionLabel release];
                
                //                resultLabel = [[UILabel alloc] initWithFrame:CGRectMake(160, 7, 100, 32)];
                resultLabel = [[UILabel alloc] initWithFrame:CGRectMake(160, -7, 100, 32)];
                
                resultLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
                resultLabel.textAlignment = NSTextAlignmentRight;
                resultLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
                resultLabel.backgroundColor = [UIColor clearColor];
                resultLabel.tag = 100;
                [cell addSubview:resultLabel];
                [resultLabel release];
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                
                //cell.contentView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"diagonal_stripe_background_tablecell.png"]];
                cell.contentView.backgroundColor = [UIColor colorWithHexString:STRIPES_BACKGROUND_COLOR];
                cell.backgroundColor = [UIColor lightGrayColor];
            }
            
            nameWebView = (UIWebView *)[cell viewWithTag:10];
            descriptionLabel = (UIWebView *)[cell viewWithTag:20];
            resultLabel = (UILabel *)[cell viewWithTag:100];
            // Set up the cell...
            ParameterResult *parameterResult = [resultArray objectAtIndex:indexPath.row];
             //NSLog(@" %d.... val %@" , indexPath.row ,parameterResult);
            NSString *nameString;
            
            if (!parameterResult.uom)
            {
                nameString = parameterResult.presentation;
                [nameWebView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, parameterResult.presentation] baseURL:[NSURL URLWithString:@""]];
            }
            else
            {
                // NSLog(@" .... val %@  .. %@" , parameterResult.presentation, parameterResult.uom);
                nameString = [NSString stringWithFormat:@"%@ [%@]", parameterResult.presentation, parameterResult.uom];
                [nameWebView loadHTMLString:[NSString stringWithFormat:@"%@%@ [%@]", cssString, parameterResult.presentation, parameterResult.uom] baseURL:[NSURL URLWithString:@""]];
            }
            
            [descriptionLabel loadHTMLString:[NSString stringWithFormat:@"%@%@", cssStringGray, parameterResult.description] baseURL:[NSURL URLWithString:@""]];
            
            resultLabel.text = parameterResult.value;
            
            UIFont *nameFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
            UIFont *resultFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
            UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
            
            float nameHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:nameString] :nameFont :200 :NSLineBreakByWordWrapping];
            float resultHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:parameterResult.value] :resultFont :100 :NSLineBreakByWordWrapping];
            float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:parameterResult.description] :descriptionFont :210 :NSLineBreakByWordWrapping];
            
            float totalHeight = MAX(nameHeight+descriptionHeight+5, resultHeight);
            
            descriptionLabel.frame = CGRectMake(descriptionLabel.frame.origin.x, totalHeight-descriptionHeight-2, descriptionLabel.frame.size.width, descriptionHeight);
            resultLabel.frame = CGRectMake(resultLabel.frame.origin.x, resultLabel.frame.origin.y, resultLabel.frame.size.width, resultHeight+10);
            
            
            CGFloat height = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if(height == 0) // hidden cells for intermediate
            {
                cell.hidden = YES;
                return cell;
            }

            
            
            for (UIView* tempView in cell.contentView.subviews) {
                if(tempView.tag == 555)
                {
                    [tempView removeFromSuperview];
                }
            }
            
            if(indexPath.row < resultArray.count && indexPath.row > 0)// && what_plain_brearing == 1)
            {
                ParameterResult *newresult = [resultArray objectAtIndex:indexPath.row];
//                UILabel *textLabel_intr;
//                UIView *myview_intr;
//                UIImageView *imgView_intr;
//                UIImageView *imgView_tintr;

                
//                NSLog(@".....paramid... %@", newresult.parameterId);
                if([newresult.parameterId isEqualToString:whereInter] )
                {
                    intermediate_result = true;
                }
                
                if ( [[tableView.superview.layer valueForKey:@"isIntermediate" ]isEqualToString:@"YES"]  && [newresult.parameterId isEqualToString:whereInter]  /*&& [newresult.presentation isEqualToString:@"G<sub>Nmax</sub>"]*/)
                {
                    
                    
                         UILabel *textLabel_intr = [[[UILabel alloc]initWithFrame:CGRectMake(10, 10, 300, 25)]autorelease];
                        textLabel_intr.font = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_TABLE];
                        textLabel_intr.textAlignment = NSTextAlignmentLeft;
                        textLabel_intr.textColor = [UIColor colorWithHexString:COLOR_HYPERLINK];
                        textLabel_intr.backgroundColor = [UIColor clearColor];
                        textLabel_intr.tag = 155;
                    
                    
                    textLabel_intr.userInteractionEnabled = YES;
                    //   textLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"red_disclosure_indicator.png"]];
                    
                    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
                    textLabel_intr.attributedText = [[NSAttributedString alloc] initWithString:@"Intermediate Results"
                                                                             attributes:underlineAttribute];
                    
                    //textLabel_intr.text = @"Intermediate Results";
                    
                        UIView *myview_intr = [[[UIView alloc] initWithFrame:CGRectMake(-1, 60, 600, 50)]autorelease];
                        myview_intr.tag = 555;
                        myview_intr.layer.borderWidth = 0.0f;
                        myview_intr.layer.borderColor = [UIColor whiteColor].CGColor;
                    
                    [myview_intr addSubview:textLabel_intr];
                    
                    UITapGestureRecognizer *tapgesture  = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                                  action:@selector(didtaplabel:)];
                    [myview_intr addGestureRecognizer:tapgesture];
                    [tapgesture release];
                    
                    
               /*
                     NSInteger (^Tapped)() = ^NSInteger(){
                        hideIntermediate = !hideIntermediate;
                        
                        if(what_plain_brearing == 1)
                            intermediate_result = true;
                        else if (what_plain_brearing == 2)
                            intermediate_result_composite = true;
                        
                        [resultsTable reloadData];
                        return 0;
                    };
                    NSInteger myval = 0;
                    
                    UITapGestureRecognizer *tapgesture  = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                                action: @selector( Tapped)];
                    
                    
                    
                    
                
                
                    NSInteger (^mathOperation)() = ^NSInteger(){
                        
                        return 0;
                    };
                    
                    NSInteger sum = Tapped();//mathOperation();
                    */
                    
                       /* UIImageView *imgView_intr = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"red_disclosure_indicator.png"]]autorelease];
                        imgView_intr.frame = CGRectMake(200, 6, 8 , 12);
                    
                    
                    
                        UIImageView *imgView_tintr = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"red_disclosure_indicator.png"]]autorelease];
                        imgView_tintr.frame = CGRectMake(200, 6, 8 , 12);
                        CGAffineTransform transform;
                        transform = CGAffineTransformRotate(imgView_tintr.transform, 33);
                        imgView_tintr.transform = transform;
                        
                    
                    
                    if(!hideIntermediate)
                    {
                        [myview_intr addSubview:imgView_tintr];
                        [imgView_intr removeFromSuperview];
                    }
                    else
                    {
                        [myview_intr addSubview:imgView_intr];
                        [imgView_tintr removeFromSuperview];
                    }*/
                    
                    [cell.contentView addSubview:myview_intr];
                    intermediate_result = false;
                    
                    
                    /*if(textLabel_intr != nil)
                    textLabel_intr = nil;
                    if(myview_intr != nil)
                    myview_intr = nil;
                     if(imgView_intr != nil)
                    imgView_intr = nil;
                     if(imgView_tintr != nil)
                    imgView_tintr   = nil;*/
                }
            }
           // else if(indexPath.row < resultArray.count && indexPath.row > 0 && what_plain_brearing == 2)
//            {
//                ParameterResult *newresult = [resultArray objectAtIndex:indexPath.row];
//                UILabel *textLabel_intr;
//                UIView *myview_intr;
//                UIImageView *imgView_intr;
//                UIImageView *imgView_tintr;
//                //first ele after all rating life param
//                //Max. basic rating life for regular relubrication
//                //Ratio of the axial to the radial load
//                          //  NSLog(@".....paramid... %@", newresult.parameterId);
//                
//                if([newresult.parameterId isEqualToString:whereInter]  )
//                {
//                    intermediate_result_composite = true;
//                }
//                
//                if ( intermediate_result_composite  && [newresult.parameterId isEqualToString:whereInter]/* && [newresult.presentation isEqualToString:@"G<sub>h</sub>"]*/)
//                {
//                    NSLog(@".....%d ... ",indexPath.row);
//                    
//                    
//                    
//                        UIView *myview_intr = [[[UIView alloc] initWithFrame:CGRectMake(-1, 60, 600, 83)]autorelease];
//                        myview_intr.tag = 555;
//                        myview_intr.layer.borderWidth = 0.5f;
//                        myview_intr.layer.borderColor = [UIColor grayColor].CGColor;
//                    
//                    
//                    
//                        UILabel *textLabel_intr = [[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 300, 25)]autorelease];
//                        textLabel_intr.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
//                        textLabel_intr.textAlignment = UITextAlignmentLeft;
//                        textLabel_intr.textColor = [UIColor colorWithHexString:COLOR_SKF_RED];
//                        textLabel_intr.backgroundColor = [UIColor clearColor];
//                        textLabel_intr.tag = 155;
//                        
//                    
//                    textLabel_intr.userInteractionEnabled = YES;
//                    textLabel_intr.text = @"Intermediate Results";
//                    [myview_intr addSubview:textLabel_intr];
//                    UITapGestureRecognizer *tapgesture  = [[UITapGestureRecognizer alloc] initWithTarget:self
//                                                                                                  action:@selector(didtaplabel:)];
//                    
//                    tapgesture.numberOfTapsRequired = 1;
//                    
//                    [textLabel_intr addGestureRecognizer:tapgesture];
//                    
//                    
//                    [tapgesture release];
//                    
//                    
//                        UIImageView *imgView_intr = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"red_disclosure_indicator.png"]]autorelease];
//                        imgView_intr.frame = CGRectMake(200, 6, 8 , 12);
//                    
//                    
//                    
//                        UIImageView *imgView_tintr = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"red_disclosure_indicator.png"]]autorelease];
//                        imgView_tintr.frame = CGRectMake(200, 6, 8 , 12);
//                        CGAffineTransform transform;
//                        transform = CGAffineTransformRotate(imgView_tintr.transform, 33);
//                        imgView_tintr.transform = transform;
//                        
//                    
//                    
//                    if(!hideIntermediate)
//                    {
//                        [myview_intr addSubview:imgView_tintr];
//                        [imgView_intr removeFromSuperview];
//                    }
//                    else
//                    {
//                        [myview_intr addSubview:imgView_intr];
//                        [imgView_tintr removeFromSuperview];
//                    }
//                    
//                    [cell.contentView addSubview:myview_intr];
//                    
//                    //intermediate_result_composite = false;
//                }
//            }
            
            
            

            
        }
        else
        {
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            //cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
            if (cell == nil) {
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                //cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                descriptionView = [[UIWebView alloc] initWithFrame:CGRectMake(20, 2, 250, 80)];
                //descriptionView.userInteractionEnabled = false;
                descriptionView.backgroundColor = [UIColor clearColor];
                descriptionView.opaque = NO;
                descriptionView.delegate = self;
                descriptionView.dataDetectorTypes = UIDataDetectorTypeLink;
                descriptionView.tag = 30;
                [cell addSubview:descriptionView];
                [descriptionView release];
                //cell.contentView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"diagonal_stripe_background_tablecell.png"]];
                cell.contentView.backgroundColor = [UIColor colorWithHexString:STRIPES_BACKGROUND_COLOR];
                cell.backgroundColor = [UIColor lightGrayColor];
                
            }
            
            descriptionView = (UIWebView *)[cell viewWithTag:30];
            
            // Set up the cell...
            
            switch (indexPath.section) {
                case TBL_SEC_ERRORS:
                {
                   // NSLog(@"Error count:%d", errorsArray.count);
                    Error *error = [errorsArray objectAtIndex:indexPath.row];
                    [descriptionView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, error.description] baseURL:[NSURL URLWithString:@""]];
                    
                    UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
                    float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:error.description] :descriptionFont :250 :NSLineBreakByWordWrapping];
                    descriptionView.frame = CGRectMake(descriptionView.frame.origin.x, descriptionView.frame.origin.y, descriptionView.frame.size.width, descriptionHeight+10);
                    break;
                }
                case TBL_SEC_WARNINGS:
                {
                    //NSLog(@"Warning count:%d", warningsArray.count);
                    
                    Warning *warning = [warningsArray objectAtIndex:indexPath.row];
                    [descriptionView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, warning.description] baseURL:[NSURL URLWithString:@""]];
                    
                    UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
                    float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:warning.description] :descriptionFont :300 :NSLineBreakByWordWrapping];
                    descriptionView.frame = CGRectMake(descriptionView.frame.origin.x, descriptionView.frame.origin.y, descriptionView.frame.size.width, descriptionHeight+10);
                    break;
                }
                case TBL_SEC_MESSAGES:
                {
                    Message *message = [messagesArray objectAtIndex:indexPath.row];
                    [descriptionView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, message.description] baseURL:[NSURL URLWithString:@""]];
                    
                    UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
                    float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:message.description] :descriptionFont :250 :NSLineBreakByWordWrapping];
                    descriptionView.frame = CGRectMake(descriptionView.frame.origin.x, descriptionView.frame.origin.y, descriptionView.frame.size.width, descriptionHeight+10);
                    break;
                }
                case TBL_SEC_PRODUCT_DATA:
                {
                    
                    //NSMutableString *databaseString = [NSMutableString stringWithFormat:@"%@ - <b>Type</b>: ", calculatedProduct.designation];
                    
                    NSMutableString *databaseString;
                    
                    if ([calculatedProduct.explorer isEqualToString:@"true"]) {
                        
                        databaseString = [NSMutableString stringWithFormat:@"%@* - <b>Type</b>: ", calculatedProduct.designation];
                        
                    }else
                        
                        databaseString = [NSMutableString stringWithFormat:@"%@ - <b>Type</b>: ", calculatedProduct.designation];
                    
                    [databaseString appendString:[NSMutableString stringWithFormat:@"%@, ", calculatedProduct.typestext ]];
                    
                    
                    // To remove Kr and dm from the bearing data section for minimum load calculation.
                    /*for (ParameterDatabase *paramDb in parameterDatabaseArray) {
                        
                        if (paramDb.uom) {
                            [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@ %@, ", paramDb.presentation, paramDb.value, paramDb.uom]];
                        } else {
                            
                            [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@, ", paramDb.presentation, paramDb.value]];
                        }
                    }*/
                    
                    // To avoid (null) values like K1, d2max, c,...

                    /*for (ParameterDatabase *paramDb in parameterDatabaseArray) {
                        
                        if (paramDb.uom) {
                            
                            if ([paramDb.parameterDatabaseId isEqualToString:@"60"] || [paramDb.parameterDatabaseId isEqualToString:@"119"]) {
                                
                                [databaseString appendString:@""];
                                
                            }else{
                                [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@ %@, ", paramDb.presentation, paramDb.value, paramDb.uom]];
                            }
                        } else {
                            
                            if ([paramDb.parameterDatabaseId isEqualToString:@"87"]) {
                                
                                [databaseString appendString:@""];
                                
                            }else{
                             
                                [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@, ", paramDb.presentation, paramDb.value]];
                            }
                        }
                    }*/
                    
                    
                    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
                    Product *product = [calculationDataManager selectedBearing];
                    

                    for (Property *property in product.properties) {
                        if (property.uom)
                        {
                            [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@ %@, ", property.presentation, property.value, property.uom]];
                        }
                        else
                        {
                            [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@, ", property.presentation, property.value]];
                        }
                    }

                    if ([databaseString length] != 0)
                    {
                        [databaseString deleteCharactersInRange:NSMakeRange([databaseString length]-2, 2)];
                        
                        [descriptionView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, databaseString] baseURL:[NSURL URLWithString:@""]];
                        UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
                        float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:databaseString] :descriptionFont :250 :NSLineBreakByWordWrapping];
                        descriptionView.frame = CGRectMake(descriptionView.frame.origin.x, descriptionView.frame.origin.y, descriptionView.frame.size.width, descriptionHeight+15);
                    }
                    break;
                }
                case TBL_SEC_INPUT_PARAMETERS:
                {
                    NSMutableString *availableString = [NSMutableString stringWithString:@""];
                    
                    for (ParameterAvailable *paramAvail in parameterAvailableArray) {
                        
                        if (![paramAvail.parameterAvailableId isEqualToString:@"-1"])
                        {
                            if (paramAvail.uom)
                            {
                                if (paramAvail.families)
                                {
                                    NSString *optionName = nil;
                                    for (Family *family in paramAvail.families) {
                                        for (Option *option in family.options) {
                                            if ([option.optionId isEqualToString:paramAvail.value]) {
                                                optionName = option.presentation;
                                            }
                                        }
                                    }
                                    
                                    [availableString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@ %@, ", paramAvail.presentation, optionName, paramAvail.uom]];
                                }
                                else
                                {
                                    [availableString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@ %@, ", paramAvail.presentation, paramAvail.value, paramAvail.uom]];
                                }
                            }
                            else
                            {
                                if (paramAvail.families)
                                {
                                    {
                                        NSString *optionName = nil;
                                        for (Family *family in paramAvail.families) {
                                            for (Option *option in family.options) {
                                                if ([option.optionId isEqualToString:paramAvail.value]) {
                                                    optionName = option.presentation;
                                                }
                                            }
                                        }
                                        
                                        if ([paramAvail.parameterAvailableId isEqualToString:@"165"]) {
                                            
                                            // Removing input param for NRB minimum load for multiple calculation
                                            CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
                                            
                                            if (calculationDataManager.selectedCalculationsArray.count >1) {
                                                
                                                [availableString appendString:[NSMutableString stringWithFormat:@""]];
                                                
                                            }else {
                                                
                                                [availableString appendString:[NSMutableString stringWithFormat:@"%@", paramAvail.description]];
                                            }
                                            
                                        }else {
                                            
                                            [availableString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@, ", paramAvail.presentation, optionName]];
                                        }
                                    }
                                }
                                else
                                {
                                    [availableString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@, ", paramAvail.presentation, paramAvail.value]];
                                }
                            }
                        }
                    }
                    if ([availableString length] != 0)
                    {
                        // Commented below code to change the text loa TO load for minimum load cals in input param section
                        [availableString deleteCharactersInRange:NSMakeRange([availableString length]-2, 2)];
                        
                        [descriptionView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, availableString] baseURL:[NSURL URLWithString:@""]];
                        
                        UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:13];
                        float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:availableString] :descriptionFont :250 :NSLineBreakByWordWrapping];
                        descriptionView.frame = CGRectMake(descriptionView.frame.origin.x, descriptionView.frame.origin.y, descriptionView.frame.size.width, descriptionHeight+0);
                    }
                    
                    
                    break;
                }
                    
                default:
                    break;
            }
            
        }
        return cell;
        
    } else {
        
        
        /***As in we are not supposed to re-use same cellidentifier for creating cell. ***/
        //static NSString *CellIdentifierInput = @"CellInput";
        NSString *CellIdentifier = [NSString stringWithFormat:@"%d_%d",indexPath.section,indexPath.row];
        
        UIWebView *nameWebView;
        UIWebView *descriptionLabel;
        UITextField *parameterField;
        //UISwitch* parameterSwitch;
        UIWebView *parameterLabel;
        UIView* separatorView;
        
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        ParameterMissing *parameterMissing = [_calculationControl getVisibleParameterAtIndex:indexPath.row];
        
        //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierInput];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            
            //cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierInput] autorelease];
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            
            nameWebView = [[UIWebView alloc] initWithFrame:CGRectMake(15, 3, 250, 80)];
            nameWebView.userInteractionEnabled = TRUE;
            nameWebView.tag = TAG_CELL_NAME_LBL;
            nameWebView.backgroundColor = [UIColor clearColor];
            nameWebView.opaque = NO;
            nameWebView.delegate = self;
            nameWebView.dataDetectorTypes = UIDataDetectorTypeLink;
            
            nameWebView.scrollView.scrollEnabled = NO;
            nameWebView.scrollView.bounces = NO;
            [cell addSubview:nameWebView];
            [nameWebView release];
            
            descriptionLabel= [[UIWebView alloc] initWithFrame:CGRectMake(15, 25, 200, 12)];
            descriptionLabel.userInteractionEnabled = false;
            descriptionLabel.backgroundColor = [UIColor clearColor];
            descriptionLabel.opaque = NO;
            descriptionLabel.dataDetectorTypes = UIDataDetectorTypeNone;
            descriptionLabel.tag = TAG_CELL_DESCR_LBL;
            [cell addSubview:descriptionLabel];
            
            [descriptionLabel release];
            
            parameterField = [[UITextField alloc] initWithFrame:CGRectMake(400, 7, 100, 32)];
            parameterField.borderStyle = UITextBorderStyleRoundedRect;
            parameterField.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
            parameterField.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_INPUT];
            parameterField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            parameterField.autocorrectionType = UITextAutocorrectionTypeNo;
            parameterField.keyboardType = UIKeyboardTypeNumberPad;
            parameterField.clearButtonMode = UITextFieldViewModeWhileEditing;
            
            parameterField.returnKeyType = UIReturnKeyDone;
            parameterField.inputAccessoryView = keyboardToolBar;
            parameterField.textAlignment = NSTextAlignmentRight;
            parameterField.tag = TAG_CELL_PARAM_FIELD;
            parameterField.delegate = self;
            parameterField.backgroundColor = [UIColor whiteColor];
           [cell addSubview:parameterField];
            
            // For iOS 7 use below code for crashing issue while entering data in text field
           /* if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
                
                [cell.contentView addSubview:parameterField];
            }else {
                
                [cell addSubview:parameterField];
            }*/
            
            [parameterField release];
            
            parameterSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(400, 15, 100, 32)];
            parameterSwitch.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            parameterSwitch.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            parameterSwitch.backgroundColor = [UIColor clearColor];
            parameterSwitch.tag = TAG_CELL_PARAM_SWITCH;
            [cell addSubview:parameterSwitch];
            [parameterSwitch release];
            [parameterSwitch addTarget:self action:@selector(switchDidChangeValue:) forControlEvents:UIControlEventValueChanged];
            
            parameterLabel = [[UIWebView alloc] initWithFrame:CGRectMake(400, 7, 150, 50)];
            parameterLabel.backgroundColor = [UIColor clearColor];
            parameterLabel.userInteractionEnabled = false;
            parameterLabel.tag = TAG_CELL_PARAM_LBL;
            parameterLabel.opaque = NO;
            parameterLabel.dataDetectorTypes = UIDataDetectorTypeNone;
           // parameterLabel.hidden = true;
            [cell addSubview:parameterLabel];
            [parameterLabel release];
            
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.contentView.layer.cornerRadius = 0;
            cell.backgroundView = [[[UIView alloc] initWithFrame:CGRectOffset(cell.bounds,0 ,-1)]autorelease];
            
            UIView* separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.bounds.size.height - 1, cell.bounds.size.width, 1)];
            separatorView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
            separatorView.backgroundColor = [UIColor lightGrayColor];
            separatorView.tag = TAG_CELL_SEPARATOR_VIEW;
            [cell addSubview:separatorView];
            
            
            
            /*** For Frequency & Time conversion **/
            
           /* if ([parameterMissing.parameterId isEqualToString:@"207"]) {
                
                if (textFieldFrequencyiPad == nil) {
                    textFieldFrequencyiPad = parameterField;
                }
            }else if ([parameterMissing.parameterId isEqualToString:@"226"]) {
                if (textFieldTimeiPad == nil) {
                    textFieldTimeiPad = parameterField;
                }
            }*/
            /*** **/
            
            
            
            if ([parameterMissing.parameterId isEqualToString:@"20"] || [parameterMissing.parameterId isEqualToString:@"21"] || [parameterMissing.parameterId isEqualToString:@"35"]) {
                
                parameterField.placeholder = @" ";
                
            }
            

        }
        
       
        // To check relubrication calcualtion exist
        /*        bool relubCalc_exist_iPad = false;
         if (calculationDataManager.mutlipleCalculation) {
         
         
         for (Calculation *calculation in calculationDataManager.selectedCalculationsArray) {
         if ([calculation.calculationId isEqualToString:@"14"]) {
         relubCalc_exist_iPad = true;
         break;
         }
         }
         }*/
        

        
/*** setting Lubrication Cleanness & lubrication - Oil & Grease **/
        
        NSLog(@"Parameter Id: %@", parameterMissing.parameterId);
        NSLog(calculationDataManager.isRelubricationCalcExist ? @"Yes" : @"No");
        
        
        NSString *lastInput = [calculationDataManager getPreviousInput:parameterMissing.name];
        
        // To check Lubrication cleanness type selected
        if ([parameterMissing.parameterId isEqualToString:@"62"]) {
                    if ([lastInput.description rangeOfString:@"ISO 4406"].location != NSNotFound) {
                lubClean_oil_iPad = true;
                lubClean_grease_iPad = false;
            }else {
                lubClean_grease_iPad = true;
                lubClean_oil_iPad = false;
            }
        }
        
        
        
        if (([parameterMissing.parameterId isEqualToString:@"96"] || [parameterMissing.parameterId isEqualToString:@"227"] || [parameterMissing.parameterId isEqualToString:@"278"]) && (calculationDataManager.mutlipleCalculation)) {
            if ((!calculationDataManager.isRelubricationCalcExist) && (lubClean_oil_iPad) ) {
                if ([lastInput.description isEqualToString:@"Grease"]) {
                    lastInput = nil;
                    [calculationDataManager setPreviousInput:parameterMissing.name withValue:@""];
                    [_calculationControl setValue:@"" forName:parameterMissing.name];
                }
            }
            
            if ((!calculationDataManager.isRelubricationCalcExist) && (lubClean_grease_iPad) && (calculationDataManager.mutlipleCalculation)){
                if (![lastInput.description isEqualToString:@"Grease"]) {
                    lastInput = nil;
                    [calculationDataManager setPreviousInput:parameterMissing.name withValue:@""];
                    [_calculationControl setValue:@"" forName:parameterMissing.name];
                }
            }
            
        }
        
        
        // To remove oil option for Relubrication calculation
        if ((calculationDataManager.isRelubricationCalcExist)) {
            if ([parameterMissing.parameterId isEqualToString:@"62"]) {
                
                if ([lastInput.description rangeOfString:@"ISO 4406"].location != NSNotFound) {
                    lastInput = nil;
                    [calculationDataManager setPreviousInput:parameterMissing.name withValue:@""];
                    [_calculationControl setValue:@"" forName:parameterMissing.name];
                }
                
            }
            
            if ([parameterMissing.parameterId isEqualToString:@"96"] || [parameterMissing.parameterId isEqualToString:@"227"] || [parameterMissing.parameterId isEqualToString:@"278"]) {
                if (![lastInput.description isEqualToString:@"Grease"]) {
                    lastInput = nil;
                    [calculationDataManager setPreviousInput:parameterMissing.name withValue:@""];
                    [_calculationControl setValue:@"" forName:parameterMissing.name];
                }
            }
        }
        

        
        // End
        
        
        
        
        nameWebView = (UIWebView *)[cell viewWithTag:TAG_CELL_NAME_LBL];
        descriptionLabel = (UIWebView *)[cell viewWithTag:TAG_CELL_DESCR_LBL];
        parameterField = (UITextField *)[cell viewWithTag:TAG_CELL_PARAM_FIELD];
        parameterLabel = (UIWebView *)[cell viewWithTag:TAG_CELL_PARAM_LBL];
        parameterSwitch = (UISwitch *)[cell viewWithTag:TAG_CELL_PARAM_SWITCH];
        separatorView = [cell viewWithTag:TAG_CELL_SEPARATOR_VIEW];
        
        separatorView.hidden = indexPath.row == [tableView.dataSource tableView:tableView numberOfRowsInSection:indexPath.section] - 1;
        
//       ParameterMissing *parameterMissing = [_calculationControl getVisibleParameterAtIndex:indexPath.row];
        
        if([parameterMissing.parameterId isEqualToString:@"165"]) {
            
            parameterYbearingMinLoad = [_calculationControl getVisibleParameterAtIndex:indexPath.row];
            
        } else if ([parameterMissing.parameterId isEqualToString:@"96"]) {
            
            parameterOilJet = [_calculationControl getVisibleParameterAtIndex:indexPath.row];
        }
        
        
        //NSString* currentValue = [_calculationControl valueForId:parameterMissing.parameterId];
        
        BOOL typeIsCheckBox = [parameterMissing.dataType isEqualToString:@"0"];
        BOOL typeIsOptions = !typeIsCheckBox && parameterMissing.families;
        BOOL cellIsMissingInputValue = [_calculationControl isParameterIdStrMissingValue:parameterMissing.parameterId];
        BOOL cellHasInvalidMissingInputValue = [_calculationControl hasParameterIdStrInvalidValue:parameterMissing.parameterId];
        
        //if (currentValue !=nil)
               
        NSString* currentValue = [_calculationControl valueForId:parameterMissing.parameterId];
        if (currentValue !=nil /*&& ![parameterMissing.parameterId isEqualToString:@"226"] && ![parameterMissing.parameterId isEqualToString:@"207"]*/) {
            parameterSwitch.on = [currentValue isEqualToString:@"true"];
            [parameterLabel loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, currentValue] baseURL:[NSURL URLWithString:@""]];
            parameterField.text = currentValue;
        } else /*if(![parameterMissing.parameterId isEqualToString:@"226"] && ![parameterMissing.parameterId isEqualToString:@"207"])*/{
            parameterSwitch.on = FALSE;
            [parameterLabel loadHTMLString:[NSString stringWithFormat:@"%@", cssString] baseURL:[NSURL URLWithString:@""]];
            //parameterField.text = @"";
            
            if ([parameterMissing.parameterId isEqualToString:@"97"]) {
                
                parameterField.text = @"0.0";
            }
            else
            {
                parameterField.text = @"";
            }

        }
        
        nameWebView.hidden = false;
    
        NSString *nameString;
        
        if (!parameterMissing.uom)
        {
            nameString = parameterMissing.presentation;
            [nameWebView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, parameterMissing.presentation] baseURL:[NSURL URLWithString:@""]];
        }
        else
        {
            nameString = [NSString stringWithFormat:@"%@ [%@]", parameterMissing.presentation, parameterMissing.uom];
            [nameWebView loadHTMLString:[NSString stringWithFormat:@"%@%@ [%@]", cssString, parameterMissing.presentation, parameterMissing.uom] baseURL:[NSURL URLWithString:@""]];
        }
        
        if (![parameterMissing.presentation isEqualToString:parameterMissing.description])
        {
            if ([parameterMissing.description length] == 0) {
                
                descriptionLabel.hidden = true;
                
            }else {
                
                descriptionLabel.hidden = false;
                
                [descriptionLabel loadHTMLString:[NSString stringWithFormat:@"%@%@", cssStringGray, parameterMissing.description] baseURL:[NSURL URLWithString:@""]];
                
            }
            //            [descriptionLabel loadHTMLString:[NSString stringWithFormat:@"%@%@", cssStringGray, parameterMissing.description] baseURL:[NSURL URLWithString:@""]];
            
        } else {
            
            [descriptionLabel loadHTMLString:[NSString stringWithFormat:@"%@%@", cssStringGray, @""] baseURL:[NSURL URLWithString:@""]];
        }
        
        if (typeIsCheckBox)
        {
            parameterSwitch.hidden = false;
            parameterField.hidden = true;
            parameterLabel.hidden = true;
            [cell setUserInteractionEnabled:YES];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        else if (typeIsOptions)
        {
            
            if ([parameterMissing.parameterId isEqualToString:@"165"]) {
                
                if ([nameString isEqualToString:@"Do you wish to continue?"]) {
                    
                    nameWebView.hidden = true;
                }
                parameterField.hidden = true;
                parameterSwitch.hidden = true;
                [cell setUserInteractionEnabled:NO];
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                
            }else {
                
                parameterSwitch.hidden = true;
                parameterField.hidden = true;
                parameterLabel.hidden = false;
                [cell setUserInteractionEnabled:YES];
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                
                [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
            }
        }
        else
        {
            parameterSwitch.hidden = true;
            parameterField.hidden = false;
            parameterLabel.hidden = true;
            [cell setUserInteractionEnabled:YES];
            
            if ([indexPathsForTextFields indexOfObject:indexPath] == NSNotFound) {
                
                [indexPathsForTextFields addObject:indexPath];
            }
            
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
        cell.tag = [parameterMissing.parameterId intValue];
        
        UIFont *nameFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
        UIFont *optionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_SUBTEXT];
        UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_SUBTEXT];
        
        float nameHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:nameString] :nameFont :200 :NSLineBreakByWordWrapping];
        float optionHeight = 44;
        BOOL optionHasValue = parameterField.text != nil && ![parameterField.text  isEqualToString:@""];
        if (typeIsOptions && optionHasValue)
        {
            optionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:parameterField.text] :optionFont :parameterLabel.frame.size.width :NSLineBreakByWordWrapping];
        }
        
        NSString *descriptionString;
        if (![parameterMissing.presentation isEqualToString:parameterMissing.description])
        {
            descriptionString = parameterMissing.description;
        }
        else {
            
            descriptionString = @"";
        }
        
        float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:descriptionString] :descriptionFont :200 :NSLineBreakByWordWrapping];
        
        float totalHeight = MAX(nameHeight+descriptionHeight+5, optionHeight);
        
        descriptionLabel.frame = CGRectMake(descriptionLabel.frame.origin.x, totalHeight-descriptionHeight-2, descriptionLabel.frame.size.width, descriptionHeight);
        parameterLabel.frame = CGRectMake(parameterLabel.frame.origin.x, parameterLabel.frame.origin.y, parameterLabel.frame.size.width, optionHeight);
        parameterLabel.center = CGPointMake(parameterLabel.center.x, totalHeight/2);
        parameterField.center = CGPointMake(450, totalHeight/2);
        
        if (cellIsMissingInputValue || cellHasInvalidMissingInputValue) {
            [cell.backgroundView setBackgroundColor:[UIColor colorWithHexString:@"fff8e6"]];
        } else {
            //cell.backgroundView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"diagonal_stripe_background_tablecell.png"]];
            cell.backgroundView.backgroundColor = [UIColor colorWithHexString:STRIPES_BACKGROUND_COLOR];
        }
        cell.backgroundColor = [UIColor lightGrayColor];
        return cell;
    }
    
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    activeField = nil;
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    activeField = nil;
    return YES;
}


/* ORIENTATION RESET */
- (void)willAnimateRotationToInterfaceOrientation: (UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
        
        [self resetInputTable];
        
        // To remove add calculation popover while rotating - By BNP
        if ([self.addCalculationPopover isPopoverVisible]) {
            
            [self.addCalculationPopover dismissPopoverAnimated:YES];
        }
        
        
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        selectedProduct = calculationDataManager.selectedBearing;
        
        if (selectedProduct != NULL) {
            
            [[bearingViewContainer subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
            
            [self paintBearingParams:[selectedProduct.properties count]];
        }
        
       
        
        if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {
            
            backgroundImage.frame = CGRectMake(0, 0, 768, 1024);
            
        }else {
            
            backgroundImage.frame = CGRectMake(0, 20, 768, 1024);
        }

        
        //backgroundImage.image = [UIImage imageNamed:@"main_bg_ipad.png"];
        
        if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {

            infoButton.frame = CGRectMake(20, 9, 22, 22);
            logoImageView.frame = CGRectMake(700, 9, 50, 25);


            
        }else {

            infoButton.frame = CGRectMake(20, 29, 22, 22);
            logoImageView.frame = CGRectMake(700, 29, 50, 25);


        }

        
        NSMutableArray *calculationResultViews = [[NSMutableArray alloc] init];
        
        for(UIView *view in [resultsContainerScrollView subviews]) {
            if (view.tag > 999) {
                [calculationResultViews addObject:view];
            }
        }
        
        int counter = 1;
        int currPos = 0;
        for(int i = [calculationResultViews count]-1; i>=0; i--) {
            
            UIView *view = [calculationResultViews objectAtIndex:i];
            [self loadResultsForTag:view.tag];
            
            for(UITableView *resultTableView in [view subviews]) {
                if ([resultTableView isKindOfClass:[UITableView class]]) {
                    
                    view.frame = resultItemRectPortrait;
                    resultTableView.frame = resultItemTableViewRectPortrait;
                    break;
                    
                }
            }
            for(UIImageView *resultBackgroundImage in [view subviews]) {
                if ([resultBackgroundImage isKindOfClass:[UIImageView class]]) {
                    
                    UIImage *image = [UIImage imageNamed:@"kvitto_bg_portrait_big.png"];
                    
                    [resultBackgroundImage setImage:image];
                    
                    resultBackgroundImage.frame = CGRectMake(0, 0, image.size.width, image.size.height);
                    resultBackgroundImage.opaque = NO;
                }
            }
            
            [view.layer setOpaque:NO];
            view.opaque = NO;
            
            CGRect resultContainerFrame = view.frame;
            resultContainerFrame.origin.x = currPos;
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.5];
            [UIView setAnimationDelay:0];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            
            view.frame = resultContainerFrame;
            
            [UIView commitAnimations];
            
            counter++;
            currPos += 302;
        }
        
        [calculationResultViews release];
        
        bearingViewContainer.frame = bearingContainerRectPortrait;
        
        resultsContainerScrollView.frame = resultsContainerScrollViewRectPortrait;
        [resultsContainerScrollView setContentSize:CGSizeMake(currPos, 300)];
        
        if(keyboardIsShowing) {
            
//            inputParametersTableiPad.frame = inputParametersTableRectPortrait;
//            parametersViewContainer.frame = inputParametersContainerRectPortrait;
//            [self moveInputTable];
            
        } else {
            
            inputParametersTableiPad.frame = inputParametersTableRectPortrait;
            parametersViewContainer.frame = inputParametersContainerRectPortrait;
            doCalculationButton.frame = doCalculationButtonRectPortrait;
            doResetButton.frame = doResetButtonRectPortrait;
            doReportButton.frame = doReportButtonRectPortrait;
        }
        
        removeCalculationTypeButton.frame = removeCalculationTypeButtonRectPortrait;
        lineView.frame = CGRectMake(20, 298, 688, 1);
        
        [self reorganizeTabs];
        
    }
    else {
        
        [self.view layoutSubviews];
        
        
            lineView.frame = CGRectMake(20, 378, 628, 1);
        
        
        
//        UIView *lineview = [self.view ]
        // To remove add calculation popover while rotating
        if ([self.addCalculationPopover isPopoverVisible]) {
            
            [self.addCalculationPopover dismissPopoverAnimated:YES];
        }
        
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        selectedProduct = calculationDataManager.selectedBearing;
        
        if (selectedProduct != NULL) {
            
            [[bearingViewContainer subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
            
            [self paintBearingParams:[selectedProduct.properties count]];
        }
        
        CGRect resultContainerFrame = resultsContainerScrollView.frame;
        CGRect bearingContainerFrame = bearingViewContainer.frame;
        
        resultContainerFrame.origin.x = 570;
        
        
        if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {
            
            resultContainerFrame.origin.y = 84;
            
        }else {
            
            resultContainerFrame.origin.y = 104;
        }

        
        resultContainerFrame.size.width = 456;
        resultContainerFrame.size.height = 704;
        
        bearingContainerFrame.origin.x = 20;
        bearingContainerFrame.origin.y = 61;
        bearingContainerFrame.size.width = 728;
        bearingContainerFrame.size.height = 161;
        
        if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {
            
            logoImageView.frame = CGRectMake(960, 9, 50, 25);
            infoButton.frame = CGRectMake(20, 9, 22, 22);

            
        }else {
            
            logoImageView.frame = CGRectMake(960, 29, 50, 25);
            infoButton.frame = CGRectMake(20, 29, 22, 22);

        }
        
       
        
        if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {
            
            backgroundImage.frame = CGRectMake(0, 0, 1024, 768);
            
        }else {
            
            backgroundImage.frame = CGRectMake(0, 20, 1024, 768	);
        }

        //backgroundImage.image = [UIImage imageNamed:@"main_bg_ipad_landscape.png"];
        
        NSMutableArray *calculationResultViews = [[NSMutableArray alloc] init];
        
        for(UIView *view in [resultsContainerScrollView subviews])
        {
            if (view.tag > 999) {
                [calculationResultViews addObject:view];
            }
        }
        
        int counter = 1;
        int currPos=0;
        
        if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {
            
            
        }else {
            currPos=20;
        }

        int parentHeight = 0;
        
        for(int i = [calculationResultViews count]-1; i>=0; i--)
            
        {
            UIView *view = [calculationResultViews objectAtIndex:i];
            [self loadResultsForTag:view.tag];
            
            for(UITableView *resultTableView in [view subviews]) {
                if ([resultTableView isKindOfClass:[UITableView class]]) {
                    
                    
                    view.frame = CGRectMake(540, -resultTableView.contentSize.height+80, 302, resultTableView.contentSize.height+80);
                    resultTableView.frame = CGRectMake(10, 50, 280, resultTableView.contentSize.height);
                    
                    parentHeight = (resultTableView.contentSize.height)+80;
                    break;
                }
            }
            
            for(UIImageView *resultBackgroundImage in [view subviews]) {
                if ([resultBackgroundImage isKindOfClass:[UIImageView class]]) {
                    
                    UIImage *image = [[UIImage imageNamed:@"kvitto_bg_landscape_big.png"] stretchableImageWithLeftCapWidth:0 topCapHeight:40];
                    
                    [resultBackgroundImage setImage:image];
                    
                    resultBackgroundImage.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
                    resultBackgroundImage.opaque = NO;
                }
            }
            [view.layer setOpaque:NO];
            view.opaque = NO;
            
            CGRect resultContainerFrame = view.frame;
            resultContainerFrame.origin.y = currPos;
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.5];
            [UIView setAnimationDelay:0];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            
            view.frame = resultContainerFrame;
            
            [UIView commitAnimations];
            
            counter++;
            currPos += parentHeight;
        }
        
        [calculationResultViews release];
        
        [resultsContainerScrollView setContentSize:CGSizeMake(300, currPos)];
        
        resultsContainerScrollView.frame = resultsContainerScrollViewRectLandscape;
        bearingViewContainer.frame = bearingContainerRectLandscape;
        
        if(keyboardIsShowing) {
            inputParametersTableiPad.frame = inputParametersTableRectLandscape;
            parametersViewContainer.frame = inputParametersContainerRectLandscape;
            [self moveInputTable];
            
        } else {
            
            inputParametersTableiPad.frame = inputParametersTableRectLandscape;
            parametersViewContainer.frame = inputParametersContainerRectLandscape;
            doCalculationButton.frame = doCalculationButtonRectLandscape;
            doResetButton.frame = doResetButtonRectLandscape;
            doReportButton.frame = doReportButtonRectLandscape;
        }
        
        removeCalculationTypeButton.frame = removeCalculationTypeButtonRectLandscape;
        
        [self reorganizeTabs];
    }
    
    [inputParametersTableiPad reloadData];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    /** Selected calculations Title */
    
    if (isHeaderShown) {
        
        //UIView *selectedCalsView = [[UIView alloc] initWithFrame:CGRectZero];
        selectedCalsView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320, 44)];
        
        
        NSString *selectedCalsColor = COLOR_SKF_BLUE;
        
        UILabel *selectedCalsLable = [[UILabel alloc] initWithFrame:CGRectZero];
        selectedCalsLable.backgroundColor = [UIColor clearColor];
        selectedCalsLable.opaque = NO;
        selectedCalsLable.textColor = [UIColor colorWithHexString:selectedCalsColor];
        selectedCalsLable.highlightedTextColor = [UIColor colorWithHexString:selectedCalsColor];
        selectedCalsLable.font = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_GROUPED_CALCULATION_TITLE];
        selectedCalsLable.frame = CGRectZero;
        selectedCalsLable.lineBreakMode = NSLineBreakByWordWrapping;
        selectedCalsLable.numberOfLines = 0;
        
        if (calculationDataManager.mutlipleCalculation == TRUE) {
            
            if( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait ||  [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown){
                [selectedCalsLable setFrame:CGRectMake(0.0, 0.0, 690, 44)];
                
            } else if ( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
                [selectedCalsLable setFrame:CGRectMake(0.0, 0.0, 620, 44)];
            }
            
            
            
            
            NSMutableString *title = [[[NSMutableString alloc] initWithString:@""] autorelease];
            
            
            if (calculationDataManager.mutlipleCalculation) {
                
                [title appendString: GetLocalizedString(@"Selected_calculation")];
                [title appendString:@": "];
                
                for(int index=0; index<calculationDataManager.selectedCalculationsArray.count; index++) {
                    
                    Calculation *calculation =  [calculationDataManager.selectedCalculationsArray objectAtIndex:index];
                    [title appendFormat:@"%@ ", calculation.presentation];
                    
                    if (index < calculationDataManager.selectedCalculationsArray.count-1 ) {
                        
                        [title appendFormat:@"| "];
                    }
                }
            }
            
            //            else {
            //
            //                [title appendString: GetLocalizedString(@"Selected_calculation")];
            //                [title appendString:@": "];
            //                [title appendFormat:@"%@ ", calculationDataManager.selectedCalculation.presentation];
            //
            //            }
            
            
            
            
            //        for(int index=0; index<calculationDataManager.selectedCalculationsArray.count; index++) {
            //
            //            Calculation *calculation =  [calculationDataManager.selectedCalculationsArray objectAtIndex:index];
            //            [title appendFormat:@"%@ ", calculation.presentation];
            //            if (index < calculationDataManager.selectedCalculationsArray.count-1 ) {
            //
            //                [title appendFormat:@"| "];
            //            }
            //        }
            selectedCalsLable.text = title;
            [selectedCalsLable sizeToFit];
            CGSize maximumLabelSize = CGSizeMake(600, 500);
            
            CGSize expectedLabelSize = [title sizeWithFont:selectedCalsLable.font constrainedToSize:maximumLabelSize lineBreakMode:selectedCalsLable.lineBreakMode];
            //adjust the label the the new height.
            CGRect newFrame = selectedCalsLable.frame;
            newFrame.size.height = expectedLabelSize.height;
            selectedCalsLable.frame = newFrame;
            
        }
        
        /** Force action Image */
        
        if ((![selectedProduct.type isEqualToString:@"3_1"]) && (![selectedProduct.type isEqualToString:@"3_3a"]) &&  (![selectedProduct.type isEqualToString:@"3_3b"]) &&  (![selectedProduct.type isEqualToString:@"3_4"])) {
            
            
            UIImageView *forceActionImage= [[[UIImageView alloc] initWithFrame:CGRectMake(250, selectedCalsLable.frame.origin.y+ selectedCalsLable.frame.size.height-10, 250, 200)] autorelease];
            
            float scale1 = [UIScreen mainScreen].scale;
            CGSize diaImageViewSize = CGSizeMake(320, 380);
            CGSize diaImageSize = CGSizeMake(scale1 * (diaImageViewSize.width - 1), scale1 * (diaImageViewSize.height - 1));
            
//            NSMutableString* forceImagePath =
//            [NSString stringWithFormat:@"/Image?bearingType=%@&productId=%@&measurement=%@&calculation=1&withForce=true&width=%d&height=%d",
//             selectedProduct.type,selectedProduct.productId,[calculationDataManager selectedMeasurement],
//             (int)diaImageSize.width, (int)diaImageSize.height];
            
            
            
            bool radial_load = false , axial_load = false;
            
            if([_calculationControl getParameterObject:@"20"] != Nil)
            {
                radial_load = true;
            }
            if ([_calculationControl getParameterObject:@"21"] != Nil)
            {
                axial_load = true;
            }
            
            
            NSString* forceImagePath =
            [NSString stringWithFormat:@"/Image?bearingType=%@&productId=%@&measurement=%@&calculation=1&withForce=true&width=%d&height=%d&withFa=%s&withFr=%s",
             selectedProduct.type,selectedProduct.productId,[calculationDataManager selectedMeasurement],
             (int)diaImageSize.width, (int)diaImageSize.height , (axial_load) ?"true":"false" , (radial_load) ? "true":"false" ];
   
            
            
            
            
            NSMutableString* forceImageUrl = [NSMutableString stringWithFormat:@"%@%@", calculationDataManager.baseURL,forceImagePath];
            forceActionImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:forceImageUrl]]];
            forceActionImage.backgroundColor = [UIColor clearColor];
            forceActionImage.contentMode = UIViewContentModeScaleAspectFit;
            
            /** Displaying image desclaimer text for force action image */
            
            UIWebView *imageDesclaimerView = [[UIWebView alloc] initWithFrame:CGRectZero];
            
            if( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait ||  [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown){
                
                [imageDesclaimerView setFrame:CGRectMake(10, selectedCalsLable.frame.size.height + forceActionImage.frame.size.height-30, 670, 20)];
                
            } else if ( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
                
                [imageDesclaimerView setFrame:CGRectMake(10, selectedCalsLable.frame.size.height + forceActionImage.frame.size.height-30, 670, 20)];
            }
            
            [imageDesclaimerView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssStringRed, [self fixBR:[calculationDataManager getTranslationString:@"ImageDisclaimer"]]] baseURL:[NSURL URLWithString:@""]];
            
            imageDesclaimerView.scrollView.scrollEnabled = NO;
            
            [selectedCalsView setFrame:CGRectMake(0, 0, 320, selectedCalsLable.frame.size.height + forceActionImage.frame.size.height + imageDesclaimerView.frame.size.height -20)];
            selectedCalsView.backgroundColor = [UIColor whiteColor];
            
            [selectedCalsView addSubview:forceActionImage];
            [selectedCalsView addSubview:imageDesclaimerView];
            [imageDesclaimerView release];
            
            
        }
        
        [selectedCalsView addSubview:selectedCalsLable];
        [selectedCalsLable release];
        
        [inputParametersTableiPad setTableHeaderView:selectedCalsView ];
        inputParametersTableiPad.tableHeaderView.hidden = FALSE;
        
    } else {
        
        inputParametersTableiPad.tableHeaderView.hidden = TRUE;
    }
    
    if(tableView.tag == 9920) {
        return 6;
    } else {
        return 1;
    }
}


-(NSString *)fixBR:(NSString *)inString {
    
    return [inString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"];
}


/***** Option Selected *****/
-(void)ipadOptionSelected: (NSNotification*)notification {
    
    if ([optionSelectPopover isPopoverVisible]) {
        [optionSelectPopover dismissPopoverAnimated:YES];
        [optionSelectPopover release];
    }
    
    if (keyboardIsShowingOption) {
        
        [activeField becomeFirstResponder];
        
        
        //[self moveInputTable];
        
        
        if ( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
            
            [self moveInputTable];
        }

        
    }
    [inputParametersTableiPad reloadData];
}

- (void)showSettings:(id)sender {
    
    if (!connected) {
        
        [self showAlertForNetworkConnection];
        return;
    }
    
    
    if (self.settingsPopover == nil){
        SettingsViewController* settingsViewController = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
        settingsViewController.showHeaderView = FALSE;
        [settingsViewController setTitle:@"Settings"];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:settingsViewController];
        self.settingsPopover = [[[UIPopoverController alloc] initWithContentViewController:navigationController] autorelease];

        
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
            
        }else {
        
            NSString *navigationBarTintColor = NAVIGATION_BACKGROUND_COLOR;
            //set bar color
            navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:navigationBarTintColor];
            //optional, i don't want my bar to be translucent
            navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
            navigationController.navigationBar.translucent = NO;
            
            //set back button color
            [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
            //set back button arrow color
            [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
            settingsViewController.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            [self.settingsPopover setBackgroundColor:[UIColor colorWithHexString:NAVIGATION_BACKGROUND_COLOR]];

            
            
        }
        
        [settingsViewController release];
        [navigationController release];
    }
    
    [self.settingsPopover presentPopoverFromRect:[sender frame] inView:[self view] permittedArrowDirections:UIPopoverArrowDirectionLeft animated:TRUE];
}


/***** Request to ge the input Parameters *****/
-(void)sendNewRequest
{
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    NSArray *selectedCalculationArray = [calculationDataManager selectedCalculationsArray];
    
    if(calculationDataManager.mutlipleCalculation == false) {
        
        //[calculationDataManager getParametersForCalculation:calculationDataManager.selectedCalculation.calculationId andProductId:[selectedProduct productId] userData:self];
        [calculationDataManager getParametersForCalculation:[NSString stringWithFormat:@"%d", selectedCalculationId] andProductId:[selectedProduct productId] userData:self];

        
    }else if (calculationDataManager.mutlipleCalculation == true) {
        
        //        for (Calculation *cal in selectedCalculationArray) {
        //
        //            [calculationDataManager getParametersForCalculation:cal.calculationId andProductId:[selectedProduct productId] userData:self];
        //        }
        
        NSMutableString *selectedCalculationsId = [[[NSMutableString alloc] initWithString:@""] autorelease];
        
        int count =0;
        for (Calculation *calculation in selectedCalculationArray) {
            
            [selectedCalculationsId appendString:calculation.calculationId];
            
            if (count++ < (selectedCalculationArray.count -1)) {
                
                [selectedCalculationsId appendFormat:@","];
            }
        }
        
        
        hideIntermediate = true;
        what_plain_brearing = 0;
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        
       

        [calculationDataManager getParametersForCalculation:selectedCalculationsId andProductId:[selectedProduct productId] userData:self];
        
        
    }
    
    
}

/***** Request for Result *****/
/*
 -(void)sendNewRequestForResult {
 
 
 if (!connected) {
 
 [self setSearchingModeEnd];
 }
 
 CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
 // *dict = [arrayWithCalcID objectAtIndex:calculationsCount];
 NSString *calId=nil;
 
 if(calculationDataManager.mutlipleCalculation) {
 
 calId = [[calculationDataManager.selectedCalculationsArray objectAtIndex:calculationsCount] calculationId];
 
 }
 else {
 
 calId = calculationDataManager.selectedCalculation.calculationId;
 }
 
 if ([calId isEqualToString:@"5"]) {                                             // Sending H=-1 for Radiolubrication - Oil jet
 
 //NSString *value =  [dict valueForKey:@"radioLubrication"];
 NSString *value =  [params valueForKey:@"radioLubrication"];
 
 if([value isEqualToString:@"3"]) {
 
 //[dict setValue:@"-1" forKeyPath:@"H"];
 [params setValue:@"-1" forKeyPath:@"H"];
 }
 }
 
 [calculationDataManager doCalculation:calId withParameters:params userData:self];
 
 }
 */
-(void)sendNewRequestForResult {
    
    
    if (!connected) {
        
        [self setSearchingModeEnd];
    }
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    // *dict = [arrayWithCalcID objectAtIndex:calculationsCount];
    NSString *calId=nil;
    
    if(calculationDataManager.mutlipleCalculation) {
        
        NSMutableString *selectedCalculationsId = [[[NSMutableString alloc] initWithString:@""] autorelease];
        
        int count =0;
        for (Calculation *calculation in calculationDataManager.selectedCalculationsArray) {
            
            [selectedCalculationsId appendString:calculation.calculationId];
            
            if (count++ < (calculationDataManager.selectedCalculationsArray.count-1)) {
                
                [selectedCalculationsId appendFormat:@","];
            }
        }
        
        calId = selectedCalculationsId;
        
    }
    else {
        
        //calId = calculationDataManager.selectedCalculation.calculationId;
        calId = [NSString stringWithFormat:@"%d", selectedCalculationId];

    }
    
    if ([calId isEqualToString:@"5"]) {                                             // Sending H=-1 for Radiolubrication - Oil jet
        
        //NSString *value =  [dict valueForKey:@"radioLubrication"];
        NSString *value =  [params valueForKey:@"radioLubrication"];
        
        if([value isEqualToString:@"3"]) {
            
            //[dict setValue:@"-1" forKeyPath:@"H"];
            [params setValue:@"-1" forKeyPath:@"H"];
        }
    }
        
    [params setValue:@"1" forKey:@"mobile"];
    
    
    
    [calculationDataManager doCalculation:calId withParameters:params userData:self];
    
}



- (void)setSearchingModeStart {
    
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    
    HUD = [[MBProgressHUD alloc] initWithFrame:CGRectMake(250, 300, 250, 250)];
    HUD.labelText = [calculationDataManager getTranslationString:@"Loading"];
    HUD.graceTime = 0.5f;
    HUD.taskInProgress = YES;
    HUD.allowsCancelation = YES;
    HUD.delegate = self;
    HUD.labelFont = [UIFont fontWithName:CHEVIN_BOLD size:20.f];
    [self.view addSubview:HUD];
    [HUD show:YES];
    // [self.view setUserInteractionEnabled:NO];
    
    [doCalculationButton setUserInteractionEnabled:NO];
    [doResetButton setUserInteractionEnabled:NO];
    [doReportButton setUserInteractionEnabled:NO];
    [addCalculationButton setUserInteractionEnabled:NO];
    [changeBearingButton setUserInteractionEnabled: NO];
    
    
}
- (void)setSearchingModeEnd {
    
    [HUD hide:YES];
    HUD.taskInProgress = NO;
    HUD.graceTime = 1.0f;
    [HUD removeFromSuperview];
    [self.view setUserInteractionEnabled:YES];
    [HUD release];
    HUD = nil;
    
    [addCalculationButton setUserInteractionEnabled:YES];
    [changeBearingButton setUserInteractionEnabled: YES];
    [doCalculationButton setUserInteractionEnabled:YES];
    [doReportButton setUserInteractionEnabled:YES];
    [doResetButton setUserInteractionEnabled:YES];
}

- (void)setSearchingModeStartForPDFReport {
    
    HUD = [[MBProgressHUD alloc] initWithFrame:CGRectMake(250, 300, 250, 250)];
    HUD.labelText = @"Please wait";
    HUD.graceTime = 0.5f;
    HUD.taskInProgress = YES;
    HUD.allowsCancelation = YES;
    HUD.delegate = self;
    HUD.labelFont = [UIFont fontWithName:CHEVIN_BOLD size:20.f];
    
   //[self.view setUserInteractionEnabled:NO];
    [self.view addSubview:HUD];
    [HUD show:YES];
    
}

- (void)setSearchingModeEndForPDFReport {
    
    [HUD hide:YES];
    HUD.taskInProgress = NO;
    [HUD removeFromSuperview];
    
    [self.view setUserInteractionEnabled:YES];
    //self.navigationItem.hidesBackButton = NO;
    
    [HUD release];
    HUD = nil;
}

-(void) RemoveUnsupportedCalculationsiPad {
    
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    objectsToDiscard = [[NSMutableArray alloc]init];
    
    for(Calculation *calc in calculationDataManager.selectedCalculationsArray) {
        
        for(Error *err in noFormulaErrorArray) {
            
            NSString *calcName = [self getCalculationNameFromError:err.description];
            
            if ([calcName isEqualToString:calc.presentation]) {
                [objectsToDiscard addObject:calc];
                
                if ([calc.calculationId isEqualToString:@"14"]) {
                    
                    calculationDataManager.isRelubricationCalcExist = FALSE;
                    
                }
            }
            
        }
    }
    
    
}


- (NSString *)getCalculationNameFromError:(NSString *)errString {
    
    NSMutableString *errorMessages = [[[NSMutableString alloc]init]autorelease];
    
    NSRange range = [errString rangeOfString:@"is" options:NSBackwardsSearch];
    [errorMessages appendString:[errString substringToIndex:range.location-1]];
    
    [errorMessages replaceOccurrencesOfString:@"Calculation " withString:@""options:NSCaseInsensitiveSearch
                                        range:NSMakeRange(0, [errorMessages length])];
    return errorMessages;
}

-(void) resetObjects {
    
    calculationsCount=0;
    [CalculationControl setNum:0];
    [_calculationControl resetMissingParameters];
    [noFormulaErrorArray removeAllObjects];

    
}

-(void) handleErrors {
    
    if (noFormulaErrorArray.count == 0) {
        return;
    }
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    [self RemoveUnsupportedCalculationsiPad];
    

    
    //if ( noFormulaErrorArray.count == 1) {
    
    if(!calculationDataManager.mutlipleCalculation && noFormulaErrorArray.count == 1) {
        
        for (Error* noFormulaError in noFormulaErrorArray){
            
                       /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                        message:noFormulaError.description
                                                                       delegate:self
                                                              cancelButtonTitle:GetLocalizedString(@"OK")
                                                             otherButtonTitles:nil];
                        alert.tag = 2;
                        [alert show];
                        [alert release];*/
            
            selectedCalsView.hidden = YES;
            
            [noParametersAvailableError removeFromSuperview];
            noParametersAvailableError = [[DisplayParametersErrorIllustration_iPad alloc] initWithFrame:CGRectMake(60, 20, 539, 223)];
            noParametersAvailableError.helpString = [NSString stringWithString:noFormulaError.description];
            noParametersAvailableError.opaque = NO;
            noParametersAvailableError.backgroundColor = [UIColor clearColor];
            [parametersViewContainer addSubview:noParametersAvailableError];
 
        }

    }else  if (calculationDataManager.mutlipleCalculation &&  (noFormulaErrorArray.count == calculationDataManager.selectedCalculationsArray.count)) {
        
        [self showAlertForUnsupportedCalculation];
        
        isHeaderShown = NO;
    }
    
    else
    {
        NSMutableString *errorMessages = [[[NSMutableString alloc]init]autorelease];
        NSMutableString *alertTitle = [[[NSMutableString alloc]init]autorelease];
        for (Calculation* noFormulaCalc in objectsToDiscard) {
            
            [errorMessages appendString:noFormulaCalc.presentation];
            [errorMessages appendFormat:@"\n\n"];
        }
        
        alertTitle = [[NSMutableString alloc] initWithString:GetLocalizedString(@"The_following_calculations_are_not_possible")];
        [alertTitle appendFormat:@" %@",selectedProduct.designation];
        [alertTitle appendFormat:@"."];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle
                                                        message:errorMessages
                                                       delegate:self
                                              cancelButtonTitle:GetLocalizedString(@"OK")
                                              otherButtonTitles:nil];
        alert.tag = 2;
        [alert show];
        [alert release];
        
    }
}

- (void) showAlertForUnsupportedCalculation {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    if (noFormulaErrorArray != nil) {
        if(calculationDataManager.mutlipleCalculation==TRUE) {
            isHeaderShown = NO;
            selectedCalsView.hidden = YES;
            [noParametersAvailableError removeFromSuperview];
            noParametersAvailableError = [[DisplayParametersErrorIllustration_iPad alloc] initWithFrame:CGRectMake(60, 20, 539, 223)];
            //noParametersAvailableError.helpString = [NSString stringWithFormat:@"Selected calculations are not possible for %@", selectedProduct.designation];
            noParametersAvailableError.helpString = [NSString stringWithFormat:@"%@: %@", GetLocalizedString(@"Unsupported_calculations"),selectedProduct.designation];
            
            
            noParametersAvailableError.opaque = NO;
            noParametersAvailableError.backgroundColor = [UIColor clearColor];
            [parametersViewContainer addSubview:noParametersAvailableError];
            
            
        }
    }
}


-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;
{
    NSURL *requestURL =[ [ request URL ] retain ];
    NSString *urlString = [requestURL absoluteString];
    NSURL *hyperlinkURL = nil;
    
    if ([urlString isEqualToString:@"file:///"] == false) {
        
        CalculationDataManager *calculateDataManager = [CalculationDataManager sharedManager];
        urlString = [urlString stringByReplacingOccurrencesOfString:@"file:///" withString:[NSString stringWithFormat:@"%@/",[calculateDataManager baseURL]]];
        hyperlinkURL = [NSURL URLWithString:urlString];
        [self showHyperlinkView:hyperlinkURL];
    }
    [requestURL release ];
    return YES;
}

-(void)setNewSize:(CGSize) newSize {
    
    [hyperlinkPopover setPopoverContentSize:newSize animated:YES];
}

-(void) showHyperlinkView:(NSURL *) url{
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 370, 440)];
    [webView loadRequest:request];
    webView.scalesPageToFit= YES;
    
    HyperlinkViewController *hyperlinkView = [[[HyperlinkViewController alloc] initWithNibName:@"HyperlinkViewController" bundle:nil]autorelease];
    [hyperlinkView.view addSubview:webView];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:hyperlinkView];
    
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        
    }else {
        NSString *navigationBarTintColor = NAVIGATION_BACKGROUND_COLOR;
        navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:navigationBarTintColor];
        navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        navigationController.navigationBar.translucent = NO;
        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
        //set back button arrow color
        [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
        hyperlinkView.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
    }

    hyperlinkPopover = [[[UIPopoverController alloc] initWithContentViewController:navigationController] retain];
    CGRect popoverFrame =  CGRectMake(200,250,370,440);
    hyperlinkPopover.popoverContentSize=CGSizeMake(370, 440);
    [hyperlinkPopover presentPopoverFromRect:popoverFrame inView:self.view permittedArrowDirections:0 animated:YES];
}



/*** PDF Report Generation - Save | Print | Email ***/
- (NSInteger) numberOfPreviewItemsInPreviewController: (QLPreviewController *) controller {
    return 1;
}

- (id <QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index {
    
    NSURL *url = [NSURL fileURLWithPath:[[self documentDirectoryPathForResource] stringByAppendingPathComponent:lastReportFileName]];
    return url;
}

-(NSString*)documentDirectoryPathForResource {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return  [paths lastObject];
    [self setSearchingModeEndForPDFReport];
}


    
-(void)saveReport:(id)sender {
    
    NSMutableDictionary *event =
    [[GAIDictionaryBuilder createEventWithCategory:@"UI"
                                            action:@"buttonPress"
                                             label:@"savebutton-iPad"
                                             value:nil] build];
    [[GAI sharedInstance].defaultTracker send:event];
    [[GAI sharedInstance] dispatch];

    if (!connected) {
        [self showAlertForNetworkConnection];
        return;
    }

    [self setSearchingModeStartForPDFReport];
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Add code here to do background processing
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    NSString *reportPath = @"/GenerateReport?reportId=";
    NSString *mobile = @"&mobile=1";
    UIButton *saveButton = (UIButton*)sender;
    NSString *pdfReportAdress = [NSString stringWithFormat:@"%@%@%ld%@",calculationDataManager.baseURL,reportPath,(long)saveButton.tag,mobile];
    //NSLog(@"URL: %@",pdfReportAdress);
    NSURL *url = [NSURL URLWithString:pdfReportAdress];
    NSData *myData = [NSData dataWithContentsOfURL:url];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
    req.timeoutInterval = 100000;
    //NSLog(@"Data: %lu",(unsigned long)[myData length]);
    
    //make a file name to write the data to using the documents directory:
    NSDate *currentDateTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd_HH.mm.ss"];
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    NSLog(@"%@", dateInStringFormated);
    [dateFormatter release];
    
    NSString *pdfReportName = @"SBC_Report_";
    NSString *fileFormat = @".pdf";
    NSString *fileName = [NSString stringWithFormat:@"%@%@%@",pdfReportName,dateInStringFormated,fileFormat];
    lastReportFileName = [fileName copy];
    NSLog(@"Pdf file name: %@",lastReportFileName);
    
    BOOL success = [myData writeToFile:[[self documentDirectoryPathForResource] stringByAppendingPathComponent:lastReportFileName] atomically:YES];
    
        dispatch_async( dispatch_get_main_queue(), ^{
            
            // Add code here to update the UI/send notifications based on the
            // results of the background processing
            
    if (success) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[calculationDataManager getTranslationString:@"Report_Downloaded"] message:[calculationDataManager getTranslationString:@"View_Report"]  delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alert.tag = 1001;
        [alert show];
        [alert release];
        
    }else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[calculationDataManager getTranslationString:@"Download_Error"] message:[calculationDataManager getTranslationString:@"Download_Report_Error"]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
        [self setSearchingModeEndForPDFReport];
            
        });
    });
}





- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL

                                               usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    return interactionController;
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)interactionController {
    
    return self;
}

- (void)dismissReportViewPopover {
    
    if ([reportViewPopover isPopoverVisible]) {
        [reportViewPopover dismissPopoverAnimated:YES];
        [reportViewPopover release];
    }
}


-(IBAction)reportButtonClicked:(id)sender
{
   if (reportsViewController == nil) {
        reportsViewController = [[ReportsViewController alloc] initWithNibName:@"ReportsViewController" bundle:[NSBundle mainBundle]];
        reportsViewController.baseViewController = self;
    }
    
   if(reportNavigationController == nil) {
        reportNavigationController = [[UINavigationController alloc] initWithRootViewController:reportsViewController];
    }
    
    if (floor(NSFoundationVersionNumber)> NSFoundationVersionNumber_iOS_6_1) {
        reportNavigationController.navigationBar.barTintColor = [UIColor colorWithHexString:NAVIGATION_BACKGROUND_COLOR];
        reportNavigationController.navigationBar.barStyle = UIBarStyleBlack;
        reportNavigationController.navigationBar.translucent = NO;
    }
    
    reportViewPopover = [[UIPopoverController alloc] initWithContentViewController:reportNavigationController];
    reportViewPopover.popoverContentSize=CGSizeMake(320, 480);
    reportViewPopover.delegate = self;
    [reportViewPopover presentPopoverFromRect:doReportButton.frame inView:parametersViewContainer permittedArrowDirections:0 animated:YES];
}


/*** END ***/





@end
