//
//  ReportnavigationViewController.h
//  fiveinone
//
//  Created by Admin on 03/12/13.
//  Copyright (c) 2013 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuickLook/QuickLook.h>
#import "DirectoryWatcher.h"

@interface ReportnavigationViewController : UINavigationController <QLPreviewControllerDataSource, DirectoryWatcherDelegate,QLPreviewControllerDelegate,UIDocumentInteractionControllerDelegate, UITableViewDataSource, UITableViewDelegate>
{
    
    
    UITableView *theTableView;

}

@property(nonatomic,retain) IBOutlet UITableView *theTableView;


@end
