//
//  ReportnavigationViewController.m
//  fiveinone
//
//  Created by Admin on 03/12/13.
//  Copyright (c) 2013 HiQ. All rights reserved.
//

#import "ReportnavigationViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "MainViewController_iPad.h"

#define kRowHeight 58.0f

@interface ReportnavigationViewController ()

@property (nonatomic, strong) DirectoryWatcher *docWatcher;
@property (nonatomic, strong) NSMutableArray *documentURLs;
@property (nonatomic, strong) UIDocumentInteractionController *docInteractionController;
@end

@implementation ReportnavigationViewController
@synthesize theTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    self.docWatcher = [DirectoryWatcher watchFolderWithPath:[self applicationDocumentsDirectory] delegate:self];
    self.documentURLs = [NSMutableArray array];
    // scan for existing documents
    [self directoryDidChange:self.docWatcher];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setupDocumentControllerWithURL:(NSURL *)url {
    
    if (self.docInteractionController == nil) {
        self.docInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        self.docInteractionController.delegate = self;
    }else{
        self.docInteractionController.URL = url;
    }
    
}

-(void) viewDidUnload {
    self.documentURLs = nil;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifier = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    NSURL *fileURL = [self.documentURLs objectAtIndex:indexPath.row];
    [self setupDocumentControllerWithURL:fileURL];
    cell.textLabel.text = [[fileURL path] lastPathComponent];
    NSInteger iconCount = [self.docInteractionController.icons count];
    if (iconCount > 0){
        cell.imageView.image = [self.docInteractionController.icons objectAtIndex:iconCount - 1];
    }
    
    NSString *fileURLString = [self.docInteractionController.URL path];
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:fileURLString error:nil];
    NSInteger fileSize = [[fileAttributes objectForKey:NSFileSize] intValue];
    NSString *fileSizeStr = [NSByteCountFormatter stringFromByteCount:fileSize countStyle:NSByteCountFormatterCountStyleFile];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@", fileSizeStr, self.docInteractionController.UTI];
    return cell;
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kRowHeight;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSURL *fileURL = [self.documentURLs objectAtIndex:indexPath.row];
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtURL:fileURL error:&error];
        [self.documentURLs removeObjectAtIndex:indexPath.row];
        [tableView reloadData];
    }
}

#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSURL *fileURL = [self.documentURLs objectAtIndex:indexPath.row];
    [self setupDocumentControllerWithURL:fileURL];
    [self.docInteractionController presentPreviewAnimated:YES];
    
    
    /*QLPreviewController *previewController = [[QLPreviewController alloc] init];
     previewController.dataSource = self;
     previewController.delegate = self;
     
     
     // start previewing the document at the current section index
     
     previewController.currentPreviewItemIndex = indexPath.row;
     
     if(baseViewController)
     {
     [self presentModalViewController:previewController animated:YES];
     }
     else
     {
     [[self navigationController] pushViewController:previewController animated:YES];
     }*/
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return self.documentURLs.count;
}







#pragma mark - UIDocumentInteractionControllerDelegate



- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)interactionController

{
    
    //if(baseViewController != nil)
    //return  baseViewController;
    
    return self;
    
    
}





#pragma mark - QLPreviewControllerDelegate
- (void)previewControllerWillDismiss:(QLPreviewController *)controller
{
    [self.theTableView reloadData];
}

- (void)previewControllerDidDismiss:(QLPreviewController *)controller

{
    
    // if the preview dismissed (done button touched), use this method to post-process previews
    
    //[self.tableView reloadData];
    
    
    
}


#pragma mark - QLPreviewControllerDataSource



// Returns the number of items that the preview controller should preview

- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)previewController

{
    
    return self.documentURLs.count;;
    
}


// returns the item that the preview controller should preview

- (id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx

{
    
    NSURL *fileURL = [self.documentURLs objectAtIndex:idx];
    
    return fileURL;
    
}



#pragma mark - File system support

- (NSString *)applicationDocumentsDirectory {
    
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}



- (void)directoryDidChange:(DirectoryWatcher *)folderWatcher
{
    [self.documentURLs removeAllObjects];    // clear out the old docs and start over
    
    NSString *documentsDirectoryPath = [self applicationDocumentsDirectory];
    NSArray *documentsDirectoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectoryPath error:NULL];
    
    for (NSString* curFileName in [documentsDirectoryContents objectEnumerator]){
        
        NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:curFileName];
        
        NSURL *fileURL = [NSURL fileURLWithPath:filePath];
        
        BOOL isDirectory;
        
        [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
        
        // proceed to add the document URL to our list (ignore the "Inbox" folder)
        
        if (!(isDirectory && [curFileName isEqualToString:@"Inbox"])) {
            
            [self.documentURLs addObject:fileURL];
        }
    }
    
    [self.theTableView reloadData];
    
}



@end
