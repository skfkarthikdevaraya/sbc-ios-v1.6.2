//
//  SettingsViewController_iPad.h
//  fiveinone
//
//  Created by Tommy Eriksson on 4/13/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculationDataManager.h"

@interface SettingsViewController_iPad : UIViewController<UITableViewDelegate, UITableViewDataSource, CalculationDataManagerDelegate>

@property (nonatomic, retain) IBOutlet UISegmentedControl*  settingsSelect;
@property (nonatomic, retain) IBOutlet UITableView* tableView;
@property (nonatomic, retain) IBOutlet UINavigationBar* navigationBar;
@property (nonatomic, retain) IBOutlet UIToolbar* segmentedToolbar;

- (IBAction)selectedSetting:(id)sender;

@end
