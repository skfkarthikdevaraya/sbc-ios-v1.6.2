//
//  SettingsViewController_iPad.m
//  fiveinone
//
//  Created by Tommy Eriksson on 4/13/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import "SettingsViewController_iPad.h"
#import "Language.h"
#import "Measurement.h"

@implementation SettingsViewController_iPad

@synthesize tableView;
@synthesize navigationBar;
@synthesize settingsSelect;
@synthesize segmentedToolbar;

#define SEGMENT_LANG 0
#define SEGMENT_MEASUREMENT 1

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)dealloc{
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager removeObserver:self];
    [super dealloc];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    

    // Do any additional setup after loading the view from its nib.
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager addObserver:self];
    [calculationDataManager initTranslation:self];
    [calculationDataManager getLanguages:self];
    [calculationDataManager getMeasurements:self];
    
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [tableView setBackgroundColor:[UIColor clearColor]];
    [tableView setBackgroundView:nil];
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

// TABLE VIEW - Delegate
-(void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    BOOL isLanguageSettings = [settingsSelect selectedSegmentIndex] == SEGMENT_LANG;
    BOOL isMeasurementSettings = [settingsSelect selectedSegmentIndex] == SEGMENT_MEASUREMENT;
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];    
    
    if (isLanguageSettings){
        [calculationDataManager setUserSelectedLanguageWithIndex:indexPath.row];
        [calculationDataManager getTranslation:self];
    }
    
    if (isMeasurementSettings){
        
            if (indexPath.row == [calculationDataManager getUserSelectedMeasurementAsIndex]) {
                
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil                                  
                                                    message:GetLocalizedString(@"Unit_change_measurment")
                                                    delegate:self
                                                    cancelButtonTitle:GetLocalizedString(@"No")
                                                    otherButtonTitles:GetLocalizedString(@"Yes"), nil];
            alert.tag = 3;
            [alert show];
            [alert release];
            
        }

        
        [calculationDataManager setUserSelectedMeasurementWithIndex:indexPath.row];         
    }
    
    [tableView reloadData];
}




- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 3) {
        
        if (buttonIndex == 1) {
            
            NSMutableDictionary *sendParams = [NSMutableDictionary dictionary];
            
            CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
            [calculationDataManager getUpdatedProducts:sendParams userData:self];
            
            [self.navigationController popToRootViewControllerAnimated:YES];

        }
        
            }
}

// TABLE VIEW - Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    if ([settingsSelect selectedSegmentIndex] == SEGMENT_LANG){
        return [calculationDataManager languageArray] == nil ? 0 : [[calculationDataManager languageArray] count]; 
    } else if ([settingsSelect selectedSegmentIndex] == SEGMENT_MEASUREMENT){
        return [calculationDataManager measurementArray] == nil ? 0 : [[calculationDataManager measurementArray] count]; 
    } 
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        cell.textLabel.userInteractionEnabled = false;
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
        cell.textLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    }
    
    BOOL isLanguageSettings = [settingsSelect selectedSegmentIndex] == SEGMENT_LANG;
    BOOL isMeasurementSettings = [settingsSelect selectedSegmentIndex] == SEGMENT_MEASUREMENT;
    
    if (isLanguageSettings){
        Language* lang = [[calculationDataManager languageArray] objectAtIndex:indexPath.row];
        cell.textLabel.text = lang.description;
//        BOOL isSelected = indexPath.row == [calculationDataManager getUserSelectedLanguageAsIndex];
//        cell.accessoryType = isSelected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
        
        [cell setSelectionStyle:UITableViewCellSeparatorStyleSingleLine];
    }
    
    if (isMeasurementSettings){
        Measurement* measurement = [[calculationDataManager measurementArray] objectAtIndex:indexPath.row];
        cell.textLabel.text = measurement.description;
        BOOL isSelected = indexPath.row == [calculationDataManager getUserSelectedMeasurementAsIndex];
        cell.accessoryType = isSelected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
        

    }
    
    
    
    
    return cell;   
}

- (IBAction)selectedSetting:(id)sender {
    [tableView reloadData];
}


- (void)dataManagerReturnedLangauges:(BOOL)success{
    if (success){
        [tableView reloadData];
    }
}

- (void)dataManagerReturnedMeasurements:(BOOL)success{
    if (success){
        [tableView reloadData];
    }
}

- (void)dataManagerReturnedTranslation:(BOOL)success{
    if (success){
        CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
        [settingsSelect setTitle:[calculationDataManager getTranslationString:@"Language"] forSegmentAtIndex:SEGMENT_LANG];
        [settingsSelect setTitle:[calculationDataManager getTranslationString:@"select_unit_system."] forSegmentAtIndex:SEGMENT_MEASUREMENT];
        
        [self setTitle:[calculationDataManager getTranslationString:@"Settings"]];
    }
}

@end
