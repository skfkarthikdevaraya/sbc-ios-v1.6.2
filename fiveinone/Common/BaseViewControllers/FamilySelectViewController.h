//
//  OptionsViewController.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/26/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OptionSelectViewController.h"
#import "CalculationDataManager.h"

//@protocol FamilyDelegate;

@interface FamilySelectViewController : UIViewController <UIWebViewDelegate, CalculationDataManagerDelegate> {
    NSArray *optionsArray;
    UITableView *optionsTable;
    NSString *parameterName;
    NSString *parameterPresentation;
    NSIndexPath *parentIndexPath;
    NSString *previousSelectedOption;
    id <OptionsDelegate> delegate;
    

}

@property (nonatomic, retain) NSArray *optionsArray;
@property (nonatomic, retain) IBOutlet UITableView *optionsTable;
@property (nonatomic, assign) id <OptionsDelegate> delegate;
@property (nonatomic, retain) NSString *parameterName;
@property (nonatomic, retain) NSString *parameterPresentation;
@property (nonatomic, retain) NSIndexPath *parentIndexPath;
@property (nonatomic, retain) NSString *previousSelectedOption;
@property (nonatomic, retain) NSString *cssString;

@end


//@protocol FamilyDelegate <NSObject>
//// shoppinglistname == nil on cancel
//- (void)selectedOption:(NSString *)selectedOption withIndex:(NSInteger)selectedIndex andName:(NSString *)name andIndexPath:indexPath;
//
//@end

