//
//  OptionsViewController.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/26/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "FamilySelectViewController.h"
#import "OptionSelectViewController.h"
#import "CalculationDataManager.h"
#import "TitleManager.h"
#import "Option.h"
#import "Family.h"
#import "fiveinoneAppDelegate_iPhone.h"

@implementation FamilySelectViewController


@synthesize optionsArray;
@synthesize optionsTable;
@synthesize delegate;
@synthesize parameterName;
@synthesize parameterPresentation;
@synthesize parentIndexPath; 
@synthesize previousSelectedOption;
@synthesize cssString;


-(NSString *) stringByStrippingHTML:(NSString *)inString{
    NSRange r;
    NSString *s = [[inString copy] autorelease];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

-(float) calculateHeightOfTextFromWidth:(NSString*)text: (UIFont*)withFont: (float)width :(UILineBreakMode)lineBreakMode
{
    [text retain];
    [withFont retain];
    CGSize suggestedSize = [text sizeWithFont:withFont constrainedToSize:CGSizeMake(width, FLT_MAX) lineBreakMode:lineBreakMode];
    CGSize lineHeightSize = [text sizeWithFont:withFont];
    
    float height = 0;
    if (![text isEqualToString:@""])
    {
        CGFloat rows = suggestedSize.height / lineHeightSize.height;
        height = rows * (lineHeightSize.height + 5);
        height += 5;
    }
    [text release];
    [withFont release];
    
    return height;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    cssString = @"<html> \n"
     "<head> \n"
     "<style type=\"text/css\"> \n"
     "body {font-family: \"SKF Chevin Medium\"; font-size: 16; margin:4px 0px; background-color:transparent; color:#333333 word-wrap:break-word;}\n"
     "</style> \n"
     "</head> \n";
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager initTranslation:self];
    
    if ([[calculationDataManager previousInputs] valueForKey:parameterName])
    {
        previousSelectedOption = [[calculationDataManager previousInputs] valueForKey:parameterName];
    }
    
    [optionsTable setBackgroundView:nil];
    [optionsTable setBackgroundColor:[UIColor clearColor]];
    [optionsTable setOpaque:NO];
    
    
    if (floor(NSFoundationVersionNumber)> NSFoundationVersionNumber_iOS_6_1) {

    //iOS 7 - Navigation bg & text color change in option screen
    NSString *navigationBarTintColor = NAVIGATION_BACKGROUND_COLOR;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:navigationBarTintColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];

    
    //IOS 7 - To remove table view header title space
    self.optionsTable.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.optionsTable.bounds.size.width, 0.01f)];
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    return [optionsArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Family *family = [optionsArray objectAtIndex:indexPath.row];
    UIFont *myFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
    float height = [self calculateHeightOfTextFromWidth:[self stringByStrippingHTML:family.name] :myFont :280 :NSLineBreakByWordWrapping];
    return MAX(height, 44);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";  
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UIWebView *parameterLabel;
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        //UIWebView *parameterLabel = [[UIWebView alloc] initWithFrame:CGRectMake(15, 0, 280, 80)];
        parameterLabel = [[UIWebView alloc] initWithFrame:CGRectMake(15, 0, 280, 80)];
        parameterLabel.backgroundColor = [UIColor clearColor];
        parameterLabel.opaque = NO;
        parameterLabel.userInteractionEnabled = false;
        parameterLabel.tag = 10010;
        parameterLabel.delegate = self;
        [cell addSubview:parameterLabel];
        [parameterLabel release];
        cell.textLabel.hidden = YES;
        
    }
    
    parameterLabel = (UIWebView *)[cell viewWithTag:10010];

    Family *family = [optionsArray objectAtIndex:indexPath.row];
    NSString *input = family.name;

    cell.textLabel.text = input;
    [parameterLabel loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, input] baseURL:[NSURL URLWithString:@""]];
    
    if ([previousSelectedOption isEqualToString:cell.textLabel.text])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    NSLog(calculationDataManager.isRelubricationCalcExist ? @"Yes" : @"No");
    
    

    if (calculationDataManager.isRelubricationCalcExist) {
        if (indexPath.row == 0 || indexPath.row == 1) {
            [cell setBackgroundColor:[UIColor colorWithHexString:TEXT_COLOR_GREY2]];
            cell.accessoryType = UITableViewCellAccessoryNone;
            [cell setUserInteractionEnabled:NO];
        }
    }

    
    
    return cell;
}

- (void)dataManagerReturnedTranslation:(BOOL)success{
    if (success){
        CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
        self.navigationItem.titleView = [TitleManager getTitle:[calculationDataManager getTranslationString:@"Options"]];
        
//        //iOS 7 - Navigation bg & text color change in option screen
//        NSString *navigationBarTintColor = NAVIGATION_BACKGROUND_COLOR;
//        self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:navigationBarTintColor];
//        self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
//        self.navigationController.navigationBar.translucent = NO;

    }
}

@end
