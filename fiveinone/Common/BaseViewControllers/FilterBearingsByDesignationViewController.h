//
//  FilterBearingsByDesignationViewController.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/14/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculationDataManager.h"

@interface FilterBearingsByDesignationViewController : UIViewController <CalculationDataManagerDelegate, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate> {
    UISearchBar *designationSearchBar;
    UIToolbar *segmentedToolBar;
    UIView *parentViewContainer;
    NSMutableArray *foundBearingsArray;
    NSArray *previousBearingsArray;
    UINavigationController *parentNavigationController;
    UIImageView *viewBackground;
    UITableView *bearingsListTable;
    
    NSString *cssStringGray;

    
}

@property (nonatomic, retain) IBOutlet UISearchBar *designationSearchBar;
@property (nonatomic, retain) UIToolbar *segmentedToolBar;
@property (nonatomic, retain) UIView *parentViewContainer;
@property (nonatomic, retain) UINavigationController *parentNavigationController;
@property (nonatomic, retain) IBOutlet UIImageView *viewBackground;
@property (nonatomic, retain) NSMutableArray *foundBearingsArray;
@property (nonatomic, retain) UITableView *bearingListTable;

@property (nonatomic, retain) NSString *cssStringGray;
-(IBAction)keyboardDismiss:(id)sender;

@end
