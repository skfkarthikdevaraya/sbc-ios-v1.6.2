//
//  FilterBearingsByDesignationViewController.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/14/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "FilterBearingsByDesignationViewController.h"
#import "CalculateViewController.h"
#import "Product.h"
#import "Property.h"
#import "FilterBearingsViewController.h"
#import "SelectCalculationViewController.h"
#import "SelectBearingViewController.h"
#import "fiveinoneAppDelegate_iPhone.h"
#import "NSString+StripHTML.h"
#import "TitleManager.h"
#import "Property.h"

@implementation FilterBearingsByDesignationViewController

@synthesize designationSearchBar;
@synthesize segmentedToolBar;
@synthesize parentViewContainer;
@synthesize parentNavigationController;
@synthesize viewBackground;
@synthesize foundBearingsArray;
@synthesize bearingListTable;

@synthesize cssStringGray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc{
    CalculationDataManager* calculatorDataManager = [CalculationDataManager sharedManager];
    [calculatorDataManager removeObserver:self];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(float) calculateHeightOfTextFromWidth:(NSString*)text: (UIFont*)withFont: (float)width :(UILineBreakMode)lineBreakMode
{
    [text retain];
    [withFont retain];
    CGSize suggestedSize = [text sizeWithFont:withFont constrainedToSize:CGSizeMake(width, FLT_MAX) lineBreakMode:lineBreakMode];
    CGSize lineHeightSize = [text sizeWithFont:withFont];
    
    float height = 0;
    if (![text isEqualToString:@""])
    {
        CGFloat rows = suggestedSize.height / lineHeightSize.height;
        height = rows * (lineHeightSize.height + 5);
        height += 5;
    }
    [text release];
    [withFont release];
    
    return height;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	// create the parent view that will hold header Label
	UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 44.0)];
    
	// create the button object
	UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.opaque = NO;
	headerLabel.textColor = [UIColor colorWithHexString:COLOR_SKF_RED];
	headerLabel.highlightedTextColor = [UIColor colorWithHexString:COLOR_SKF_RED];
	headerLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_GROUPED_TABLE_HEADER];
	headerLabel.frame = CGRectMake(10.0, 0.0, 300.0, 44.0);
    
	// If you want to align the header text as centered
	// headerLabel.frame = CGRectMake(150.0, 0.0, 300.0, 44.0);
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
	headerLabel.text = [calculationDataManager getTranslationString:@"Previously_used_bearings"]; // i.e. array element
	[customView addSubview:headerLabel];
    [headerLabel release];
    
	return [customView autorelease];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 44.0;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [previousBearingsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell;
    UILabel *nameLabel;
   // UILabel *parametersLabel;
    UIWebView *subScriptLabelView;
    
    //UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:11];
    NSMutableString *nameLabelString;
    
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
        
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 6, 280, 20)];
        nameLabel.userInteractionEnabled = false;
        nameLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];

        nameLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
        nameLabel.backgroundColor = [UIColor clearColor];
        nameLabel.tag = 20;
        [cell addSubview:nameLabel];
        [nameLabel release];
        
      /*  parametersLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        parametersLabel.textColor = [UIColor grayColor];
        parametersLabel.tag = 100;
        parametersLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_SUBTEXT2];
        parametersLabel.frame = CGRectMake(20, 26, 240, 12);
        parametersLabel.backgroundColor = [UIColor clearColor];
        [cell addSubview:parametersLabel];
        [parametersLabel release];*/
        
        subScriptLabelView= [[UIWebView alloc] initWithFrame:CGRectZero];
        subScriptLabelView.frame = CGRectMake(20, 23, 280, 30);

        subScriptLabelView.backgroundColor = [UIColor clearColor];
        subScriptLabelView.userInteractionEnabled = NO;
        subScriptLabelView.opaque = NO;
        subScriptLabelView.tag = 101;
        subScriptLabelView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        [cell addSubview:subScriptLabelView];
        [subScriptLabelView release];
    }
    
    nameLabel = (UILabel *)[cell viewWithTag:20];
   // parametersLabel = (UILabel *)[cell viewWithTag:100];
    
    subScriptLabelView = (UIWebView *)[cell viewWithTag:101];
    
    nameLabel.font       = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
   // parametersLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_SUBTEXT2];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    Product *product = [previousBearingsArray objectAtIndex:indexPath.row];
    NSString *cellValue = product.designation;
    nameLabel.text = cellValue;
    
    nameLabelString = [NSMutableString stringWithString:@""];

    
    for (Property *property in product.properties)
    {
        if (property.uom) {
            
            [nameLabelString appendString:[NSMutableString stringWithFormat:@"%@: %@ %@, ", property.presentation, property.value, property.uom]];
        }
        else {
            
            [nameLabelString appendString:[NSMutableString stringWithFormat:@"%@: %@, ", property.presentation, property.value]];
        }
    }
    
    
    if ([nameLabelString length] != 0)
    {
        [nameLabelString deleteCharactersInRange:NSMakeRange([nameLabelString length]-2, 2)];
    }
    
    [subScriptLabelView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssStringGray, nameLabelString] baseURL:nil];
    
   // NSLog(@"nameLabelString: %@", nameLabelString);
    


    
    
    return cell;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell;
    UILabel *nameLabel;
    UILabel *parametersLabel;
    UIWebView *subScriptLabelView;
    
    UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:11];
    NSMutableString *nameLabelString;
    
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 6, 280, 20)];
        nameLabel.userInteractionEnabled = false;
        nameLabel.backgroundColor = [UIColor clearColor];
        nameLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
        nameLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
        nameLabel.tag = 20;
        [cell addSubview:nameLabel];
        [nameLabel release];
        
        
        subScriptLabelView= [[UIWebView alloc] initWithFrame:CGRectZero];
        subScriptLabelView.frame = CGRectMake(20, 23, 280, 30);
        subScriptLabelView.backgroundColor = [UIColor clearColor];
        subScriptLabelView.userInteractionEnabled = NO;
        subScriptLabelView.opaque = NO;
        subScriptLabelView.tag = 101;
        subScriptLabelView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        [cell addSubview:subScriptLabelView];
        [subScriptLabelView release];
    }
    
    nameLabel = (UILabel *)[cell viewWithTag:20];
    subScriptLabelView = (UIWebView *)[cell viewWithTag:101];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    Product *product = [foundBearingsArray objectAtIndex:indexPath.row];
    NSString *cellValue = product.designation;
    nameLabel.text = cellValue;
    
    nameLabelString = [NSMutableString stringWithString:@""];
    
    
    for (Property *property in product.properties)
    {
        if (property.uom) {
            
            [nameLabelString appendString:[NSMutableString stringWithFormat:@"%@: %@ %@, ", property.presentation, property.value, property.uom]];
        }
        else {
            
            [nameLabelString appendString:[NSMutableString stringWithFormat:@"%@: %@, ", property.presentation, property.value]];
        }
    }
    
    
    if ([nameLabelString length] != 0)
    {
        [nameLabelString deleteCharactersInRange:NSMakeRange([nameLabelString length]-2, 2)];
    }
    
    [subScriptLabelView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssStringGray, nameLabelString] baseURL:nil];
    
    NSLog(@"nameLabelString: %@", nameLabelString);
    
    return cell;
}
*/


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        CalculationDataManager *calculateDataManager = [CalculationDataManager sharedManager];
        [calculateDataManager removeBearingFromBearingsArray:(Product *)[previousBearingsArray objectAtIndex:indexPath.row]];
        
        previousBearingsArray = [calculateDataManager previousBearingsArray];
        [tableView reloadData];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    CalculationDataManager *calculateDataManager = [CalculationDataManager sharedManager];
    previousBearingsArray = [calculateDataManager previousBearingsArray];
    [bearingsListTable reloadData];
}




- (void)viewDidLoad
{
    [super viewDidLoad];
    
    cssStringGray = @"<html> \n"
    "<head> \n"
    "<style type=\"text/css\"> \n"
    "body {font-family: \"SKF Chevin Medium\"; font-size: 9; margin:4px 0px;background-color:transparent; color:#808080; word-wrap:break-word;}\n"
    "a {font-family: \"SKF Chevin Medium\"; font-size: 9; color:#808080; text-decoration: none;}\n"
    "</style> \n"
    "</head> \n";
    
    
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager addObserver:self];
    [calculationDataManager initTranslation:self];
    // Do any additional setup after loading the view from its nib.
    designationSearchBar.delegate = self;
    
    /* NSString *navigationBarTintColor = NAVIGATION_BAR_TINT_COLOR;
     [designationSearchBar setTintColor:[UIColor colorWithHexString:navigationBarTintColor]];*/
    
    for (id subview in designationSearchBar.subviews) {
        if ([subview isKindOfClass:[UITextField class]]) {
            [(UITextField*)subview setFont:[UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_INPUT]];
        }
    }
    
    CalculationDataManager *calculateDataManagerPreviousBearings = [CalculationDataManager sharedManager];
    previousBearingsArray = [calculateDataManagerPreviousBearings previousBearingsArray];
    
    
   // NSLog(@"previousBearingsArray: %lu",(unsigned long)previousBearingsArray.count);
    
    
    
    bearingsListTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, 320, 330) style:UITableViewStyleGrouped];
	[bearingsListTable setDataSource:self];
	[bearingsListTable setDelegate:self];
    [bearingsListTable setBackgroundView:nil];
    [bearingsListTable setBackgroundColor:[UIColor clearColor]];
    [bearingsListTable setOpaque:NO];
    
	[self.view addSubview:bearingsListTable];
    [self.view sendSubviewToBack:bearingsListTable];
    [self.view sendSubviewToBack:viewBackground];
    //bearingsListTable.tag = 500;
    //[bearingsListTable release];
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager removeObserver:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = YES;
    
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:YES animated:YES];
    
    [UIView beginAnimations:@"MyAnimation" context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3]; // 5 seconds
    
    CGRect frame = parentViewContainer.frame;
    frame.origin.y -= 44; // Move view down 100 pixels
    frame.size.height += 44;
    
    CGRect toolbarFrame = segmentedToolBar.frame;
    toolbarFrame.origin.y -= 44;
    
    parentViewContainer.frame = frame;
    segmentedToolBar.frame = toolbarFrame;
    
    [UIView commitAnimations];
    
    UIView *viewDisabled = [[UIView alloc] init];
    viewDisabled.frame = CGRectMake(0, 88, 320, 500);
    viewDisabled.backgroundColor = [UIColor blackColor];
    viewDisabled.alpha = 0.7;
    viewDisabled.tag = 200;
    
    [self.view addSubview:viewDisabled];
    [viewDisabled release];
    
    
    
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = NO;
    [searchBar sizeToFit];
    
    [searchBar setShowsCancelButton:NO animated:YES];
    
    return YES;
}



- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    searchBar.showsScopeBar = NO;
    
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:NO animated:YES];
    
    [UIView beginAnimations:@"MyAnimation" context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3]; // 5 seconds
    
    CGRect frame = parentViewContainer.frame;
    frame.origin.y += 44; // Move view down 100 pixels
    frame.size.height -= 44;
    
    CGRect toolbarFrame = segmentedToolBar.frame;
    toolbarFrame.origin.y += 44;
    
    
    parentViewContainer.frame = frame;
    segmentedToolBar.frame = toolbarFrame;
    
    UIView *disabledView = [self.view viewWithTag:200];
    
    [disabledView removeFromSuperview];
    
    [searchBar resignFirstResponder];
    [UIView commitAnimations];
}



/*- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {

    NSString *NUMBERSPERIOD = @"1234567890abcdefghijklmnopqrestuvwxyz.,";
     NSString *NUMBERS = @"1234567890abcdefghijklmnopqrestuvwxyz";

    NSString *filtered;
    NSCharacterSet *cs;
    if (([searchBar.text rangeOfString:@"."].location == NSNotFound)  && ([searchBar.text rangeOfString:@","].location == NSNotFound))  {
        
        cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERSPERIOD] invertedSet];
        filtered = [[text componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [text isEqualToString:filtered];
        
    }
        
    cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
    filtered = [[text componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [text isEqualToString:filtered];
  
}*/


-(void)dataManagerReturnedProducts:(NSArray *)returnedArray{
    
    foundBearingsArray = [[NSMutableArray alloc] init];
    
    bool oldBearing = false;
    
    SelectBearingViewController *bearingController = nil;
    
    for (UIViewController *v in parentNavigationController.viewControllers)
    {
        if ([v isKindOfClass:[SelectBearingViewController class]])
        {
            bearingController = (SelectBearingViewController *)v;
            oldBearing = true;
        }
    }
    
    if (bearingController == nil)
    {
        bearingController = [[SelectBearingViewController alloc] initWithNibName:@"SelectBearingViewController" bundle:[NSBundle mainBundle]];
    }
    
    for (int i = 0; i < [returnedArray count]; i++) {
        Product *result = [returnedArray objectAtIndex:i];
        [foundBearingsArray addObject:result];
    }
    
    bearingController.foundBearingsArray = foundBearingsArray;
    [parentNavigationController dismissModalViewControllerAnimated:YES];
    if (!oldBearing)
    {
        [parentNavigationController pushViewController:bearingController animated: NO];
    }
    
    [bearingController release];
    [foundBearingsArray release];
    
}

- (void)dataManagerReturnedTranslation:(BOOL)success{
    if (success){
        
        
        
        CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
        
        designationSearchBar.scopeButtonTitles = [NSArray arrayWithObjects:[calculationDataManager getTranslationString:@"Begins"],
                                                  [calculationDataManager getTranslationString:@"Contains"],
                                                  [calculationDataManager getTranslationString:@"Ends"],nil];
        
        
    }
}



@end
