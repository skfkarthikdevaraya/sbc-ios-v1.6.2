//
//  FilterBearingsByDimensionsViewController.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/19/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculationDataManager.h"

@interface FilterBearingsByDimensionsViewController : UIViewController <CalculationDataManagerDelegate, UITextFieldDelegate> 
{
    NSString *_sliderRangeText;
    UIButton *selectBearingTypeButton;
    UIImageView *disclosureButton;
    UINavigationController *filterBearingsNavigationController;
    UIButton *filterBearingsButton;
    UINavigationController *parentNavigationController;
    NSMutableArray *foundBearingsArray;
    
    UITextField *boreDiameterMinInput;
    UITextField *boreDiameterMaxInput;
    UITextField *outerDiameterMinInput;
    UITextField *outerDiameterMaxInput;
    
    UIImageView *bearingDiametersImage;
    
    UIToolbar *keyboardToolBar;
    UITextField *activeField;
    UIScrollView *scrollView;
    
    UILabel *bearingsImageOuterTitle;
    UILabel *bearingsImageBoreTitle;
    UIView *lineView;

    BOOL showingAlert;
    
//    UIScrollView *imageScroll;
//    UIImageView *imageView;
    
}


@property (nonatomic, assign) BOOL showingAlert;
@property (nonatomic, assign) BOOL connected;

//@property (nonatomic, retain)  UIScrollView *imageScroll;
//@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic ,retain) IBOutlet UIView *lineView;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIButton *selectBearingTypeButton; 
@property (nonatomic, retain) UINavigationController *filterBearingsNavigationController;
@property (nonatomic, retain) IBOutlet UIButton *filterBearingsButton;
@property (nonatomic, retain) UINavigationController *parentNavigationController;

@property (nonatomic, retain) IBOutlet UITextField *boreDiameterMinInput;
@property (nonatomic, retain) IBOutlet UITextField *boreDiameterMaxInput; 
@property (nonatomic, retain) IBOutlet UITextField *outerDiameterMinInput; 
@property (nonatomic, retain) IBOutlet UITextField *outerDiameterMaxInput; 

@property (nonatomic, retain) UIImageView *bearingDiametersImage; 

@property (nonatomic, retain) UIToolbar *keyboardToolBar;
@property (nonatomic, retain) UITextField *activeField;

@property (retain, nonatomic) IBOutlet UILabel *bearingTypeLabel;
@property (retain, nonatomic) IBOutlet UILabel *outerMinLabel;
@property (retain, nonatomic) IBOutlet UILabel *outerMaxLabel;
@property (retain, nonatomic) IBOutlet UILabel *boreMinLabel;
@property (retain, nonatomic) IBOutlet UILabel *boreMaxLabel;

@property (nonatomic, retain) IBOutlet UILabel *bearingsImageOuterTitle;
@property (nonatomic, retain) IBOutlet UILabel *bearingsImageBoreTitle;



-(IBAction)selectBearingType:(id)sender;
-(IBAction)filterBearingsButtonClicked:(id)sender;

@end
