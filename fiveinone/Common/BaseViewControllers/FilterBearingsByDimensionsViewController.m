//
//  FilterBearingsByDimensionsViewController.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/19/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "FilterBearingsByDimensionsViewController.h"
#import "SelectBearingTypeViewController_iPhone.h"
#import "FilterBearingsViewController.h"
#import "SelectBearingViewController_iPhone.h"
#import "CalculationDataManager.h"
#import "fiveinoneAppDelegate_iPhone.h"


@implementation FilterBearingsByDimensionsViewController

@synthesize scrollView;
@synthesize selectBearingTypeButton;
@synthesize filterBearingsNavigationController;
@synthesize filterBearingsButton;
@synthesize parentNavigationController;
@synthesize boreDiameterMinInput;
@synthesize boreDiameterMaxInput;
@synthesize outerDiameterMinInput;
@synthesize outerDiameterMaxInput;
@synthesize bearingDiametersImage;
@synthesize keyboardToolBar;
@synthesize activeField;
@synthesize bearingTypeLabel;
@synthesize outerMinLabel;
@synthesize outerMaxLabel;
@synthesize boreMinLabel;
@synthesize boreMaxLabel;
@synthesize bearingsImageBoreTitle;
@synthesize bearingsImageOuterTitle;
@synthesize connected;
@synthesize lineView;
//@synthesize imageScroll;
//@synthesize imageView;

-(IBAction)selectBearingType:(id)sender {
    
    if (!connected) {
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        [calculationDataManager showAlertForNetworkConnection];
        return;
        
        
    }
    
    SelectBearingTypeViewController_iPhone *selectBearingTypeViewController = [[SelectBearingTypeViewController_iPhone alloc] initWithNibName:@"SelectBearingTypeViewController" bundle:[NSBundle mainBundle]];
    
    selectBearingTypeViewController.filterBearingsNavigationController = filterBearingsNavigationController;
    
    //fiveinoneAppDelegate_iPhone *rootDelegate = (fiveinoneAppDelegate_iPhone *)[UIApplication sharedApplication].delegate;
    
    [parentNavigationController pushViewController:selectBearingTypeViewController animated:YES];
    [selectBearingTypeViewController release];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        bearingDiametersImage.tag = -1;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager addObserver:self];
    [calculationDataManager initTranslation:self];
    
    if (calculationDataManager.selectedBearingType != nil) {
        [selectBearingTypeButton setTitle:calculationDataManager.selectedBearingType.presentation forState:UIControlStateNormal];
        [selectBearingTypeButton setTitle:calculationDataManager.selectedBearingType.presentation forState:UIControlStateHighlighted];
        [selectBearingTypeButton setTitle:calculationDataManager.selectedBearingType.presentation forState:UIControlStateDisabled];
        [selectBearingTypeButton setTitle:calculationDataManager.selectedBearingType.presentation forState:UIControlStateSelected];
    } 
    
    disclosureButton = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosureButton.png"]] autorelease];
    disclosureButton.frame = CGRectMake(0,0, disclosureButton.frame.size.height,disclosureButton.frame.size.height);
    disclosureButton.contentMode = UIViewContentModeCenter;
    disclosureButton.center = CGPointMake(270, 19);
    [selectBearingTypeButton addSubview:disclosureButton];
    
    
    /*boreDiameterMinInput.keyboardType = UIKeyboardTypeDecimalPad;
    boreDiameterMaxInput.keyboardType = UIKeyboardTypeDecimalPad; 
    outerDiameterMinInput.keyboardType = UIKeyboardTypeDecimalPad;
    outerDiameterMaxInput.keyboardType = UIKeyboardTypeDecimalPad;*/
    
    
    boreDiameterMinInput.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    boreDiameterMaxInput.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    outerDiameterMinInput.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    outerDiameterMaxInput.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    
    
    boreDiameterMinInput.clearButtonMode = UITextFieldViewModeWhileEditing;
    boreDiameterMaxInput.clearButtonMode = UITextFieldViewModeWhileEditing; 
    outerDiameterMinInput.clearButtonMode = UITextFieldViewModeWhileEditing;
    outerDiameterMaxInput.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    boreDiameterMinInput.text = [calculationDataManager getPreviousInput:@"diameterInnerMin"];
    boreDiameterMaxInput.text = [calculationDataManager getPreviousInput:@"diameterInnerMax"];
    outerDiameterMinInput.text = [calculationDataManager getPreviousInput:@"diameterOuterMin"];
    outerDiameterMaxInput.text = [calculationDataManager getPreviousInput:@"diameterOuterMax"];
    
    boreDiameterMinInput.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_INPUT];
    boreDiameterMaxInput.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_INPUT];
    outerDiameterMinInput.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_INPUT];
    outerDiameterMaxInput.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_INPUT];
    
    outerMinLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON2];
    outerMaxLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON2];
    boreMinLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON2];
    boreMaxLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON2];
    
    outerMinLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    outerMaxLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    boreMinLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    boreMaxLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    
    boreDiameterMinInput.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    boreDiameterMaxInput.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    outerDiameterMinInput.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    outerDiameterMaxInput.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    
    bearingsImageBoreTitle.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    bearingsImageOuterTitle.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    
    selectBearingTypeButton.titleLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_GREY];
    //filterBearingsButton.titleLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    
    bearingTypeLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    
    bearingsImageBoreTitle.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON];
    bearingsImageOuterTitle.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON];
    
    selectBearingTypeButton.titleLabel.font = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_BUTTON];
    //filterBearingsButton.titleLabel.font = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_BUTTON_LARGE];
    selectBearingTypeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    
    bearingTypeLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];

    // For Displaying CrossSection Bearing Type Image at Dimensions
    
   // [calculationDataManager initTranslation:self];
    bearingDiametersImage = [[[UIImageView alloc] initWithFrame:CGRectMake(10, 165, 300, 175)] autorelease];
    
    [self.view addSubview:bearingDiametersImage];


    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:RKReachabilityDidChangeNotification
                                               object:nil];
    
    showingAlert = false;
    connected = true;
    
    /*** TO add horizontal line the dimensions screen for bearing type button ***/
    /*UIView *lineTop = [[UIView alloc] initWithFrame:CGRectMake(27, 1, 285, 1)];
    lineTop.backgroundColor = [UIColor colorWithWhite:0.7 alpha:1];
    [self.view addSubview:lineTop];
    [lineTop release];
    
    UIView *lineBottom = [[UIView alloc] initWithFrame:CGRectMake(27, 44, 285, 1)];
    lineBottom.backgroundColor = [UIColor colorWithWhite:0.7 alpha:1];
    [self.view addSubview:lineBottom];
    [lineBottom release];*/
    
    /*** END ***/
    
}

- (void)reachabilityChanged:(NSNotification*)notification {
    RKReachabilityObserver* observer = (RKReachabilityObserver*)[notification object];
    
    if (![observer isNetworkReachable])
    {
        
        
        // [self setSearchingModeEnd];
        connected = false;
        [self showingAlert];
        DLog(@"Disconnected");
    }
    if ([observer isNetworkReachable]) {
        connected = true;
        DLog(@"Connected");
    }
}



- (void)moveKeyboard:(int)height
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= (height + 44);
    CGPoint origin = activeField.frame.origin;
    origin.y -= scrollView.contentOffset.y;
    if (!CGRectContainsPoint(aRect, origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-(aRect.size.height)); 
        [scrollView setContentOffset:scrollPoint animated:YES];
    }
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
    [self moveKeyboard:260+44];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}


- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [self moveKeyboard:kbSize.height+44];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}


- (void)nextButton:(id)sender
{
    if (activeField == outerDiameterMinInput) {
        [outerDiameterMaxInput becomeFirstResponder];
    } else if (activeField == outerDiameterMaxInput) {
        [boreDiameterMinInput becomeFirstResponder];
    } else if (activeField == boreDiameterMinInput) {
        [boreDiameterMaxInput becomeFirstResponder];
    } else if (activeField == boreDiameterMaxInput) {
        [outerDiameterMinInput becomeFirstResponder];
    }
    [self moveKeyboard:260+44];
}

- (void)prevButton:(id)sender
{    
    if (activeField == outerDiameterMinInput) {
        [boreDiameterMaxInput becomeFirstResponder];
    } else if (activeField == boreDiameterMaxInput) {
        [boreDiameterMinInput becomeFirstResponder];
    } else if (activeField == boreDiameterMinInput) {
        [outerDiameterMaxInput becomeFirstResponder];
    } else if (activeField == outerDiameterMaxInput) {
        [outerDiameterMinInput becomeFirstResponder];
    }
    [self moveKeyboard:260+44];
}

- (void)viewDidUnload
{
    [bearingDiametersImage release];
    bearingDiametersImage = nil;

    
    [scrollView release];
    scrollView = nil;
    [self setScrollView:nil];
    [bearingTypeLabel release];
    bearingTypeLabel = nil;
    [self setBearingTypeLabel:nil];
    [self setBearingTypeLabel:nil];
    [self setBearingTypeLabel:nil];
    [self setBearingTypeLabel:nil];
    [bearingTypeLabel release];
    bearingTypeLabel = nil;
    [self setBearingTypeLabel:nil];
    [self setOuterMinLabel:nil];
    [self setOuterMaxLabel:nil];
    [self setBoreMinLabel:nil];
    [self setBoreMaxLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    if (calculationDataManager.selectedBearingType) {
        
        [selectBearingTypeButton setTitle:calculationDataManager.selectedBearingType.presentation forState:UIControlStateNormal];
        [selectBearingTypeButton setTitle:calculationDataManager.selectedBearingType.presentation forState:UIControlStateHighlighted];
        [selectBearingTypeButton setTitle:calculationDataManager.selectedBearingType.presentation forState:UIControlStateDisabled];
        [selectBearingTypeButton setTitle:calculationDataManager.selectedBearingType.presentation forState:UIControlStateSelected];
    } else {
        
    }
    
 
    
    [self loadBearingImage];

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}




/** CROSS SECTION IMAGE @ DIMENSIONS*/
- (void)loadBearingImage{
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    int currentLoadedImage = bearingDiametersImage.tag;
    int needToLoadImage = [calculationDataManager.selectedBearingType.bearingTypeId intValue];
    BOOL needsReload = currentLoadedImage != needToLoadImage;
    
    if (needsReload){
        
        float scale = [UIScreen mainScreen].scale;
        CGSize imageViewSize = CGSizeMake(320, 285);
        CGSize imageSize = CGSizeMake(scale * (imageViewSize.width - 1), scale * (imageViewSize.height - 1));
        
        NSMutableString* imagePath = [NSString stringWithFormat:@"/Image?bearingTypeId=%@&measurement=%@&withDia=true&width=%d&height=%d",
                                      calculationDataManager.selectedBearingType.bearingTypeId,[calculationDataManager selectedMeasurement],
                                      (int)imageSize.width, (int)imageSize.height];
        NSString* imageUrl = [NSString stringWithFormat:@"%@%@", calculationDataManager.baseURL, imagePath];
        NSData* imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
        
        if (imageData != nil)
        {
            UIImage * img = [UIImage imageWithData:imageData];
            img = [UIImage imageWithCGImage:img.CGImage scale:scale orientation:img.imageOrientation];
            [bearingDiametersImage setImage:img];
            
            self.bearingDiametersImage.contentMode = UIViewContentModeScaleAspectFit;
        }
        else
        {            
            
            UIImage * img = [[[UIImage alloc] initWithData:imageData] autorelease];

            bearingDiametersImage.image = img;
            
        }
    }
    else
    {
        
        float scale = [UIScreen mainScreen].scale;
        CGSize imageViewSize = CGSizeMake(320, 280);
        CGSize imageSize = CGSizeMake(scale * (imageViewSize.width - 1), scale * (imageViewSize.height - 1));
        
        NSMutableString* imagePath = [NSString stringWithFormat:@"/Image?bearingTypeId=%d&measurement=%@&withDia=true&width=%d&height=%d",2,
                                      [calculationDataManager selectedMeasurement],
                                      (int)imageSize.width, (int)imageSize.height];
        NSString* imageUrl = [NSString stringWithFormat:@"%@%@", calculationDataManager.baseURL, imagePath];
        NSData* imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
        
        if (imageData != nil){
            UIImage * img = [UIImage imageWithData:imageData];
            img = [UIImage imageWithCGImage:img.CGImage scale:scale orientation:img.imageOrientation];
            [bearingDiametersImage setImage:img];
            self.bearingDiametersImage.contentMode = UIViewContentModeScaleAspectFit;
        }
        
    }
    
}

- (void)viewWillDisappear:(BOOL)animated
{

    NSMutableDictionary *inputs = [NSMutableDictionary dictionaryWithKeysAndObjects:@"diameterInnerMin", boreDiameterMinInput,
                                   @"diameterInnerMax", boreDiameterMaxInput, 
                                   @"diameterOuterMin", outerDiameterMinInput, 
                                   @"diameterOuterMax", outerDiameterMaxInput, nil];
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    for (id key in inputs) 
    {
        UITextField *inputField = [inputs valueForKey:key];
        if ([inputField.text length] != 0)
        {
            [calculationDataManager setPreviousInput:key withValue:inputField.text];
        }
    }
}

- (void) filterBearingsButtonClicked:(id)sender {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];

    if (!connected) {
        [calculationDataManager showAlertForNetworkConnection];
        return;
        
        
    }

    if (calculationDataManager.selectedBearingType.name == nil) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[calculationDataManager getTranslationString:@"Please_select_a_bearing_type."] delegate:self cancelButtonTitle:[calculationDataManager getTranslationString:@"OK"] otherButtonTitles:nil];
        [alert show];
        [alert release];
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[calculationDataManager getTranslationString:@"Please_Fill_in_All_Required_Fields."] delegate:self cancelButtonTitle:[calculationDataManager getTranslationString:@"OK"] otherButtonTitles:nil];
        
        bool stopIt = false;
        
        NSMutableDictionary *sendParams = [NSMutableDictionary dictionary];
        
       // Below code is commented to allow user to find bearing by enter either bore/outer/Both diameters 
        /*NSMutableDictionary *inputs = [NSMutableDictionary dictionaryWithKeysAndObjects:@"diameterInnerMin", boreDiameterMinInput,
                                       @"diameterInnerMax", boreDiameterMaxInput,
                                       @"diameterOuterMin", outerDiameterMinInput,
                                       @"diameterOuterMax", outerDiameterMaxInput, nil];        
        for (id key in inputs)
        {
            UITextField *inputField = [inputs valueForKey:key];
            if ([inputField.text length] == 0){
                
                [alert show];
                stopIt = true;
                break;
                
            }else {
                [calculationDataManager setPreviousInput:key withValue:inputField.text];
                NSString *replacedString = [inputField.text stringByReplacingOccurrencesOfString:@"," withString:@"."];
                [sendParams setObject:replacedString forKey:key];
            }
        }*/

        NSMutableDictionary *inputs = [NSMutableDictionary dictionaryWithKeysAndObjects:@"diameterInnerMin", boreDiameterMinInput,
                                       @"diameterInnerMax", boreDiameterMaxInput,
                                       @"diameterOuterMin", outerDiameterMinInput,
                                       @"diameterOuterMax", outerDiameterMaxInput, nil];        
        for (id key in inputs)
        {
            UITextField *inputField = [inputs valueForKey:key];
            
            if (([outerDiameterMinInput.text length] == 0 || [outerDiameterMaxInput.text length] == 0) &&
                ([boreDiameterMinInput.text length] == 0 || [boreDiameterMaxInput.text length] == 0)) {
                
                [alert show];
                stopIt = true;
                break;
                
            }else  if ([inputField.text length] != 0){
                    
                [calculationDataManager setPreviousInput:key withValue:inputField.text];
                NSString *replacedString = [inputField.text stringByReplacingOccurrencesOfString:@"," withString:@"."];
                [sendParams setObject:replacedString forKey:key];
            }
            
        }
        
        
        [sendParams setObject:[calculationDataManager.selectedBearingType.name lowercaseString] forKey:@"bearingType"];
        
        if(!calculationDataManager.mutlipleCalculation) {
            [sendParams setObject:calculationDataManager.selectedCalculation.calculationId forKey:@"calculation"];
        } else
        {
            Calculation *calc = [calculationDataManager.selectedCalculationsArray objectAtIndex:0];
            [sendParams setObject:calc.calculationId forKey:@"calculation"];
        }
        
        
        if (!stopIt)
        {
            [calculationDataManager getProducts:sendParams userData:self];
        }
        
        [alert release];
        
    }
}




- (void)calculationDataManager:(CalculationDataManager *)calculationDataManager returnedArray:(NSArray *)returnedArray {
    
    if (calculationDataManager.lastUserData != self){
        return;
    }
    
    foundBearingsArray = [[NSMutableArray alloc] init];
    
    bool oldBearing = false;
    
    SelectBearingViewController_iPhone *bearingController = nil;
    
    for (UIViewController *v in parentNavigationController.viewControllers)
    {
        if ([v isKindOfClass:[SelectBearingViewController_iPhone class]])
        {
            bearingController = (SelectBearingViewController_iPhone *)v;
            oldBearing = true;
        }
    }
    
    if (bearingController == nil)
    {
        bearingController = [[SelectBearingViewController_iPhone alloc] initWithNibName:@"SelectBearingViewController" bundle:[NSBundle mainBundle]];
    }
    
    for (int i = 0; i < [returnedArray count]; i++) {
        Product *result = [returnedArray objectAtIndex:i];
        [foundBearingsArray addObject:result];
    }
    
    bearingController.foundBearingsArray = foundBearingsArray;
    [parentNavigationController dismissModalViewControllerAnimated:YES];
    if (!oldBearing)
    {
        fiveinoneAppDelegate_iPhone *rootDelegate = (fiveinoneAppDelegate_iPhone *)[UIApplication sharedApplication].delegate;
        [rootDelegate.navigationController pushViewController:bearingController animated: YES];
        [bearingController release];
    }
}

-(void) done {
    [[self.view superview] endEditing:YES];
}

- (void)dealloc {
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager removeObserver:(self)];
    
    [scrollView release];
    [bearingTypeLabel release];
    [outerMinLabel release];
    [outerMaxLabel release];
    [boreMinLabel release];
    [boreMaxLabel release];
    [super dealloc];
}

- (void)dataManagerReturnedTranslation:(BOOL)success{
    if (success){
        CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];

        if (calculationDataManager.selectedBearingType == nil) {
            [selectBearingTypeButton setTitle:[calculationDataManager getTranslationString:@"No_bearing_type_selected."] forState:UIControlStateNormal];
            [selectBearingTypeButton setTitle:[calculationDataManager getTranslationString:@"No_bearing_type_selected."]forState:UIControlStateHighlighted];
            [selectBearingTypeButton setTitle:[calculationDataManager getTranslationString:@"No_bearing_type_selected."] forState:UIControlStateDisabled];
            [selectBearingTypeButton setTitle:[calculationDataManager getTranslationString:@"No_bearing_type_selected."] forState:UIControlStateSelected];
        }
    
        outerMinLabel.text = [calculationDataManager getTranslationString:@"OuterDiameterMin"];
        outerMaxLabel.text = [calculationDataManager getTranslationString:@"OuterDiameterMax"];
        boreMinLabel.text = [calculationDataManager getTranslationString:@"InnerDiameterMin"];
        boreMaxLabel.text = [calculationDataManager getTranslationString:@"InnerDiameterMax"];
        
        bearingTypeLabel.text = [calculationDataManager getTranslationString:@"Bearing_type"];        
        
        [filterBearingsButton setTitle:[calculationDataManager getTranslationString:@"Filter_bearings"] forState:UIControlStateNormal];
        [filterBearingsButton setTitle:[calculationDataManager getTranslationString:@"Filter_bearings"] forState:UIControlStateHighlighted];
        [filterBearingsButton setTitle:[calculationDataManager getTranslationString:@"Filter_bearings"] forState:UIControlStateDisabled];
        [filterBearingsButton setTitle:[calculationDataManager getTranslationString:@"Filter_bearings"] forState:UIControlStateSelected];
        
    }
}

@end
