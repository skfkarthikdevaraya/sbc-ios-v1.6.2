//
//  FilterBearingsViewController.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/22/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculationDataManager.h"

@interface FilterBearingsViewController : UIViewController<CalculationDataManagerDelegate> {    
    UIBarButtonItem *cancelButton;
    UISegmentedControl *segmentedControl;
    UIToolbar *segmentedToolBar;
    UIView *viewContainer;
    UIViewController *currentViewController;
}

@property (nonatomic, retain) IBOutlet UIBarButtonItem *cancelButton;
@property (nonatomic, retain) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic, retain) IBOutlet UIView *viewContainer;
@property (nonatomic, retain) IBOutlet UIToolbar *segmentedToolBar;

@end
