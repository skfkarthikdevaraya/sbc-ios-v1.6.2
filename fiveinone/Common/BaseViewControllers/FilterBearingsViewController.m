//
//  FilterBearingsViewController.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/22/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "FilterBearingsViewController.h"
#import "FilterBearingsByDesignationViewController.h"
#import "FilterBearingsByDimensionsViewController.h"

@implementation FilterBearingsViewController

@synthesize cancelButton;
@synthesize segmentedControl;
@synthesize viewContainer;
@synthesize segmentedToolBar;
//@synthesize filterBearingsNavigationController;
//@synthesize parentNavigationController;

- (void)cancelFilterBearings:(id)sender {
    //[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];

}


- (void)changeUISegmentFont:(UIView*) aView selectedText:(NSString*)text
{
    NSString* typeName = NSStringFromClass ([aView class]);
    
    if ([typeName compare:@"UISegmentLabel" options:NSLiteralSearch] == NSOrderedSame) 
    {
        UILabel* label = (UILabel*)aView;
        if ([label.text isEqualToString:text])
        {
            label.textColor = [UIColor colorWithHexString:TEXT_COLOR_WHITE];
            label.shadowColor = [UIColor colorWithWhite:1.0 alpha:0];
        }
        else
        {
            label.textColor = [UIColor colorWithHexString:TEXT_COLOR_GREY];
            label.shadowColor = [UIColor colorWithWhite:1.0 alpha:0];
        }
        
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setFont:[UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_SEGMENTED_CONTROL]];
        
    }
    
    NSArray* subs = [aView subviews];
    NSEnumerator* iter = [subs objectEnumerator];
    UIView* subView;
    
    while (subView = [iter nextObject]) 
    {
        [self changeUISegmentFont:subView selectedText:text];
    }
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    //self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"main_background_grey.png"]];
    
    self.view.backgroundColor = [UIColor whiteColor];

    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager initTranslation:self];    
    
   /* NSString *navigationBarTintColor = NAVIGATION_TINT_COLOR;
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithHexString:navigationBarTintColor]];*/
    
    NSString *segmentedControlTintColor = TITLE_TINT_COLOR;

    [self.segmentedControl setTintColor:[UIColor colorWithHexString:segmentedControlTintColor]];
    [self.segmentedToolBar setTintColor:[UIColor colorWithHexString:segmentedControlTintColor]];

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dataManagerReturnedTranslation:(BOOL)success{
    if (success){
        CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
        UILabel *skfChevinTitleLabel = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
        skfChevinTitleLabel.backgroundColor = [UIColor clearColor];
        skfChevinTitleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_NAVIGATION_TITLE];
       // skfChevinTitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        skfChevinTitleLabel.textAlignment = NSTextAlignmentCenter;
        
        
        /*** iOS - 7 To change the navigation title color in the settings popviewcontroller ***/
        
        //Old code
        skfChevinTitleLabel.textColor = [UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f]; // change this color
        
//        if ([[[UIDevice currentDevice] systemVersion] floatValue] >=7) {
//            
//            NSString *navigationBarTintColoriOS7 = TITLE_TINT_COLOR_IOS7;
//            skfChevinTitleLabel.textColor = [UIColor colorWithHexString:navigationBarTintColoriOS7];
//            
//        }else {
//            
//            skfChevinTitleLabel.textColor = [UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f]; // change this color
//        }
        /*** END ***/

        self.navigationItem.titleView = skfChevinTitleLabel;
        skfChevinTitleLabel.text = [calculationDataManager getTranslationString:@"Filter_bearings"];
        [skfChevinTitleLabel sizeToFit];
        self.title = [calculationDataManager getTranslationString:@"Filter_bearings"];
        
        
        
        [segmentedControl setTitle:[calculationDataManager getTranslationString:@"Designation"]forSegmentAtIndex:0];
        [segmentedControl setTitle:[calculationDataManager getTranslationString:@"Dimensions"]forSegmentAtIndex:1];
        
        UIFont *boldFont = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_BUTTON];
        NSDictionary *fontDict = [[NSDictionary alloc] initWithObjectsAndKeys:boldFont, NSFontAttributeName, nil];
        [segmentedControl setTitleTextAttributes:fontDict forState:UIControlStateNormal];

        
        
        
        
            }
}

@end
