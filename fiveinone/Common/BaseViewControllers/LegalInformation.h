//
//  LegalInformation.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 11/2/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LegalInformation : UIViewController {
    UIButton* btnAgree;
    UIWebView* legalText;
    UIToolbar* toolBar;
    UISegmentedControl* segmentedControl;
    NSArray* legalHeaders;
    NSString *cssString;
    UILabel *appTitleLabel;
    UILabel *appVersionLabel;
    
    BOOL _withoutAgree;
    
}

@property (nonatomic,retain) IBOutlet UIButton *btnAgree;
@property (nonatomic,retain) IBOutlet UIWebView *legalText;
@property (nonatomic,retain) IBOutlet UIToolbar *toolBar;
@property (nonatomic,retain) NSString *cssString;
@property (nonatomic, retain) IBOutlet UILabel *appTitleLabel;
@property (nonatomic, retain) IBOutlet UILabel *appVersionLabel;
@property (nonatomic, retain) IBOutlet UIView *lineView_legal;
@property (nonatomic, assign) BOOL withoutAgree;

- (void)pickOne:(id)sender;
- (void)gotTranslation:(id)sender;

@end
