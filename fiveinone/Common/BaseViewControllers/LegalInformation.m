//
//  LegalInformation.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 11/2/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "LegalInformation.h"
#import "SettingsView.h"
#import <QuartzCore/QuartzCore.h>
#import "CalculationDataManager.h"


@implementation LegalInformation

@synthesize btnAgree, legalText, toolBar, cssString;
@synthesize appTitleLabel;
@synthesize appVersionLabel;
@synthesize withoutAgree = _withoutAgree;


-(void)dealloc{
    [btnAgree release];
    [legalText release];
    [legalHeaders release];
    [toolBar release];
	[super dealloc];
}

-(NSString *)fixBR:(NSString *)inString
{
    return [inString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"];
}

- (void) pickOne:(id)sender{
    UISegmentedControl *segmentedControlr = (UISegmentedControl *)sender;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        //[segmentedControlr setBackgroundColor:[UIColor colorWithHexString:NAVIGATION_BACKGROUND_COLOR]];
        //[segmentedControlr setTintColor:[UIColor whiteColor]];

    }
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    switch ([segmentedControlr selectedSegmentIndex]) {
        case 0:
            [legalText loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, [self fixBR:[calculationDataManager getTranslationString:@"LEGAL.OWNERSHIP_TEXT"]]] baseURL:[NSURL URLWithString:@""]];
            break;
        case 1:
            [legalText loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, [self fixBR:[calculationDataManager getTranslationString:@"LEGAL.PRIVACY_TEXT"]]] baseURL:[NSURL URLWithString:@""]];
            break;
        case 2:
            [legalText loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, [self fixBR:[calculationDataManager getTranslationString:@"LEGAL.TERMS_TEXT"]]] baseURL:[NSURL URLWithString:@""]];
            break;
        default:
            break;
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _withoutAgree = FALSE;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)changeUISegmentFont:(UIView*) aView selectedText:(NSString*)text
{
	NSString* typeName = NSStringFromClass ([aView class]);
    
	if ([typeName compare:@"UISegmentLabel" options:NSLiteralSearch] == NSOrderedSame)
    {
		UILabel* label = (UILabel*)aView;
        if ([label.text isEqualToString:text])
        {
            /*** iOS - 7 Label was white color when u pick any segment tab. So commenting the label color ***/
            // label.textColor = [UIColor colorWithHexString:TEXT_COLOR_WHITE];
            label.shadowColor = [UIColor colorWithWhite:1.0 alpha:0];
        }
        else
        {
            //label.textColor = [UIColor colorWithHexString:TEXT_COLOR_WHITE];
            label.shadowColor = [UIColor colorWithWhite:1.0 alpha:0];
        }
        
		[label setTextAlignment:NSTextAlignmentCenter];
		//[label setFont:[UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_SEGMENTED_CONTROL]];
        
	}
    
	NSArray* subs = [aView subviews];
	NSEnumerator* iter = [subs objectEnumerator];
	UIView* subView;
    
	while (subView = [iter nextObject])
    {
		[self changeUISegmentFont:subView selectedText:text];
	}
}

- (void)didChangeSegmentControl:(UISegmentedControl *)control {
    
    NSUInteger i = control.selectedSegmentIndex;
    NSString* selectedText = [legalHeaders objectAtIndex:i];
    [self changeUISegmentFont:control selectedText:selectedText];
}


- (void)initializeSegmentedControl {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    legalHeaders= [[NSArray alloc]
                   initWithObjects:
                   [calculationDataManager getTranslationString:@"TermsAndConditions"],
                   [calculationDataManager getTranslationString:@"Privacy_Policy"],
                   [calculationDataManager getTranslationString:@"Ownership"], nil];
    
	segmentedControl = [[UISegmentedControl alloc] initWithItems:legalHeaders];
    segmentedControl.frame = CGRectMake(10, 6, 300, 30);
	segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    [segmentedControl addTarget:self action:@selector(didChangeSegmentControl:) forControlEvents:UIControlEventValueChanged];
   // toolBar.opaque = NO;
    toolBar.backgroundColor = [UIColor whiteColor];
    
    segmentedControl.opaque = NO;
    segmentedControl.backgroundColor = [UIColor whiteColor];
    segmentedControl.tintColor =  [UIColor colorWithHexString:SEGMENTED_CONTROL_TINT];
    
    
   /* if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        [segmentedControl setBackgroundColor:[UIColor colorWithHexString:NAVIGATION_BACKGROUND_COLOR]];
        segmentedControl.tintColor =  [UIColor whiteColor];
        
        [segmentedControl.layer setCornerRadius:4.0f];
        [segmentedControl.layer setBorderColor:[UIColor colorWithHexString:NAVIGATION_BACKGROUND_COLOR].CGColor];
        [segmentedControl.layer setBorderWidth:1.5f];
        //    [segmentedControl.layer setShadowColor:[UIColor blackColor].CGColor];
        //    [segmentedControl.layer setShadowOpacity:0.8];
        //    [segmentedControl.layer setShadowRadius:3.0];
        //    [segmentedControl.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
        
        
    }*/
    
    
    UIFont *boldFont = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_LEGAL];
    NSDictionary *fontDict = [[NSDictionary alloc] initWithObjectsAndKeys:boldFont, NSFontAttributeName, nil];
    [segmentedControl setTitleTextAttributes:fontDict forState:UIControlStateNormal];
    
    [segmentedControl setWidth:130 forSegmentAtIndex:0];
    [segmentedControl setWidth:90 forSegmentAtIndex:1];
    [segmentedControl setWidth:80 forSegmentAtIndex:2];
    
    [segmentedControl addTarget:self
	                     action:@selector(pickOne:)
	           forControlEvents:UIControlEventValueChanged];
    
    toolBar.frame = CGRectMake(0, -2, 320, 40);
    segmentedControl.selectedSegmentIndex = 0;
    
    
    [legalText loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, [self fixBR:[calculationDataManager getTranslationString:@"LEGAL.OWNERSHIP_TEXT"]]] baseURL:[NSURL URLWithString:@""]];
    
    [self changeUISegmentFont:segmentedControl selectedText:[legalHeaders objectAtIndex:0]];
    
    [toolBar addSubview:segmentedControl];

    [segmentedControl release];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //DialSetAppDelegate *appDelegate = (DialSetAppDelegate*)[[UIApplication sharedApplication] delegate];
    if (_withoutAgree)
    {
        btnAgree.hidden = YES;
        // Make frame a bit higher
        CGRect rect = legalText.frame;
        //rect.size.height = rect.size.height + 55;
        rect.size.height = rect.size.height + 50;
        legalText.frame = rect;
        
    }
    else
    {
        self.navigationItem.hidesBackButton = YES;
    }
    
    
}

- (void)gotTranslation:(id)sender
{
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [btnAgree setTitle:[calculationDataManager getTranslationString:@"I_Agree"] forState:UIControlStateNormal];
    appTitleLabel.text = [calculationDataManager getTranslationString:@"Application_name"];
    
    [self initializeSegmentedControl];
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    if (_withoutAgree){
        [btnAgree setHidden:TRUE];
        
        
    }
    
    
    cssString = @"<html> \n"
    "<head> \n"
    "<style type=\"text/css\"> \n"
    "body {font-family: \"SKF Chevin Medium\"; font-size: 12; padding:15px; margin:4px 0px;background-color:white; color:#333333 word-wrap:break-word;}\n"
    "</style> \n"
    "</head> \n";
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    //NSString *date = [infoDictionary objectForKey:@"CFBuildDate"];
    NSString *version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *build = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    appTitleLabel.font      = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_APPTITLE];
    appVersionLabel.font    = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_APPVERSION];
    appTitleLabel.textColor = [UIColor colorWithRed:218.0/255.0 green:30.0/255.0 blue:60.0/255.0 alpha:1];
    appVersionLabel.textColor = [UIColor colorWithRed:94.0/255.0 green:94.0/255.0 blue:94.0/255.0 alpha:1];
    
    
    NSString * appVersionString = [NSString stringWithFormat:@"Version: %@ (Build: %@)",version,build];
    appTitleLabel.text = APP_TITLE;
    appVersionLabel.text = appVersionString;
    
    //btnAgree.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
    // Proceed button with iOS 6 style | for iOS7 style just comment below code
    /*UIImage *agreeButtonImage = [UIImage imageNamed:@"ipad_blue_button.png"];
     UIImage *agreeButtonImageStrechable = [agreeButtonImage stretchableImageWithLeftCapWidth:12 topCapHeight:0];
     [btnAgree setBackgroundImage:agreeButtonImageStrechable forState:UIControlStateNormal];
     [btnAgree setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateSelected];
     [btnAgree setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateNormal];
     btnAgree.titleLabel.font = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_BUTTON_LARGE];
     btnAgree.titleLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_WHITE];*/
    
    
    legalText.layer.cornerRadius = 8;
    legalText.layer.masksToBounds = YES;
    legalText.opaque = NO;
    legalText.backgroundColor = [UIColor clearColor];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    
    
    if ([[prefs valueForKey:@"firstrun"] isEqualToString:@"NO"]) {
        [self gotTranslation:nil];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(gotTranslation:)
                                                     name:@"kGotTranslation"
                                                   object:nil];
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
