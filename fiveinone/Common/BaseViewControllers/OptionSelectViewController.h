//
//  OptionsViewController.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/26/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculationDataManager.h"

@protocol OptionsDelegate;

@interface OptionSelectViewController : UIViewController <UIWebViewDelegate, CalculationDataManagerDelegate> {
    NSInteger familyNr;
    NSArray *optionsArray;
    UITableView *optionsTable;
    NSString *parameterName;
    NSString *parameterPresentation;
    NSIndexPath *parentIndexPath;
    NSString *previousSelectedOption;
    
}

@property (nonatomic, assign) NSInteger familyNr;
@property (nonatomic, retain) NSArray *optionsArray;
@property (nonatomic, retain) IBOutlet UITableView *optionsTable;
@property (nonatomic, assign) id <OptionsDelegate> delegate;
@property (nonatomic, retain) NSString *parameterName;
@property (nonatomic, retain) NSString *parameterPresentation;
@property (nonatomic, retain) NSIndexPath *parentIndexPath;
@property (nonatomic, retain) NSString *previousSelectedOption;
@property (nonatomic, retain) NSString *cssString;
@property (nonatomic, retain) NSString *cssStringDescription;

@end


@protocol OptionsDelegate <NSObject>
// shoppinglistname == nil on cancel
- (void)selectedOption:(NSString *)selectedOption withIndex:(NSInteger)selectedIndex andName:(NSString *)name andIndexPath:indexPath;

@end

