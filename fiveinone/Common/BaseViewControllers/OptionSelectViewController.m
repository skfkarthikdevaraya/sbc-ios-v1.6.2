//
//  OptionsViewController.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/26/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "OptionSelectViewController.h"
#import "CalculationDataManager.h"
#import "TitleManager.h"
#import "Option.h"
#import "Family.h"
#import "NSString+StripHTML.h"

@implementation OptionSelectViewController

@synthesize familyNr;
@synthesize optionsArray;
@synthesize optionsTable;
@synthesize delegate;
@synthesize parameterName;
@synthesize parameterPresentation;
@synthesize parentIndexPath; 
@synthesize previousSelectedOption;
@synthesize cssString;
@synthesize cssStringDescription;

bool lubClean_parameter_disable = FALSE;

BOOL lubClean_grease_disable = FALSE;
BOOL lubClean_oil_disable = FALSE;

-(float) calculateHeightOfTextFromWidth:(NSString*)text: (UIFont*)withFont: (float)width :(UILineBreakMode)lineBreakMode
{
    NSString *textAdd = [text stringByAppendingString:@"MM"];
    [text retain];
    [withFont retain];
    CGSize suggestedSize = [textAdd sizeWithFont:withFont constrainedToSize:CGSizeMake(width, FLT_MAX) lineBreakMode:lineBreakMode];
    CGSize lineHeightSize = [textAdd sizeWithFont:withFont];
    
    float height = 0;
    if (![text isEqualToString:@""])
    {
        CGFloat rows = suggestedSize.height / lineHeightSize.height;
        height = rows * (lineHeightSize.height + 2);
        height += 5;
    }
    [text release];
    [withFont release];
    
    return height;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    cssString = @"<html> \n"
     "<head> \n"
     "<style type=\"text/css\"> \n"
     "body {font-family: \"SKF Chevin Medium\"; font-size: 14; margin:4px 0px;background-color:transparent; color:#333333; word-wrap:break-word;}\n"
     "</style> \n"
     "</head> \n";
    
    cssStringDescription = @"<html> \n"
     "<head> \n"
     "<style type=\"text/css\"> \n"
     "body {font-family: \"SKF Chevin Medium\"; font-size: 12; margin:4px 0px;background-color:transparent; color:gray; word-wrap:break-word;}\n"
     "</style> \n"
     "</head> \n";

    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager initTranslation:self];
    
    if ([[calculationDataManager previousInputs] valueForKey:parameterName])
    {
        previousSelectedOption = [[calculationDataManager previousInputs] valueForKey:parameterName];
    }
    
    [optionsTable setBackgroundView:nil];
    [optionsTable setBackgroundColor:[UIColor clearColor]];
    //[optionsTable setOpaque:NO];
    
    if (floor(NSFoundationVersionNumber)> NSFoundationVersionNumber_iOS_6_1) {

    //iOS 7 - Navigation bg & text color change in option screen
    NSString *navigationBarTintColor = NAVIGATION_BACKGROUND_COLOR;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:navigationBarTintColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];

    
    //IOS 7 - To remove table view header title space
    self.optionsTable.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.optionsTable.bounds.size.width, 0.01f)];
 
    }
    

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    Family *family = [optionsArray objectAtIndex:familyNr];

    return [family.options count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Family *family = [optionsArray objectAtIndex:familyNr];
    Option *option = [family.options objectAtIndex:indexPath.row];
    UIFont *presentationFont = [UIFont fontWithName:CHEVIN_MEDIUM size:14];
    UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
    float presentationHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:option.presentation] :presentationFont :280 :NSLineBreakByWordWrapping];
    float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:option.description] :descriptionFont :280 :NSLineBreakByWordWrapping];
    return MAX(presentationHeight + descriptionHeight, 50);

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";  
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UIWebView *nameWebView;
    UIWebView *descriptionWebView;
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        nameWebView = [[UIWebView alloc] initWithFrame:CGRectMake(15, 3, 280, 80)];
        nameWebView.userInteractionEnabled = false;
        nameWebView.tag = 10010;
        nameWebView.backgroundColor = [UIColor clearColor];
        nameWebView.dataDetectorTypes = UIDataDetectorTypeNone;
        nameWebView.opaque = NO;
        [cell addSubview:nameWebView];
        [nameWebView release];
        
        descriptionWebView = [[UIWebView alloc] initWithFrame:CGRectMake(15, 25, 280, 80)];
        descriptionWebView.backgroundColor = [UIColor clearColor];
        descriptionWebView.userInteractionEnabled = false;
        descriptionWebView.opaque = NO;
        descriptionWebView.dataDetectorTypes = UIDataDetectorTypeNone;
        descriptionWebView.tag = 10020;
        [cell addSubview:descriptionWebView];
        [descriptionWebView release];
        
        cell.textLabel.hidden = YES;
        
    }
    
    nameWebView = (UIWebView *)[cell viewWithTag:10010];
    descriptionWebView = (UIWebView *)[cell viewWithTag:10020];
    
    
    Family *family = [optionsArray objectAtIndex:familyNr];
    Option *option = [family.options objectAtIndex:indexPath.row];
    NSString *input = option.presentation;

    cell.tag = [option.optionId intValue];
    cell.textLabel.text = input;
    [nameWebView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, input] baseURL:[NSURL URLWithString:@""]];
    
	if (option.description != nil) {
		[descriptionWebView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssStringDescription, option.description] baseURL:[NSURL URLWithString:@""]];
	}
	else {
		[descriptionWebView loadHTMLString:[NSString stringWithFormat:@"%@", cssStringDescription] baseURL:[NSURL URLWithString:@""]];
	}

	if ([previousSelectedOption isEqualToString:cell.textLabel.text]) {
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	}
	else {
		cell.accessoryType = UITableViewCellAccessoryNone;
	}
    
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    /*NSLog(calculationDataManager.isRelubricationCalcExist ? @"Yes" : @"No");
    NSLog(calculationDataManager.mutlipleCalculation ? @"Yes" : @"No");*/
    
    
    // To disable oil options when Relubrication Cacluation selected
	if (calculationDataManager.isRelubricationCalcExist) {
		if ([parameterName isEqualToString:@"radioLubrication"] || [parameterName isEqualToString:@"radioLubrication_CARB"] || [parameterName isEqualToString:@"radiaoLubrication_SRTB"]) {
			if (![option.presentation isEqualToString:@"Grease"]) {
				[cell setBackgroundColor:[UIColor colorWithHexString:TEXT_COLOR_GREY2]];
				cell.accessoryType = UITableViewCellAccessoryNone;
				[cell setUserInteractionEnabled:NO];
			}
		}
	}
    
    
    // To check Normal cleanness parameter
    if ([parameterName isEqualToString:@"oil_system"]) {
        lubClean_parameter_disable = TRUE;
    }

    // To check Lubrication cleaniness value
    if ([parameterName isEqualToString:@"oil_system"]) {
        if ( [family.name isEqualToString:@"Grease lubrication"]) {
            lubClean_grease_disable = TRUE;
            lubClean_oil_disable = FALSE;
        }else if(( [family.name isEqualToString:@"Circulation oil with on-line filter"]) ||
                 ([family.name isEqualToString:@"Oil lubricant without filter or off-line filter"]) ){
            lubClean_grease_disable = FALSE;
            lubClean_oil_disable = TRUE;
        }
    }
    

    
    
  
    
    
    
    if ((!calculationDataManager.isRelubricationCalcExist) && (lubClean_grease_disable) && (calculationDataManager.mutlipleCalculation)) {
        if ([parameterName isEqualToString:@"radioLubrication"] || [parameterName isEqualToString:@"radioLubrication_CARB"] || [parameterName isEqualToString:@"radiaoLubrication_SRTB"] ) {
            
           
            
           // if (([family.name isEqualToString:@"Lubrication"]) && (![option.optionId isEqualToString:@"0"])) {
                
                
            if (![option.presentation isEqualToString:@"Grease"]) {
                [cell setBackgroundColor:[UIColor colorWithHexString:TEXT_COLOR_GREY2]];
                cell.accessoryType = UITableViewCellAccessoryNone;
                [cell setUserInteractionEnabled:NO];
            }
        }
    }
    
    if ((!calculationDataManager.isRelubricationCalcExist) && (lubClean_oil_disable) && (calculationDataManager.mutlipleCalculation)) {
        if ([parameterName isEqualToString:@"radioLubrication"] || [parameterName isEqualToString:@"radioLubrication_CARB"]  || [parameterName isEqualToString:@"radiaoLubrication_SRTB"]) {
            if ([option.presentation isEqualToString:@"Grease"]) {
                [cell setBackgroundColor:[UIColor colorWithHexString:TEXT_COLOR_GREY2]];
                cell.accessoryType = UITableViewCellAccessoryNone;
                [cell setUserInteractionEnabled:NO];
            }
        }
    }

    
    
    
    return cell;
}

- (void)dataManagerReturnedTranslation:(BOOL)success{
    if (success){
        CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
        self.navigationItem.titleView = [TitleManager getTitle:[calculationDataManager getTranslationString:@"Options"]];

    }
}

@end
