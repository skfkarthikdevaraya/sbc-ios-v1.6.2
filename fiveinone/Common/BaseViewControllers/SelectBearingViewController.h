//
//  SelectBearingViewController.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/18/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculationDataManager.h"

@interface SelectBearingViewController : UIViewController<CalculationDataManagerDelegate> {
    NSMutableArray *foundBearingsArray;
    
    UITableView *bearingsListTable;
    UISearchBar *bearingsSearchBar;
    
    NSString *cssStringGray;

}

@property (nonatomic, retain) IBOutlet UITableView *bearingsListTable;
@property (nonatomic, retain) IBOutlet UISearchBar *bearingsSearchBar;
@property (nonatomic, retain) NSMutableArray *foundBearingsArray;

@property (nonatomic, retain) NSString *cssStringGray;


@end
