//
//  SelectBearingViewController.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/18/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "SelectBearingViewController.h"
#import "CalculateViewController.h"
#import "FilterBearingsViewController_iPhone.h"
#import "TitleManager.h"
#import "Property.h"
#import "NSString+StripHTML.h"

@implementation SelectBearingViewController

@synthesize bearingsListTable;
@synthesize bearingsSearchBar;
@synthesize foundBearingsArray;
@synthesize cssStringGray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager initTranslation:self];
    
    [bearingsListTable setBackgroundView:nil];
    [bearingsListTable setBackgroundColor:[UIColor clearColor]];
    [bearingsListTable setOpaque:NO];
    
    cssStringGray = @"<html> \n"
    "<head> \n"
    "<style type=\"text/css\"> \n"
    "body { font-family: \"SKF Chevin Medium\"; font-size:9; margin:4px 0px;background-color:transparent; color:#808080; word-wrap:break-word; text-decoration: none;}\n"
    "a {font-family: \"SKF Chevin Medium\"; font-size:9; color:#808080; text-decoration: none;}\n"
    "</style> \n"
    "</head> \n";
    
    
    /*** iOS 7 - TO remove the space of header ***/
    
    self.bearingsListTable.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.bearingsListTable.bounds.size.width, 0.01f)];
    
    /*** END ***/
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [foundBearingsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell;
    UILabel *nameLabel;
    //UILabel *parametersLabel;
    UIWebView *subScriptLabelView;
    
    //UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:11];
    NSMutableString *nameLabelString;
    
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 6, 280, 20)];
        nameLabel.userInteractionEnabled = false;
        nameLabel.backgroundColor = [UIColor clearColor];
        nameLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
        nameLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
        nameLabel.tag = 20;
        [cell addSubview:nameLabel];
        [nameLabel release];
        
        
        subScriptLabelView= [[UIWebView alloc] initWithFrame:CGRectZero];
        subScriptLabelView.frame = CGRectMake(20, 23, 280, 30);
        subScriptLabelView.backgroundColor = [UIColor clearColor];
        subScriptLabelView.userInteractionEnabled = NO;
        subScriptLabelView.opaque = NO;
        subScriptLabelView.tag = 101;
        subScriptLabelView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        [cell addSubview:subScriptLabelView];
        [subScriptLabelView release];
    }
    
    nameLabel = (UILabel *)[cell viewWithTag:20];
    subScriptLabelView = (UIWebView *)[cell viewWithTag:101];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    Product *product = [foundBearingsArray objectAtIndex:indexPath.row];

    
    // To remove bearing designation from the list which dont have the properties values
    for (int i=0;i<[foundBearingsArray count]; i++) {
        Product *product = [foundBearingsArray objectAtIndex:i];
        if (product.properties == nil) {
            [foundBearingsArray removeObject:product];
            i--;
            [bearingsListTable reloadData];
        }
    }       // End
    
    
    NSString *cellValue = product.designation;
    nameLabel.text = cellValue;
    nameLabelString = [NSMutableString stringWithString:@""];
    
    for (Property *property in product.properties) {
        if (property.uom) {
            [nameLabelString appendString:[NSMutableString stringWithFormat:@"%@: %@ %@, ", property.presentation, property.value, property.uom]];
        }else {
            [nameLabelString appendString:[NSMutableString stringWithFormat:@"%@: %@, ", property.presentation, property.value]];
        }
    }
    
    if ([nameLabelString length] != 0) {
        [nameLabelString deleteCharactersInRange:NSMakeRange([nameLabelString length]-2, 2)];
    }
    
    [subScriptLabelView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssStringGray, nameLabelString] baseURL:nil];
    return cell;
    
}

- (void)viewDidUnload

{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dataManagerReturnedTranslation:(BOOL)success{
    if (success){
        CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
        
        self.navigationItem.titleView = [TitleManager getTitle:[calculationDataManager getTranslationString:@"Select_bearing"]];
        self.title = [calculationDataManager getTranslationString:@"Select_bearing"];
        
        bearingsSearchBar.scopeButtonTitles = [NSArray arrayWithObjects:
                                               [calculationDataManager getTranslationString:@"Begins"],
                                               [calculationDataManager getTranslationString:@"Contains"],
                                               [calculationDataManager getTranslationString:@"Ends"], nil];
    }
}
-(float) calculateHeightOfTextFromWidth:(NSString*)text: (UIFont*)withFont: (float)width :(UILineBreakMode)lineBreakMode
{
    [text retain];
    [withFont retain];
    CGSize suggestedSize = [text sizeWithFont:withFont constrainedToSize:CGSizeMake(width, FLT_MAX) lineBreakMode:lineBreakMode];
    CGSize lineHeightSize = [text sizeWithFont:withFont];
    
    float height = 0;
    if (![text isEqualToString:@""])
    {
        CGFloat rows = suggestedSize.height / lineHeightSize.height;
        height = rows * (lineHeightSize.height + 5);
        height += 5;
    }
    [text release];
    [withFont release];
    
    return height;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
}

@end
