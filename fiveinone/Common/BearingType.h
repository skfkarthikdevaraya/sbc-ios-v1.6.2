//
//  BearingType.h
//  fiveinone
//
//  Created by Erik Lindberg on 2011-09-28.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BearingType : NSObject
{
    NSString *bearingTypeId;
    NSString *name;
    NSString *iconUrl;
    NSString *imageUrl;
    NSString *presentation;
}

@property (nonatomic, retain) NSString *bearingTypeId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *iconUrl;
@property (nonatomic, retain) NSString *imageUrl;
@property (nonatomic, retain) NSString *presentation;

- (id)initWithCoder:(NSCoder *)coder;
- (void)encodeWithCoder:(NSCoder *)coder;

@end
