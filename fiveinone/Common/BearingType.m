//
//  BearingType.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-09-28.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "BearingType.h"

@implementation BearingType

@synthesize bearingTypeId;
@synthesize name;
@synthesize iconUrl;
@synthesize imageUrl;
@synthesize presentation;

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    if (self != nil)
    {
        self.bearingTypeId = [coder decodeObjectForKey:@"bearingTypeId"];
        self.name = [coder decodeObjectForKey:@"name"];
        self.iconUrl = [coder decodeObjectForKey:@"iconUrl"];
        self.imageUrl = [coder decodeObjectForKey:@"imageUrl"];
        self.presentation = [coder decodeObjectForKey:@"presentation"];
    }   
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self.bearingTypeId forKey:@"bearingTypeId"];
    [coder encodeObject:self.name forKey:@"name"];
    [coder encodeObject:self.iconUrl forKey:@"iconUrl"];
    [coder encodeObject:self.imageUrl forKey:@"imageUrl"];
    [coder encodeObject:self.presentation forKey:@"presentation"];
}
    
@end

