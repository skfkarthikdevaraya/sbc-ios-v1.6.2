//
//  Product.h
//  fiveinone
//
//  Created by Erik Lindberg on 2011-09-15.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calculation : NSObject
{
    
}

@property (nonatomic, retain) NSString *calculationId;
@property (nonatomic, retain) NSString *presentation;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *calculationUrl;
@property (nonatomic, retain) NSString *level;

@property (nonatomic, retain) NSArray *resultParams;
@property (nonatomic, retain) NSArray *resultDatabaseParams;
@property (nonatomic, retain) NSArray *availableParams;
@property (nonatomic, retain) NSArray *errors;
@property (nonatomic, retain) NSArray *warnings;
@property (nonatomic, retain) NSArray *messages;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *reportId;

@end
