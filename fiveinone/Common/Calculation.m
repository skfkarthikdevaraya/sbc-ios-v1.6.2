//
//  Calculation.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-09-15.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "Calculation.h"

@implementation Calculation

@synthesize calculationId;
@synthesize presentation;
@synthesize description;
@synthesize calculationUrl;
@synthesize level;

@synthesize resultDatabaseParams;
@synthesize resultParams;
@synthesize availableParams;
@synthesize errors;
@synthesize warnings;
@synthesize messages;
@synthesize name;
@synthesize reportId;

@end
