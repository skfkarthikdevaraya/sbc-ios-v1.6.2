//
//  CalculationControl.h
//  fiveinone
//
//  Created by Tommy Eriksson on 4/20/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Error.h"
#import "Warning.h"
#import "ParameterMissing.h"
#import "ResultsViewController.h"

@interface CalculationControl : NSObject{
    
    NSMutableArray* _missingParameters;
    NSMutableArray* _visibleMissingParameters;
    
    NSMutableArray* _errors;
    NSMutableArray* _warnings;

    NSMutableDictionary* _parameterValues;
    
    NSMutableSet* _missingInputValues;
    NSMutableSet* _invalidInputValues;
    NSMutableDictionary* _inputValueValidationMessages;
    
    
    //MultipleCalc
    
    NSMutableArray *commonParams ; 
    NSMutableArray *arrayWithCalID;
    

}

typedef enum {VISIBLE_PARAM_BY_ID, VISIBLE_PARAM_BY_NAME, AVAILABLE_PARAM_BY_ID,
              AVAILABLE_PARAM_BY_NAME} eParamListType;

- (void) prefillValuesWithPreviousInputsForParameters;
- (void) prefillValuesWithDefaultsForParameters;
- (BOOL) updateMissingInputValues;
- (BOOL) updateInvalidInputValues;
- (NSString*) getInvalidInputWarnings;

- (BOOL) updateIncorrectInputValues;

-(void) updateParameterErrorMessgge;

-(void) updateInputValuesWithMinMaxValue;

-(NSMutableDictionary*)getParameterValuesByType:(eParamListType)paramType;

-(void) setValue:(NSString*)value forId:(int)parameterId;
-(void) setValue:(NSString*)value forIdString:(NSString*)parameterId;
-(void) setValue:(NSString*)value forName:(NSString*)name;

-(void) removeValueForId:(int)parameterId;
-(void) removeValueForIdStr:(NSString*)parameterId;

-(id) valueForId:(NSString*)parameterId;

-(BOOL) isParameterIdMissingValue:(int)parameterId;
-(BOOL) isParameterIdStrMissingValue:(NSString*)parameterId;
-(BOOL) hasParameterIdStrInvalidValue:(NSString*)parameterId;
-(void) resetMissingValueForParameterId:(int)parameterId;
-(void) resetMissingValueForParameterIdStr:(NSString*)parameterId;

-(NSString*)nameForParameterId:(int)parameterId;

-(void) updateErrors:(NSArray*) newErrors;
-(Error*) getNoFormulaError;
-(NSArray*)getNoFormulaErrors;

-(void)updateWarnings:(NSArray*) newWarnings;
-(Warning*)getNoFormulaWarning;
-(NSArray*)getNoFormulaWarnings;

-(void)updateMissingParameters:(NSArray*)newMissingParameters;
-(NSArray*)getVisibleParameters;
-(void)resetVisibleParameters;
-(ParameterMissing*)getVisibleParameterAtIndex:(NSUInteger)index;
-(ParameterMissing*)getParameterById:(NSString*)parameterId;
-(ParameterMissing*)getParameterByIdAsInt:(int)parameterId;

- (NSString *)validateParameterValue:(ParameterMissing*)parameter value:(NSString*)value;
// MultipleCalc - modified for result screen
-(NSMutableArray*)getParameterValuesByType:(eParamListType)paramType forResultView:(ResultsViewController *)resultView;
-(void)resetMissingParameters;

+(void)setNum:(int)newNum;

-(void) sortMissingParameterArray;

-(void) resetInputParameters;

-(BOOL)CheckParameterValues;

-(ParameterMissing*)getParameterObject:(NSString*)parameterId;

@property (nonatomic, retain) NSMutableArray *commonParams;
@property (nonatomic, retain) NSMutableArray *arrayWithCalID;

@end
