
//
//  CalculationControl.m
//  fiveinone
//
//  Created by Tommy Eriksson on 4/20/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import "CalculationControl.h"
#import "CalculationDataManager.h"
#import "ParameterMissing.h"
#import "Family.h"
#import "Option.h"
#import "SelectCalculationViewController.h"


// MultipleCalc 

static int num = 0;

//
@implementation CalculationControl

@synthesize commonParams;
@synthesize arrayWithCalID;

-(id)init{
    self = [super init];
    if (self != nil){
        _missingParameters = [[NSMutableArray alloc] init];
        _visibleMissingParameters = [[NSMutableArray alloc] init];
        _errors = [[NSMutableArray alloc] init];
        _warnings = [[NSMutableArray alloc] init];        
        
        _parameterValues = [[NSMutableDictionary alloc] init];
        _missingInputValues = [[NSMutableSet alloc] init];
        _invalidInputValues = [[NSMutableSet alloc] init];
        _inputValueValidationMessages = [[NSMutableDictionary alloc] init];
        
        // MultipleCalc
        commonParams = [[NSMutableArray alloc] init];
        

    }
    return self;
}

-(void)dealloc{
    
    //MultipleCalc
    num = 0;
    
    [_missingParameters release];
    _missingParameters = nil;
    [_visibleMissingParameters release];
    _visibleMissingParameters = nil;
    [_errors release];
    _errors = nil;
    [_warnings release];
    _warnings = nil;
    [_parameterValues release];
    _parameterValues = nil;
    [_missingInputValues release];
    _missingInputValues = nil;
    [_invalidInputValues release];
    _invalidInputValues = nil;
    [_inputValueValidationMessages release];
    _inputValueValidationMessages = nil;
    
    [super dealloc];
}

- (void) prefillValuesWithPreviousInputsForParameters
{ 
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];

    [_parameterValues removeAllObjects];

    for (ParameterMissing* missingParameter in _missingParameters) {
        NSString *lastInput = [calculationDataManager getPreviousInput:missingParameter.name];
        
        // Check that the value is available for this selection.
        if (missingParameter.families)
        {
            BOOL isValidOption = FALSE;
            for (Family *family in missingParameter.families) {
                for (Option *option in family.options) {
                    if ([option.presentation isEqualToString:lastInput]) {
                        isValidOption = TRUE;
                        break;
                    }
                }
            }
            if (!isValidOption){
                lastInput = nil;
            }
        }
        
        if(lastInput != nil) {
            [_parameterValues setObject:lastInput forKey:missingParameter.parameterId]; 
        }
	}    
}

- (void) prefillValuesWithDefaultsForParameters
{
    for (ParameterMissing* missingParameter in _missingParameters) {
        
        NSString *valueDefault = missingParameter.valueDefault;
        NSString *value = [_parameterValues valueForKey:missingParameter.parameterId];
        BOOL hasDefault = valueDefault != nil;
        BOOL hasValue = value != nil && [value length] > 0;
        
        if (hasDefault && !hasValue){
            [_parameterValues setObject:valueDefault forKey:missingParameter.parameterId];
        }
	}
}

- (BOOL) updateMissingInputValues{
    BOOL inputMissing = FALSE;
    [_missingInputValues removeAllObjects];
    for (ParameterMissing *parameterMissing in _visibleMissingParameters) {
    
        NSLog(@"Param Id: %@",parameterMissing.parameterId);
        if ([parameterMissing.parameterId isEqualToString:@"165"]) {        // Breaking validation YB & YBR bearing for minimum load calculation
            break;
        }
        NSString* inputString = [self valueForId:parameterMissing.parameterId];
        if ([inputString isEqualToString:@""] || inputString == nil) {
            [_missingInputValues addObject:parameterMissing.parameterId];
            inputMissing = TRUE;
        }
    }
    return inputMissing;
}


- (BOOL) updateIncorrectInputValues {
    
    BOOL inputIncorrect = FALSE;
    [_missingInputValues removeAllObjects];
    for (ParameterMissing *parameterMissing in _visibleMissingParameters) {
        NSString* inputString = [self valueForId:parameterMissing.parameterId];
        
//        NSMutableString *modifyString = [inputString mutableCopy];
//         [modifyString replaceOccurrencesOfString:@"," withString:@"." options:NSCaseInsensitiveSearch
//                                           range:NSMakeRange(0, [modifyString length])];
//        NSString *replaceMultipleZeros = [inputString stringByReplacingOccurrencesOfString:@"^0+"
//                                                           withString:@""
//                                                              options:NSRegularExpressionSearch
//                                                                range:NSMakeRange(0, inputString.length)];
//        NSNumber *validInputNumber = [NSDecimalNumber decimalNumberWithString:modifyString];
//        
//        NSLog(@"Zero: %@", replaceMultipleZeros);
//        NSLog(@"Inputs: %@, %@", inputString, validInputNumber);
//        NSLog(@"Modified string: %@", modifyString);

        //if ([inputString length] == 1) {
            
            if ([inputString isEqualToString:@","] || [inputString isEqualToString:@"."] || [inputString isEqualToString:@"-"] /*|| [inputString isEqualToString:@".,"] || [inputString isEqualToString:@",."] || [inputString isEqualToString:@".,-"] || [inputString isEqualToString:@".-"] || [inputString isEqualToString:@",-"]*/) {
                [_missingInputValues addObject:parameterMissing.parameterId];
                inputIncorrect = TRUE;
            }
        //}
        /*else if ([validInputNumber isEqual:@0]) {
            
            [_missingInputValues addObject:parameterMissing.parameterId];
            inputIncorrect = TRUE;
            
        }*/
        
        
    }
    return inputIncorrect;
}


-(BOOL)CheckParameterValues {
    
    [_missingInputValues removeAllObjects];
    for (ParameterMissing *parameterMissing in _visibleMissingParameters)
    {
       NSString* inputString = [self valueForId:parameterMissing.parameterId];
        if ([inputString isEqualToString:@""] || inputString == nil) {
            [_missingInputValues addObject:parameterMissing.parameterId];
           
        }
    }    

    if (_missingInputValues.count <  _visibleMissingParameters.count ) {
        return true;
    }else {
        return false;
    }
    
}
- (NSString *)validateParameterValue:(ParameterMissing*)parameter value:(NSString*)value{
    
    BOOL isTooLarge = FALSE;
    BOOL isTooSmall = FALSE;
    
    NSString* validationMessage = nil;
    
    // Check against parameter Max/Min values
    BOOL validValue = value && [value length] > 0;
    BOOL validMinValue = parameter.valueMin && [parameter.valueMin length] > 0;
    BOOL validMaxValue = parameter.valueMax && [parameter.valueMax length] > 0;
    
    isTooSmall = validValue && validMinValue && [value doubleValue] < [parameter.valueMin doubleValue];
    isTooLarge = validValue && validMaxValue && [value doubleValue] > [parameter.valueMax doubleValue];
    
    if (isTooLarge){
        validationMessage = GetLocalizedString(@"valueTooHigh");
    } else if (isTooSmall){
        validationMessage = GetLocalizedString(@"valueTooLow");
    }
    
    if (validationMessage == nil && validValue){
        // Check againts larger/smaller than dependant values
        NSString* dependantValue = nil;
        ParameterMissing* dependantParameter = nil;
        if (parameter.depends.smallerThan){
            dependantValue = [self valueForId:parameter.depends.smallerThan];
            dependantParameter = [self getParameterById:parameter.depends.smallerThan];
            BOOL hasValidDependantValue = dependantValue != nil && [dependantValue length] > 0;
            isTooLarge |= hasValidDependantValue && value && [value doubleValue] >= [dependantValue doubleValue];
        }
        
        if (parameter.depends.biggerThan){
            dependantValue = [self valueForId:parameter.depends.biggerThan];
            dependantParameter = [self getParameterById:parameter.depends.biggerThan];
            BOOL hasValidDependantValue = dependantValue != nil && [dependantValue length] > 0;
            isTooSmall |= hasValidDependantValue && value && [value doubleValue] <= [dependantValue doubleValue];
        }
        
        NSArray* parameters = [NSArray arrayWithObjects:
                               value == nil ? @"" : value,
                               parameter.presentation == nil ? @"" : parameter.presentation,
                               dependantValue == nil ? @"" : dependantValue,
                               dependantParameter.presentation == nil ? @"" : dependantParameter.presentation,
                               nil];
        if (isTooLarge){
            validationMessage = GetLocalizedStringParam(@"NotSmallerThan", parameters);
        } else if (isTooSmall){
            validationMessage = GetLocalizedStringParam(@"NotBiggerThan", parameters);
        }
    }
    
    return validationMessage;
}

- (BOOL) updateInvalidInputValues{
    BOOL inputInvalid = FALSE;
    [_invalidInputValues removeAllObjects];
    for (ParameterMissing *parameterMissing in _visibleMissingParameters)
    {
        NSString* inputString = [self valueForId:parameterMissing.parameterId];
        
        
        if ([inputString rangeOfString:@","].location != NSNotFound) {
        NSString *newString = [inputString stringByReplacingOccurrencesOfString:@"," withString:@"."];
        inputString = newString;
        }
        
//        /*** To allow comma (,) for input param: μ */
//        NSMutableString *mutableQueryString = [inputString mutableCopy];
//        [mutableQueryString replaceOccurrencesOfString:@"," withString:@"." options:0 range:NSMakeRange(0, [mutableQueryString length])];
//        [NSString stringWithString:mutableQueryString];
//        inputString = mutableQueryString;

        
        
        
        NSString* validationErrMsg = [self validateParameterValue:parameterMissing value:inputString];
        if (validationErrMsg != nil){
            [_invalidInputValues addObject:parameterMissing.parameterId];
            [_inputValueValidationMessages setValue:validationErrMsg forKey:parameterMissing.parameterId];
            inputInvalid = TRUE;
        }
    }
    return inputInvalid;    
}

//-(NSString*) getInvalidInputWarnings{
//    NSString* text = @"";
//    for (NSString* parameterKey in _invalidInputValues){
//        
//        NSString* validationTextItem = [_inputValueValidationMessages valueForKey:parameterKey];
//        text = [NSString stringWithFormat:@"%@%@.\n", text, validationTextItem];
//        
//        
//        NSLog(@"Alert message: %@, %@",text, validationTextItem);
//    }
//    
//    return [text length] == 0 ? nil : text;
//}







-(NSString*) getInvalidInputWarnings {
    
    NSString* text = @"";
    for (NSString* parameterKey in _invalidInputValues){
        
        ParameterMissing *parameter =  [self getParameterObject:parameterKey];
        NSString* validationTextItem = [_inputValueValidationMessages valueForKey:parameterKey];

        NSString* valueMinimum = @"";
        NSString* valueMaximum = @"";
        valueMinimum = parameter.valueMin;
        valueMaximum = parameter.valueMax;
        
            
            if (valueMinimum != nil ) {
                
                NSMutableString *mutableString= [NSMutableString stringWithString:validationTextItem];
                
                if (parameter.uom == nil) {
                    
                    if ([mutableString rangeOfString:@"Max: /*2*//*3*/"].location != NSNotFound) {
                        
                        [mutableString replaceOccurrencesOfString:@"/*2*//*3*/" withString:valueMaximum options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableString length])];
                }
                    
                    if ([mutableString rangeOfString:@"Min: /*0*//*1*/"].location != NSNotFound){
                        
                        [mutableString replaceOccurrencesOfString:@"/*0*//*1*/" withString:valueMinimum options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableString length])];
                    }
                    
                    if ([mutableString rangeOfString:@"Min: /*2*//*3*/"].location != NSNotFound) {
                        
                        [mutableString replaceOccurrencesOfString:@"/*2*//*3*/" withString:valueMinimum options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableString length])];
                        
                    }
                    
                    if ([mutableString rangeOfString:@"Max: /*0*//*1*/"].location != NSNotFound){
                        
                        [mutableString replaceOccurrencesOfString:@"/*0*//*1*/" withString:valueMaximum options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableString length])];
                    }
                }
                
                        
            if ([mutableString rangeOfString:@" and Max: /*2*//*3*/"].location != NSNotFound) {
                
                if (valueMaximum != nil) {
                    
                    [mutableString replaceOccurrencesOfString:@"/*2*/" withString:valueMaximum options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableString length])];
                    
                    [mutableString replaceOccurrencesOfString:@"/*3*/" withString:[self stringByStrippingHTML:parameter.uom] options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableString length])];
                }
                
                // As there is no value maximum, here removing text "and Max: /*2*//*3*/" from the warning message.
                else if (valueMaximum == nil) {
                    
                    [mutableString replaceOccurrencesOfString:@" and Max: /*2*/" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableString length])];
                    
                    [mutableString replaceOccurrencesOfString:@"/*3*/" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableString length])];

                    
                }
            }
            
            
            if ([mutableString rangeOfString:@"Min: /*0*//*1*/"].location != NSNotFound){
                        
                [mutableString replaceOccurrencesOfString:@"/*0*/" withString:valueMinimum options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableString length])];
                [mutableString replaceOccurrencesOfString:@"/*1*/" withString:[self stringByStrippingHTML:parameter.uom] options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableString length])];
                }
            if ([mutableString rangeOfString:@"Max: /*0*//*1*/"].location != NSNotFound) {
                
                [mutableString replaceOccurrencesOfString:@"/*0*/" withString:valueMaximum options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableString length])];
                
                [mutableString replaceOccurrencesOfString:@"/*1*/" withString:[self stringByStrippingHTML:parameter.uom] options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableString length])];
                
            }
            
            if ([mutableString rangeOfString:@"Min: /*2*//*3*/"].location != NSNotFound){
                
                [mutableString replaceOccurrencesOfString:@"/*2*/" withString:valueMinimum options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableString length])];
                [mutableString replaceOccurrencesOfString:@"/*3*/" withString:[self stringByStrippingHTML:parameter.uom] options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableString length])];
            }
            
            text = [NSString stringWithFormat:@"%@%@.\n\n", text, mutableString];
        }
    }
    
    return [text length] == 0 ? nil : text;
}





-(NSString *) stringByStrippingHTML:(NSString *)inString{
    NSRange r;
    NSString *s = [[inString copy] autorelease];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}
    
-(ParameterMissing*)getParameterObject:(NSString*)parameterId {

        ParameterMissing* foundParameterObject = nil;
    
            for (ParameterMissing* parameter in _missingParameters){
            
                if ([parameterId isEqualToString:parameter.parameterId]){
                    foundParameterObject = parameter;
                    break;
                }
            }
        
    
    return foundParameterObject;

    
}








-(NSMutableDictionary*)getParameterValuesByType:(eParamListType)paramType{
    NSMutableDictionary *sendParams = [NSMutableDictionary dictionary];
    
    NSArray* parameters = nil;
    BOOL isVisibleOnly = paramType == VISIBLE_PARAM_BY_ID ||
    paramType == VISIBLE_PARAM_BY_NAME;
    
    if (isVisibleOnly){
        parameters = [self getVisibleParameters];
    } else {
        parameters = _missingParameters;
    }
    
    for (ParameterMissing *parameterMissing in parameters)
    {
        NSString *inputString = [_parameterValues valueForKey:parameterMissing.parameterId];
        BOOL inputIsValid = inputString != nil && ![inputString isEqualToString:@""];
        
        if (inputIsValid)
        {
            if (parameterMissing.families)
            {
                NSInteger selectedOption = -1;
                for (Family *family in parameterMissing.families) {
                    for (Option *option in family.options) {
                        if ([option.presentation isEqualToString:inputString]) {
                            selectedOption = [option.optionId integerValue];
                        }
                    }
                }
                
              
                
               // inputString = [NSString stringWithFormat:@"%d", selectedOption];
                
                NSString *temp = [NSString stringWithFormat:@"%ld", (long)selectedOption];
                if (([temp isEqualToString:@"0"] && inputString.length >0) && [parameterMissing.parameterId isEqualToString:@"163"]) {
                   
                }else{
                     inputString = temp;
                }
                
                
            }
            else
            {
                inputString = [inputString stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            switch (paramType) {
                case VISIBLE_PARAM_BY_ID:
                case AVAILABLE_PARAM_BY_ID:
                    [sendParams setObject:inputString forKey:parameterMissing.parameterId];
                    break;
                case VISIBLE_PARAM_BY_NAME:
                case AVAILABLE_PARAM_BY_NAME:
                    [sendParams setObject:inputString forKey:parameterMissing.name];
                    break;
                default:
                    break;
            }
        }
    }
    return sendParams;
}


/** Added for multipleCalc result */

-(NSMutableArray*)getParameterValuesByType:(eParamListType)paramType forResultView:(ResultsViewController *)resultView
{
    NSMutableArray *newSendParams = nil;
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    if(!calculationDataManager.mutlipleCalculation)
    {
        newSendParams  = [NSMutableArray arrayWithCapacity:1];
        [newSendParams addObject:[self getParameterValuesByType:paramType]];
        return newSendParams;
    }
    
        
    NSLog(@"commonParams: %d", commonParams.count);
    NSMutableDictionary *sendParams = nil;
    
    NSArray* parameters = nil;
    BOOL isVisibleOnly = paramType == VISIBLE_PARAM_BY_ID ||
    paramType == VISIBLE_PARAM_BY_NAME;
    
    if (isVisibleOnly){
        parameters = [self getVisibleParameters];
    } else {
        parameters = _missingParameters;
    }
    
    //-------------------- Modified for Result screen
    
    arrayWithCalID = nil;
    
    if (paramType ==  VISIBLE_PARAM_BY_NAME)
    {
        NSLog(@"parameters count:%d",[parameters count]);
        NSMutableArray *array  = nil;
        NSString *calID = nil;
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        NSMutableArray *selectedCalculationArray = [NSMutableArray arrayWithArray:calculationDataManager.selectedCalculationsArray];
        //[selectedCalculationArray removeObjectsInArray:[calculationDataManager unsupportedCalculationsArray]];
        arrayWithCalID = [NSMutableArray arrayWithCapacity:selectedCalculationArray];
        for(Calculation     *cal in selectedCalculationArray)
        {
            array = [NSMutableArray array];
            
            calID = cal.calculationId;
            for (ParameterMissing *param in parameters)
            {
                NSLog(@"param.calculationID:%@,......callID:%@", param.calculationID, calID);
                [array addObject:param];
            }
            
            [arrayWithCalID addObject:array];
            array  = nil;
        }
        
    }
    
    
    newSendParams  = [NSMutableArray arrayWithCapacity:[arrayWithCalID count]];
    
    //--------------------
    
    for (NSMutableArray *paramArray in arrayWithCalID)
    {
       // NSLog(@"Count after commonParams added: %d", paramArray.count);
        
        sendParams = [NSMutableDictionary dictionary];
        for (ParameterMissing *parameterMissing in paramArray)
        {
            NSString *inputString = [_parameterValues valueForKey:parameterMissing.parameterId];
            BOOL inputIsValid = inputString != nil && ![inputString isEqualToString:@""];
            
            if (inputIsValid)
            {
                if (parameterMissing.families)
                {
                    NSInteger selectedOption = -1;
                    for (Family *family in parameterMissing.families) {
                        for (Option *option in family.options) {
                            if ([option.presentation isEqualToString:inputString]) {
                                selectedOption = [option.optionId integerValue];
                            }
                        }
                    }
                    inputString = [NSString stringWithFormat:@"%d", selectedOption];
                }
                else
                {
                    inputString = [inputString stringByReplacingOccurrencesOfString:@"," withString:@"."];
                }
                
                switch (paramType) {
                    case VISIBLE_PARAM_BY_ID:
                    case AVAILABLE_PARAM_BY_ID:
                        [sendParams setObject:inputString forKey:parameterMissing.parameterId];
                        //[sendParams setObject:inputString forKey:parameterMissing.parameterId];

                        break;
                    case VISIBLE_PARAM_BY_NAME:
                    case AVAILABLE_PARAM_BY_NAME:
                        [sendParams setObject:inputString forKey:parameterMissing.name];
                        
                        NSLog(@"%@", parameterMissing.name);
                        break;
                    default:
                        break;
                }
            }
        }
        [newSendParams addObject:sendParams];
        
        sendParams = nil;
    }
    return newSendParams;
}

/** */

-(void) setValue:(NSString*)value forId:(int)parameterId{
    [self setValue:value forIdString:[NSString stringWithFormat:@"%d", parameterId]];
}

-(void) setValue:(NSString*)value forIdString:(NSString*)parameterId{
    [_parameterValues setObject:value forKey:parameterId];
}

-(void) setValue:(NSString*)value forName:(NSString*)name{
    
    NSString* idString = nil;
    for (ParameterMissing* missingParameter in _missingParameters){
        if ([missingParameter.name isEqualToString:name]){
            idString = missingParameter.parameterId;
            break;
        }
    }
    
    if (idString != nil){
        [self setValue:value forIdString:idString];
    } else {
        assert(@"FAILED TO FIND PARAMETER");
    }
}


-(void) removeValueForId:(int)parameterId{
    [self removeValueForIdStr:[NSString stringWithFormat:@"%d", parameterId]];
}

-(void) removeValueForIdStr:(NSString*)parameterId{
    [_parameterValues removeObjectForKey:parameterId];
}

-(id)valueForId:(NSString*)parameterId{
    return [_parameterValues valueForKey:parameterId];
}

- (void)resetInputParameters {
    
    
    [_parameterValues removeAllObjects];
    
    

}

-(BOOL) isParameterIdMissingValue:(int)parameterId{
    return [self isParameterIdStrMissingValue:[NSString stringWithFormat:@"%d",parameterId]];
}

-(BOOL) isParameterIdStrMissingValue:(NSString*)parameterId{
    return [_missingInputValues containsObject:parameterId];    
}

-(BOOL) hasParameterIdStrInvalidValue:(NSString*)parameterId{
    return [_invalidInputValues containsObject:parameterId];
}

-(void) resetMissingValueForParameterId:(int)parameterId{
    [self resetMissingValueForParameterIdStr:[NSString stringWithFormat:@"%d",parameterId]];
}

-(void) resetMissingValueForParameterIdStr:(NSString*)parameterId{
    [_missingInputValues removeObject:parameterId];
}

-(NSString*)nameForParameterId:(int)parameterId{
    NSString* parameterName = nil;
    for (ParameterMissing *parameterMissing in _missingParameters) 
    {
        if (parameterId == [parameterMissing.parameterId intValue])
        {
            parameterName = parameterMissing.name;
        }
    }    
    
    if (parameterName == nil){
        assert(@"FAILED TO FIND PARAMETER NAME");
    }
    
    return parameterName;
}

-(void) updateErrors:(NSArray*) newErrors{
    [_errors removeAllObjects];
    [_errors addObjectsFromArray:newErrors];
}

-(Error*) getNoFormulaError{
    Error *noFormulaError = nil;
    for (Error *error in _errors) {
        if ([error.key isEqualToString:@"NoFormula"]) {
            noFormulaError = error;
            break;
        }
    }    
    return noFormulaError;
}

-(NSArray*)getNoFormulaErrors{
    
    
    NSMutableArray *errorsArray = [[[NSMutableArray alloc] init]autorelease];
    for (Error* error in _errors){
        if ([error.key isEqualToString:@"NoFormula"]) {
            [errorsArray addObject:error];
        }
    }
    return errorsArray;
}


-(void)updateWarnings:(NSArray*) newWarnings{
    [_warnings removeAllObjects];
    [_warnings addObjectsFromArray:newWarnings];
}

-(Warning*)getNoFormulaWarning{
    Warning *noFormulaWarning = nil;
    for (Warning *warning in _warnings) {
        if ([warning.key isEqualToString:@"NoFormula"]) {
            noFormulaWarning = warning;
            break;
        }
    }        
    return noFormulaWarning;    
}


-(NSArray*)getNoFormulaWarnings {
    
    NSMutableArray *warningsArray = [[[NSMutableArray alloc]init]autorelease];
    for (Warning *warning in _warnings) {
        if ([warning.key isEqualToString:@"NoFormula"]) {
            [warningsArray addObject:warning];
            
        }
    }
    return warningsArray;
}


-(void)updateMissingParameters:(NSArray*)newMissingParameters {
    CalculationDataManager *calculatioDataManager = [CalculationDataManager sharedManager];
    
    if(!calculatioDataManager.mutlipleCalculation)
    {
        [_missingParameters removeAllObjects];
        [_missingParameters addObjectsFromArray:newMissingParameters];
        return;
    }
    else if (num==0 || [_missingParameters count] == 0)
    {
        [_missingParameters removeAllObjects];
        for (ParameterMissing *obj in newMissingParameters)
        {
            
            obj.calculationID = [[[calculatioDataManager selectedCalculationsArray] objectAtIndex:num] calculationId];
        }
        
        [_missingParameters addObjectsFromArray:newMissingParameters];
        
    }else{
                
        for (ParameterMissing *obj in newMissingParameters)
        {
            CalculationDataManager *calculatioDM = [CalculationDataManager sharedManager];
            obj.calculationID = [[[calculatioDM selectedCalculationsArray] objectAtIndex:num] calculationId];
            
        }
        bool found=false;
        
        for (ParameterMissing *calc in newMissingParameters)
        {
            found=false;
            
            for(ParameterMissing *objMissingParam in _missingParameters)
            {
                if([[objMissingParam parameterId] isEqualToString:[calc parameterId]])
                {
                    found=true;
                    break;
                }
            }
            
            if(!found)
            {
                [_missingParameters addObject:calc];
            }
        }
    }
    
    num++;
    
    
    
       
}


-(NSArray*)getVisibleParameters{
    if (_visibleMissingParameters == nil) {
        _visibleMissingParameters = [[NSMutableArray alloc] initWithCapacity:[_missingParameters count]];
        NSDictionary* parametersById = [self getParameterValuesByType:AVAILABLE_PARAM_BY_ID];
        for (ParameterMissing* param in _missingParameters){
            BOOL isDependantParameter = param.depends != nil;
            BOOL isVisible = (isDependantParameter && [param.depends isDepandancyFulfilled:parametersById]) || !isDependantParameter;
            if (isVisible){
                [_visibleMissingParameters addObject:param];
            }
        }
    }
    return _visibleMissingParameters;
}

-(void)resetVisibleParameters{
    [_visibleMissingParameters release];
    _visibleMissingParameters = nil;
}

-(void)resetMissingParameters{
    [_missingParameters removeAllObjects];
}

-(ParameterMissing*)getVisibleParameterAtIndex:(NSUInteger)index{
    ParameterMissing* parameterMissing = nil;
    BOOL indexIsValid = _visibleMissingParameters != nil && index < [_visibleMissingParameters count];
    if (indexIsValid){
        parameterMissing = [_visibleMissingParameters objectAtIndex:index];
    }
    return parameterMissing;
}

-(ParameterMissing*)getParameterByIdAsInt:(int)parameterId{
    return [self getParameterById:[NSString stringWithFormat:@"%d", parameterId]];
}

-(ParameterMissing*)getParameterById:(NSString*)parameterId{
    ParameterMissing* foundParameter = nil;
    for (ParameterMissing* candidate in _missingParameters){
        if ([parameterId isEqualToString:candidate.parameterId]){
            foundParameter = candidate;
        }
    }
    return foundParameter;
}





+(void)setNum:(int)newNum
{
    num = newNum;
}


-(void) sortMissingParameterArray {
    
    NSArray *sortedArray = [_missingParameters sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [(ParameterMissing*)a displayOrder];
        NSString *second = [(ParameterMissing*)b displayOrder];
        return ([first intValue] > [second intValue]);
    }];
    
    if (sortedArray.count) {
        
        [_missingParameters removeAllObjects];
        [_missingParameters addObjectsFromArray:sortedArray];
    }

    
}


@end
