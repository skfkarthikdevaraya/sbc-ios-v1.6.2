//
//  CalculationDataManager.h
//  fiveinone
//
//  Created by Erik Lindberg on 2011-09-14.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"
#import "BearingType.h"
#import "Product.h"
#import "Calculation.h"
#import "MBProgressHUD.h"
#import "Version.h"

#define GetLocalizedString(k) [[CalculationDataManager sharedManager] getTranslationString:k]
#define GetLocalizedStringParam(k, v) [[CalculationDataManager sharedManager] getTranslationStringParameter:k, v]

@protocol CalculationDataManagerDelegate;

@interface CalculationDataManager : NSObject <RKObjectLoaderDelegate, UIAlertViewDelegate, MBProgressHUDDelegate>
{
    NSMutableArray *lastCalculationsQueue;
    RKObjectManager *manager;

    BearingType *selectedBearingType;
    Product *selectedBearing;
    Calculation *selectedCalculation;

    NSMutableArray *translationArray;
    NSMutableArray *languageArray;    
    NSString *selectedLanguage;
    NSMutableArray *measurementArray;    
    NSString *selectedMeasurement;    
    NSMutableArray *lastBearingsQueue;
    NSMutableDictionary *previousInputs;
    BOOL showingAlert;
    
    MBProgressHUD *HUD;
    
    NSMutableArray* _observers;
    
    //MultipleCalc - adding selected calc to array
    //NSArray *selectedCalculationsArray;
    NSMutableArray *selectedCalculationsArray;
    BOOL mutlipleCalculation;
    NSMutableArray *unsupportedCalculationsArray;
    NSInteger serverCallCount;
    
    int stopIntroText;
    
   // int serverStatus;
    
    BOOL isRelubricationCalcExist;

    
}

@property (nonatomic, retain) NSMutableArray *lastCalculationsQueue;
@property (nonatomic, retain) RKObjectManager *manager;

@property (nonatomic, retain) BearingType *selectedBearingType;
@property (nonatomic, retain) Product *selectedBearing;
@property (nonatomic, retain) Calculation *selectedCalculation;

@property (nonatomic, retain) Version *version;
@property (nonatomic, retain) NSMutableArray *translationArray;
@property (nonatomic, retain) NSMutableArray *languageArray;
@property (nonatomic, retain) NSString *selectedLanguage;
@property (nonatomic, retain) NSMutableArray *measurementArray;
@property (nonatomic, retain) NSString *selectedMeasurement;
@property (nonatomic, retain) NSMutableArray *lastBearingsQueue;
@property (nonatomic, retain) NSMutableDictionary *previousInputs;
@property (nonatomic, assign) BOOL showingAlert;
@property (nonatomic, assign) BOOL connected;

@property (nonatomic, retain) id lastUserData;

@property(nonatomic, assign)    int stopIntroText;
//@property(nonatomic, assign)    int serverStatus;

@property (nonatomic, assign) BOOL isRelubricationCalcExist;

@property (readonly) NSString* baseURL;
@property (readonly) NSString* frontendURL;


//MultipleCalc - adding selected calc to array
//@property(nonatomic, retain) NSArray *selectedCalculationsArray;
@property(nonatomic, retain) NSMutableArray *selectedCalculationsArray;
@property (nonatomic, assign) BOOL mutlipleCalculation;
@property(nonatomic, retain) NSMutableArray *unsupportedCalculationsArray;



-(void) showAlertForNetworkConnection;

- (void)showConnectedAlert;

+ (CalculationDataManager*)sharedManager;
- (void)cancelAllRequests;
- (void)setupBaseURL;
- (void)setupFrontendURL;


- (void)addObserver:(id<CalculationDataManagerDelegate>)observer;
- (void)removeObserver:(id<CalculationDataManagerDelegate>)observer;

- (void)removeBearingFromBearingsArray:(Product *)bearing;
- (NSArray *)previousBearingsArray;
- (NSDictionary *)previousInputParametersForProductId:(NSString *)productId;
- (NSString *)getPreviousInput:(NSString *)name;
- (void)setPreviousInput:(NSString *)name withValue:(NSString *)value;
- (void)setBearing:(Product *)bearing;
- (void)setBearingType:(BearingType *)bearingType;
- (void)setCalculation:(Calculation *)calculation;

- (Product *)getBearing;
-(void) resetInputParameters;


- (NSString *)getTranslationString:(NSString *)keyString;
- (NSString *)getTranslationStringParameter:(NSString *)keyString, NSArray* values;
- (void)initTranslation:(id<CalculationDataManagerDelegate>)delegate;

- (void)getLegend:(id)userData;

- (void)checkSupportedVersion:(NSString*)versionString userData:(id)userData;

- (void)getLanguages:(id)userData;
- (NSArray*)getLanguageValues;
- (NSString*)getUserSelectedLanguage;
- (NSString*)getUserSelectedLanguageAsDescription;
- (NSInteger)getUserSelectedLanguageAsIndex;
- (void)setUserSelectedLangauge:(NSString*)aUserSelectedLanguage;
- (void)setUserSelectedLanguageWithIndex:(NSUInteger)index;

- (void)getMeasurements:(id)userData;
- (NSArray*)getMeasurementValues;
- (NSString*)getUserSelectedMeasurement;
- (NSString*)getUserSelectedMeasurementAsDescription;
- (NSInteger)getUserSelectedMeasurementAsIndex;
- (void)setUserSelectedMeasurement:(NSString*)aUserSelectedMeasurement;
- (void)setUserSelectedMeasurementWithIndex:(NSUInteger)index;

- (void)updateBearingsQueueForUnitChange:(NSArray *)returnedArray;

- (void)getTranslation:(id)userData;

- (void)getAllCalculations:(id)userData;

- (void)getAllCalculationsForTab:(id)userData;

- (void)getCalculationsForBearing:(NSString *)productId userData:(id)userData;

- (void)getAllBearingTypes:(id)userData;

- (void)getProducts:(NSDictionary *)params userData:(id)userData;

- (void)getUpdatedProducts:(NSDictionary *)params userData:(id)userData;


- (void)getParametersForCalculation:(NSString *)calculation andProductId:(NSString *)productId userData:(id)userData;
- (void)getParametersForCalculation:(NSString *)calculation userData:(id)userData;

- (void)doCalculation:(NSString *)calculation withParameters:(NSDictionary *)params userData:(id)userData;
-(void)notifyForCalculationChangeForCalculation:(Calculation *)calc;

- (void)setSearchingModeEnabled:(BOOL)isSearching;

@end

@protocol CalculationDataManagerDelegate <NSObject>
// shoppinglistname == nil on cancel
@optional
- (void)calculationDataManager:(CalculationDataManager *)calculationDataManager returnedArray:(NSArray *)returnedArray;
- (void)dataManagerReturnedLegend:(NSArray *)returnedArray;
- (void)dataManagerReturnedVersionCheck:(BOOL)success;
- (void)dataManagerReturnedTranslation:(BOOL)success;
- (void)dataManagerReturnedLangauges:(BOOL)success;
- (void)dataManagerReturnedMeasurements:(BOOL)success;
- (void)dataManagerReturnedCalculations:(NSArray *)returnedArray;
- (void)dataManagerReturnedBearingTypes:(NSArray *)returnedArray;
- (void)dataManagerReturnedProducts:(NSArray *)returnedArray;


- (void)dataManagerReturnedParametersForCalculation:(NSArray *)returnedArray;
- (void)dataManagerReturnedParametersErrors:(NSArray *)returnedArray;
- (void)dataManagerReturnedParametersWarnings:(NSArray *)returnedArray;
- (void)dataManagerReturnedCalculationErrors:(NSArray *)returnedArray;
- (void)dataManagerReturnedCalculationWarnings:(NSArray *)returnedArray;
- (void)dataManagerReturnedCalculationMessages:(NSArray *)returnedArray;
- (void)dataManagerReturnedCalculationResults:(NSArray *)returnedArray;
- (void)dataManagerReturnedCalculationDatabase:(NSArray *)returnedArray;
- (void)dataManagerReturnedCalculationDictionary:(NSDictionary *)returnedDictionary;

@end
