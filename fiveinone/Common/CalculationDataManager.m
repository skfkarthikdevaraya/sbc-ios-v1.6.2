
//  CalculationDataManager.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-09-14.
//  Copyright 2011 HiQ. All rights reserved.
//
#import "CalculationDataManager.h"
#import "RestKit/RestKit.h"
#import "RestKit/RKXMLParserLibXML.h"
#import "NSMutableArray+QueueAdditions.h"
#import "Legend.h"
#import "ParameterResult.h"
#import "ParameterMissing.h"
#import "ParameterDatabase.h"
#import "ParameterAvailable.h"
#import "Family.h"
#import "Option.h"
#import "Error.h"
#import "Warning.h"
#import "Message.h"
#import "Property.h"
#import "Product.h"
#import "Calculation.h"
#import "BearingType.h"
#import "Translation.h"
#import "Language.h"
#import "Measurement.h"
#import "CalculationResult.h"
#import "WeakReference.h"
#import "ParameterDependancy.h"
#import "SelectCalculationViewController_iPad.h"


@interface CalculationDataManager(Private)
-(void)notifyCalculationDataManager:(CalculationDataManager *)calculationDataManager returnedArray:(NSArray *)returnedArray;
-(void)notifyDataManagerReturnedLegend:(NSArray *)returnedArray;
-(void)notifyDataManagerReturnedTranslation:(BOOL)success;
-(void)notifyDataManagerReturnedVersionCheck:(BOOL)success;
-(void)notifyDataManagerReturnedLangauges:(BOOL)success;
-(void)notifyDataManagerReturnedMeasurements:(BOOL)success;
-(void)notifyDataManagerReturnedCalculations:(NSArray *)returnedArray;
-(void)notifyDataManagerReturnedBearingTypes:(NSArray *)returnedArray;
-(void)notifyDataManagerReturnedProducts:(NSArray *)returnedArray;

//-(void)notifyDataManagerReturnedProductsForDetails:(NSArray *)returnedArray;

-(void)notifyDataManagerReturnedParametersForCalculation:(NSArray *)returnedArray;
-(void)notifyDataManagerReturnedParametersErrors:(NSArray *)returnedArray;
-(void)notifyDataManagerReturnedParametersWarnings:(NSArray *)returnedArray;
-(void)notifyDataManagerReturnedCalculationErrors:(NSArray *)returnedArray;
-(void)notifyDataManagerReturnedCalculationWarnings:(NSArray *)returnedArray;
-(void)notifyDataManagerReturnedCalculationMessages:(NSArray *)returnedArray;
-(void)notifyDataManagerReturnedCalculationResults:(NSArray *)returnedArray;
-(void)notifyDataManagerReturnedCalculationDatabase:(NSArray *)returnedArray;
-(void)notifyDataManagerReturnedCalculationDictionary:(NSDictionary *)returnedDictionary;
@end

@interface CDMUserData : NSObject
@property (nonatomic, retain) NSNumber* requestType;
@property (nonatomic, retain) id userData;
@end
@implementation CDMUserData
@synthesize requestType;
@synthesize userData;
+(CDMUserData*)userData:(id)aUserData withRequestType:(NSNumber*)aRequestType{
    CDMUserData* cdm = [[[CDMUserData alloc] init] autorelease];
    cdm.requestType = aRequestType;
    cdm.userData = aUserData;
    return cdm;
}
-(void)dealloc{
    self.requestType = nil;
    self.userData = nil;
    [super dealloc];
}
@end

@implementation CalculationDataManager

@synthesize lastCalculationsQueue;
@synthesize manager;

@synthesize selectedBearingType;
@synthesize selectedBearing;
@synthesize selectedCalculation;

@synthesize version;
@synthesize translationArray;
@synthesize languageArray;
@synthesize selectedLanguage;
@synthesize measurementArray;
@synthesize selectedMeasurement;
@synthesize lastBearingsQueue;
@synthesize previousInputs;
@synthesize showingAlert;
@synthesize connected;

@synthesize lastUserData;

//static BOOL serverCals;

int stopIntroText = -1;
static int serverStatus;

//MultipleCalc - adding selected calc to array
@synthesize selectedCalculationsArray;
@synthesize mutlipleCalculation;
@synthesize unsupportedCalculationsArray;

@synthesize frontendURL;
@synthesize isRelubricationCalcExist;

enum RequestTags
{
    kGetLegend = 0,
    kGetCalculations,
    kGetBearingTypes,
    kGetParametersForCalculation,
    kGetParametersForCalculations,
    kDoCalculation,
    kGetProductsForDesignation,
    kGetUpdateProductsForDesignation,
   // kGetProductsForDetails,
    kGetTranslation,
    kGetLanguages,
    kGetMeasurements,
    kGetVersionCheck,
    NUMBEROREQUESTTAGS
};

- (id)init{
    self = [super init];
    if (self){
        _observers = [NSMutableArray mutableArrayUsingWeakReferences];
        [_observers retain];
        
        selectedCalculationsArray = [[NSMutableArray alloc] init];
        unsupportedCalculationsArray = [[NSMutableArray alloc] init];
    }
    
    
    return self;
}

- (void)dealloc{
    [_observers release];
    _observers = nil;
    
    [selectedCalculationsArray removeAllObjects];
    [selectedCalculationsArray release];
    selectedCalculationsArray = nil;
    
    [unsupportedCalculationsArray removeAllObjects];
    [unsupportedCalculationsArray release];
    unsupportedCalculationsArray=nil;
    
    [super dealloc];
}

- (void)setupBaseURL
{
    NSString* serverAddress = [[NSUserDefaults standardUserDefaults] stringForKey:@"serverAddress"];
    if (serverAddress == nil) {
        
		serverAddress = @"http://webtools.skf.com/BearingCalculator";
		//serverAddress = @"http://webtools3.skf.com/BearingCalculator";
		//serverAddress = @"http://webtools4.skf.com/BearingCalculator";
		//serverAddress = @"http://163.157.29.214:8080/BearingCalculator";
        //serverAddress = @"http://163.157.29.212:8080/BearingCalculator";

    }
    
    
    
    [RKObjectManager sharedManager].client.baseURL = serverAddress;

    [self cancelAllRequests];
}
 


-(NSString*)baseURL {
    
    return [RKObjectManager sharedManager].client.baseURL;
}



- (void)setupClass
{
    NSString *serverAddress = [[NSUserDefaults standardUserDefaults] stringForKey:@"serverAddress"];
    
    if (serverAddress == nil) {
        
        serverAddress = @"http://webtools.skf.com/BearingCalculator";
        //serverAddress = @"http://webtools3.skf.com/BearingCalculator";
        //serverAddress = @"http://webtools4.skf.com/BearingCalculator";
        //serverAddress = @"http://163.157.29.214:8080/BearingCalculator";
        //serverAddress = @"http://163.157.29.212:8080/BearingCalculator";
        
    }
    
    [RKObjectManager objectManagerWithBaseURL:serverAddress];
    
    [[RKParserRegistry sharedRegistry] setParserClass:[RKXMLParserLibXML class] forMIMEType:@"text/xml"];
    
    RKObjectMapping *legendMapping = [RKObjectMapping mappingForClass:[Legend class]];
    [legendMapping mapAttributes:@"description", @"presentation", @"subCalculation", @"dataType", nil];
    [legendMapping mapKeyPath:@"id" toAttribute:@"parameterId"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:legendMapping forKeyPath:@"Legend.parameters.parameter"];
    
    RKObjectMapping *parametersResultsMapping = [RKObjectMapping mappingForClass:[ParameterResult class]];
    [parametersResultsMapping mapAttributes:@"name", @"description", @"presentation", @"subCalculation", @"dataType", @"value", @"uom", nil];
    [parametersResultsMapping mapKeyPath:@"id" toAttribute:@"parameterId"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:parametersResultsMapping forKeyPath:@"calculation.parameters-result.parameter"];
    
    RKObjectMapping *parametersDatabaseMapping = [RKObjectMapping mappingForClass:[ParameterDatabase class]];
    [parametersDatabaseMapping mapAttributes:@"name", @"description", @"presentation", @"subCalculation", @"dataType", @"value", @"uom", nil];
    [parametersDatabaseMapping mapKeyPath:@"value-min" toAttribute:@"valueMin"];
    [parametersDatabaseMapping mapKeyPath:@"value-max" toAttribute:@"valueMax"];
    [parametersDatabaseMapping mapKeyPath:@"id" toAttribute:@"parameterDatabaseId"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:parametersDatabaseMapping forKeyPath:@"calculation.parameters-database.parameter"];
    
    RKObjectMapping *optionMapping = [RKObjectMapping mappingForClass:[Option class]];
    [optionMapping mapAttributes:@"presentation", @"description", @"skip", nil];
    [optionMapping mapKeyPath:@"id" toAttribute:@"optionId"];
            
    RKObjectMapping *familyMapping = [RKObjectMapping mappingForClass:[Family class]];
    [familyMapping mapAttributes:@"name", nil];
    [familyMapping mapKeyPath:@"id" toAttribute:@"familyId"];
    [familyMapping mapKeyPath:@"option" toRelationship:@"options" withMapping:optionMapping];
    
    RKObjectMapping *dependantParameterMapping = [RKObjectMapping mappingForClass:[ParameterDependancy class]];
    [dependantParameterMapping mapKeyPath:@"dependsOn" toAttribute:@"dependsOn"];
    [dependantParameterMapping mapKeyPath:@"dependsOnValue" toAttribute:@"dependsOnValue"];
    [dependantParameterMapping mapKeyPath:@"bigger-than" toAttribute:@"biggerThan"];
    [dependantParameterMapping mapKeyPath:@"smaller-than" toAttribute:@"smallerThan"];
    
    RKObjectMapping *parametersMissingMapping = [RKObjectMapping mappingForClass:[ParameterMissing class]];
    [parametersMissingMapping mapAttributes:@"name", @"description", @"presentation", @"subCalculation", @"dataType", @"uom", @"displayOrder", nil];
    [parametersMissingMapping mapKeyPath:@"value-min" toAttribute:@"valueMin"];
    [parametersMissingMapping mapKeyPath:@"value-max" toAttribute:@"valueMax"];
    [parametersMissingMapping mapKeyPath:@"value-default" toAttribute:@"valueDefault"];    
    [parametersMissingMapping mapKeyPath:@"id" toAttribute:@"parameterId"];
    [parametersMissingMapping mapKeyPath:@"options.family" toRelationship:@"families" withMapping:familyMapping];
    [parametersMissingMapping mapKeyPath:@"depends" toRelationship:@"depends" withMapping:dependantParameterMapping];
    
    [[RKObjectManager sharedManager].mappingProvider setMapping:parametersMissingMapping forKeyPath:@"calculation.parameters-missing.parameter"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:parametersMissingMapping forKeyPath:@"calculations.parameters-required.parameter"];
    
    RKObjectMapping *parametersAvailableMapping = [RKObjectMapping mappingForClass:[ParameterAvailable class]];
    [parametersAvailableMapping mapAttributes:@"name", @"description", @"presentation", @"subCalculation", @"dataType", @"value", @"uom", nil];
    [parametersAvailableMapping mapKeyPath:@"value-min" toAttribute:@"valueMin"];
    [parametersAvailableMapping mapKeyPath:@"value-max" toAttribute:@"valueMax"];
    [parametersAvailableMapping mapKeyPath:@"id" toAttribute:@"parameterAvailableId"];
    [parametersAvailableMapping mapKeyPath:@"options.family" toRelationship:@"families" withMapping:familyMapping];
    [[RKObjectManager sharedManager].mappingProvider setMapping:parametersAvailableMapping forKeyPath:@"calculation.parameters-available.parameter"];
    
    RKObjectMapping *propertyMapping = [RKObjectMapping mappingForClass:[Property class]];
    [propertyMapping mapAttributes:@"presentation", @"value", @"uom", nil];
    [propertyMapping mapKeyPath:@"id" toAttribute:@"propertyId"];

    RKObjectMapping *productMapping = [RKObjectMapping mappingForClass:[Product class]];
    [productMapping mapAttributes:@"designation", @"typeId", @"typestext", @"type", @"explorer", nil];
    //[productMapping mapAttributes:@"designation", @"typeId", @"typestext", @"type",  nil];
    [productMapping mapKeyPath:@"id" toAttribute:@"productId"];
    [productMapping mapKeyPath:@"properties.property" toRelationship:@"properties" withMapping:propertyMapping];
    [[RKObjectManager sharedManager].mappingProvider setMapping:productMapping forKeyPath:@"products.products.product"];
    
    /*** SK updated - for MC from server side ***/
    RKObjectMapping *errorMapping = [RKObjectMapping mappingForClass:[Error class]];
    [errorMapping mapAttributes:@"description", @"key", nil];
    [errorMapping mapKeyPath:@"id" toAttribute:@"errorId"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:errorMapping forKeyPath:@"calculation.errors.error"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:errorMapping forKeyPath:@"calculations.errors.error"];
    
    RKObjectMapping *warningMapping = [RKObjectMapping mappingForClass:[Warning class]];
    [warningMapping mapAttributes:@"description", @"key", nil];
    [warningMapping mapKeyPath:@"id" toAttribute:@"warningId"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:warningMapping forKeyPath:@"calculation.warnings.warning"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:warningMapping forKeyPath:@"calculations.warnings.warning"];
    
    
    RKObjectMapping *messageMapping = [RKObjectMapping mappingForClass:[Message class]];
    [messageMapping mapAttributes:@"description", @"key", nil];
    [messageMapping mapKeyPath:@"id" toAttribute:@"messageId"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:messageMapping forKeyPath:@"calculation.messages.message"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:messageMapping forKeyPath:@"calculations.messages.message"];
    
    RKObjectMapping *calculationMapping = [RKObjectMapping mappingForClass:[Calculation class]];
    [calculationMapping mapAttributes:@"presentation", @"description", @"calculationUrl", @"level", nil];
    [calculationMapping mapKeyPath:@"id" toAttribute:@"calculationId"];
   // [[RKObjectManager sharedManager].mappingProvider setMapping:calculationMapping forKeyPath:@"calculations.calculations.calculation"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:calculationMapping forKeyPath:@"calculations.calculation"];
    
    RKObjectMapping *calculationMapping2 = [RKObjectMapping mappingForClass:[Calculation class]];
    [calculationMapping2 mapAttributes:@"presentation",@"description", @"calculationUrl", @"level", @"name", @"reportId", nil];
    [calculationMapping2 mapKeyPath:@"id" toAttribute:@"calculationId"];
    [calculationMapping2 mapKeyPath:@"parameters-available.parameter" toRelationship:@"availableParams" withMapping:parametersAvailableMapping];
    [calculationMapping2 mapKeyPath:@"parameters-database.parameter" toRelationship:@"resultDatabaseParams" withMapping:parametersDatabaseMapping];
    [calculationMapping2 mapKeyPath:@"parameters-result.parameter" toRelationship:@"resultParams" withMapping:parametersResultsMapping];
    [calculationMapping2 mapKeyPath:@"warnings.warning" toRelationship:@"warnings" withMapping:warningMapping];
    [calculationMapping2 mapKeyPath:@"errors.error" toRelationship:@"errors" withMapping:errorMapping];
    [calculationMapping2 mapKeyPath:@"messages.message" toRelationship:@"messages" withMapping:messageMapping];
    
    [[RKObjectManager sharedManager].mappingProvider setMapping:calculationMapping2 forKeyPath:@"calculations.calculations.calculation"];
    
    /*** END ***/
    
    RKObjectMapping *bearingTypeMapping = [RKObjectMapping mappingForClass:[BearingType class]];
    [bearingTypeMapping mapAttributes:@"presentation", @"name", @"iconUrl",  @"imageUrl", nil];
    [bearingTypeMapping mapKeyPath:@"id" toAttribute:@"bearingTypeId"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:bearingTypeMapping forKeyPath:@"bearing-groups.bearing-group.type"];
    
    RKObjectMapping *languageMapping = [RKObjectMapping mappingForClass:[Language class]];
    [languageMapping mapAttributes:@"description", @"name", nil];
    [languageMapping mapKeyPath:@"id" toAttribute:@"languageId"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:languageMapping forKeyPath:@"languages.languages.language"];

    RKObjectMapping *measurementMapping = [RKObjectMapping mappingForClass:[Measurement class]];
    [measurementMapping mapAttributes:@"description", nil];
    [measurementMapping mapKeyPath:@"id" toAttribute:@"measurementId"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:measurementMapping forKeyPath:@"measurements.measurements.measurement"];

    RKObjectMapping *translationMapping = [RKObjectMapping mappingForClass:[Translation class]];
    [translationMapping mapAttributes:@"description", @"name", nil];
    [translationMapping mapKeyPath:@"id" toAttribute:@"translationId"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:translationMapping forKeyPath:@"phrases.phrase.translation"];

    RKObjectMapping *versionMapping = [RKObjectMapping mappingForClass:[Version class]];
    [versionMapping mapAttributes:@"protocol", @"Android", @"Apple", nil];
    [[RKObjectManager sharedManager].mappingProvider setMapping:versionMapping forKeyPath:@"Version"];
    
   /* RKObjectMapping *errorMapping = [RKObjectMapping mappingForClass:[Error class]];
    [errorMapping mapAttributes:@"description", @"key", nil];
    [errorMapping mapKeyPath:@"id" toAttribute:@"errorId"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:errorMapping forKeyPath:@"calculation.errors.error"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:errorMapping forKeyPath:@"calculations.errors.error"];
    
    RKObjectMapping *warningMapping = [RKObjectMapping mappingForClass:[Warning class]];
    [warningMapping mapAttributes:@"description", @"key", nil];
    [warningMapping mapKeyPath:@"id" toAttribute:@"warningId"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:warningMapping forKeyPath:@"calculation.warnings.warning"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:warningMapping forKeyPath:@"calculations.warnings.warning"];

    
    RKObjectMapping *messageMapping = [RKObjectMapping mappingForClass:[Message class]];
    [messageMapping mapAttributes:@"description", @"key", nil];
    [messageMapping mapKeyPath:@"id" toAttribute:@"messageId"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:messageMapping forKeyPath:@"calculation.messages.message"];
    [[RKObjectManager sharedManager].mappingProvider setMapping:messageMapping forKeyPath:@"calculations.messages.message"]; */

    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSData *dataRepresentingSavedArray = [prefs objectForKey:@"lastBearings"];
    if (dataRepresentingSavedArray != nil)
    {
        NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        if (oldSavedArray != nil)
            lastBearingsQueue = [oldSavedArray mutableCopy];
        else
            lastBearingsQueue = [[NSMutableArray alloc] init];
    }
    else
    {
        lastBearingsQueue = [[NSMutableArray alloc] init];
        [prefs setObject:[NSKeyedArchiver archivedDataWithRootObject:lastBearingsQueue] forKey:@"lastBearings"];
        [prefs synchronize];
    }
    
    lastCalculationsQueue = [NSMutableArray alloc];
    lastCalculationsQueue = [prefs valueForKey:@"lastCalculations"];
    if (lastCalculationsQueue == nil)
    {
        lastCalculationsQueue = [[NSMutableArray alloc] init];
        [prefs setValue:lastCalculationsQueue forKey:@"lastCalculations"];
        [prefs synchronize];
    }
    
    previousInputs = [NSMutableDictionary alloc];
    previousInputs = [prefs valueForKey:@"previousInputs"];
    if (previousInputs == nil)
    {
        previousInputs = [[NSMutableDictionary alloc] init];
        [prefs setValue:previousInputs forKey:@"previousInputs"];
       [prefs synchronize];
    }
    
    selectedLanguage = [prefs valueForKey:@"selectedLanguage"];
    if (selectedLanguage == nil){
        selectedLanguage = @"EN";
        [prefs setValue:selectedLanguage forKey:@"selectedLanguage"];
    }
        
    
    selectedMeasurement = [prefs valueForKey:@"selectedMeasurement"];
    if (selectedMeasurement == nil){
        selectedMeasurement = @"SI";
        [prefs setValue:selectedMeasurement forKey:@"selectedMeasurement"];
    }

    selectedBearingType = [BearingType alloc];
    NSData *myEncodedObject = [prefs valueForKey:@"selectedBearingType"];
    selectedBearingType = (BearingType*)[[NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject] retain];
    if (selectedBearingType == nil)
    {
        selectedBearingType = [[BearingType alloc] init];
        NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:selectedBearingType];
        [prefs setValue:myEncodedObject forKey:@"selectedBearingType"];
        [prefs synchronize];
    }

    [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(reachabilityChanged:)
                                                name:RKReachabilityDidChangeNotification
                                              object:nil];
    
    showingAlert = false;
    connected = true;
    mutlipleCalculation = false;
}

+ (CalculationDataManager *)sharedManager
{
    static CalculationDataManager *sharedCalculationDataManager = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedCalculationDataManager = [[self alloc] init];
        [sharedCalculationDataManager setupClass];
    });
    return sharedCalculationDataManager;
}

- (void)addObserver:(id<CalculationDataManagerDelegate>)observer
{
    [_observers addObject:observer];
}

- (void)removeObserver:(id<CalculationDataManagerDelegate>)observer
{
    [_observers removeObject:observer];
}

- (void)showConnectedAlert
{
	showingAlert = true;
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[self getTranslationString:@"Network_error!"]
	                                                message:[self getTranslationString:@"This_application_need_a_network_connection."]
	                                               delegate:nil
	                                      cancelButtonTitle:[self getTranslationString:@"OK"]
	                                      otherButtonTitles:nil];
	[alert show];
	[alert release];
}

- (void)reachabilityChanged:(NSNotification*)notification {
    RKReachabilityObserver* observer = (RKReachabilityObserver*)[notification object];
    
    if (![observer isNetworkReachable])
    { 
        connected = false;
        [self showConnectedAlert];
        DLog(@"Disconnected");
    }
    if ([observer isNetworkReachable]) {
        connected = true;
        DLog(@"Connected");
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    showingAlert = false;
    
    // To show server connection error
    NSString * buttontext = [alertView buttonTitleAtIndex:buttonIndex];
    if([buttontext isEqualToString:@"Exit"])
    {
        exit(0);
    }

    
    
}

- (void)hudDidCancel
{
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager cancelAllRequests];

}

- (void)hudWasHidden:(MBProgressHUD *)hud {

}

- (void)setSearchingModeEnabled:(BOOL)isSearching
{

    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    //when network action, toggle network indicator and activity indicator

	if (isSearching) {
		HUD = [[MBProgressHUD alloc] initWithWindow:window];
		HUD.labelText = [self getTranslationString:@"Searching"];
        HUD.graceTime = 0.5f;
        HUD.taskInProgress = YES;
        HUD.allowsCancelation = YES;
        HUD.delegate = self;
        HUD.labelFont = [UIFont fontWithName:CHEVIN_BOLD size:20.f];
        [window addSubview:HUD];
		[HUD show:YES];
        //[window setUserInteractionEnabled:NO];
        
	} else {
        
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if(HUD!=nil)
        {
            [HUD hide:YES];
            HUD.taskInProgress = NO;
            [HUD removeFromSuperview];
            [HUD release];
            HUD = nil;
            [window setUserInteractionEnabled:YES];
            
        }
	}
    
}



- (void)cancelAllRequests
{
    RKRequestQueue *requestQueue = [[RKObjectManager sharedManager] requestQueue];
    [requestQueue cancelAllRequests];
    
    [self setSearchingModeEnabled:NO];
}


- (void)updateBearingsQueueForUnitChange:(NSArray *)returnedArray
{
    int i =0, j=0 ;
    
    
	for (j = 0; j < returnedArray.count; j++) {
		for (i = 0; i < lastBearingsQueue.count; i++) {
			Product *prod = [returnedArray objectAtIndex:j];
			Product *prod2 = [lastBearingsQueue objectAtIndex:i];

			if ([prod.productId isEqualToString:prod2.productId]) {
				break;
			}
		}
	}
    
	[lastBearingsQueue replaceObjectAtIndex:i withObject:[returnedArray objectAtIndex:j - 1]];

	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	[prefs setObject:[NSKeyedArchiver archivedDataWithRootObject:lastBearingsQueue] forKey:@"lastBearings"];
	[prefs synchronize];


	CalculationDataManager *calculateDataManager = [CalculationDataManager sharedManager];


	Product *selected_Bearing = [calculateDataManager getBearing];
	Product *tempProduct = (Product *)[lastBearingsQueue objectAtIndex:i];

	if ([selected_Bearing.productId isEqualToString:tempProduct.productId]) {
		[calculateDataManager setBearing:(Product *)[lastBearingsQueue objectAtIndex:i]];
		[[NSNotificationCenter defaultCenter] postNotificationName:@"ipadBearingChanged" object:self];
	}
    
}




- (void)updateBearingsQueue:(Product *)bearing
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

	NSData *dataRepresentingSavedArray = [prefs objectForKey:@"lastBearings"];
	if (dataRepresentingSavedArray != nil) {
		NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
		if (oldSavedArray != nil)
			lastBearingsQueue = [[NSMutableArray alloc] initWithArray:oldSavedArray];
		else
			lastBearingsQueue = [[NSMutableArray alloc] init];
	}

	Boolean found = false;

	if ([lastBearingsQueue count] != 0) {
		for (int i = 0; i < [lastBearingsQueue count]; i++) {
			Product *currentBearing = [lastBearingsQueue objectAtIndex:i];
			NSString *lastProductId = [currentBearing valueForKey:@"productId"];
			if ([[bearing valueForKey:@"productId"] isEqualToString:lastProductId]) {
				[lastBearingsQueue moveObjectFromIndex:i toIndex:0];
				found = true;
				break;
			}
		}

		if (!found) {
			[lastBearingsQueue insertObject:bearing atIndex:0];
			if ([lastBearingsQueue count] > 10) {
				[lastBearingsQueue removeObjectAtIndex:10];
			}
		}
	}
	else {
		[lastBearingsQueue enqueue:bearing];
	}

	[[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:lastBearingsQueue] forKey:@"lastBearings"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)removeBearingFromBearingsArray:(Product *)bearing
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

	NSData *dataRepresentingSavedArray = [prefs objectForKey:@"lastBearings"];
	if (dataRepresentingSavedArray != nil) {
		NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
		if (oldSavedArray != nil)
			lastBearingsQueue = [oldSavedArray mutableCopy];
		else
			lastBearingsQueue = [[NSMutableArray alloc] init];
	}

	for (int i = 0; i < [lastBearingsQueue count]; i++) {
		if ([[bearing valueForKey:@"productId"] isEqualToString:[[lastBearingsQueue objectAtIndex:i] valueForKey:@"productId"]]) {
			[lastBearingsQueue removeObjectAtIndex:i];
			break;
		}
	}

	[[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:lastBearingsQueue] forKey:@"lastBearings"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSArray *)previousCalculationsArray
{
    NSMutableArray *returnArray = [NSMutableArray array];  
    for (NSDictionary *bearingInfo in lastCalculationsQueue) {
        NSDictionary *dict = [NSDictionary dictionary];
        [dict setValue:[bearingInfo valueForKey:@"productId"] forKey:@"productId"];
        [dict setValue:[bearingInfo valueForKey:@"designation"] forKey:@"designation"];
        [returnArray addObject:dict];
    }
    return returnArray;
}

- (NSArray *)previousBearingsArray
{
    return lastBearingsQueue;
}

- (NSDictionary *)previousInputParametersForProductId:(NSString *)productId
{
    for (NSDictionary *dict in lastCalculationsQueue) {
        if ([[dict objectForKey:@"productId"] isEqualToString:productId])
        {
            return dict;
        }
    }
    return nil;
}

- (NSString *)getPreviousInput:(NSString *)name
{
    return [previousInputs objectForKey:name];
}


-(void) resetInputParameters {
    
    previousInputs = [previousInputs mutableCopy];
    [previousInputs removeAllObjects];

    
    
}

- (void)setPreviousInput:(NSString *)name withValue:(NSString *)value
{
    NSLog(@"Value: %@",value);
    NSLog(@"Key: %@",name);
    
    if (value.length <=0) {
        return;
    }
    previousInputs = [previousInputs mutableCopy];
    [previousInputs setObject:value forKey:name];
    //[previousInputs mutableCopy];
    [[NSUserDefaults standardUserDefaults] setValue:[previousInputs mutableCopy] forKey:@"previousInputs"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    

    
    /*NSMutableArray *myArray = [[NSMutableArray alloc]init];
    NSMutableDictionary *myDictionary = [[NSMutableDictionary alloc]init];
    [myDictionary setObject:value forKey:name];
    [myArray addObject:myDictionary];
    //  [[NSUserDefaults standardUserDefaults] setObject:myArray forKey:@"previousInputs"];
    [[NSUserDefaults standardUserDefaults] synchronize];*/
    
    
}


- (NSArray*)getLanguageValues
{
    NSMutableArray* values = [[[NSMutableArray alloc] initWithCapacity:[languageArray count]] autorelease];
    for (Language* lang in languageArray){
        [values addObject:lang.description];
    }
    return values;
}

- (NSString*)getUserSelectedLanguage
{
    return self.selectedLanguage;
}

- (NSString*)getUserSelectedLanguageAsDescription
{
    for (Language* lang in languageArray){
        if ([lang.name isEqualToString:selectedLanguage]){
            return lang.description;
        }
    }
    return self.selectedLanguage;
}

- (NSInteger)getUserSelectedLanguageAsIndex
{
    NSInteger index = 0;
    for (Language* lang in languageArray){
        if ([lang.name isEqualToString:selectedLanguage]){
            return index;
        }
        index++;
    }
    return -1;
}

- (void)setUserSelectedLangauge:(NSString*)aUserSelectedLanguage
{
    self.selectedLanguage = aUserSelectedLanguage;
    [[NSUserDefaults standardUserDefaults] setValue:selectedLanguage forKey:@"selectedLanguage"];
    [[NSUserDefaults standardUserDefaults] synchronize];    
}

- (void)setUserSelectedLanguageWithIndex:(NSUInteger)index
{
    BOOL validIndex = index < [languageArray count];
    assert(validIndex);
    
    if (validIndex){
        Language* language = [languageArray objectAtIndex:index];
        [self setUserSelectedLangauge:language.name];
    }
}

- (NSArray*)getMeasurementValues
{
    NSMutableArray* values = [[[NSMutableArray alloc] initWithCapacity:[measurementArray count]] autorelease];
    for (Measurement* measurement in measurementArray){
        [values addObject:measurement.description];
    }
    return values;
}


- (NSString*)getUserSelectedMeasurement
{
    return self.selectedMeasurement;
}

- (NSString*)getUserSelectedMeasurementAsDescription
{
    for (Measurement* measurement in measurementArray){
        if ([measurement.description isEqualToString:selectedMeasurement]){
            return measurement.description;
        }
    }
    return self.selectedMeasurement;
}

- (NSInteger)getUserSelectedMeasurementAsIndex
{
    NSInteger index = 0;
    for (Measurement* measurement in measurementArray){
        if ([measurement.description isEqualToString:selectedMeasurement]){
            return index;
        }
        index++;
    }
    return -1;
}

- (void)setUserSelectedMeasurement:(NSString *)aUserSelectedMeasurement
{
    self.selectedMeasurement = aUserSelectedMeasurement;
    [[NSUserDefaults standardUserDefaults] setValue:selectedMeasurement forKey:@"selectedMeasurement"];
    [[NSUserDefaults standardUserDefaults] synchronize];    
}

- (void)setUserSelectedMeasurementWithIndex:(NSUInteger)index
{
    BOOL validIndex = index < [measurementArray count];
    assert(validIndex);
    
    if (validIndex){
        Measurement* measurement = [measurementArray objectAtIndex:index];
        [self setUserSelectedMeasurement:measurement.description];
    }
}

- (void)setBearing:(Product *)bearing
{
    selectedBearing = bearing;
    [self updateBearingsQueue:bearing];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kBearingDidChange" object:self];
}
           
- (Product *)getBearing {
    
    return selectedBearing;

}
         
           
- (void)setBearingType:(BearingType *)bearingType
{
    selectedBearingType = bearingType;
    NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:selectedBearingType];
    [[NSUserDefaults standardUserDefaults] setValue:myEncodedObject forKey:@"selectedBearingType"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kBearingTypeDidChange" object:self];
}

- (void)setCalculation:(Calculation *)calculation
{
    selectedCalculation =  calculation;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kCalculationDidChange" object:self];

}



/** Modified for MultipleCalc **/

- (NSString *)getTranslationString:(NSString *)keyString
{
    for (Translation *translation in translationArray) {
        if ([translation.name isEqualToString:keyString])
        {
            return (translation.description);
        }
    }
    return [keyString stringByReplacingOccurrencesOfString:@"_" withString:@" "];
}


- (NSString *)getTranslationStringParameter:(NSString *)keyString, NSArray* values
{
    NSString* translationString = [self getTranslationString:keyString];
    int pos = 0;
    for (NSString* value in values){
        NSString* replaceKey = [NSString stringWithFormat:@"/*%d*/", pos];
        translationString = [translationString stringByReplacingOccurrencesOfString:replaceKey withString:value];
        pos ++;
    }
    return translationString;
}


- (void)initTranslation:(id<CalculationDataManagerDelegate>)delegate{
  if ([translationArray count] > 0) {
	  [delegate dataManagerReturnedTranslation:TRUE];
  }
  else {
	  if (!connected) {
		  [self showConnectedAlert];
		  return;
	  }
	  [self getTranslation:delegate];
  }
}

- (void)checkSupportedVersion:(NSString*)versionString userData:(id)userData
{
    if (!connected) {
        [self showConnectedAlert];
        return;
    }
    NSString *getString = @"/Version";
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithKeysAndObjects:@"protocolVersion", versionString, nil];
    NSString *queryString = [dict URLEncodedString];
    NSString *resourcePath = [NSString stringWithFormat:@"%@?%@", getString, queryString];
    
    RKObjectLoader *loader = [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePath delegate:self];
    loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetVersionCheck]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    DLog(@"%@%@", [RKObjectManager sharedManager].client.baseURL, resourcePath);
}

- (void)getLegend:(id)userData
{
    if (!connected) {
        [self showConnectedAlert];
        return;
    }
    NSString *resourcePath = @"/Legend";
    RKObjectLoader *loader = [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePath delegate:self];
    loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetCalculations]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self setSearchingModeEnabled:YES];
    DLog(@"%@%@", [RKObjectManager sharedManager].client.baseURL, resourcePath);
}

-(void)getLanguages:(id)userData
{
    if (!connected) {
        [self showConnectedAlert];
        return;
    }
    NSString *getString = @"/Languages";
    NSString *resourcePath =  getString;
    
    RKObjectLoader *loader = [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePath delegate:self];
    loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetLanguages]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    DLog(@"%@%@", [RKObjectManager sharedManager].client.baseURL, resourcePath);

}

- (void)getMeasurements:(id)userData
{
    if (!connected) {
        [self showConnectedAlert];
        return;
    }
    NSString *getString = @"/Measurements";
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithKeysAndObjects:@"language", selectedLanguage, nil];
    NSString *queryString = [dict URLEncodedString];
    NSString *resourcePath = [NSString stringWithFormat:@"%@?%@", getString, queryString];
    
    RKObjectLoader *loader = [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePath delegate:self];
    loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetMeasurements]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self setSearchingModeEnabled:YES];
    DLog(@"%@%@", [RKObjectManager sharedManager].client.baseURL, resourcePath);
}



- (void)getTranslation:(id)userData
{
//    if (!connected) {
//        [self showConnectedAlert];
//        return;
//    }
    NSString *getString = @"/Phrase";
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithKeysAndObjects:@"language", selectedLanguage, nil];
    NSString *queryString = [dict URLEncodedString];
    NSString *resourcePath = [NSString stringWithFormat:@"%@?%@", getString, queryString];
    
    RKObjectLoader *loader = [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePath delegate:self];

    serverStatus = 10;
    loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetTranslation]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    //[self setSearchingModeEnabled:YES];
    DLog(@"%@%@", [RKObjectManager sharedManager].client.baseURL, resourcePath);
}

- (void)getAllCalculations:(id)userData
{
    if (!connected) {
        [self showConnectedAlert];
        return;
    }
    NSString *getString = @"/Calculations";
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithKeysAndObjects:@"language", selectedLanguage, nil];
    NSString *queryString = [dict URLEncodedString];
    NSString *resourcePath = [NSString stringWithFormat:@"%@?%@", getString, queryString];
    NSLog(@"getAllCalculations:%@", resourcePath);

    RKObjectLoader *loader = [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePath delegate:self];
    loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetCalculations]];
    //[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    //[self setSearchingModeEnabled:YES];
    DLog(@"%@%@", [RKObjectManager sharedManager].client.baseURL, resourcePath);
}

- (void)getAllCalculationsForTab:(id)userData
{
    if (!connected) {
        [self showConnectedAlert];
        return;
    }
    
    NSMutableString *getString = [NSMutableString stringWithFormat:@"/Calculations?productId=%@", selectedBearing.productId];
    NSLog(@"%@", getString);
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithKeysAndObjects:@"language", selectedLanguage, nil];
    NSString *queryString = [dict URLEncodedString];
    NSString *resourcePath = [NSString stringWithFormat:@"%@&%@", getString, queryString];
    RKObjectLoader *loader = [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePath delegate:self];
    loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetCalculations]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    //[self setSearchingModeEnabled:YES];
   // serverCallCount++;
    DLog(@"%@%@", [RKObjectManager sharedManager].client.baseURL, resourcePath);
}


- (void)getCalculationsForBearing:(NSString *)productId userData:(id)userData
{
    if (!connected) {
        [self showConnectedAlert];
        return;
    }
    NSString *getString = @"/Calculations";
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithKeysAndObjects:@"productId", productId, @"measurement", selectedMeasurement, @"language", selectedLanguage, nil];
    NSString *queryString = [dict URLEncodedString];
    NSString *resourcePath = [NSString stringWithFormat:@"%@?%@", getString, queryString];

    RKObjectLoader *loader = [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePath delegate:self];
    loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetCalculations]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self setSearchingModeEnabled:YES];
    DLog(@"%@%@", [RKObjectManager sharedManager].client.baseURL, resourcePath);
}

- (void)getAllBearingTypes:(id)userData
{
//    if (!connected) {
//        [self showConnectedAlert];
//        return;
//    }
    NSString *getString = @"/BearingType";
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithKeysAndObjects:@"measurement", selectedMeasurement, @"language", selectedLanguage, nil];
    NSString *queryString = [dict URLEncodedString];
    NSString *resourcePath = [NSString stringWithFormat:@"%@?%@", getString, queryString];
    
    RKObjectLoader *loader = [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePath delegate:self];
    loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetBearingTypes]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
   // [self setSearchingModeEnabled:YES];
    DLog(@"%@%@", [RKObjectManager sharedManager].client.baseURL, resourcePath);
}

/** Modified for MultipleCalc */

- (void)getProducts:(NSDictionary *)params userData:(id)userData
{
    if (!connected) {
        [self showConnectedAlert];
        return;
    }
    NSString *getString = @"/Products";
    
    if(mutlipleCalculation)
    {
        for (Calculation *cal in selectedCalculationsArray)
        {
            if (([cal.calculationId length] != 0) && ([params objectForKey:@"calculation"] == nil))
            {
                [params setValue:cal.calculationId forKey:@"calculation"];
            }
            
        }
    }
    else
    {
        if (([selectedCalculation.calculationId length] != 0) && ([params objectForKey:@"calculation"] == nil))
        {
            [params setValue:selectedCalculation.calculationId forKey:@"calculation"];
        }
    }
    
    
    [params setValue:selectedMeasurement forKey:@"measurement"];
    [params setValue:selectedLanguage forKey:@"language"];
    NSString *queryString = [params URLEncodedString];
    
    /** For allowing special character like space, crossmark, backslash etc... for selecting bearing in designation tab. */
    
    NSMutableString *mutableQueryString = [queryString mutableCopy];
    
    NSLog(@"url before: %@", mutableQueryString);
    
    [mutableQueryString replaceOccurrencesOfString:@"%20" withString:@" " options:0 range:NSMakeRange(0, [mutableQueryString length])];
    [mutableQueryString replaceOccurrencesOfString:@"%2F" withString:@"/" options:0 range:NSMakeRange(0, [mutableQueryString length])];
    [mutableQueryString replaceOccurrencesOfString:@"%2C" withString:@"." options:0 range:NSMakeRange(0, [mutableQueryString length])];

        NSLog(@"url after: %@", mutableQueryString);
    
    [NSString stringWithString:mutableQueryString];
    queryString = mutableQueryString;
    
    /** */
    
    NSString *resourcePath = [NSString stringWithFormat:@"%@?%@", getString, queryString];
    RKObjectLoader *loader = [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePath delegate:self];
    loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetProductsForDesignation]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self setSearchingModeEnabled:YES];
    DLog(@"%@%@", [RKObjectManager sharedManager].client.baseURL, resourcePath);
}


- (void)getUpdatedProducts:(NSDictionary *)params1 userData:(id)userData
{
    if (!connected) {
        [self showConnectedAlert];
        return;
    }
    NSString *getString = @"/Products";

   NSString *product_id ;
    
    NSArray *product_list = [self previousBearingsArray];
   
    for (int i=0; i<product_list.count; i++) {
         NSMutableDictionary *params = [NSMutableDictionary dictionary];
        Product* pp = [product_list  objectAtIndex:i] ;
        product_id = pp.productId;
        
        NSLog(@"ProductID: %@, %@, %@",product_id,pp.designation,pp.typeId);
    
    
    
    
    [params setValue:@"1" forKey:@"calculation"];
    [params setValue:product_id forKey:@"productId"];
    [params setValue:selectedMeasurement forKey:@"measurement"];
    [params setValue:selectedLanguage forKey:@"language"];
    
    NSString *queryString = [params URLEncodedString];
    
    
    
    NSString *resourcePath = [NSString stringWithFormat:@"%@?%@", getString, queryString];
    RKObjectLoader *loader = [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePath delegate:self];
    loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetUpdateProductsForDesignation]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    //[self setSearchingModeEnabled:YES];
    DLog(@"%@%@", [RKObjectManager sharedManager].client.baseURL, resourcePath);
    }
}


/*- (void)getParametersForCalculation:(NSString *)calculation andProductId:(NSString *)productId userData:(id)userData
{
    if (!connected) {
        [self showConnectedAlert];
        return;
    }
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    NSString *getString = @"/Calculate";
    if(mutlipleCalculation) {
        getString = @"/Calculations";
    }else  if(!calculationDataManager.mutlipleCalculation) {
        getString = @"/Calculate";
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithKeysAndObjects:@"productId", productId, @"calculation", calculation, @"measurement", selectedMeasurement, @"language", selectedLanguage, nil];
    NSString *queryString = [dict URLEncodedString];
    NSString *resourcePath = [NSString stringWithFormat:@"%@?%@", getString, queryString];
    
    
    NSMutableString *resourcePathEncoded = [NSMutableString stringWithString:resourcePath];
    
    [resourcePathEncoded replaceOccurrencesOfString:@"%2C" withString:@"," options:0 range:NSMakeRange(0, [resourcePathEncoded length])];

    
    RKObjectLoader *loader = [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePathEncoded delegate:self];
    //loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetParametersForCalculation]];

    if(mutlipleCalculation) {
        loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetParametersForCalculations]];
    }
    else {
        loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetParametersForCalculation]];
    }
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    DLog(@"%@%@", [RKObjectManager sharedManager].client.baseURL, resourcePathEncoded);
}*/


- (void)getParametersForCalculation:(NSString *)calculation andProductId:(NSString *)productId userData:(id)userData
{
    if (!connected) {
        [self showConnectedAlert];
        return;
    }
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    NSLog(mutlipleCalculation ? @"Yes" : @"No");
    NSLog(@"CalculationId: %@",selectedCalculation.calculationId);
    
    isRelubricationCalcExist = FALSE;
    
    if(mutlipleCalculation)
    {
        for (Calculation *cal in selectedCalculationsArray)
        {
            if ([cal.calculationId isEqualToString:@"14"]) {
                
                isRelubricationCalcExist = TRUE;
                break;
                
            }
        }
        
    }else if ([selectedCalculation.calculationId isEqualToString:@"14"]) {
        
        isRelubricationCalcExist = TRUE;
    }
    
    NSLog(isRelubricationCalcExist ? @"YES" : @"NO");

    NSString *getString = @"/Calculate";
    if(UI_USER_INTERFACE_IDIOM () == UIUserInterfaceIdiomPad)
    {
        NSLog(@"Its iPad");
        if(mutlipleCalculation) {
            getString = @"/Calculations";
        }else  if(!calculationDataManager.mutlipleCalculation) {
            getString = @"/Calculate";
        }
    }else if (UI_USER_INTERFACE_IDIOM () == UIUserInterfaceIdiomPhone)
    {
        NSLog(@"Its iPhone");
                if (calculationDataManager.selectedCalculationsArray.count >1) {
            getString = @"/Calculations";
        }else {
            getString = @"/Calculate";
        }
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithKeysAndObjects:@"productId", productId, @"calculation", calculation, @"measurement", selectedMeasurement, @"language", selectedLanguage, nil];
    NSString *queryString = [dict URLEncodedString];
    NSString *resourcePath = [NSString stringWithFormat:@"%@?%@", getString, queryString];
    NSMutableString *resourcePathEncoded = [NSMutableString stringWithString:resourcePath];
    [resourcePathEncoded replaceOccurrencesOfString:@"%2C" withString:@"," options:0 range:NSMakeRange(0, [resourcePathEncoded length])];
    RKObjectLoader *loader = [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePathEncoded delegate:self];
    //loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetParametersForCalculation]];
    
    if(UI_USER_INTERFACE_IDIOM () == UIUserInterfaceIdiomPad)
    {
      
        if(mutlipleCalculation) {
            loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetParametersForCalculations]];
        }
        else {
            loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetParametersForCalculation]];
        }
    }else if (UI_USER_INTERFACE_IDIOM () == UIUserInterfaceIdiomPhone)
    {
        if (calculationDataManager.selectedCalculationsArray.count >1) {
            
            loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetParametersForCalculations]];
            
        }else {
            
            loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetParametersForCalculation]];
            
        }
    }
    
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    DLog(@"%@%@", [RKObjectManager sharedManager].client.baseURL, resourcePathEncoded);
}

- (void)getParametersForCalculation:(NSString *)calculation userData:(id)userData
{
    if (!connected) {
        [self showConnectedAlert];
        return;
    }
    NSString *getString = @"/Calculate";
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:calculation, @"calculation", selectedMeasurement, @"measurement", selectedLanguage, @"language", nil];
    NSString *queryString = [dict URLEncodedString];
    NSString *resourcePath = [NSString stringWithFormat:@"%@?%@", getString, queryString];

    RKObjectLoader *loader = [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePath delegate:self];
    //loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetParametersForCalculation]];
    if(mutlipleCalculation) {
        loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetParametersForCalculations]];
    }
    else {
        loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kGetParametersForCalculation]];
    }

    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    //[self setSearchingModeEnabled:YES];
    DLog(@"%@%@", [RKObjectManager sharedManager].client.baseURL, resourcePath);
}

- (void)doCalculationForProductId:(NSString *)productId andCalculation:(NSString *)calculation andParameters:(NSDictionary *)params userData:(id)userData
{
    if (!connected) {
        [self showConnectedAlert];
        return;
    }
    NSString *getString = @"/Calculate";
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:productId, @"productId", calculation, @"calculation", selectedMeasurement, @"measurement", selectedLanguage, @"language", nil];
    [dict addEntriesFromDictionary:params];
    NSString *queryString = [dict URLEncodedString];
    
    NSString *resourcePath = [NSString stringWithFormat:@"%@?%@", getString, queryString];
    RKObjectLoader *loader = [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePath delegate:self];
    loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kDoCalculation]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self setSearchingModeEnabled:YES];
    DLog(@"%@%@", [RKObjectManager sharedManager].client.baseURL, resourcePath);
}

- (void)doCalculation:(NSString *)calculation withParameters:(NSDictionary *)params userData:(id)userData
{
    if (!connected) {
        [self showConnectedAlert];
        return;
    }
    NSString *getString = @"/Calculate";
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:calculation, @"calculation", selectedMeasurement, @"measurement", selectedLanguage, @"language", nil];
    [dict addEntriesFromDictionary:params];
    NSString *queryString = [dict URLEncodedString];
    
    NSString *resourcePath = [NSString stringWithFormat:@"%@?%@", getString, queryString];
    
    // MOdified by SK for MCR server side
    //RKObjectLoader *loader = [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePath delegate:self];
    
    NSMutableString *resourcePathEncoded = [NSMutableString stringWithString:resourcePath];
    [resourcePathEncoded replaceOccurrencesOfString:@"%2C" withString:@"," options:0 range:NSMakeRange(0, [resourcePathEncoded length])];
    RKObjectLoader *loader = [[RKObjectManager sharedManager] loadObjectsAtResourcePath:resourcePathEncoded delegate:self];

    loader.userData = [CDMUserData userData:userData withRequestType:[NSNumber numberWithInt:kDoCalculation]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    //[self setSearchingModeEnabled:YES];
    DLog(@"%@%@", [RKObjectManager sharedManager].client.baseURL, resourcePath);
}

- (void)removeSkippedOptions:(NSArray *)families
{
    for (Family *family in families) {
        NSMutableArray *cleanedArray = [family.options mutableCopy];
        
        NSMutableArray *discardedItems = [NSMutableArray array];
        
        for (Option *option in family.options) {
            if ([option.skip isEqualToString:@"true"])
                [discardedItems addObject:option];
        }

        [cleanedArray removeObjectsInArray:discardedItems];
        
        family.options = cleanedArray;
        [cleanedArray release];
    }
    
}

-(void)packResults:(NSArray*)values inArray:(NSMutableArray*)results ofClass:(Class)clazz
{
    if ([values isKindOfClass:[clazz class]])
    {
        [results addObject:values];
    }
    else
    {
        for (id obj in values)
        {
            [results addObject:obj];
        }
    }    
}

- (void)objectLoader:(RKObjectLoader *)objectLoader didLoadObjectDictionary:(NSDictionary *)dictionary
{
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    NSArray *legends = [dictionary objectForKey:@"Legend.parameters.parameter"];
    NSArray *parametersMissing = [dictionary objectForKey:@"calculation.parameters-missing.parameter"];
    NSArray *parametersMissing2 = [dictionary objectForKey:@"calculations.parameters-required.parameter"];
    NSArray *parametersResults = [dictionary objectForKey:@"calculation.parameters-result.parameter"];
    NSArray *parametersDatabase = [dictionary objectForKey:@"calculation.parameters-database.parameter"];
    NSArray *parametersAvailable = [dictionary objectForKey:@"calculation.parameters-available.parameter"];
    NSArray *products = [dictionary objectForKey:@"products.products.product"];
    NSArray *calculations = [dictionary objectForKey:@"calculations.calculations.calculation"];
    NSArray *bearingTypes = [dictionary objectForKey:@"bearing-groups.bearing-group.type"];
    NSArray *translations = [dictionary objectForKey:@"phrases.phrase.translation"];
    NSArray *languages = [dictionary objectForKey:@"languages.languages.language"];    
    NSArray *mesurements = [dictionary objectForKey:@"measurements.measurements.measurement"];    
    NSArray *warnings = [dictionary objectForKey:@"calculation.warnings.warning"];
    NSArray *warnings2 = [dictionary objectForKey:@"calculations.warnings.warning"];
    NSArray *errors = [dictionary objectForKey:@"calculation.errors.error"];
    NSArray *errors2 = [dictionary objectForKey:@"calculations.errors.error"];
    NSArray *messages = [dictionary objectForKey:@"calculation.messages.message"];
    //NSArray *messages2 = [dictionary objectForKey:@"calculations.messages.message"];

    NSArray *versions = [dictionary objectForKey:@"Version"];
    
    
    
    
    NSMutableArray *results = [NSMutableArray array];
    NSMutableArray *databaseResults = [NSMutableArray array];
    NSMutableArray *availablesResults = [NSMutableArray array];
    NSMutableArray *errorResults = [NSMutableArray array];
    NSMutableArray *warningResults = [NSMutableArray array];
    NSMutableArray *messageResults = [NSMutableArray array];
    
    if ([dictionary count] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: [calculationDataManager getTranslationString:@"No_results"]
                              message: [calculationDataManager getTranslationString:@"The_search_did_not_yield_any_results."]
                              delegate: nil
                              cancelButtonTitle:[calculationDataManager getTranslationString:@"OK"]
                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    
    CDMUserData* reqUserData = objectLoader.userData;
    lastUserData = reqUserData.userData;    
    
    switch ([reqUserData.requestType intValue]) {
        case kGetLegend:
            [self packResults:legends inArray:results ofClass:[Legend class]];
            [self notifyDataManagerReturnedLegend:results];
            break;
            
        case kGetCalculations:
            [self packResults:calculations inArray:results ofClass:[Calculation class]];
            [self notifyDataManagerReturnedCalculations:results];
            break;
            
        case kGetBearingTypes:
            [self packResults:bearingTypes inArray:results ofClass:[BearingType class]];
            [self notifyDataManagerReturnedBearingTypes:results];
            break;

        case kGetProductsForDesignation:
            [self packResults:products inArray:results ofClass:[Product class]];
            [self notifyDataManagerReturnedProducts:results];
            break;
            
        case kGetUpdateProductsForDesignation:
            [self packResults:products inArray:results ofClass:[Product class]];
            [self updateBearingsQueueForUnitChange:results];
            break;
            
//        case kGetProductsForDetails:
//            [self packResults:products inArray:results ofClass:[Product class]];
//            [self notifyDataManagerReturnedProductsForDetails:results];
//            break;

            
        case kGetParametersForCalculation:
            if ([parametersMissing isKindOfClass:[ParameterMissing class]])
            {
                ParameterMissing *oneParam = (ParameterMissing *)parametersMissing;
                [self removeSkippedOptions:oneParam.families];
                [results addObject:oneParam];
            }
            else
            {
                for (ParameterMissing *parameterMissing in parametersMissing)
                {
                    [self removeSkippedOptions:parameterMissing.families];
                    [results addObject:parameterMissing];
                }
            }
            
            /** ERRORS **/
            [self packResults:errors inArray:errorResults ofClass:[Error class]];
            [self notifyDataManagerReturnedParametersErrors:errorResults];
            
            /** WARNINGS **/
            [self packResults:warnings inArray:warningResults ofClass:[Warning class]];
            [self notifyDataManagerReturnedParametersWarnings:warningResults];

            [self notifyDataManagerReturnedParametersForCalculation:results];
            break;
        
        case kGetParametersForCalculations:
            if ([parametersMissing2 isKindOfClass:[ParameterMissing class]])
            {
                ParameterMissing *oneParam = (ParameterMissing *)parametersMissing2;
                [self removeSkippedOptions:oneParam.families];
                [results addObject:oneParam];
            }
            else
            {
                for (ParameterMissing *parameterMissing in parametersMissing2)
                {
                    [self removeSkippedOptions:parameterMissing.families];
                    [results addObject:parameterMissing];
                }
            }
            
            /** ERRORS **/
            [self packResults:errors2 inArray:errorResults ofClass:[Error class]];
            [self notifyDataManagerReturnedParametersErrors:errorResults];
            
            /** WARNINGS **/
            [self packResults:warnings2 inArray:warningResults ofClass:[Warning class]];
            [self notifyDataManagerReturnedParametersWarnings:warningResults];
            
            [self notifyDataManagerReturnedParametersForCalculation:results];
            break;
    
        case kDoCalculation:
            [self packResults:parametersResults inArray:results ofClass:[ParameterResult class]];
            [self packResults:parametersDatabase inArray:databaseResults ofClass:[ParameterDatabase class]];
            [self packResults:parametersAvailable inArray:availablesResults ofClass:[ParameterResult class]];
            [self packResults:errors inArray:errorResults ofClass:[Error class]];
            [self packResults:warnings inArray:warningResults ofClass:[Warning class]];
            [self packResults:messages inArray:messageResults ofClass:[Message class]];

            // for multiple calculations result
            [self packResults:calculations inArray:results ofClass:[Calculation class]];
            
            [self notifyDataManagerReturnedParametersErrors:errorResults];
            [self notifyDataManagerReturnedParametersWarnings:warningResults];
            [self notifyDataManagerReturnedCalculationMessages:messageResults];
            [self notifyDataManagerReturnedCalculationResults:results];
            [self notifyDataManagerReturnedCalculationDatabase:databaseResults];
            
            NSMutableDictionary *dictionaryResults = [NSMutableDictionary dictionaryWithKeysAndObjects:@"results", results,
                                                      @"availablesResults", availablesResults,
                                                      @"databaseResults", databaseResults, 
                                                      @"errorResults", errorResults, 
                                                      @"warningResults", warningResults,
                                                      @"messageResults", messageResults,
                                                      @"selectedProduct", selectedBearing,nil];
            
            [self notifyDataManagerReturnedCalculationDictionary:dictionaryResults];
            break;
            
        case kGetTranslation:
            [self packResults:translations inArray:results ofClass:[Translation class]];
            NSLog(@"%d",[results count]);
            self.translationArray = results;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kGotTranslation" object:self];
            
            [self notifyDataManagerReturnedTranslation:[results count] != 0];

            break;
            
        case kGetVersionCheck:
            [self packResults:versions inArray:results ofClass:[Version class]];
            
            self.version = [results count] != 0 ? [results objectAtIndex:0] : nil;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kGotVersion" object:self];
            
            [self notifyDataManagerReturnedVersionCheck:[results count] != 0];
            
            break;
            
        case kGetLanguages:
            [self packResults:languages inArray:results ofClass:[Language class]];
            
            self.languageArray = results;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kGotLanguages" object:self];
            
            [self notifyDataManagerReturnedLangauges:[results count] != 0];
            
            break;
            
        case kGetMeasurements:
            [self packResults:mesurements inArray:results ofClass:[Measurement class]];
            
            self.measurementArray = results;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kGotMeasurements" object:self];
            
            [self notifyDataManagerReturnedMeasurements:[results count] != 0];

            break;
            
            
        default:
            break;
            
    }
    
    /*
    for (id obj in results){
        if ([obj respondsToSelector:@selector(setDescription:)]){
            [obj setDescription:[NSString stringWithFormat:@"S:%@", [obj description]]];
        }
        if ([obj respondsToSelector:@selector(setPresentation:)]){
            [obj setPresentation:[NSString stringWithFormat:@"S:%@", [obj presentation]]];
        }
    }
     */
    
    [self notifyCalculationDataManager:self returnedArray:results];
    
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;   // Commented for multiple calc
//    [self setSearchingModeEnabled:NO];
    
}

- (void)objectLoader:(RKObjectLoader *)objectLoader didFailWithError:(NSError *)error
{
    ALog(@"%@", error.userInfo);
    
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [self setSearchingModeEnabled:NO];

    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    if (error.code == 1001)
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: [calculationDataManager getTranslationString:@"No_results"]
                              message: [calculationDataManager getTranslationString:@"The_search_did_not_yield_any_results."]
                              delegate: nil
                              cancelButtonTitle:[calculationDataManager getTranslationString:@"OK"]
                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        
    }else if(error.code == -1001 && serverStatus == 10){
        
        
        
        //ALog(@" network error in calculation datamanager .......... ////////////////////////  ");
        UIAlertView * alert =   [[UIAlertView alloc]
                                 initWithTitle:@"Server error"
                                 message:@"Server is not responding, \nplease try after sometime."
                                 delegate:self
                                 cancelButtonTitle:@"Exit"
                                 otherButtonTitles:nil];
        
        [alert show];
        [alert release];
        
    }

    
    
        
}

-(void) showAlertForNetworkConnection {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[calculationDataManager getTranslationString:@"Network_error!"]
                                                    message:[calculationDataManager getTranslationString:@"This_application_need_a_network_connection."]
                                                   delegate:nil
                                          cancelButtonTitle:[calculationDataManager getTranslationString:@"OK"]
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
    
    
}

- (void)request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
    DLog(@"Body length: %d", [[response body] length]);
    DLog (@"Retrieved : %@", [response bodyAsString]);
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self setSearchingModeEnabled:NO];
    
 
}



- (void)notifyCalculationDataManager:(CalculationDataManager *)calculationDataManager returnedArray:(NSArray *)returnedArray
{
    for (id<CalculationDataManagerDelegate> aDelegate in _observers){
        if ([aDelegate respondsToSelector:@selector(calculationDataManager:returnedArray:)]){
            [aDelegate calculationDataManager:calculationDataManager returnedArray:returnedArray];
        }
    }

    
}


/****** Methods for MultipleCalc *****/

/** adding selected calc to array*/
//-(void)setSelectedCalculationsArray:(NSArray *)aselectedCalculationsArray
//{
//    selectedCalculationsArray = aselectedCalculationsArray;
//    
//}




-(void)notifyForCalculationChangeForCalculation:(Calculation *)calc
{
    selectedCalculation = calc;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kCalculationDidChange" object:self];
}

#define NOTIFY_RETURN(METHOD, PARM) \
{\
    for (id<CalculationDataManagerDelegate> aDelegate in _observers){\
        if ([aDelegate respondsToSelector:@selector(METHOD:)]){\
            [aDelegate METHOD:PARM];\
        }\
    }\
}\

- (void)notifyDataManagerReturnedLegend:(NSArray *)returnedArray
NOTIFY_RETURN(dataManagerReturnedLegend, returnedArray)

- (void)notifyDataManagerReturnedTranslation:(BOOL)success
NOTIFY_RETURN(dataManagerReturnedTranslation, success)

- (void)notifyDataManagerReturnedLangauges:(BOOL)success
NOTIFY_RETURN(dataManagerReturnedLangauges, success)

- (void)notifyDataManagerReturnedVersionCheck:(BOOL)success
NOTIFY_RETURN(dataManagerReturnedVersionCheck, success)

- (void)notifyDataManagerReturnedMeasurements:(BOOL)success
NOTIFY_RETURN(dataManagerReturnedMeasurements, success)

- (void)notifyDataManagerReturnedCalculations:(NSArray *)returnedArray
NOTIFY_RETURN(dataManagerReturnedCalculations, returnedArray)

- (void)notifyDataManagerReturnedBearingTypes:(NSArray *)returnedArray
NOTIFY_RETURN(dataManagerReturnedBearingTypes, returnedArray)

- (void)notifyDataManagerReturnedProducts:(NSArray *)returnedArray
NOTIFY_RETURN(dataManagerReturnedProducts, returnedArray)

- (void)notifyDataManagerReturnedParametersForCalculation:(NSArray *)returnedArray
NOTIFY_RETURN(dataManagerReturnedParametersForCalculation, returnedArray)

- (void)notifyDataManagerReturnedParametersErrors:(NSArray *)returnedArray
NOTIFY_RETURN(dataManagerReturnedParametersErrors, returnedArray)

- (void)notifyDataManagerReturnedParametersWarnings:(NSArray *)returnedArray
NOTIFY_RETURN(dataManagerReturnedParametersWarnings, returnedArray)

- (void)notifyDataManagerReturnedCalculationErrors:(NSArray *)returnedArray
NOTIFY_RETURN(dataManagerReturnedCalculationErrors, returnedArray)

- (void)notifyDataManagerReturnedCalculationWarnings:(NSArray *)returnedArray
NOTIFY_RETURN(dataManagerReturnedCalculationWarnings, returnedArray)

- (void)notifyDataManagerReturnedCalculationMessages:(NSArray *)returnedArray
NOTIFY_RETURN(dataManagerReturnedCalculationMessages, returnedArray)

- (void)notifyDataManagerReturnedCalculationResults:(NSArray *)returnedArray
NOTIFY_RETURN(dataManagerReturnedCalculationResults, returnedArray)

- (void)notifyDataManagerReturnedCalculationDatabase:(NSArray *)returnedArray
NOTIFY_RETURN(dataManagerReturnedCalculationDatabase, returnedArray)

- (void)notifyDataManagerReturnedCalculationDictionary:(NSDictionary *)returnedDictionary
NOTIFY_RETURN(dataManagerReturnedCalculationDictionary, returnedDictionary)


@end
