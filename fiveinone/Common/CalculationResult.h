//
//  CalculationResult.h
//  fiveinone
//
//  Created by Erik Lindberg on 2011-10-24.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculationResult : NSObject

//TODO - Support for Arrays
@property (nonatomic, retain) NSArray *errors;
@property (nonatomic, retain) NSArray *warnings;

@end
