//
//  DirectoryWatcher.h
//  fiveinone
//
//  Created by Admin on 27/11/13.
//  Copyright (c) 2013 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DirectoryWatcher;



@protocol DirectoryWatcherDelegate <NSObject>

@required

- (void)directoryDidChange:(DirectoryWatcher *)folderWatcher;

@end



@interface DirectoryWatcher : NSObject

{
    
    id <DirectoryWatcherDelegate> __weak delegate;
    
    
    
    int dirFD;
    
    int kq;
    
    
    
    CFFileDescriptorRef dirKQRef;
    
}

@property (nonatomic, weak) id <DirectoryWatcherDelegate> delegate;



+ (DirectoryWatcher *)watchFolderWithPath:(NSString *)watchPath delegate:(id<DirectoryWatcherDelegate>)watchDelegate;

- (void)invalidate;

@end