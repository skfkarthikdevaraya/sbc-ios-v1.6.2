//
//  Error.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-10-29.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "Error.h"

@implementation Error

@synthesize key;
@synthesize description;
@synthesize errorId;

@end
