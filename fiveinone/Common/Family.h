//
//  Family.h
//  fiveinone
//
//  Created by Erik Lindberg on 2011-10-29.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Family : NSObject

@property (nonatomic, retain) NSString *familyId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSArray *options;

@end
