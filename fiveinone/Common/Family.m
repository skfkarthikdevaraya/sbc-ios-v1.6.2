//
//  Family.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-10-29.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "Family.h"

@implementation Family

@synthesize familyId;
@synthesize name;
@synthesize options;

@end
