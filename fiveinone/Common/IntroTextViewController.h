//
//  IntroTextViewController.h
//  fiveinone
//
//  Created by Admin on 22/11/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface IntroTextViewController : UIViewController <MBProgressHUDDelegate> {
    
//#import "GAITrackedViewController.h"
//@interface IntroTextViewController : GAITrackedViewController <MBProgressHUDDelegate> {


    
        UIButton* buttonSkip;
        UIWebView* introText;
        NSString *cssString;
        UILabel *appTitleLabel;
        UILabel *appVersionLabel;
        UILabel *dontShowAgain;
    
        MBProgressHUD *HUD;
        
}
    
@property (nonatomic,retain) IBOutlet UIButton *buttonSkip;
@property (nonatomic,retain) IBOutlet UIWebView *introText;
@property (nonatomic,retain) NSString *cssString;
@property (nonatomic, retain) IBOutlet UILabel *appTitleLabel;
@property (nonatomic, retain) IBOutlet UILabel *appVersionLabel;
@property (nonatomic,retain)  IBOutlet UILabel *dontShowAgain;

@property (nonatomic, retain) MBProgressHUD *HUD;

- (void)gotTranslation:(id)sender;



@end
