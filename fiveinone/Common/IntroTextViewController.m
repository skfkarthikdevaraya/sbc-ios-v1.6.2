//
//  IntroTextViewController.m
//  fiveinone
//
//  Created by Admin on 22/11/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import "IntroTextViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "CalculationDataManager.h"
#import "IntroTextViewController_iPhone.h"


@implementation IntroTextViewController

@synthesize buttonSkip, introText, cssString;
@synthesize appTitleLabel;
@synthesize appVersionLabel;
@synthesize dontShowAgain;
@synthesize HUD;


- (void)dealloc {
    [buttonSkip release];
    [introText release];
    [super dealloc];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    //[self setSearchingModeStart];
    
    cssString = @"<html> \n"
    "<head> \n"
    "<style type=\"text/css\"> \n"
    "body {font-family: \"SKF Chevin Medium\"; font-size: 13; padding:15px; margin:4px 0px;background-color:none; color:#333333 word-wrap:break-word;}\n"
    "</style> \n"
    "</head> \n";
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    //NSString *date = [infoDictionary objectForKey:@"CFBuildDate"];
    NSString *version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *build = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    appTitleLabel.font      = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_APPTITLE];
    appVersionLabel.font    = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_APPVERSION];
    appTitleLabel.textColor = [UIColor colorWithRed:218.0/255.0 green:30.0/255.0 blue:60.0/255.0 alpha:1];
    appVersionLabel.textColor = [UIColor colorWithRed:94.0/255.0 green:94.0/255.0 blue:94.0/255.0 alpha:1];
    
    
    NSString * appVersionString = [NSString stringWithFormat:@"Version: %@ (Build: %@)",version,build];
    appTitleLabel.text = APP_TITLE;
    appVersionLabel.text = appVersionString;
    
    //buttonSkip.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
    
    // Proceed button with iOS 6 style | for iOS7 style just comment below code
    /*UIImage *proceedButtonImage = [UIImage imageNamed:@"ipad_blue_button.png"];
    UIImage *proceedButtonImageStrechable = [proceedButtonImage stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [buttonSkip setBackgroundImage:proceedButtonImageStrechable forState:UIControlStateNormal];
    [buttonSkip setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateSelected];
    [buttonSkip setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateNormal];
    buttonSkip.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON];
    buttonSkip.titleLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_WHITE];*/
    
    introText.layer.cornerRadius = 8;
    introText.layer.masksToBounds = YES;
    [introText setBackgroundColor:[UIColor clearColor]];
    [introText setOpaque:NO];
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if ([[prefs valueForKey:@"firstrun"] isEqualToString:@"NO"]) {
        [self gotTranslation:nil];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gotTranslation:)
                                                 name:@"kGotTranslation"
                                               object:nil];

    //dontShowAgain = [[UILabel alloc]initWithFrame:CGRectMake(50.0, 420.0, 130.0, 21.0)];
    dontShowAgain.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_MESSAGES];
    dontShowAgain.textColor = [UIColor colorWithHexString:LABEL_COLOR_LT_BLACK];
    dontShowAgain.backgroundColor = [UIColor clearColor];
    dontShowAgain.adjustsFontSizeToFitWidth = YES;
    //dontShowAgain.minimumScaleFactor = FONT_SIZE_FOR_MESSAGES;
    //[self.view addSubview:dontShowAgain];
    buttonSkip.hidden = YES;
    dontShowAgain.hidden = YES;
    
    
}


-(NSString *)fixBR:(NSString *)inString
{
    return [inString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"];
}

- (void)gotTranslation:(id)sender {
           
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [buttonSkip setTitle:[calculationDataManager getTranslationString:@"proceed"] forState:UIControlStateNormal];
    
    appTitleLabel.text = [calculationDataManager getTranslationString:@"Application_name"];
    [introText loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString,[self fixBR:[calculationDataManager getTranslationString:@"introText_iOS"]]] baseURL:[NSURL URLWithString:@""]];
    
    
    [dontShowAgain setText:[calculationDataManager getTranslationString:@"Dont_show_again"]];

    
    
    [self setSearchingModeEnd];
    

    
   
        
}

//- (void)setSearchingModeStart {
//    
//    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
//    
//    HUD = [[MBProgressHUD alloc] initWithFrame:CGRectMake(35, 100, 250, 250)];
//    HUD.labelText = [calculationDataManager getTranslationString:@"Searching"];
//    HUD.graceTime = 0.5f;
//    HUD.taskInProgress = YES;
//    HUD.allowsCancelation = YES;
//    HUD.delegate = self;
//    HUD.labelFont = [UIFont fontWithName:CHEVIN_BOLD size:20.f];
//    [self.view addSubview:HUD];
//    [HUD show:YES];
//    //[self.view setUserInteractionEnabled:NO];
//}
- (void)setSearchingModeEnd {
    
    [HUD hide:YES];
    HUD.taskInProgress = NO;
    [HUD removeFromSuperview];
    [self.view setUserInteractionEnabled:YES];
    self.navigationItem.hidesBackButton = NO;
    
    buttonSkip.hidden = NO;
    dontShowAgain.hidden = NO;

    [HUD release];
    HUD = nil;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end
