//
//  Language.h
//  fiveinone
//
//  Created by Tommy Eriksson on 4/4/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Language : NSObject
{
    
}

@property (nonatomic, retain) NSString *languageId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;

@end
