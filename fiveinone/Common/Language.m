//
//  Language.m
//  fiveinone
//
//  Created by Tommy Eriksson on 4/4/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import "Language.h"

@implementation Language

@synthesize languageId;
@synthesize description;
@synthesize name;

@end
