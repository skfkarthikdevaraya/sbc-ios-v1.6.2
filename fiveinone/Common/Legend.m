//
//  Calculation.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-09-15.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "Legend.h"

@implementation Legend

@synthesize parameterId;
@synthesize description;
@synthesize presentation;
@synthesize subCalculation;
@synthesize dataType;

@end
