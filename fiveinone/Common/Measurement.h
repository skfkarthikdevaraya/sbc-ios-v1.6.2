//
//  Measurement.h
//  fiveinone
//
//  Created by Tommy Eriksson on 4/4/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Measurement : NSObject
{
    
}

@property (nonatomic, retain) NSString *measurementId;
@property (nonatomic, retain) NSString *description;

@end
