//
//  Measurement.m
//  fiveinone
//
//  Created by Tommy Eriksson on 4/4/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import "Measurement.h"

@implementation Measurement

@synthesize measurementId;
@synthesize description;

@end
