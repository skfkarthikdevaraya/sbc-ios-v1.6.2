//
//  Message.h
//  fiveinone
//
//  Created by Erik Lindberg on 2011-10-31.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject

@property (nonatomic, retain) NSString *key;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *messageId;

@end
