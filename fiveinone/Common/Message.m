//
//  Message.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-10-31.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "Message.h"

@implementation Message

@synthesize key;
@synthesize description;
@synthesize messageId;

@end
