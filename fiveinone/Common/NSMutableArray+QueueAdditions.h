//
//  NSMutableArray+QueueAdditions.h
//  fiveinone
//
//  Created by Erik Lindberg on 2011-09-22.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (QueueAdditions)
- (id) dequeue;
- (void) enqueue:(id)obj;
- (void)moveObjectFromIndex:(NSUInteger)from toIndex:(NSUInteger)to;
@end