//
//  NSString+StripHTML.h
//  fiveinone
//
//  Created by Erik Lindberg on 2011-11-10.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (StripHTML)

+ (NSString *)stringByStrippingHTML:(NSString *)inString;

@end
