//
//  NSString+StripHTML.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-11-10.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "NSString+StripHTML.h"

@implementation NSString (StripHTML)

+(NSString *)stringByStrippingHTML:(NSString *)inString
{
    if (inString == nil)
    {
        return @"";
    }
    
    NSMutableString *modifyString = [inString mutableCopy];
    [modifyString replaceOccurrencesOfString:@"&lt;" withString:@"<" options:NSCaseInsensitiveSearch 
                                       range:NSMakeRange(0, [modifyString length])];
    [modifyString replaceOccurrencesOfString:@"&gt;" withString:@">" options:NSCaseInsensitiveSearch 
                                       range:NSMakeRange(0, [modifyString length])];
    [modifyString replaceOccurrencesOfString:@"&amp;" withString:@"&"options:NSCaseInsensitiveSearch 
                                       range:NSMakeRange(0, [modifyString length])];
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"&([^;]+);" options:NSRegularExpressionCaseInsensitive error:&error];
    NSArray *matches = [regex matchesInString:modifyString
                                      options:0
                                        range:NSMakeRange(0, [modifyString length])];
    int offset = 0;
    for (int i=0; i<[matches count]; i++) {
        NSTextCheckingResult *matchTextChecking = [matches objectAtIndex:i];
        NSRange match = [matchTextChecking range];
        NSRange matchAndOffset = NSMakeRange(match.location-offset,match.length);
        [modifyString replaceCharactersInRange:matchAndOffset withString:@"M"];
        offset = offset + match.length;
    }
    
    NSMutableString *ms = [NSMutableString stringWithCapacity:[modifyString length]];
    
    NSScanner *scanner = [NSScanner scannerWithString:modifyString];
    [scanner setCharactersToBeSkipped:nil];
    NSString *s = nil;
    while (![scanner isAtEnd])
    {
        [scanner scanUpToString:@"<" intoString:&s];
        if (s != nil)
            [ms appendString:s];
        [scanner scanUpToString:@">" intoString:NULL];
        if (![scanner isAtEnd])
            [scanner setScanLocation:[scanner scanLocation]+1];
        s = nil;
    }
    
    [modifyString release];
    
    return ms;
}





@end
