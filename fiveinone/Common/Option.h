//
//  Option.h
//  fiveinone
//
//  Created by Erik Lindberg on 2011-10-29.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Option : NSObject

@property (nonatomic, retain) NSString *optionId;
@property (nonatomic, retain) NSString *presentation;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *skip;

@end
