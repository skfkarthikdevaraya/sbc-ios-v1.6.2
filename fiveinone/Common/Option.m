//
//  Option.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-10-29.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "Option.h"

@implementation Option

@synthesize optionId;
@synthesize presentation;
@synthesize description;
@synthesize skip;

@end
