//
//  ParametersAvailable.h
//  fiveinone
//
//  Created by Erik Lindberg on 2011-11-02.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParameterAvailable : NSObject

@property (nonatomic, retain) NSString *parameterAvailableId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *presentation;
@property (nonatomic, retain) NSString *uom;
@property (nonatomic, retain) NSString *value;
@property (nonatomic, retain) NSString *subCalculation;
@property (nonatomic, retain) NSString *dataType;
@property (nonatomic, retain) NSString *valueMin;
@property (nonatomic, retain) NSString *valueMax;
@property (nonatomic, retain) NSArray *families;

@end
