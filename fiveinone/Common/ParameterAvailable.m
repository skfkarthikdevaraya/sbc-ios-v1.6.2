//
//  ParametersAvailable.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-11-02.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "ParameterAvailable.h"

@implementation ParameterAvailable

@synthesize parameterAvailableId;
@synthesize name;
@synthesize description;
@synthesize presentation;
@synthesize uom;
@synthesize value;
@synthesize subCalculation;
@synthesize dataType;
@synthesize valueMin;
@synthesize valueMax;
@synthesize families;

@end
