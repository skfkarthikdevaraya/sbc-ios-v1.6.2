//
//  ParameterDatabase.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-11-01.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "ParameterDatabase.h"

@implementation ParameterDatabase

@synthesize parameterDatabaseId;
@synthesize name;
@synthesize description;
@synthesize presentation;
@synthesize uom;
@synthesize value;
@synthesize subCalculation;
@synthesize dataType;
@synthesize valueMin;
@synthesize valueMax;

@end
