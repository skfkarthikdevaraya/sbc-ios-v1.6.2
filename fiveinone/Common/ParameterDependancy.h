//
//  ParameterDependancy.h
//  fiveinone
//
//  Created by Tommy Eriksson on 4/24/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParameterDependancy : NSObject

@property (nonatomic, retain) NSString *dependsOn;
@property (nonatomic, retain) NSString *dependsOnValue;
@property (nonatomic, retain) NSString *smallerThan;
@property (nonatomic, retain) NSString *biggerThan;

-(BOOL)isDepandancyFulfilled:(NSDictionary*)setParameters;

@end
