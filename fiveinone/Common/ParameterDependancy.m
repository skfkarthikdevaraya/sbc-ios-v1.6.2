//
//  ParameterDependancy.m
//  fiveinone
//
//  Created by Tommy Eriksson on 4/24/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import "ParameterDependancy.h"

@implementation ParameterDependancy

@synthesize dependsOn;
@synthesize dependsOnValue;

@synthesize smallerThan;
@synthesize biggerThan;

-(BOOL)isDepandancyFulfilled:(NSDictionary*)setParameters{
    BOOL valueIsSet = [setParameters valueForKey:dependsOn] != nil && dependsOnValue != nil && dependsOn != nil;
    BOOL depandancyFulfilled = valueIsSet && [dependsOnValue isEqualToString:[setParameters valueForKey:dependsOn]];
    
    NSLog(@"%@ %@ %@", dependsOn, dependsOnValue, [setParameters valueForKey:dependsOn]);
    
    return depandancyFulfilled;
}

@end
