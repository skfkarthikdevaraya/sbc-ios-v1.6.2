//
//  ParameterMissing.h
//  fiveinone
//
//  Created by Erik Lindberg on 2011-09-15.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParameterDependancy.h"

@interface ParameterMissing : NSObject
{
    NSString *parameterId;

}

@property (nonatomic, retain) NSString *parameterId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *presentation;
@property (nonatomic, retain) NSString *subCalculation;
@property (nonatomic, retain) NSString *dataType;
@property (nonatomic, retain) NSString *uom;
@property (nonatomic, retain) NSString *valueMin;
@property (nonatomic, retain) NSString *valueMax;
@property (nonatomic, retain) NSString *valueDefault;
@property (nonatomic, retain) NSArray *families;
@property (nonatomic, retain) ParameterDependancy* depends;

@property (nonatomic, retain) NSString *displayOrder;

// Modified for MultipleCalc
@property (nonatomic, retain) NSString *calculationID;



@end
