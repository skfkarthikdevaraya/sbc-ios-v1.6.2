//
//  ParameterMissing.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-09-15.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "ParameterMissing.h"

@implementation ParameterMissing

@synthesize parameterId;
@synthesize name;
@synthesize description;
@synthesize presentation;
@synthesize subCalculation;
@synthesize dataType;
@synthesize uom;
@synthesize valueMin;
@synthesize valueMax;
@synthesize valueDefault;
@synthesize families;
@synthesize depends;
@synthesize displayOrder;


// Modified for MultipleCalc
@synthesize calculationID;

@end
