//
//  ParameterResult.h
//  fiveinone
//
//  Created by Erik Lindberg on 2011-09-15.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParameterResult : NSObject
{
    
}

@property (nonatomic, retain) NSString *parameterId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *presentation;
@property (nonatomic, retain) NSString *value;
@property (nonatomic, retain) NSString *subCalculation;
@property (nonatomic, retain) NSString *dataType;
@property (nonatomic, retain) NSString *uom;

@end
