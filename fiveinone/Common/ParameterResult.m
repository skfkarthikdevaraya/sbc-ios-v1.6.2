//
//  ParameterResult.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-09-15.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "ParameterResult.h"

@implementation ParameterResult

@synthesize parameterId;
@synthesize name;
@synthesize description;
@synthesize presentation;
@synthesize value;
@synthesize subCalculation;
@synthesize dataType;
@synthesize uom;

@end
