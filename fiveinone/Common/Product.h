//
//  Product.h
//  fiveinone
//
//  Created by Erik Lindberg on 2011-09-15.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject <NSCopying>
{
    NSString *productId;
    NSString *designation;
    NSString *typeId;
    NSString *typestext;
    NSString *type;
    NSArray  *properties;
   
   // BOOL explorer;
    NSString *explorer;
    
}

@property (nonatomic, retain) NSString *productId;
@property (nonatomic, retain) NSString *designation;
@property (nonatomic, retain) NSString *typeId;
@property (nonatomic, retain) NSString *typestext;
@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSArray *properties;

//@property(nonatomic, assign)  BOOL explorer;
@property (nonatomic,retain)  NSString *explorer;



- (id)initWithCoder:(NSCoder *)coder;
- (void)encodeWithCoder:(NSCoder *)coder;

@end
