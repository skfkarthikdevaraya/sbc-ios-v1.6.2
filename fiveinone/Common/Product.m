//
//  Product.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-09-15.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "Product.h"

@implementation Product

@synthesize productId;
@synthesize designation;
@synthesize typeId;
@synthesize typestext;
@synthesize type;
@synthesize properties;
@synthesize explorer;

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    if (self != nil)
    {
        self.productId = [coder decodeObjectForKey:@"productId"];
        self.designation = [coder decodeObjectForKey:@"designation"];
        self.typeId = [coder decodeObjectForKey:@"typeId"];
        self.typestext = [coder decodeObjectForKey:@"typestext"];
        self.type = [coder decodeObjectForKey:@"type"];
        self.properties = [coder decodeObjectForKey:@"properties"];
        self.explorer = [coder decodeObjectForKey:@"explorer"];
        
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self.productId forKey:@"productId"];
    [coder encodeObject:self.designation forKey:@"designation"];
    [coder encodeObject:self.typeId forKey:@"typeId"];
    [coder encodeObject:self.typestext forKey:@"typestext"];
    [coder encodeObject:self.type forKey:@"type"];
    [coder encodeObject:self.properties forKey:@"properties"];
    [coder encodeObject:self.explorer forKey:@"explorer"];
}

- (id)copyWithZone:(NSZone *)zone {
    
    Product *objectCopy = [[Product allocWithZone:zone] init];

    objectCopy.productId = [[self.productId copyWithZone:zone] autorelease];
    objectCopy.designation = [[self.designation copyWithZone:zone] autorelease];
    objectCopy.typeId = [[self.typeId copyWithZone:zone] autorelease];
    objectCopy.typestext = [[self.typestext copyWithZone:zone] autorelease];
    objectCopy.type = [[self.type copyWithZone:zone] autorelease];
    objectCopy.properties = [[self.properties copyWithZone:zone] autorelease];
    objectCopy.explorer = [[self.explorer copyWithZone:zone] autorelease];
    
    return objectCopy;
    
}


@end
