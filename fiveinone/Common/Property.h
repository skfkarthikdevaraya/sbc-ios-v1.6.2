//
//  Property.h
//  fiveinone
//
//  Created by Erik Lindberg on 2011-11-14.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Property : NSObject
{
    NSString *propertyId;
    NSString *presentation;
    NSString *value;
    NSString *uom;
}

@property (nonatomic, retain) NSString *propertyId;
@property (nonatomic, retain) NSString *presentation;
@property (nonatomic, retain) NSString *value;
@property (nonatomic, retain) NSString *uom;

- (id)initWithCoder:(NSCoder *)coder;
- (void)encodeWithCoder:(NSCoder *)coder;

@end
