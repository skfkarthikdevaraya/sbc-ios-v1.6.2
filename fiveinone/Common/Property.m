//
//  Property.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-11-14.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "Property.h"

@implementation Property

@synthesize propertyId;
@synthesize presentation;
@synthesize value;
@synthesize uom;

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    if (self != nil)
    {
        self.propertyId = [coder decodeObjectForKey:@"propertyId"];
        self.presentation = [coder decodeObjectForKey:@"presentation"];
        self.value = [coder decodeObjectForKey:@"value"];
        self.uom = [coder decodeObjectForKey:@"uom"];
    }   
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self.propertyId forKey:@"propertyId"];
    [coder encodeObject:self.presentation forKey:@"presentation"];
    [coder encodeObject:self.value forKey:@"value"];
    [coder encodeObject:self.uom forKey:@"uom"];
}

@end
