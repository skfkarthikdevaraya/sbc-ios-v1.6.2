//
//  Title.h
//  DialSet
//
//  Created by Martin Lannsjö on 9/27/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TitleManager : NSObject{
    
}

+(UILabel*)getTitle: (NSString*) title;

+(UIBarButtonItem*)getSKFLogo;
+(UIBarButtonItem*)fillLogoSpace;

@end
