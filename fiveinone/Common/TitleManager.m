//
//  Title.m
//  DialSet
//
//  Created by Martin Lannsjö on 9/27/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "TitleManager.h"

@implementation TitleManager

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

+(UILabel*)getTitle:(NSString *)title
{
    UILabel *skfChevinTitleLabel = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
    skfChevinTitleLabel.backgroundColor = [UIColor clearColor];
    skfChevinTitleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_NAVIGATION_TITLE];
    //skfChevinTitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    skfChevinTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    skfChevinTitleLabel.textColor = [UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f]; //
    
    skfChevinTitleLabel.text = title;
    [skfChevinTitleLabel sizeToFit];
    
    return skfChevinTitleLabel;
}

+(UIBarButtonItem*)getSKFLogo
{
    //UIImage *image = [UIImage imageNamed:@"skf_logo_white_94x48_red.png"];
    UIImage *image = [UIImage imageNamed:@"SKF_logo_white_94x48.png"];

    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage: [image stretchableImageWithLeftCapWidth:7.0 topCapHeight:0.0] forState:UIControlStateNormal];
    [button setBackgroundImage: [image stretchableImageWithLeftCapWidth:7.0 topCapHeight:0.0] forState:UIControlStateHighlighted];
    UIView *v;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {

        button.frame= CGRectMake(0.0, 0.0, image.size.width/2.0, image.size.height/2.0);
         v=[[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, image.size.width/2.0+5, image.size.height/2.0) ];

        
    }else {
        
        button.frame= CGRectMake(15.0, 0.0, image.size.width/1.75, image.size.height/1.75);
        v=[[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, image.size.width/1.75+5, image.size.height/1.75) ];


    }
    button.alpha = 0.9;
    [v addSubview:button];
    UIBarButtonItem *logo = [[[UIBarButtonItem alloc] initWithCustomView:v] autorelease];
    [v release];
    return logo;
}




+(UIBarButtonItem*)fillLogoSpace
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame= CGRectMake(0.0, 0.0, 56, 14);
    button.backgroundColor = [UIColor clearColor];
    UIView *v=[[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 50, 14) ];
    [v addSubview:button];
    UIBarButtonItem *logo = [[[UIBarButtonItem alloc] initWithCustomView:v] autorelease];
    [v release];
    return logo;
}

@end
