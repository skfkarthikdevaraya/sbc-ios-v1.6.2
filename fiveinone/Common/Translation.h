//
//  Translation.h
//  fiveinone
//
//  Created by Erik Lindberg on 2011-10-20.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Translation : NSObject
{
    
}

@property (nonatomic, retain) NSString *translationId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;

@end
