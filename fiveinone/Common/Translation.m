//
//  Translation.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-10-20.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "Translation.h"

@implementation Translation

@synthesize translationId;
@synthesize description;
@synthesize name;

@end

