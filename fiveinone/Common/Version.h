//
//  Version.h
//  fiveinone
//
//  Created by Tommy Eriksson on 7/2/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Version : NSObject

@property (nonatomic, retain) NSString* protocol;
@property (nonatomic, retain) NSString* Android;
@property (nonatomic, retain) NSString* Apple;

-(BOOL)isVersionSupported;
-(BOOL)isVersionDeprecated;
-(BOOL)isVersionNotSupported;
@end
