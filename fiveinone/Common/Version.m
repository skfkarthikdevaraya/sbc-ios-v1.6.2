//
//  Version.m
//  fiveinone
//
//  Created by Tommy Eriksson on 7/2/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import "Version.h"

@implementation Version

@synthesize protocol;
@synthesize Android;
@synthesize Apple;

-(BOOL)isVersionSupported{
    return [@"versionSupported" isEqualToString:protocol];
}

-(BOOL)isVersionDeprecated{
    return [@"versionDeprecated" isEqualToString:protocol];
}

-(BOOL)isVersionNotSupported{
    return [@"versionNotSupported" isEqualToString:protocol];    
}

@end
