//
//  VersionCheckControl.h
//  fiveinone
//
//  Created by Tommy Eriksson on 7/2/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CalculationDataManager.h"

@interface VersionCheckControl : NSObject <CalculationDataManagerDelegate, UIAlertViewDelegate>{
    NSString* upgradeURL;
}

-(void)performVersionCheck;

@end
