//
//  VersionCheckControl.m
//  fiveinone
//
//  Created by Tommy Eriksson on 7/2/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import "VersionCheckControl.h"
#import "CalculationDataManager.h"

@implementation VersionCheckControl

#define BTN_INDEX_UPGRADE 0
#define DEFAULT_UPGRADE_URL @"itms://itunes.apple.com/us/app/skf-bearing-calculator/id485691812?mt=8"
#define CURRENT_API_VERSION @"1"

-(id)init{
    self = [super init];
    if (self != nil){
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];        
        [calculationDataManager addObserver:self];
    }
    return self;
}
-(void)dealloc{
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager removeObserver:self];

    [super dealloc];
}

-(void) performVersionCheck{
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager checkSupportedVersion:CURRENT_API_VERSION userData:nil];
}

-(void)dataManagerReturnedVersionCheck:(BOOL)success{
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        
    BOOL requiresUpdate = calculationDataManager.version.isVersionNotSupported;
    BOOL updateAvailable = calculationDataManager.version.isVersionDeprecated || requiresUpdate;
    
    upgradeURL = calculationDataManager.version.Apple;
    
    NSString* upgradeMessage = requiresUpdate ? 
    GetLocalizedString(@"UpgradeMust") :
    GetLocalizedString(@"UpgradeShould");
    
    if (updateAvailable) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:GetLocalizedString(@"version_expired") 
                                                            message:upgradeMessage
                                                           delegate:self 
                                                  cancelButtonTitle:nil 
                                                  otherButtonTitles:GetLocalizedString(@"Upgrade"), requiresUpdate ? nil : GetLocalizedString(@"not_now"), nil];  
        [alertView show];
        [alertView release];
    }
    
    if (requiresUpdate) {
        [calculationDataManager cancelAllRequests];
    }
}

-(NSString*)ugradeUrlOrDefault{
    BOOL validURLPrefix = [upgradeURL hasPrefix:@"http://"] || [upgradeURL hasPrefix:@"itms://"];
    BOOL useDefault = upgradeURL == nil ||!validURLPrefix;
    return useDefault ? DEFAULT_UPGRADE_URL : upgradeURL;
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex == BTN_INDEX_UPGRADE){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[self ugradeUrlOrDefault]]];
    }
}

@end
