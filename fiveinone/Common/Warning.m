//
//  Warning.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-10-29.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "Warning.h"

@implementation Warning

@synthesize key;
@synthesize description;
@synthesize warningId;

@end
