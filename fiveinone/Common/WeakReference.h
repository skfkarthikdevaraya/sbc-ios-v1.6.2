//
//  WeakReference.h
//  DynafleetMobile
//
//  Created by Tommy Eriksson on 9/29/11.
//  Copyright 2011 HiQ Göteborg. All rights reserved.
//
@interface NSMutableArray (WeakReferences)
+ (id)mutableArrayUsingWeakReferences;
+ (id)mutableArrayUsingWeakReferencesWithCapacity:(NSUInteger)capacity;
@end

@implementation NSMutableArray (WeakReferences)
+ (id)mutableArrayUsingWeakReferences
{
	return [self mutableArrayUsingWeakReferencesWithCapacity:0];
}

+ (id)mutableArrayUsingWeakReferencesWithCapacity:(NSUInteger)capacity
{
	CFArrayCallBacks callbacks = {0, NULL, NULL, CFCopyDescription, CFEqual};
	// We create a weak reference array
	return [((id)(CFArrayCreateMutable(0, capacity, &callbacks))) autorelease];
}

+(id)mutableArrayUsingWeakReferencesWithArray:(NSArray*)array
{
    return [((id)(CFArrayCreateMutableCopy(0, 0, (CFArrayRef)array))) autorelease];
}

@end
