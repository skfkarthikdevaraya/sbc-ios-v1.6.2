//
//  DisplayHelpIllustrationCalculationType_iPad.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 11/28/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ShapeUtilities.h"
#import "CalculationDataManager.h"

@interface DisplayHelpIllustrationCalculationType_iPad : UIView {
    
    CAShapeLayer *shapeLayer;
    UILabel *helpText;
    int delay;
    
}

@property (nonatomic, retain) CAShapeLayer *shapeLayer;
@property (nonatomic, assign) int delay;
@property (nonatomic, retain) UILabel *helpText;

@end
