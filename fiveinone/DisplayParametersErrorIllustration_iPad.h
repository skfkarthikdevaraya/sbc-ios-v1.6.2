//
//  DisplayParametersErrorIllustration.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 11/28/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ShapeUtilities.h"
#import "CalculationDataManager.h"

@interface DisplayParametersErrorIllustration_iPad : UIView {
    
    CAShapeLayer *shapeLayer;
    UILabel *helpText;
    NSString *helpString;
}

@property (nonatomic, retain) CAShapeLayer *shapeLayer;
@property (nonatomic, retain) UILabel *helpText;
@property (nonatomic, retain) NSString *helpString;


@end
