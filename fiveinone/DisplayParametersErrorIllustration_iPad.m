//
//  DisplayParametersErrorIllustration.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 11/28/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "DisplayParametersErrorIllustration_iPad.h"

@implementation DisplayParametersErrorIllustration_iPad

@synthesize shapeLayer;
@synthesize helpText;
@synthesize helpString;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    UIImageView *helpIllustration = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"select_calculation_help_stroke.png"]];
    helpIllustration.opaque = NO;
    helpIllustration.layer.opacity = 0.7;
    helpIllustration.backgroundColor = [UIColor clearColor];
    
    [self addSubview:helpIllustration];
    [helpIllustration release];
    
    
    //CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    helpText = [[UILabel alloc] initWithFrame:CGRectMake(130, 110, 350, 80)];
    helpText.textAlignment = NSTextAlignmentCenter;
    helpText.font = [UIFont fontWithName:CHEVIN_BOLD size:IPAD_FONT_SIZE_FOR_NAVIGATION_TITLE];
    helpText.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    helpText.text = helpString;
    helpText.layer.opacity = 0.7;
    helpText.numberOfLines = 0;
    helpText.opaque = NO;
    helpText.backgroundColor = [UIColor clearColor];
    
    [self addSubview:helpText];
    [helpText release];
    
    
    NSString *fileText = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"noParametersAvailablePath"
                                                                                            ofType:@"txt"]
                                                   encoding:NSUTF8StringEncoding error:nil];
    
    CGPathRef bearingPath = [ShapeUtilities createCGPathRefFromEPSString:fileText];
    
    shapeLayer = [[CAShapeLayer alloc] init];
    [shapeLayer setFillColor:[[UIColor blackColor] CGColor]];
    shapeLayer.frame = helpIllustration.bounds;
    
    shapeLayer.geometryFlipped = NO;
    shapeLayer.path = bearingPath;
    shapeLayer.strokeColor = [[UIColor redColor] CGColor];
    shapeLayer.lineWidth = 20.0f;
    shapeLayer.lineJoin = kCALineJoinBevel;
    shapeLayer.fillColor = nil;
    shapeLayer.opacity = 0;
    
    
    // Set shape layer bounds and position
    // ...
    
    // Set the mask for the image view's layer
    [[helpIllustration layer] setMask:shapeLayer];
    
    [shapeLayer removeAllAnimations];
    
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = 1.0;
   // pathAnimation.beginTime = CACurrentMediaTime() + 1;
    pathAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    pathAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    pathAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    pathAnimation.delegate = self;
    pathAnimation.autoreverses = NO;
    
    [shapeLayer addAnimation:pathAnimation forKey:@"strokeEnd"];
    
}

-(void)animationDidStop:(CABasicAnimation *)anim finished:(BOOL)flag {
    
}

- (void)animationDidStart:(CAAnimation *)theAnimation
{
    
    shapeLayer.opacity = 1;
    
}


@end
