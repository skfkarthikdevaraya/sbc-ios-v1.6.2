//
//  UIColor+HexString.h
//  DialSet
//
//  Created by Martin Lannsjö on 9/26/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIColor(HexString)

+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length;
+ (UIColor *) colorWithHexString: (NSString *) hexString ;

@end