//
//  fiveinoneAppDelegate.h
//  fiveinone
//
//  Created by Erik Lindberg on 2011-08-19.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VersionCheckControl.h"

@interface fiveinoneAppDelegate : NSObject <UIApplicationDelegate> {
    VersionCheckControl* _versionCheck;
}




@end
