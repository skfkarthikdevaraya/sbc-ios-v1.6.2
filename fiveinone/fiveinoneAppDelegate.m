//
//  fiveinoneAppDelegate.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-08-19.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <RestKit/RestKit.h>
#import "fiveinoneAppDelegate.h"
#import "CalculationDataManager.h"
#import "VersionCheckControl.h"
#import "GAI.h"

@implementation fiveinoneAppDelegate

-(void)dealloc{
    [_versionCheck release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    /*** Google Analytics **/
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 20;
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-30046188-11"];
    NSLog(@"%@",tracker);
    /*** **/

    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *build = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if (!([[prefs valueForKey:@"build"] isEqualToString:build])) {
        [[NSUserDefaults standardUserDefaults] setPersistentDomain:[NSDictionary dictionary] forName:[[NSBundle mainBundle] bundleIdentifier]];
        [prefs setValue:build forKey:@"build"];
        [prefs synchronize];
    }
    [self doVersionCheck];
    return YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager setupBaseURL];
    [self doVersionCheck];
}

-(void)doVersionCheck{
    if (_versionCheck == nil){
        _versionCheck = [[VersionCheckControl alloc] init];
    }
    [_versionCheck performVersionCheck];
}

@end
