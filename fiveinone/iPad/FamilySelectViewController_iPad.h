//
//  SelectOptionViewController_iPad.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/26/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FamilySelectViewController.h"

@interface FamilySelectViewController_iPad : FamilySelectViewController

@end
