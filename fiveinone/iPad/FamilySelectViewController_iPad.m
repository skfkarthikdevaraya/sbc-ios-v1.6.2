//
//  SelectOptionViewController_iPad.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/26/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "FamilySelectViewController_iPad.h"
#import "OptionSelectViewController_iPad.h"
#import "Option.h"
#import "Family.h"

@implementation FamilySelectViewController_iPad

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    Family *family = [optionsArray objectAtIndex:indexPath.section];
//    Option *option = [family.options objectAtIndex:indexPath.row];
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
   /* bool relub_exists = false;
    for (Calculation *cal in calculationDataManager.selectedCalculationsArray){
        if ([cal.calculationId isEqualToString:@"14"]) {
            relub_exists = true;
            break;
        }
    }*/
    
    NSLog(calculationDataManager.isRelubricationCalcExist ? @"Yes" : @"No");
    
    if (calculationDataManager.isRelubricationCalcExist && (indexPath.row !=2)) {
        
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        return;
    }else {

    
    OptionSelectViewController_iPad *optionController = [[OptionSelectViewController_iPad alloc] initWithNibName:@"OptionSelectViewController" bundle:[NSBundle mainBundle]];
    
    optionController.optionsArray = optionsArray;
    optionController.parameterName = parameterName;
    optionController.familyNr = indexPath.row;
    
    optionController.parameterPresentation = parameterPresentation;
    
    
    optionController.parentIndexPath = parentIndexPath;
    
    optionController.delegate = delegate;
    
    //fiveinoneAppDelegate_iPhone *rootDelegate = (fiveinoneAppDelegate_iPhone *)[UIApplication sharedApplication].delegate;
    //[rootDelegate.navigationController pushViewController:optionController animated: YES];
    [self.navigationController pushViewController:optionController animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    }
    

    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[self setContentSizeForViewInPopover:CGSizeMake(320, 280)];
    [self setPreferredContentSize:CGSizeMake(320, 280)];
    
    
    
    
}

@end
