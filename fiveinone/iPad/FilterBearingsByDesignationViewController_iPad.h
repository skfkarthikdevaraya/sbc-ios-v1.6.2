//
//  FilterBearingsByDesignationViewController_iPad.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/14/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterBearingsByDesignationViewController.h"



@interface FilterBearingsByDesignationViewController_iPad : FilterBearingsByDesignationViewController
{
    
    UIViewController * managingViewController;
    BOOL showingAlert;
    
}


@property (nonatomic, assign) BOOL showingAlert;
@property (nonatomic, assign) BOOL connected;


@property (nonatomic, retain) UIViewController * managingViewController;

- (id)initWithParentViewController:(UIViewController *)aViewController;

@end




