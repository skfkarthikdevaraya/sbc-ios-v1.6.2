//
//  FilterBearingsByDesignationViewController_iPad.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/14/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//


#import "FilterBearingsByDesignationViewController_iPad.h"
#import "FilterBearingsByDesignationViewController.h"
#import "CalculateViewController.h"
#import "Product.h"
#import "FilterBearingsViewController.h"
#import "SelectCalculationViewController.h"
#import "SelectBearingViewController_iPad.h"
#import "fiveinoneAppDelegate_iPad.h"

@implementation FilterBearingsByDesignationViewController_iPad

@synthesize managingViewController;

@synthesize connected;

- (id)initWithParentViewController:(UIViewController *)aViewController {
    if (self = [super initWithNibName:@"FilterBearingsByDesignationViewController" bundle:nil]) {
        self.managingViewController = aViewController;
        self.title = @"Filter by designation";
        self.parentNavigationController = aViewController.navigationController;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

//- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
//    
//    [searchBar resignFirstResponder];
//}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:RKReachabilityDidChangeNotification
                                               object:nil];
    
    showingAlert = false;
    connected = true;
    
}

- (void)reachabilityChanged:(NSNotification*)notification {
    RKReachabilityObserver* observer = (RKReachabilityObserver*)[notification object];
    
    if (![observer isNetworkReachable])
    {
        
        
       // [self setSearchingModeEnd];
        connected = false;
        [self showingAlert];
        DLog(@"Disconnected");
    }
    if ([observer isNetworkReachable]) {
        connected = true;
        DLog(@"Connected");
    }
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = YES;
    
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:YES animated:YES];
    
    // To change the cancel button title in searchbar
    for (UIView *subview in designationSearchBar.subviews){
        if([subview isKindOfClass:[UIButton class]]){
            [(UIButton*)subview setTitle:@"Cancel" forState:UIControlStateNormal];
        }
    }
    
    
    
    [UIView beginAnimations:@"MyAnimation" context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3]; // 5 seconds
    
    CGRect frame = parentViewContainer.frame;
    frame.origin.y -= 44; // Move view down 100 pixels
    frame.size.height += 44;
    
    CGRect toolbarFrame = segmentedToolBar.frame;
    toolbarFrame.origin.y -= 44;
    
    parentViewContainer.frame = frame;
    segmentedToolBar.frame = toolbarFrame;
    
    [UIView commitAnimations];
    
    UIView *viewDisabled = [[UIView alloc] init];
    viewDisabled.frame = CGRectMake(0, 88, 320, 500);
    viewDisabled.backgroundColor = [UIColor blackColor];
    viewDisabled.alpha = 0.7;
    viewDisabled.tag = 200;
    
    [self.view addSubview:viewDisabled];
    [viewDisabled release];
    
    
    
    return YES;
}

-(void) searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {

    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    if (!connected) {
        
        [calculationDataManager showAlertForNetworkConnection];
        return;
    }
    
    
    if ([searchBar.text length] >= 3) {
                
        NSString *searchType = @"";
        switch ([searchBar selectedScopeButtonIndex]) {
            case 0:
                searchType = @"designationStartsWith";
                break;
            case 1:
                searchType = @"designation";
                break;
            case 2:
                searchType = @"designationEndsWith";
                break;
            default:
                break;
        }
        
        [calculationDataManager getProducts:[NSMutableDictionary dictionaryWithKeysAndObjects:searchType, searchBar.text, @"calculation", @"1", nil] userData:self];
        searchBar.showsScopeBar = NO;  
        
        [searchBar sizeToFit];  
        [searchBar setShowsCancelButton:NO animated:YES]; 
        
        [searchBar resignFirstResponder];
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[calculationDataManager getTranslationString:@"Please_fill_in_at_least_3_characters."] delegate:self cancelButtonTitle:[calculationDataManager getTranslationString:@"OK"] otherButtonTitles:nil];
        
        [alert show];
        
        [alert release];
        
    }

}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    
    [searchBar resignFirstResponder];
    
//    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
//    
//    if (!connected) {
//        
//        [calculationDataManager showAlertForNetworkConnection];
//        return;
//    }
//    
//    
//    if ([searchBar.text length] >= 3) {
//        
//        NSString *searchType = @"";
//        switch ([searchBar selectedScopeButtonIndex]) {
//            case 0:
//                searchType = @"designationStartsWith";
//                break;
//            case 1:
//                searchType = @"designation";
//                break;
//            case 2:
//                searchType = @"designationEndsWith";
//                break;
//            default:
//                break;
//        }
//        
//        [calculationDataManager getProducts:[NSMutableDictionary dictionaryWithKeysAndObjects:searchType, searchBar.text, @"calculation", @"1", nil] userData:self];
//        searchBar.showsScopeBar = NO;
//        
//        [searchBar sizeToFit];
//        [searchBar setShowsCancelButton:NO animated:YES];
//        
//        [searchBar resignFirstResponder];
//        
//    } else {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[calculationDataManager getTranslationString:@"Please_fill_in_at_least_3_characters."] delegate:self cancelButtonTitle:[calculationDataManager getTranslationString:@"OK"] otherButtonTitles:nil];
//        
//        [alert show];
//        
//        [alert release];
//        
//    }    
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CalculationDataManager *calculateDataManager = [CalculationDataManager sharedManager];
    [calculateDataManager setBearing:(Product *)[previousBearingsArray objectAtIndex:indexPath.row]];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ipadBearingChanged" object:self];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //[self setContentSizeForViewInPopover:CGSizeMake(320, 216)];
     
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dataManagerReturnedProducts:(NSArray *)returnedArray {
    
    
    foundBearingsArray = [[NSMutableArray alloc] init];
    
    bool oldBearing = false;
    
    SelectBearingViewController_iPad *bearingController = nil;
    
    for (UIViewController *v in parentNavigationController.viewControllers)
    {
        if ([v isKindOfClass:[SelectBearingViewController_iPad class]])
        {
            bearingController = (SelectBearingViewController_iPad *)v;
            oldBearing = true;
        }
    }
    
    if (bearingController == nil)
    {
        bearingController = [[[SelectBearingViewController_iPad alloc] initWithNibName:@"SelectBearingViewController" bundle:[NSBundle mainBundle]] autorelease];
    }
    
    for (int i = 0; i < [returnedArray count]; i++) {
        Product *result = [returnedArray objectAtIndex:i];
        
        if (result.properties.count > 0) {

        [foundBearingsArray addObject:result];

        }
        
        }
    
    bearingController.foundBearingsArray = foundBearingsArray;
    if (!oldBearing)
    {
        [self.parentNavigationController pushViewController:bearingController animated: YES];
    }
}


- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}

@end

