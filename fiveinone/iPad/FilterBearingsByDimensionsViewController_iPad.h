//
//  FilterBearingsByDimensionsViewController_iPad.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/20/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculationDataManager.h"
#import "FilterBearingsByDimensionsViewController.h"




@interface FilterBearingsByDimensionsViewController_iPad : FilterBearingsByDimensionsViewController <CalculationDataManagerDelegate, UITextFieldDelegate> {
    
    UIView *parentViewContainer;
    UIToolbar *segmentedToolbar;
    UIViewController * managingViewController;
}


@property (nonatomic, retain) UIView *parentViewContainer;
@property (nonatomic, retain) UIToolbar *segmentedToolbar;
@property (nonatomic, retain) UIViewController * managingViewController;

-(IBAction)selectBearingType:(id)sender;
-(IBAction)filterBearingsButtonClicked:(id)sender;
-(id)initWithParentViewController:(UIViewController *)aViewController;

@end





