//
//  FilterBearingsByDimensionsViewController_iPad.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/20/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "FilterBearingsByDimensionsViewController_iPad.h"
#import "SelectBearingTypeViewController_iPad.h"
#import "FilterBearingsViewController_iPad.h"
#import "SelectBearingViewController_iPad.h"
#import "CalculationDataManager.h"
#import "fiveinoneAppDelegate_iPad.h"


@implementation FilterBearingsByDimensionsViewController_iPad

@synthesize parentViewContainer;
@synthesize segmentedToolbar;

@synthesize managingViewController;

- (id)initWithParentViewController:(UIViewController *)aViewController {
    if (self = [super initWithNibName:@"FilterBearingsByDimensionsViewController" bundle:nil]) {
        self.managingViewController = aViewController;
        self.title = @"Filter by dimensions";
        self.parentNavigationController = aViewController.navigationController;
    }
    return self;
}

-(IBAction)selectBearingType:(id)sender {
    
    SelectBearingTypeViewController_iPad *selectBearingTypeViewController = [[SelectBearingTypeViewController_iPad alloc] initWithNibName:@"SelectBearingTypeViewController" bundle:[NSBundle mainBundle]];
    
    selectBearingTypeViewController.filterBearingsNavigationController = filterBearingsNavigationController;
    
    [parentNavigationController pushViewController:selectBearingTypeViewController animated:YES];
    [selectBearingTypeViewController release];

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)moveKeyboard:(int)height
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= (height + 44);
    CGPoint origin = activeField.frame.origin;
    origin.y -= scrollView.contentOffset.y;
    if (!CGRectContainsPoint(aRect, origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-(aRect.size.height)); 
        [scrollView setContentOffset:scrollPoint animated:YES];
    }
    
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


-(void)viewDidLoad {
    [super viewDidLoad];
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager addObserver:self];
    
    //UIImage *blueButtonImage = [UIImage imageNamed:@"ipad_blue_button.png"];
   // UIImage *blueButtonImageStrechable = [blueButtonImage stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    
    CGRect oldButtonFrame = filterBearingsButton.frame;
    
    [filterBearingsButton removeFromSuperview];
    
    
    filterBearingsButton = [UIButton buttonWithType:UIButtonTypeSystem];
    filterBearingsButton.frame = oldButtonFrame;
    
    [filterBearingsButton addTarget:self 
                            action:@selector(filterBearingsButtonClicked:)
                  forControlEvents:UIControlEventTouchUpInside];
    
    [scrollView addSubview:filterBearingsButton];
    
    
   // [filterBearingsButton setBackgroundImage:blueButtonImageStrechable forState:UIControlStateNormal];
    /*[filterBearingsButton setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateSelected];
    [filterBearingsButton setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateNormal];*/
    filterBearingsButton.tintColor = [UIColor colorWithHexString:SKF_BLUE_COLOR];
    filterBearingsButton.titleLabel.font = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_BUTTON_MEDIUM];
    
    keyboardToolBar = [[UIToolbar alloc] init];
    [keyboardToolBar setBarStyle:UIBarStyleBlackTranslucent];
    [keyboardToolBar sizeToFit];
    
    UIBarButtonItem *prevButton =[[UIBarButtonItem alloc] initWithTitle:[calculationDataManager getTranslationString:@"Prev"] style:UIBarButtonItemStyleBordered target:self action:@selector(prevButton:)];
    UIBarButtonItem *nextButton =[[UIBarButtonItem alloc] initWithTitle:[calculationDataManager getTranslationString:@"Next"] style:UIBarButtonItemStyleBordered target:self action:@selector(nextButton:)];
    UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    NSArray *itemsArray = [NSArray arrayWithObjects:prevButton, nextButton, flexButton, nil];
    
    [flexButton release];
    [nextButton release];
    [prevButton release];
    [keyboardToolBar setItems:itemsArray];
    
    boreDiameterMinInput.inputAccessoryView = keyboardToolBar;
    boreDiameterMaxInput.inputAccessoryView = keyboardToolBar; 
    outerDiameterMinInput.inputAccessoryView = keyboardToolBar;
    outerDiameterMaxInput.inputAccessoryView = keyboardToolBar;
    
    boreDiameterMinInput.returnKeyType = UIReturnKeyDone;
    boreDiameterMaxInput.returnKeyType = UIReturnKeyDone;
    outerDiameterMinInput.returnKeyType = UIReturnKeyDone;
    outerDiameterMaxInput.returnKeyType = UIReturnKeyDone;
    
    boreDiameterMinInput.keyboardType = UIKeyboardTypeNumberPad;
    boreDiameterMaxInput.keyboardType = UIKeyboardTypeNumberPad;
    outerDiameterMinInput.keyboardType = UIKeyboardTypeNumberPad;
    outerDiameterMaxInput.keyboardType = UIKeyboardTypeNumberPad;

    boreDiameterMinInput.delegate = self;
    boreDiameterMaxInput.delegate = self;
    outerDiameterMinInput.delegate = self;
    outerDiameterMaxInput.delegate = self;

    [calculationDataManager initTranslation:self];
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *NUMBERSPERIOD = @"1234567890., ";
    NSString *NUMBERS = @"1234567890";
    NSCharacterSet *cs;
    NSString *filtered;
    
    // Check for period
    if (([textField.text rangeOfString:@"."].location == NSNotFound) && ([textField.text rangeOfString:@","].location == NSNotFound))
    {
        cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERSPERIOD] invertedSet];
        filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    
    // Period is in use
    cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
    filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [string isEqualToString:filtered];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{

    
    if( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight ){
        
        [UIView beginAnimations:@"MyAnimation" context:nil];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:1]; 
        
        self.scrollView.frame = CGRectMake(0, 0, 320, 250);
        [self.scrollView setContentSize:CGSizeMake(320, 395)];
        
        [UIView commitAnimations];
    
    }
    
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    if( [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight ){
        
        [self.scrollView setContentSize:CGSizeMake(320, 416)];
        self.scrollView.frame = CGRectMake(0, 0, 320, 416);
        
    }
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [scrollView release];
    scrollView = nil;
    [self setScrollView:nil];
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager removeObserver:self];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)nextButton:(id)sender
{
    if (activeField == outerDiameterMinInput) {
        [outerDiameterMaxInput becomeFirstResponder];
    } else if (activeField == outerDiameterMaxInput) {
        [boreDiameterMinInput becomeFirstResponder];
    } else if (activeField == boreDiameterMinInput) {
        [boreDiameterMaxInput becomeFirstResponder];
    } else if (activeField == boreDiameterMaxInput) {
        [outerDiameterMinInput becomeFirstResponder];
    }
}

- (void)prevButton:(id)sender
{    
    if (activeField == outerDiameterMinInput) {
        [boreDiameterMaxInput becomeFirstResponder];
    } else if (activeField == boreDiameterMaxInput) {
        [boreDiameterMinInput becomeFirstResponder];
    } else if (activeField == boreDiameterMinInput) {
        [outerDiameterMaxInput becomeFirstResponder];
    } else if (activeField == outerDiameterMaxInput) {
        [outerDiameterMinInput becomeFirstResponder];
    }
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}



- (void) filterBearingsButtonClicked:(id)sender {
        
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    if (calculationDataManager.selectedBearingType.name == nil) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[calculationDataManager getTranslationString:@"Please_select_a_bearing_type."] delegate:self cancelButtonTitle:[calculationDataManager getTranslationString:@"OK"] otherButtonTitles:nil];
        [alert show];
        [alert release];
        
    } else {

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[calculationDataManager getTranslationString:@"Please_Fill_in_All_Required_Fields."] delegate:self cancelButtonTitle:[calculationDataManager getTranslationString:@"OK"] otherButtonTitles:nil];

        
        bool stopIt = false;
        
        NSMutableDictionary *sendParams = [NSMutableDictionary dictionary];
        
        if (boreDiameterMinInput.text == nil)
        {
            boreDiameterMinInput.text = @"";
        }
        if (boreDiameterMaxInput.text == nil)
        {
            boreDiameterMaxInput.text = @"";
        }
        if (outerDiameterMinInput.text == nil)
        {
            outerDiameterMinInput.text = @"";
        }
        if (outerDiameterMaxInput.text == nil)
        {
            outerDiameterMaxInput.text = @"";
        }
        
        NSMutableDictionary *inputStrings = [NSMutableDictionary dictionaryWithKeysAndObjects:@"diameterInnerMin", boreDiameterMinInput.text,
                                             @"diameterInnerMax", boreDiameterMaxInput.text, 
                                             @"diameterOuterMin", outerDiameterMinInput.text, 
                                             @"diameterOuterMax", outerDiameterMaxInput.text, nil];

    // Commented the below code for accepting only input values to get the Bearing list. All 4 fields are not manditory.
        
     /*   for (id key in inputStrings)
        {

            NSString *inputString = [inputStrings valueForKey:key];
            if ([inputString length] == 0) {

                [alert show];
                stopIt = true;
                break;
            }
            else
            {
                [calculationDataManager setPreviousInput:key withValue:inputString];
                NSString *replacedString = [inputString stringByReplacingOccurrencesOfString:@"," withString:@"."];
                [sendParams setObject:replacedString forKey:key];
            }
            
        }*/
        
        for (id key in inputStrings) {
         
         NSString *inputString = [inputStrings valueForKey:key];
         if (([outerDiameterMinInput.text length] == 0 || [outerDiameterMaxInput.text length] == 0) &&
            ([boreDiameterMinInput.text length] == 0   || [boreDiameterMaxInput.text length] == 0) ) {
                 
             [alert show];
             stopIt = true;
             break;
         }else if ([inputString length] != 0) {

             [calculationDataManager setPreviousInput:key withValue:inputString];
             NSString *replacedString = [inputString stringByReplacingOccurrencesOfString:@"," withString:@"."];
             [sendParams setObject:replacedString forKey:key];
            }

         }

        [sendParams setObject:[calculationDataManager.selectedBearingType.name lowercaseString] forKey:@"bearingType"];
        //[sendParams setObject:calculationDataManager.selectedCalculation.calculationId forKey:@"calculation"];
        
        [sendParams setObject:@"1" forKey:@"calculation"];
        
        if (!stopIt)
        {
            [calculationDataManager getProducts:sendParams userData:self];
        }
        [alert release];
    }
}

- (void)calculationDataManager:(CalculationDataManager *)calculationDataManager returnedArray:(NSArray *)returnedArray {
    if (calculationDataManager.lastUserData != self){
        return;
    }

    foundBearingsArray = [[NSMutableArray alloc] init];
    
    bool oldBearing = false;
    
    SelectBearingViewController_iPad *bearingController = nil;
    
    for (UIViewController *v in parentNavigationController.viewControllers)
    {
        if ([v isKindOfClass:[SelectBearingViewController_iPad class]])
        {
            bearingController = (SelectBearingViewController_iPad *)v;
            oldBearing = true;
        }
    }
    
    if (bearingController == nil)
    {
        bearingController = [[SelectBearingViewController_iPad alloc] initWithNibName:@"SelectBearingViewController" bundle:[NSBundle mainBundle]];
    }
    
    for (int i = 0; i < [returnedArray count]; i++) {
        Product *result = [returnedArray objectAtIndex:i];
        [foundBearingsArray addObject:result];
    }
    
    bearingController.foundBearingsArray = foundBearingsArray;

    if (!oldBearing)
    {
        [self.parentNavigationController pushViewController:bearingController animated: YES];

    }
}

- (void)dataManagerReturnedTranslation:(BOOL)success{
    [super dataManagerReturnedTranslation:success];
    if (success){
        CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
        [filterBearingsButton setTitle:[calculationDataManager getTranslationString:@"Filter_bearings"] forState:UIControlStateNormal];
        [filterBearingsButton setTitle:[calculationDataManager getTranslationString:@"Filter_bearings"] forState:UIControlStateDisabled];
        [filterBearingsButton setTitle:[calculationDataManager getTranslationString:@"Filter_bearings"] forState:UIControlStateSelected];
        [filterBearingsButton setTitle:[calculationDataManager getTranslationString:@"Filter_bearings"] forState:UIControlStateHighlighted];
    }
}

- (void)dealloc {
    CalculationDataManager* calculatorDataManager = [CalculationDataManager sharedManager];
    [calculatorDataManager removeObserver:self];
    
    [scrollView release];
    [scrollView release];
    [super dealloc];
}
@end

