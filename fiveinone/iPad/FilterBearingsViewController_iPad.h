//
//  PopoverFilterBearingsViewController.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/13/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterBearingsViewController.h"
#import "NSArray+PerformSelector.h"


@interface FilterBearingsViewController_iPad : FilterBearingsViewController <UINavigationControllerDelegate>
{
    UINavigationController *parentNavigationController;
    UIViewController       *activeViewController;
    NSArray                *segmentedViewControllers;
    
}

@property (nonatomic,retain) UINavigationController                 *parentNavigationController;
@property (nonatomic, retain, readwrite) UIViewController            * activeViewController;
@property (nonatomic, retain, readwrite) NSArray                     * segmentedViewControllers;


-(IBAction)segmentedControlIndexChanged:(id)sender;
- (NSArray *)segmentedViewControllerContent;
@end
