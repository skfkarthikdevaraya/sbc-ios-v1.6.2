//
//  PopoverFilterBearingsViewController.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/13/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "FilterBearingsViewController_iPad.h"
#import "FilterBearingsByDesignationViewController_iPad.h"
#import "FilterBearingsByDimensionsViewController_iPad.h"
#import "FilterBearingsViewController.h"
#import "MainViewController_iPad.h"

@implementation FilterBearingsViewController_iPad

@synthesize activeViewController, segmentedViewControllers, parentNavigationController;


- (void)cancelFilterBearings:(id)sender {   
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissChangeBearingPopover" object:self];
}


- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    
    UIView *disabledView = [self.view viewWithTag:200];
    [disabledView removeFromSuperview]; 

}

- (void)keyboardWillBeShown:(NSNotification*)aNotification
{
    
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    self.segmentedViewControllers = [self segmentedViewControllerContent];
    parentNavigationController = self.navigationController;
    NSArray * segmentTitles = [self.segmentedViewControllers arrayByPerformingSelector:@selector(title)];
    
    self.segmentedControl = [[UISegmentedControl alloc] initWithItems:segmentTitles];
    self.segmentedControl.selectedSegmentIndex = 0;
    self.segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    
    [self.segmentedControl addTarget:self
                              action:@selector(didChangeSegmentControl:)
                    forControlEvents:UIControlEventValueChanged];
    
    [self segmentedControlIndexChanged:self.segmentedControl]; // kick everything off

    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];

    UIBarButtonItem *cancelItem = [[[UIBarButtonItem alloc] initWithTitle:[calculationDataManager getTranslationString:@"Cancel"] style:UIBarButtonItemStyleBordered target:self action:@selector(cancelFilterBearings:)] autorelease];
        
        
    self.navigationItem.leftBarButtonItem = cancelItem;
    

    
}

- (NSArray *)segmentedViewControllerContent {
    
    UIViewController * controller1 = [[FilterBearingsByDesignationViewController_iPad alloc] initWithParentViewController:self];
    UIViewController * controller2 = [[FilterBearingsByDimensionsViewController_iPad alloc] initWithParentViewController:self];
    
    
    NSArray * controllers = [NSArray arrayWithObjects:controller1, controller2, nil];
    
    [controller1 release];
    [controller2 release];
    
    return controllers;
}





- (void)segmentedControlIndexChanged:(UISegmentedControl *)control {
    
    
    if (self.activeViewController) {
        [self.activeViewController viewWillDisappear:NO];
        [self.activeViewController.view removeFromSuperview];
        [self.activeViewController viewDidDisappear:NO];
    }
    
    
    self.activeViewController = [self.segmentedViewControllers objectAtIndex:control.selectedSegmentIndex];
    
    
    
    
    [self.activeViewController viewWillAppear:NO];
    [self.viewContainer addSubview:self.activeViewController.view];
    [self.activeViewController viewDidAppear:NO];
    
 
    NSString * segmentTitle = [control titleForSegmentAtIndex:control.selectedSegmentIndex];
    self.navigationItem.backBarButtonItem  = [[UIBarButtonItem alloc] initWithTitle:segmentTitle style:UIBarButtonItemStylePlain target:nil action:nil];
   
}






- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.activeViewController viewWillAppear:animated];
    self.preferredContentSize = CGSizeMake(320, 416);
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil]; 
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification object:nil]; 
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.activeViewController viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.activeViewController viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.activeViewController viewDidDisappear:animated];
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [viewController viewDidAppear:animated];
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [viewController viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    for (UIViewController * viewController in self.segmentedViewControllers) {
        [viewController didReceiveMemoryWarning];
    }
}

- (void)viewDidUnload {
    self.segmentedControl         = nil;
    self.segmentedViewControllers = nil;
    self.activeViewController     = nil;
    
    [super viewDidUnload];
}

- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}

@end
