//
//  LegalInformation_iPad.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 11/2/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LegalInformation.h"
#import "CalculationDataManager.h"

@interface LegalInformation_iPad : LegalInformation

- (IBAction)buttonAgreeClicked:(id)sender;

@end
