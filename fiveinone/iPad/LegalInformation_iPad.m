//
//  LegalInformation_iPad.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 11/2/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "LegalInformation_iPad.h"

@implementation LegalInformation_iPad

- (IBAction)buttonAgreeClicked:(id)sender
{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setValue:@"NO" forKey:@"firstrun"];
    [prefs synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ipadLegalInfoAgreed" object:self];
    /*
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
    [self.navigationController popViewControllerAnimated:YES];
    [UIView commitAnimations];
     */
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    /*** iOS - 7 To remove the background & rounder corner in I Agree button in iPad ***/
    
    //Old Code
    
   /* UIImage *blueButtonImage = [UIImage imageNamed:@"ipad_blue_button.png"];
    UIImage *blueButtonImageStrechable = [blueButtonImage stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    CGRect oldButtonFrame = btnAgree.frame;
    [btnAgree removeFromSuperview];
    btnAgree = [UIButton buttonWithType:UIButtonTypeCustom];
    btnAgree.frame = oldButtonFrame;
    [btnAgree setBackgroundImage:blueButtonImageStrechable forState:UIControlStateNormal];
    [btnAgree setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateSelected];
    [btnAgree setTitleColor:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:1.f] forState:UIControlStateNormal];
    btnAgree.titleLabel.font = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_BUTTON];

    [btnAgree addTarget:self action:@selector(buttonAgreeClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnAgree];*/

    
    [btnAgree addTarget:self action:@selector(buttonAgreeClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btnAgree];
    
    /*** END ***/
    
    [super gotTranslation:nil];
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
