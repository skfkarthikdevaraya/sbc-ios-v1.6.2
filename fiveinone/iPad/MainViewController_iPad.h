//
//  MainViewController_iPad.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/13/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <QuickLook/QuickLook.h>

#import "FilterBearingsViewController_iPad.h"
#import "Product.h"
#import "OptionSelectViewController.h"
#import "CalculationDataManager.h"
#import "DisplayHelpIllustrationBearing_iPad.h"
#import "DisplayHelpIllustrationCalculationType_iPad.h"
#import "DisplayParametersErrorIllustration_iPad.h"
#import "CalculationControl.h"
#import "MBProgressHUD.h"
#import "ReportsViewController.h"
#import "ReportnavigationViewController.h"
#import "SelectCalculationViewController_iPad.h"
#import "GAITrackedViewController.h"
#import "IntermediatResultViewController_iPad.h"



@interface MainViewController_iPad : GAITrackedViewController <UIPopoverControllerDelegate, UITableViewDelegate, UITableViewDataSource, CalculationDataManagerDelegate, UITextFieldDelegate, OptionsDelegate, UIScrollViewDelegate, MBProgressHUDDelegate, UIWebViewDelegate,QLPreviewControllerDataSource,QLPreviewControllerDelegate, UIDocumentInteractionControllerDelegate,DirectoryWatcherDelegate> {


//@interface MainViewController_iPad : UIViewController <UIPopoverControllerDelegate, UITableViewDelegate, UITableViewDataSource, CalculationDataManagerDelegate, UITextFieldDelegate, OptionsDelegate, UIScrollViewDelegate, MBProgressHUDDelegate, UIWebViewDelegate,QLPreviewControllerDataSource,QLPreviewControllerDelegate, UIDocumentInteractionControllerDelegate,DirectoryWatcherDelegate> {

    

    
    UIPopoverController *changeBearingPopover;
    UIPopoverController *changeBearingPopver;
    UIPopoverController *optionSelectPopover;
    UIPopoverController *reportViewPopover;
    UIPopoverController *intermediatResultPopover;
    UIPopoverController *hyperlinkPopover;
    

    
    NSMutableArray *resultsParameterArray;
    NSMutableArray *errorsArray;
    NSMutableArray *warningsArray;
    NSMutableArray *messagesArray;
    NSMutableArray *parameterDatabaseArray;
    NSMutableArray *parameterAvailableArray;
    NSMutableArray *resultArray;
    NSMutableArray *tempResultArray;
    NSMutableDictionary *params;
    NSMutableArray *noFormulaErrorArray;
    NSMutableArray *objectsToDiscard;
    NSMutableArray *indexPathsForTextFields;
    NSMutableDictionary *storedReturnedDictionaries;

    NSString *cssString;
    NSString *cssStringGray;
    NSString *cssStringBlue;
    NSString *cssStringRed;
    NSString *cssStringBrgParam;
    NSString *currentDeviceOrientation;
    NSString *reportId;
    NSString *lastReportFileName;
    NSString *whereInter;


    
    UITableView *inputParametersTableiPad;
    UITableView *resultsTable;

    UIImageView *backgroundImage;
    UIImageView *logoImageView;
   // UIImageView *ProdDataImage;
    
    UIScrollView *resultsContainerScrollView;
    
    UIButton *changeBearingButton;
    UIButton *addCalculationButton;
    UIButton *doCalculationButton;
    UIButton *doResetButton;
    UIButton *doReportButton;
    UIButton *infoButton;
    UIButton *removeCalculationTypeButton;
    
    UILabel *selectedDesignationTitle;
    UILabel *bearingTitleLabel;
    UILabel *bearingTypeTextTitle;
    UILabel *bearingTypeTextContent;
    UILabel *skfChevinTitleLabel;
    
    UIView *selectedCalsView;
    UIView *removedView;
    UIView *resultItemView;
    UIView *lineView;
    UIView *resultsViewContainer;
    UIView *bearingViewContainer;
    UIView *parametersViewContainer;
    UIView *resultsContainerScrollViewContent;
    
    UINavigationBar *topNavigationBar;
    UINavigationItem *topBarNavigationItem;
    UINavigationController *reportNavigationController;
    
    NSInteger calculationResultsCount;
    NSInteger calculationsCount;
    NSInteger lastvalue;
    
    UITextField *textFieldFrequencyiPad;
    UITextField *textFieldTimeiPad;
    UITextField *currTextField;
    
    UIToolbar *keyboardToolBar;
    UISwitch* parameterSwitch;

    
    BOOL tabsOverlapping;
    BOOL keyboardIsShowing;
    BOOL keyboardIsShowingOption;
    BOOL intermediate_result;
    BOOL intermediate_result_composite ;
    BOOL isHeaderShown;
    BOOL connected;
    BOOL isLanscape;
    BOOL bearingChanged;
    BOOL lubClean_grease_iPad;
    BOOL lubClean_oil_iPad;
    BOOL lubClean_parameter_iPad;
    BOOL hideIntermediate;
    int what_plain_brearing ;
    int selectedCalculationId;
    double freqVal;
    double timeVal;
    
    /** CONTAINTER RECTS **/
    
    CGRect bearingContainerRectPortrait;
    CGRect inputParametersContainerPortrait;
    CGRect inputParametersTableRectPortrait; 
    CGRect resultItemRectPortrait;
    CGRect resultsContainerScrollViewRectPortrait;
    CGRect resultItemTableViewRectPortrait;
    CGRect doCalculationButtonRectPortrait;
    CGRect doResetButtonRectPortrait;
    CGRect doReportButtonRectPortrait;
    CGRect lineViewRectPortrait;
    
    CGRect bearingContainerRectLandscape;
    CGRect inputParametersContainerLandscape;
    CGRect inputParametersTableRectLandscape; 
    CGRect resultItemRectLandscape;
    CGRect resultsContainerScrollViewRectLandscape;
    CGRect resultItemTableViewRectLandscape;
    CGRect doCalculationButtonRectLandscape;
    CGRect doResetButtonRectLandscape;
    CGRect doReportButtonRectLandscape;
    CGRect lineViewRectLandscape;
    
    UIView *helpIllustrationBearing;
    
    DisplayHelpIllustrationCalculationType_iPad *helpIllustrationCalculationType;
    DisplayParametersErrorIllustration_iPad *noParametersAvailableError;
    IntermediatResultViewController_iPad *intermediatResultViewController_iPad;
    SelectCalculationViewController_iPad *selectCalculationViewController;
    ParameterMissing *parameterYbearingMinLoad;
    ParameterMissing *parameterOilJet;
    ReportsViewController *reportsViewController;
    CalculationControl* _calculationControl;
    Product *selectedProduct;
    Product *calculatedProduct;
   
}


@property (nonatomic, assign) double freqVal;
@property (nonatomic, assign) double timeVal;
//@property (nonatomic,retain)  UIImageView *ProdDataImage;
@property (nonatomic, assign) BOOL intermediate_result;
@property (nonatomic, assign) BOOL intermediate_result_composite ;
@property (nonatomic, assign) int what_plain_brearing ;
@property (nonatomic, retain) UIImageView *resultViewProductImage;
@property (nonatomic, assign) BOOL tabsOverlapping;
@property (nonatomic, assign) BOOL keyboardIsShowing;
@property (nonatomic, assign) BOOL keyboardIsShowingOption;
@property (nonatomic, assign) BOOL isLanscape;
@property (nonatomic, assign) int selectedCalculationId;
@property (nonatomic, assign) CGRect bearingContainerRectPortrait;
@property (nonatomic, assign) CGRect inputParametersContainerRectPortrait;
@property (nonatomic, assign) CGRect inputParametersTableRectPortrait; 
@property (nonatomic, assign) CGRect resultItemRectPortrait;
@property (nonatomic, assign) CGRect resultsContainerScrollViewRectPortrait;
@property (nonatomic, assign) CGRect resultItemTableViewRectPortrait;
@property (nonatomic, assign) CGRect doCalculationButtonRectPortrait;
@property (nonatomic, assign) CGRect doResetButtonRectPortrait;
@property (nonatomic, assign) CGRect doReportButtonRectPortrait;
@property (nonatomic, assign) CGRect lineViewRectPortrait;
@property (nonatomic, assign) CGRect removeCalculationTypeButtonRectPortrait;
@property (nonatomic, assign) CGRect bearingContainerRectLandscape;
@property (nonatomic, assign) CGRect inputParametersContainerRectLandscape;
@property (nonatomic, assign) CGRect inputParametersTableRectLandscape; 
@property (nonatomic, assign) CGRect resultItemRectLandscape;
@property (nonatomic, assign) CGRect resultsContainerScrollViewRectLandscape;
@property (nonatomic, assign) CGRect resultItemTableViewRectLandscape;
@property (nonatomic, assign) CGRect doCalculationButtonRectLandscape;
@property (nonatomic, assign) CGRect doResetButtonRectLandscape;
@property (nonatomic, assign) CGRect doReportButtonRectLandscape;
@property (nonatomic, assign) CGRect lineViewRectLandscape;
@property (nonatomic, assign) CGRect removeCalculationTypeButtonRectLandscape;
@property (nonatomic, retain) IBOutlet UIButton *removeCalculationTypeButton;
@property (nonatomic, retain) UILabel *skfChevinTitleLabel;
@property (nonatomic, retain) UIView *removedView;
@property (nonatomic, retain) UIButton *infoButton;
@property (nonatomic, retain) UISwitch* parameterSwitch;
@property (nonatomic, assign) BOOL lubClean_grease_iPad;
@property (nonatomic, assign) BOOL lubClean_oil_iPad;
@property (nonatomic, assign) BOOL lubClean_parameter_iPad;
@property (nonatomic, retain) IBOutlet UIButton *changeBearingButton;
@property (nonatomic, retain) UIPopoverController *changeBearingPopover;
@property (nonatomic, retain) UIPopoverController *settingsPopover;
@property (nonatomic, retain) UIPopoverController *hyperlinkPopover;
@property (nonatomic, strong) UIPopoverController *reportViewPopover;
@property (nonatomic, retain) UIPopoverController *intermediatResultPopover;
@property (nonatomic, retain) Product *selectedProduct;
@property (nonatomic, retain) Product *calculatedProduct;
@property (nonatomic, retain) NSMutableArray *indexPathsForTextFields;
@property (nonatomic, retain) NSString *cssString;
@property (nonatomic, retain) NSString *cssStringGray;
@property (nonatomic, retain) NSString *cssStringBlue;
@property (nonatomic, retain) NSString *cssStringRed;
@property (nonatomic, retain) NSString *cssStringBrgParam;
@property (nonatomic, retain) NSString *whereInter;
@property (nonatomic, retain) UIView *selectedCalsView;
@property (nonatomic, retain) UITableView *resultsTable;
@property (nonatomic, retain) UIView *resultItemView;
@property (nonatomic, retain) IBOutlet UITableView *inputParametersTableiPad;
@property (nonatomic, retain) IBOutlet UIButton *doCalculationButton;
@property (nonatomic, retain) IBOutlet UIButton *doResetButton;
@property (nonatomic, retain) IBOutlet UIButton *doReportButton;
@property (nonatomic, retain) IBOutlet UIView *lineView;
@property (nonatomic, retain) IBOutlet UIView *resultsViewContainer;
@property (nonatomic, retain) IBOutlet UIView *bearingViewContainer;
@property (nonatomic, retain) IBOutlet UIView *parametersViewContainer;
@property (nonatomic, retain) UILabel *bearingTitleLabel;
@property (nonatomic, retain) UILabel *bearingTypeTextTitle;
@property (nonatomic, retain) UILabel *bearingTypeTextContent;
@property (nonatomic, retain) IBOutlet UIToolbar *topToolbar;
@property (nonatomic, retain) IBOutlet UILabel *topToolbarLabel;
@property (nonatomic, retain) IBOutlet UINavigationBar *topNavigationBar;
@property (nonatomic, retain) IBOutlet UINavigationItem *topBarNavigationItem;
@property (nonatomic, retain) UIButton *addCalculationButton;
@property (nonatomic, retain) UIPopoverController *addCalculationPopover;
@property (nonatomic, retain) UIPopoverController *optionSelectPopover;
@property (nonatomic, assign) NSInteger calculationResultsCount;
@property (nonatomic, retain) IBOutlet UIScrollView *resultsContainerScrollView;
@property (nonatomic, retain) IBOutlet UIView *resultsContainerScrollViewContent;
@property (nonatomic, retain) UITextField *currTextField;
@property (nonatomic, retain) UITextField *textFieldFrequencyiPad;
@property (nonatomic, retain) UITextField *textFieldTimeiPad;
@property (nonatomic, retain) IBOutlet UIImageView *backgroundImage;
@property (nonatomic, retain) NSString *currentDeviceOrientation;
@property (nonatomic, retain) UIToolbar *keyboardToolBar;
@property (nonatomic, retain) UITextField *activeField;
@property (nonatomic, retain) NSMutableDictionary *storedReturnedDictionaries;
@property (nonatomic, retain) UIImageView *logoImageView;
@property (nonatomic, retain) UIView *helpIllustrationBearing;
@property (nonatomic, retain) DisplayHelpIllustrationCalculationType_iPad *helpIllustrationCalculationType;
@property (nonatomic, retain) DisplayParametersErrorIllustration_iPad *noParametersAvailableError;
@property (nonatomic, retain) IBOutlet UILabel *selectCalsLabels;
@property (nonatomic, retain) IBOutlet UIView *selectCalsView;
@property (nonatomic, retain) ParameterMissing *parameterYbearingMinLoad;
@property (nonatomic, retain) ParameterMissing *parameterOilJet;
@property (nonatomic, assign) BOOL hideIntermediate;
@property (nonatomic, retain) NSMutableArray *objectsToDiscard;
@property (nonatomic, retain) NSMutableArray *resultArray;
@property (nonatomic, retain) NSMutableArray *tempResultArray;
@property (nonatomic, retain) NSMutableArray *noFormulaErrorArray;
@property (nonatomic, retain) NSMutableDictionary *params;
@property (nonatomic, retain) NSMutableArray *arrayWithCalcID;
@property (nonatomic, assign) NSInteger calculationsCount;
@property (nonatomic, assign) NSInteger lastvalue;
@property (nonatomic, retain) NSString *reportId;
@property (nonatomic, retain) NSString *lastReportFileName;
@property (nonatomic, retain) ReportsViewController *reportsViewController;
@property (nonatomic, retain) UINavigationController *reportNavigationController;
@property (nonatomic, retain) SelectCalculationViewController_iPad *selectCalculationViewController;
@property (nonatomic, retain) NSMutableDictionary *selectedCalculationsDictionary;
@property (nonatomic, retain) MBProgressHUD *HUD;

+ (BOOL) disableCalculation;
- (IBAction)changeBearing:(id)sender;
- (IBAction)doCalculation:(id)sender;
- (IBAction)doReset:(id)sender;
- (IBAction)reportButtonClicked:(id)sender;
- (IBAction)showLegalInfo:(id)sender;
- (IBAction)showSettings:(id)sender;
- (IBAction)removeCalulationType:(id)sender;
- (void) productLinkButtonClicked:(id)sender;
- (void)dismissKeyboard;
- (void)dismissReportViewPopover;
- (void)sendNewRequest;
- (void)sendNewRequestForResult;
- (void)gotTranslation:(id)sender;
- (void) RemoveUnsupportedCalculationsiPad;
- (void)updateMissingParameters:(NSArray*)nextParameterAvailableArray;
- (void)copyUniqueParameters: (NSMutableArray *)newParameterAvailableArray;
- (void)copyUniqueWarnings: (NSMutableArray *)newWarningsAvailableArray;
- (void)copyUniqueErrors: (NSMutableArray *)newErrorsAvailableArray;
- (void)copyUniqueMessages: (NSMutableArray *)newMessagesAvailableArray;
- (void)handleErrors;
- (void)showAlertForUnsupportedCalculation;
- (void)showHyperlinkView:(NSURL *) url;
- (void)saveReport:(id)sender;
- (void)setSearchingModeStartForPDFReport;
- (void)setSearchingModeEndForPDFReport;

/******/
@end
