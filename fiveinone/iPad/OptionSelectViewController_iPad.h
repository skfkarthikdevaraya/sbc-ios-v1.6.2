//
//  SelectOptionViewController_iPad.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/26/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OptionSelectViewController.h"

@interface OptionSelectViewController_iPad : OptionSelectViewController

@end
