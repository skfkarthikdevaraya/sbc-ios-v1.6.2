//
//  SelectOptionViewController_iPad.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/26/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "OptionSelectViewController_iPad.h"
#import "Option.h"
#import "Family.h"

bool  lubClean_grease_ipad = false;
bool  lubClean_oil_ipad = false;
bool  lubClean_parameter_ipad = false;

@implementation OptionSelectViewController_iPad


- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    Family *family = [optionsArray objectAtIndex:familyNr];
    Option *option = [family.options objectAtIndex:indexPath.row];
  
    // To check Lubrication cleanness parameter
    if ([parameterName isEqualToString:@"oil_system"]) {
        lubClean_parameter_ipad = true;
    }
    
    // To check Lubrication cleaniness value
    if ( [family.familyId isEqualToString:@"2"] && [family.name isEqualToString:@"Grease lubrication"]) {
        lubClean_grease_ipad = true;
    }else if(([family.familyId isEqualToString:@"0"] && [family.name isEqualToString:@"Circulation oil with on-line filter"]) ||
             ([family.familyId isEqualToString:@"1"] && [family.name isEqualToString:@"Oil lubricant without filter or off-line filter"]) ){
        lubClean_grease_ipad = false;
    }
    
    // To disable option for non supported lubricant
    if ((!calculationDataManager.isRelubricationCalcExist) && (lubClean_grease_ipad) && (calculationDataManager.mutlipleCalculation) ) {
        if (([family.name isEqualToString:@"Lubrication"]) && (![option.optionId isEqualToString:@"0"])) {
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
            return;
        }
    }
    
    if ((!calculationDataManager.isRelubricationCalcExist) && (!lubClean_grease_ipad) && (lubClean_parameter_ipad) && (calculationDataManager.mutlipleCalculation) ) {
        if (([family.name isEqualToString:@"Lubrication"]) && ([option.optionId isEqualToString:@"0"])) {
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
            return;
        }
    }
    
    
    if ((calculationDataManager.isRelubricationCalcExist) && ([family.name isEqualToString:@"Lubrication"]) && (![option.optionId isEqualToString:@"0"])) {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        return;
        
    }
    else {
    
    [self.delegate selectedOption:option.presentation withIndex:indexPath.row andName:parameterName andIndexPath:parentIndexPath];
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ipadOptionSelected" object:self];
        
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setPreferredContentSize:CGSizeMake(320, 280)];
}


@end
