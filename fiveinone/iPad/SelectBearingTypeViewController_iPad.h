//
//  SelectBearingTypeViewController_iPad.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/20/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectBearingTypeViewController.h"
#import "MBProgressHUD.h"


@interface SelectBearingTypeViewController_iPad : SelectBearingTypeViewController <MBProgressHUDDelegate>{
    UINavigationController *filterBearingsNavigationController;
}

@property (nonatomic,retain) UINavigationController *filterBearingsNavigationController;
@property (nonatomic, retain) MBProgressHUD *HUD;


@end
