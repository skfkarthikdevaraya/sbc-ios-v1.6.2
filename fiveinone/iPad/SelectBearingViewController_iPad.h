//
//  SelectBearingViewController_iPad.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/18/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectBearingViewController.h"
#import "CalculationDataManager.h"

@interface SelectBearingViewController_iPad : SelectBearingViewController

@end
