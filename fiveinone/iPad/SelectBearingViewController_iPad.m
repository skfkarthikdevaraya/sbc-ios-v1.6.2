//
//  SelectBearingViewController_iPad.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/18/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "SelectBearingViewController_iPad.h"

@implementation SelectBearingViewController_iPad

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {


    CalculationDataManager *calculateDataManager = [CalculationDataManager sharedManager];
    [calculateDataManager setBearing:(Product *)[foundBearingsArray objectAtIndex:indexPath.row]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ipadBearingChanged" object:self];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    

}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    
    [bearingsListTable reloadData];
    //[self setContentSizeForViewInPopover:CGSizeMake(320, 416)];
    [self setPreferredContentSize:CGSizeMake(320, 416)];
}

@end
