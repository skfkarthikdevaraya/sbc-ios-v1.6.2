//
//  SelectCalculationViewController_iPad.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/21/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculationDataManager.h"
#import "TSAlertView.h"
#import "MBProgressHUD.h"

@interface SelectCalculationViewController_iPad : UIViewController <CalculationDataManagerDelegate, TSAlertViewDelegate, MBProgressHUDDelegate> {
    NSMutableArray *calculationTypesArray;
    UITableView *theTableView;
    Calculation *currentCalculation;
    
    // Proceed Button
    UIButton *proceedButton;
    
    // Checkbox in calculation tableview
    UIButton *checkboxButton;
    BOOL isCheckboxSelected ;
    
    // Checkbox for selecting all caluculations
    UIButton *selectAllcheckbox;
    BOOL selectAllBoxChecked;

    int selectedCheckboxCount;
    
  //  BOOL inputParamFlag;
    
    NSMutableArray *selectedRowsArray;

    
}

@property (nonatomic,retain) IBOutlet UITableView *theTableView;
@property (nonatomic,retain) Calculation *currentCalculation;

// Proceed Button
@property (nonatomic, retain) IBOutlet UIButton *proceedButton;

// Checkbox in calculation tableview
@property (nonatomic, retain) UIButton *checkboxButton;
@property (nonatomic, assign) BOOL isCheckboxSelected;

// Checkbox for selecting all caluculations
@property (nonatomic, retain) UIButton *selectAllcheckbox;
@property (nonatomic, assign) BOOL selectAllBoxChecked;

@property(nonatomic, assign) int selectedCheckboxCount;

//@property(nonatomic, assign)  BOOL inputParamFlag;




// Proceed button - Action
-(IBAction) proceedButtonToAppData:(id)sender;

// Checkbox for selecting all caluculations - Action
-(void) selectAllCalculations:(id)sender;
-(void) reloadCalculations;

// Cell Dectionary for selected calc
@property (nonatomic, retain) NSMutableDictionary *cellDictionery;
@property (nonatomic, retain) MBProgressHUD *HUD;

@end
