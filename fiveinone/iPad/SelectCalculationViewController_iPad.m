//
//  SelectCalculationViewController_iPad.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/21/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "SelectCalculationViewController_iPad.h"
#import "TSAlertView.h"
#import <QuartzCore/QuartzCore.h>

#import "FilterBearingsViewController_iPad.h"
#import "CalculationDataManager.h"
#import "MBProgressHUD.h"
#import "MainViewController_iPad.h"
#import "TitleManager.h"



@implementation SelectCalculationViewController_iPad

@synthesize theTableView;
@synthesize currentCalculation;
@synthesize proceedButton;
@synthesize checkboxButton;
@synthesize isCheckboxSelected ;
@synthesize selectAllcheckbox;
@synthesize selectAllBoxChecked;
@synthesize selectedCheckboxCount;
@synthesize cellDictionery;
@synthesize HUD;

static BOOL allSelected = NO;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc{
    CalculationDataManager* calculatorDataManager = [CalculationDataManager sharedManager];
    [calculatorDataManager removeObserver:self];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    calculationTypesArray = [[NSMutableArray alloc] init];
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager addObserver:self];
    //[calculationDataManager getAllCalculations:self];
    [calculationDataManager getAllCalculationsForTab:self];
    
    self.navigationItem.title = [calculationDataManager getTranslationString:@"Calculation_type"];
    
    NSString *navigationBarTintColor = NAVIGATION_BACKGROUND_COLOR;
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithHexString:navigationBarTintColor]];

    // selected calc to dectionary
    self.cellDictionery  = [[NSMutableDictionary alloc] initWithCapacity:[calculationTypesArray count]];
    [self setSearchingModeStart];
    
    selectedRowsArray = [[NSMutableArray alloc] init];
    
    //iOS 7 - To remove table view header title space
    self.theTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.theTableView.bounds.size.width, 0.01f)];
}


/*
-(IBAction) proceedButtonToAppData:(id)sender {
    
    NSArray *keys = [self.cellDictionery allKeys];
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    keys = [keys sortedArrayUsingSelector:@selector(compare:)];

    for (NSIndexPath *object in keys ) {
        
        if ([cellDictionery objectForKey:object] == [NSNumber numberWithBool:YES]) {
            
            NSLog(@"index:%d, calculationID:%@",object.row, [[calculationTypesArray objectAtIndex:object.row] presentation]);
            [indexPaths addObject: [calculationTypesArray objectAtIndex:object.row]];
            
        }
    }
    if ([indexPaths count]== 0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:GetLocalizedString(@"Please_select_calculations_to_proceed") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alert.tag = 101;
        [alert show];
        [alert release];
        
        
    } else {
        
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        
        if(indexPaths.count > 1) {
            
            [calculationDataManager.selectedCalculationsArray removeAllObjects];
            [calculationDataManager.selectedCalculationsArray addObjectsFromArray: indexPaths];
            calculationDataManager.mutlipleCalculation = true;
        } else {
            
            calculationDataManager.selectedCalculation = (Calculation *)[indexPaths objectAtIndex:0];
            calculationDataManager.mutlipleCalculation = false;
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ipadAddCalculationType" object:self];
        
    }
 
 

}
*/

/*-(IBAction) proceedButtonToAppData:(id)sender {
    if (selectedRowsArray.count == 0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:GetLocalizedString(@"Please_select_calculations_to_proceed") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alert.tag = 101;
        [alert show];
        [alert release];
        return;
    }
    
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager.selectedCalculationsArray removeAllObjects];
    
    for (NSIndexPath *indexPath in selectedRowsArray) {
        [calculationDataManager.selectedCalculationsArray addObject: [calculationTypesArray objectAtIndex:indexPath.row]];
    }
    
    if(calculationDataManager.selectedCalculationsArray.count > 1) {


        calculationDataManager.mutlipleCalculation = true;
    }
    else {
        calculationDataManager.selectedCalculation = (Calculation *)[calculationDataManager.selectedCalculationsArray objectAtIndex:0];
        calculationDataManager.mutlipleCalculation = false;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ipadAddCalculationType" object:self];
}*/

-(IBAction) proceedButtonToAppData:(id)sender {
    if (selectedRowsArray.count == 0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:GetLocalizedString(@"Please_select_calculations_to_proceed") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alert.tag = 101;
        [alert show];
        [alert release];
        return;
    }
    
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    if(selectedRowsArray.count>1){
        [calculationDataManager.selectedCalculationsArray removeAllObjects];
        for (NSIndexPath *indexPath in selectedRowsArray) {
            [calculationDataManager.selectedCalculationsArray addObject: [calculationTypesArray objectAtIndex:indexPath.row]];
        }
        calculationDataManager.mutlipleCalculation = true;
        
    }
    else {
        NSIndexPath *indexPath = [selectedRowsArray objectAtIndex:0];
        calculationDataManager.selectedCalculation = (Calculation *)[calculationTypesArray objectAtIndex:indexPath.row];
        calculationDataManager.mutlipleCalculation = false;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ipadAddCalculationType" object:self];
}


/****** Select/Deselect all Checkbox *****/
/*
-(void)selectAllCalculations:(id)sender {
    
    selectAllBoxChecked = !selectAllBoxChecked;
    [selectAllcheckbox setSelected:selectAllBoxChecked];
        
    if (allSelected) {
        
        for (int i = 0; i<[theTableView numberOfRowsInSection:0]; i++) {
            
            if ([[cellDictionery objectForKey:[NSIndexPath indexPathForRow:i inSection:0]] boolValue]) {
                
                [self tableView:theTableView accessoryButtonTappedForRowWithIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            }
        }
        allSelected = NO;
        
    } else {
        
        for (int i = 0; i<[theTableView numberOfRowsInSection:0]; i++) {
            
            if (![[cellDictionery objectForKey:[NSIndexPath indexPathForRow:i inSection:0]] boolValue]) {
            
                [self tableView:theTableView accessoryButtonTappedForRowWithIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            }
        }
        allSelected = YES;
    }
}
*/

-(void)selectAllCalculations:(id)sender {
    
    if(!selectAllBoxChecked) {
        selectAllBoxChecked = !selectAllBoxChecked;
        [selectAllcheckbox setSelected:selectAllBoxChecked];
        
        [selectedRowsArray removeAllObjects];
        
        NSUInteger numberOfRowsInSection = [self.theTableView numberOfRowsInSection:0];
        for (NSUInteger r = 0; r < numberOfRowsInSection; ++r) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:r inSection:0];
            [selectedRowsArray addObject:indexPath];
        }
        
        [self.theTableView reloadData];
        
    }
    else {
        
        selectAllBoxChecked = !selectAllBoxChecked;
        [selectAllcheckbox setSelected:selectAllBoxChecked];
        
        [selectedRowsArray removeAllObjects];
        [self.theTableView reloadData];
    }
}


/***** Actions Selected Checkbox  ******/
/*
- (void)checkButtonTapped:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.theTableView];
    NSIndexPath *indexPath = [self.theTableView indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil)
    {
        [self tableView: self.theTableView accessoryButtonTappedForRowWithIndexPath: indexPath];
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    
    BOOL checked = [[cellDictionery objectForKey:indexPath] boolValue];
    
    if (checked) {
        
        selectedCheckboxCount--;
        
        if (selectAllBoxChecked) {
            
            [selectAllcheckbox setSelected:NO];
            allSelected = NO;
        }
        
    }else {
        
        selectedCheckboxCount++;
        if (selectedCheckboxCount == [theTableView numberOfRowsInSection:0]) {
            
            [selectAllcheckbox setSelected:YES];
            allSelected = YES;
            selectAllBoxChecked = YES;
        }
    }
    
    [cellDictionery setObject:[NSNumber numberWithBool:!checked] forKey:indexPath];
    UITableViewCell *cell = [theTableView cellForRowAtIndexPath:indexPath];
    UIButton *button = (UIButton *)cell.accessoryView;
    
    UIImage *newImage = (checked) ? [UIImage imageNamed:@"check2.png"] : [UIImage imageNamed:@"checked2.png"];
    [button setBackgroundImage:newImage forState:UIControlStateNormal];
}*/

/***** *****/


- (void) viewWillAppear:(BOOL)animated
{
    [selectedRowsArray removeAllObjects];
    
    selectAllBoxChecked = NO;
    [selectAllcheckbox setSelected:selectAllBoxChecked];
    
    [theTableView reloadData];
    [self setContentSizeForViewInPopover:CGSizeMake(345, theTableView.contentSize.height+80)];
    
    [super viewWillAppear:animated];
}



- (void) dataManagerReturnedCalculations:(NSArray *)returnedArray
{
    UILabel *selectAll = [[UILabel alloc]initWithFrame:CGRectMake(245.0, 18.0, 69.0, 21.0)];
    [selectAll setText:GetLocalizedString(@"Select_all")];
    selectAll.backgroundColor = [UIColor clearColor];
    selectAll.textColor = [UIColor blackColor];
	selectAll.highlightedTextColor = [UIColor blackColor];
    selectAll.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
    [self.view addSubview:selectAll];
    
    [proceedButton setTitle:GetLocalizedString(@"proceed") forState:UIControlStateNormal];
    
    /** Select all Checkbox **/

    selectAllcheckbox = [[[UIButton alloc] initWithFrame:CGRectMake(300,6,44,44)]autorelease];
    [selectAllcheckbox setBackgroundImage:[UIImage imageNamed:@"check2.png"]  forState:UIControlStateNormal];
    [selectAllcheckbox setBackgroundImage:[UIImage imageNamed:@"checked2.png"] forState:UIControlStateSelected];
    [selectAllcheckbox setBackgroundImage:[UIImage imageNamed:@"checked2.png"] forState:UIControlStateHighlighted];
    
    //selectAllcheckbox.adjustsImageWhenHighlighted=YES;
    
    [selectAllcheckbox addTarget:self action:@selector(selectAllCalculations:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:selectAllcheckbox];

    [proceedButton setHidden:NO];
    [self setSearchingModeEnd];
    
    // To hide shaft & housing tolerance calculation
    /*for (int i = 0; i < [returnedArray count]; i++) {
        Calculation *result = [returnedArray objectAtIndex:i];
        [calculationTypesArray addObject:result];
    }*/
  
   /* for (int i = 0; i < [returnedArray count]; i++) {
        Calculation *result = [returnedArray objectAtIndex:i];
        
        if ([result.calculationId isEqualToString:@"24"]) {
            
            [calculationTypesArray removeObject:result];
        }else{
                [calculationTypesArray addObject:result];
        }
        
    }*/
    
    [calculationTypesArray removeAllObjects];
    
    for (Calculation *calc in returnedArray) {
        
        if ([calc.calculationId isEqualToString:@"24"]) {
            
            //do nothing
        }else {
            
            
            [calculationTypesArray addObject:calc];
        }
    }
    
    [theTableView reloadData];
    [self setContentSizeForViewInPopover:CGSizeMake(345, theTableView.contentSize.height+80)];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [calculationTypesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UILabel *presentationLabel;
    UIButton *calculationInfo;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        presentationLabel = [[UILabel alloc] initWithFrame:CGRectMake(48, 17, 260, 20)];
        presentationLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
        presentationLabel.backgroundColor = [UIColor clearColor];
        presentationLabel.tag = 10015;
        [cell addSubview:presentationLabel];
        [presentationLabel release];
        
        
        /*calculationInfo = [UIButton buttonWithType:UIButtonTypeInfoDark];
        calculationInfo.frame = CGRectMake(20,17,16,16);
        
        // Change the existing touch area by 40 pixels in each direction
        // Move the x/y starting coordinates so the button remains in the same location
        CGRect rect = CGRectMake(
                                 calculationInfo.frame.origin.x - 8,
                                 calculationInfo.frame.origin.y - 8,
                                 calculationInfo.frame.size.width + 16,
                                 calculationInfo.frame.size.height + 16);
        [calculationInfo setFrame:rect];
        
        [calculationInfo addTarget:self 
                            action:@selector(showCalculationInfoPopup:)
                  forControlEvents:UIControlEventTouchUpInside];
        
        calculationInfo.tag = indexPath.row;
        [cell addSubview:calculationInfo];*/
        
        calculationInfo = [UIButton buttonWithType:UIButtonTypeCustom];
        calculationInfo.frame = CGRectMake(20,15,20,20);
        UIImage * buttonImage = [UIImage imageNamed:@"infoButton_blue.png"];
        [calculationInfo setImage:buttonImage forState:UIControlStateNormal];
        
        // Change the existing touch area by 40 pixels in each direction
        // Move the x/y starting coordinates so the button remains in the same location
        /*CGRect rect = CGRectMake(
                                 calculationInfo.frame.origin.x - 8,
                                 calculationInfo.frame.origin.y - 8,
                                 calculationInfo.frame.size.width + 16,
                                 calculationInfo.frame.size.height + 16);
        [calculationInfo setFrame:rect];*/
        
        [calculationInfo addTarget:self
                            action:@selector(showCalculationInfoPopup:)
                  forControlEvents:UIControlEventTouchUpInside];
        
        calculationInfo.tag = indexPath.row;
        [cell addSubview:calculationInfo];

    }

    presentationLabel = (UILabel *)[cell viewWithTag:10015];
    
    //cell.accessoryType = UITableViewCellAccessoryNone;
    
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    if([selectedRowsArray containsObject:indexPath]){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    

    
    Calculation *calc = [calculationTypesArray objectAtIndex:indexPath.row];
    NSString *cellValue = calc.presentation;
    NSString *calcLevel = calc.level;
    
    if ([calcLevel isEqualToString:@"0"]) {
        presentationLabel.font       = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_TABLE_LVL0];
    } else {
        presentationLabel.font       = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_LVL1];
    }
    
    presentationLabel.text = cellValue;
    
    /*
    // Checkbox in Calculation TableView
    
    BOOL checked = [[cellDictionery objectForKey:indexPath] boolValue];
    UIImage *image = (checked) ? [UIImage   imageNamed:@"checked2.png"] : [UIImage imageNamed:@"check2.png"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    CGRect frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    button.frame = frame;
    [button setBackgroundImage:image forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(checkButtonTapped:event:)  forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [UIColor clearColor];
    cell.accessoryView = button;
    
   */
    return cell;
}

- (IBAction)showCalculationInfoPopup:(UIButton *)sender {
    
    Calculation *calc = [calculationTypesArray objectAtIndex:sender.tag];
    currentCalculation = calc;
     CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:calc.presentation
                                                    message:calc.description
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:nil];
    
    if (calc.calculationUrl != nil) {
        [alert addButtonWithTitle:[calculationDataManager getTranslationString:@"More_info"]];
    }
    [alert addButtonWithTitle:[calculationDataManager getTranslationString:@"Close"]];
    alert.delegate  = self;
    [alert show];
    

    
}

- (void)alertView:(TSAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 101) {
        
        return;
    }
    
    if (currentCalculation.calculationUrl != nil) {
        switch (buttonIndex) {
            case 0:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: currentCalculation.calculationUrl]];
                break;
            case 1:
                break;
                
            default:
                break;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //[self tableView: self.theTableView accessoryButtonTappedForRowWithIndexPath: indexPath];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if(cell.accessoryType == UITableViewCellAccessoryNone) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [selectedRowsArray addObject:indexPath];
        if(selectedRowsArray.count == calculationTypesArray.count) {
            selectAllBoxChecked = YES;
            [selectAllcheckbox setSelected:selectAllBoxChecked];
        }
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
        [selectedRowsArray removeObject:indexPath];
        if(selectedRowsArray.count < calculationTypesArray.count) {
            selectAllBoxChecked = NO;
            [selectAllcheckbox setSelected:selectAllBoxChecked];
        }
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager removeObserver:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(void) reloadCalculations
{
    [calculationTypesArray removeAllObjects];
    [selectedRowsArray removeAllObjects];
    
    selectAllBoxChecked = NO;
    [selectAllcheckbox setSelected:selectAllBoxChecked];
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    //[calculationDataManager.selectedCalculationsArray removeAllObjects];
    [calculationDataManager getAllCalculationsForTab:self];
}


- (void)setSearchingModeStart {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    HUD = [[MBProgressHUD alloc] initWithFrame:CGRectMake(40, 50, 250, 250)];
    HUD.labelText = [calculationDataManager getTranslationString:@"Searching"];
    HUD.graceTime = 0.5f;
    HUD.taskInProgress = YES;
    HUD.allowsCancelation = YES;
    HUD.delegate = self;
    HUD.labelFont = [UIFont fontWithName:CHEVIN_BOLD size:20.f];
    [self.view addSubview:HUD];
    //[self.view setUserInteractionEnabled:NO];
    [HUD show:YES];

}

- (void)setSearchingModeEnd {
    
    [HUD hide:YES];
    HUD.taskInProgress = NO;
    [HUD removeFromSuperview];
    [self.view setUserInteractionEnabled:YES];
    [HUD release];
    HUD = nil;
}
@end
