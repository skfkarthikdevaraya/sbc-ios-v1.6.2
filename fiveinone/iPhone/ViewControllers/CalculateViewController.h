//
//  calculate.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/15/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculationDataManager.h"
#import "Product.h"
#import "Property.h"
#import "OptionSelectViewController_iPhone.h"
#import "FamilySelectViewController_iPhone.h"
#import "CalculationControl.h"

@interface CalculateViewController : UIViewController <UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate, CalculationDataManagerDelegate,OptionsDelegate, MBProgressHUDDelegate, UIWebViewDelegate, UIPopoverControllerDelegate> {
    
    UIButton *doCalculation;
    UIButton *doResetButton;
    UIToolbar *keyboardToolBar;
    UITableView *inputParametersTable;
    UIButton *calculationButton;
    NSInteger noFormulaErrorCount;
    NSString *selectedDesignation;
    NSMutableArray *noFormulaErrorsArray;
    NSMutableDictionary *noFormulaErrorsDictionary;
    NSMutableArray *objectsToDiscard;
    NSMutableArray *indexPathsForTextFields;
    UILabel *selectedCalculationsLabel;
    UIImageView *forceImage_iPhone;
    UILabel *imageDisclaimer;
    UIView *headerView;
    UIView *footerView;
    UIImageView *productDataImageView;
    Product *selectedProduct;
    CalculationControl* _calculationControl;
    ParameterMissing *parameterYbearingMinLoad;
    ParameterMissing *parameterOilJet;
    BOOL showingAlert;
    BOOL lubClean_grease;
    BOOL lubClean_oil;
    BOOL parameterTemp;
    int selectedArrayIndex;
    int headerHeight;
    int footerHeight;
    double freqVal;
    double timeVal;
}

@property (nonatomic, assign) int selectedArrayIndex;
@property (nonatomic, assign) int headerHeight;
@property (nonatomic, assign) int footerHeight;
@property (nonatomic, assign) double freqVal;
@property (nonatomic, assign) double timeVal;
@property (nonatomic, assign) BOOL showingAlert;
@property (nonatomic, assign) BOOL connected;
@property (nonatomic, assign) BOOL lubClean_grease;
@property (nonatomic, assign) BOOL lubClean_oil;
@property (nonatomic, assign) BOOL parameterTemp;
@property (nonatomic, assign) BOOL showHeader;
@property (nonatomic, retain) ParameterMissing *parameterYbearingMinLoad;
@property (nonatomic, retain) ParameterMissing *parameterOilJet;
@property (nonatomic, retain) ParameterMissing *parameterCoolingFactor;
@property (nonatomic, retain) IBOutlet UIButton *doCalculation;
@property (nonatomic, retain) IBOutlet UIButton *doResetButton;
@property (nonatomic, retain) IBOutlet UIToolbar *keyboardToolBar;
@property (nonatomic, retain) IBOutlet UITableView *inputParametersTable;
@property (nonatomic, retain) IBOutlet UIButton *calculationButton;
@property (nonatomic, retain) IBOutlet UITextField *activeField;
@property (nonatomic, retain) IBOutlet UITextField *currTextField;
@property (nonatomic, retain) UITextField *parameterField;
@property (nonatomic, retain) NSString *cssString;
@property (nonatomic, retain) NSString *cssStringGray;
@property (nonatomic, retain) NSString *cssStringRed;
@property (nonatomic, retain) NSMutableArray *indexPathsForTextFields;
@property (nonatomic, retain) NSString *selectedDesignation;
@property (nonatomic, retain) Product *selectedProduct;
@property (nonatomic, retain) NSMutableArray *noFormulaErrorsArray;
@property (nonatomic, retain) NSMutableDictionary *noFormulaErrorsDictionary;
@property (nonatomic, retain) MBProgressHUD *HUD;
@property (nonatomic,retain)  NSMutableArray *objectsToDiscard;
@property (nonatomic,retain) UILabel *selectedCalsLable;
@property (nonatomic,retain) UIImageView *forceImage_iPhone;
@property (nonatomic,retain) UIView *headerView;
@property (nonatomic,retain) UIView *footerView;
@property (nonatomic,retain) UIImageView *productDataImageView;
@property (nonatomic,retain) UILabel *imageDisclaimer;

-(IBAction) doCalculation: (id) sender;
-(IBAction) doReset: (id) sender;
-(void)sendNewRequest;
-(void)setSearchingModeStart;
-(void)setSearchingModeEnd;
-(void) RemoveUnsupportedCalculations;
-(void) showHyperlinkView:(NSURL *) url;
-(void) editTextfieldData:(UITextField *) textfield : (int) tag;

@end
