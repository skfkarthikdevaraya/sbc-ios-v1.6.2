//
//  calculate.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/15/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "CalculateViewController.h"
#import "ResultsViewController.h"
#import "CalculationDataManager.h"
#import "ParameterResult.h"
#import "ParameterMissing.h"
#import "ParameterAvailable.h"
#import "Calculation.h"
#import "fiveinoneAppDelegate_iPhone.h"
#import "TitleManager.h"
#import "Warning.h"
#import "Error.h"
#import "Property.h"
#import "Family.h"
#import "Option.h"
#import "NSString+StripHTML.h"
#import "MBProgressHUD.h"
#import "HyperlinkViewController.h"


@implementation CalculateViewController

#define AlertViewIphoneInitFrame CGRectMake(0, 0, 300, 100)
#define TextViewIphoneFrame CGRectMake(20, 50, 260, 80)
#define AlertViewIphonePresentFrame CGRectMake(10, 150, 300, 250)

@synthesize doCalculation;
@synthesize keyboardToolBar;
@synthesize activeField;
@synthesize indexPathsForTextFields;
@synthesize currTextField;
@synthesize inputParametersTable;
@synthesize calculationButton;
@synthesize cssString;
@synthesize cssStringGray;
@synthesize cssStringRed;
@synthesize selectedDesignation;
@synthesize selectedProduct;
@synthesize HUD;
@synthesize noFormulaErrorsDictionary;
@synthesize noFormulaErrorsArray;
@synthesize parameterYbearingMinLoad;
@synthesize parameterOilJet;
//@synthesize parameterCoolingFactor;
//@synthesize parameterFr;
//@synthesize parameterFa;
@synthesize parameterTemp;
@synthesize objectsToDiscard;
@synthesize connected;
@synthesize showingAlert;

//@synthesize parameterFieldCoolingFactor;

@synthesize headerHeight;
@synthesize footerHeight;
@synthesize doResetButton;
@synthesize footerView;
@synthesize headerView;
@synthesize productDataImageView;
@synthesize forceImage_iPhone;
@synthesize imageDisclaimer;
@synthesize selectedArrayIndex;



//@synthesize textFieldFrequency;
//@synthesize textFieldTime;
@synthesize lubClean_grease;
@synthesize lubClean_oil;
@synthesize freqVal;
@synthesize timeVal;
 int stop_update = -1;
int buffer_tag = 0;
bool block_all_calls = false;
BOOL inputIncorrect = FALSE;
-(float) calculateHeightOfTextFromWidth:(NSString*)text: (UIFont*)withFont: (float)width :(UILineBreakMode)lineBreakMode
{
    NSString *textAdd = [text stringByAppendingString:@"MM"];
    [text retain];
    [withFont retain];
    CGSize suggestedSize = [textAdd sizeWithFont:withFont constrainedToSize:CGSizeMake(width, FLT_MAX) lineBreakMode:lineBreakMode];
    CGSize lineHeightSize = [textAdd sizeWithFont:withFont];
    
    float height = 0;
    if (![text isEqualToString:@""])
    {
        CGFloat rows = suggestedSize.height / lineHeightSize.height;
        height = rows * (lineHeightSize.height + 2);
        height += 5;
    }
    [text release];
    [withFont release];
    
    return height;
}

- (void)switchDidChangeValue:(id)sender
{
    UISwitch* aSwitchCtrl = (UISwitch*)sender;
    UITableViewCell* aCell = (UITableViewCell*) aSwitchCtrl.superview;
    
    /*UITableViewCell* aCell;
    if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_6_1) {
         aCell = (UITableViewCell*) aSwitchCtrl.superview;
    }else {
         aCell = (UITableViewCell*) aSwitchCtrl.superview.superview;
    }*/

    NSString* parameterValue = aSwitchCtrl.on ? @"true" : @"false";
    [_calculationControl setValue:parameterValue forId:aCell.tag];
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    NSString* parameterName = [_calculationControl nameForParameterId:aCell.tag];
    [calculationDataManager setPreviousInput:parameterName withValue:parameterValue];
}

-(IBAction) doReset: (id) sender {
    
    if (! [_calculationControl CheckParameterValues]) { return;    }
        
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reser input Parameters"
                                                    message:GetLocalizedString(@"Reset_input_parameters")
                                                    delegate:self
                                                    cancelButtonTitle:GetLocalizedString(@"No")
                                                    otherButtonTitles:GetLocalizedString(@"Reset"), nil];
    alert.tag = 11;
    [alert show];
    [alert release];
}


-(IBAction)doCalculation:(id) sender {
    
    
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    if (!connected) {
        [calculationDataManager showAlertForNetworkConnection];
        [self setSearchingModeEnd];
        return;
    }
    
    
    
    BOOL inputMissing = [_calculationControl updateMissingInputValues];
    BOOL invalidInput = [_calculationControl updateInvalidInputValues];
    BOOL inputIncorrect = [_calculationControl updateIncorrectInputValues];
    

    
    if (inputMissing)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:GetLocalizedString(@"Please_Fill_in_All_Required_Fields.")
                                                        delegate:self
                                                        cancelButtonTitle:GetLocalizedString(@"OK")
                                                        otherButtonTitles:nil];
        alert.tag = 1;
        [alert show];
        [alert release];
        
    }else if (invalidInput){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:[_calculationControl getInvalidInputWarnings]
                                                       delegate:self
                                              cancelButtonTitle:GetLocalizedString(@"OK")
                                              otherButtonTitles:nil];
        alert.tag = 1;
        [alert show];
        [alert release];
        
    }else  if (inputIncorrect) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Please fill valid input for highlighted fields."
                                                       delegate:self
                                              cancelButtonTitle:GetLocalizedString(@"OK")
                                              otherButtonTitles:nil];
        alert.tag = 35;
        [alert show];
        [alert release];
        
    }else {
        
        NSMutableDictionary *sendParams = [_calculationControl getParameterValuesByType:VISIBLE_PARAM_BY_NAME];
        ResultsViewController *resultsController = [[ResultsViewController alloc] initWithNibName:@"ResultsViewController" bundle:nil];
        [sendParams setValue:[selectedProduct productId] forKey:@"productId"];
        if ([parameterYbearingMinLoad.parameterId isEqualToString:@"165"]) {
            [sendParams setValue:@"0" forKey:@"YB_MinLoad_Message"];
        }
//        if ([parameterCoolingFactor.parameterId isEqualToString:@"97"]) {
//            
//            if (parameterCoolingFactor.valueDefault == nil) {
//                
//                [sendParams setValue:@"0.0" forKey:@"Ws"];
//            }
//            
//        }
        resultsController.params = sendParams;
        [self.navigationController pushViewController:resultsController animated:YES];
        [resultsController release];
        resultsController = nil;
    }
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _calculationControl = [[CalculationControl alloc] init];
    }
    return self;
}


-(UITableViewCell*)getCellFromTableView:(UITableView*)tableView Sender:(id)sender {
    
    CGPoint pos = [sender convertPoint:CGPointZero toView:tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:pos];
    return [tableView cellForRowAtIndexPath:indexPath];
}

- (void)prevButton:(id)sender {
    
    UITableViewCell *cell = [self getCellFromTableView:inputParametersTable Sender:activeField];
    
    if(cell== nil) {
        
        return;
    }
    
    //NSLog(@"Cell tag:%ld", (long)cell.tag);
    //UITableViewCell *cell = (UITableViewCell*) [activeField superview];
    NSIndexPath *currentPath = [inputParametersTable indexPathForCell:cell];
    
    int currentPosition = [indexPathsForTextFields indexOfObject:currentPath];
    int nextFieldIndex;
    
    if(currentPosition==0)
    {
        [activeField becomeFirstResponder];
        return;
    }
    
    if (currentPosition > 0 && currentPosition < ([indexPathsForTextFields count]))
    {
        nextFieldIndex = currentPosition-1;
        //[inputParametersTable setContentOffset:CGPointMake(0, 0)];
    }
    else
    {
        nextFieldIndex = [indexPathsForTextFields count]-1;
        //currentPosition = [indexPathsForTextFields count];
        //[inputParametersTable setContentOffset:CGPointMake(0, inputParametersTable.frame.size.height)];
    }
    
   // NSLog(@"nextFieldIndex:%d", nextFieldIndex);
    NSIndexPath *nextIndexPath = [indexPathsForTextFields objectAtIndex:nextFieldIndex];
   // NSLog(@"nextIndexPath.row:%d", nextIndexPath.row);
    
    
    //[inputParametersTable reloadData];
    //[inputParametersTable selectRowAtIndexPath:nextIndexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    UITableViewCell *nextCell = [inputParametersTable cellForRowAtIndexPath:nextIndexPath];
    
    if(nextCell == nil)
    {
        NSArray *indexPaths = [NSArray arrayWithObject:nextIndexPath];
        
        [inputParametersTable scrollToRowAtIndexPath:nextIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:UITableViewRowAnimationNone];
        [inputParametersTable reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        nextCell = [inputParametersTable cellForRowAtIndexPath:nextIndexPath];
    }

    activeField = (UITextField *)[nextCell viewWithTag:10100];
    [activeField becomeFirstResponder];
    
    
}


- (void)nextButton:(id)sender
{
    UITableViewCell *cell = [self getCellFromTableView:inputParametersTable Sender:activeField];
    if(cell== nil)
    {
        return;
    }
    
   // NSLog(@"Cell tag:%ld", (long)cell.tag);
    
    //UITableViewCell *cell = (UITableViewCell*) [activeField superview];
    NSIndexPath *currentPath = [inputParametersTable indexPathForCell:cell];
    
    int currentPosition = [indexPathsForTextFields indexOfObject:currentPath];
    int nextFieldIndex;
    
    if(currentPosition == ([indexPathsForTextFields count]-1))
    {
        [activeField becomeFirstResponder];
        return;
    }
    
    if (currentPosition+1 < ([indexPathsForTextFields count])) {
        
        nextFieldIndex = currentPosition+1;
        
    } else {
        
        nextFieldIndex = 0;
        [inputParametersTable setContentOffset:CGPointMake(0, 0)];
    }
    
   // NSLog(@"nextFieldIndex:%d", nextFieldIndex);
    NSIndexPath *nextIndexPath = [indexPathsForTextFields objectAtIndex:nextFieldIndex];
    
    UITableViewCell *nextCell = [inputParametersTable cellForRowAtIndexPath:nextIndexPath];
    if(nextCell != nil)
    {
        activeField = (UITextField *)[nextCell viewWithTag:10100];
        [activeField becomeFirstResponder];
    }
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    selectedArrayIndex=0;
    [self setSearchingModeStart];
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    self.navigationItem.rightBarButtonItem = [TitleManager getSKFLogo];
    // Get parameters from back-end
    [calculationDataManager initTranslation:self];
    selectedProduct = calculationDataManager.selectedBearing;
    [calculationDataManager addObserver:self];
    
    indexPathsForTextFields = [[NSMutableArray alloc] init];
    noFormulaErrorsArray = [[NSMutableArray alloc] init];
    noFormulaErrorsDictionary = [[NSMutableDictionary alloc]init];
    noFormulaErrorCount=0;
    [self sendNewRequest];
    
    // Do any additional setup after loading the view from its nib.d
    cssString = @"<html> \n"
    "<head> \n"
    "<style type=\"text/css\"> \n"
    "body {font-family: \"SKF Chevin Medium\"; font-size: 12; margin:4px 0px;background-color:transparent; color:#333333; word-wrap:break-word;}\n"
    "</style> \n"
    "</head> \n";
    
    cssStringGray = @"<html> \n"
    "<head> \n"
    "<style type=\"text/css\"> \n"
    "body {font-family: \"SKF Chevin Medium\"; font-size: 12; margin:4px 0px;background-color:transparent; color:#808080; word-wrap:break-word;}\n"
    "</style> \n"
    "</head> \n";
    
    cssStringRed = @"<html> \n"
    "<head> \n"
    "<style type=\"text/css\"> \n"
    "body {font-family: \"SKF Chevin Medium\"; font-size: 11; margin:4px 0px;background-color:transparent; color:#333333; word-wrap:break-word;}\n"
    "</style> \n"
    "</head> \n";
    
    keyboardToolBar = [[UIToolbar alloc] init];
    [keyboardToolBar setBarStyle:UIBarStyleBlackTranslucent]; 
    [keyboardToolBar sizeToFit];
    
    UIBarButtonItem *prevButton =[[UIBarButtonItem alloc] initWithTitle:GetLocalizedString(@"Prev") style:UIBarButtonItemStyleBordered target:self action:@selector(prevButton:) ];
    
    UIBarButtonItem *nextButton =[[UIBarButtonItem alloc] initWithTitle:GetLocalizedString(@"Next") style:UIBarButtonItemStyleBordered target:self action:@selector(nextButton:)];

    UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
    UIBarButtonItem *doneButton =[[UIBarButtonItem alloc] initWithTitle:GetLocalizedString(@"Done") style:UIBarButtonItemStyleDone target:self  action:@selector(done)];
    
    
    NSDictionary* barButtonItemAttributes =
  @{NSFontAttributeName:
        [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON_LARGE],
    NSForegroundColorAttributeName:
        [UIColor whiteColor]
    };
    [[UIBarButtonItem appearance] setTitleTextAttributes:barButtonItemAttributes forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes: barButtonItemAttributes forState:UIControlStateHighlighted];
    [[UIBarButtonItem appearance] setTitleTextAttributes: barButtonItemAttributes forState:UIControlStateSelected];
    [[UIBarButtonItem appearance] setTitleTextAttributes: barButtonItemAttributes forState:UIControlStateDisabled];


    NSArray *itemsArray = [NSArray arrayWithObjects:prevButton, nextButton, flexButton, doneButton, nil];
    
    [keyboardToolBar setItems:itemsArray];
    [prevButton release];
    [nextButton release];
    [flexButton release];
    [doneButton release];
    
    calculationButton.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON];
    calculationButton.titleLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    
    //doCalculation.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON];
   // doCalculation.titleLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_WHITE];
    
    
    //doResetButton.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON];
    //doResetButton.titleLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_WHITE];

    [inputParametersTable setBackgroundView:nil];
    [inputParametersTable setBackgroundColor:[UIColor clearColor]];
    [inputParametersTable setOpaque:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:RKReachabilityDidChangeNotification
                                               object:nil];
    
    showingAlert = false;
    connected = true;
    //[self prepareHeaderView];
    //[self prepareFooterView];
    lubClean_oil = false;
    lubClean_grease = false;
    

}
- (void)reachabilityChanged:(NSNotification*)notification {
    RKReachabilityObserver* observer = (RKReachabilityObserver*)[notification object];
    
    if (![observer isNetworkReachable])
    {
        [self setSearchingModeEnd];
        connected = false;
        [self showingAlert];
        DLog(@"Disconnected");
    }
    if ([observer isNetworkReachable]) {
        connected = true;
        DLog(@"Connected");
    }
}

-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [noFormulaErrorsDictionary removeAllObjects];
    [noFormulaErrorsArray removeAllObjects];

    noFormulaErrorCount=0;
    
    [inputParametersTable reloadData];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    block_all_calls = false;
}


-(void)viewWillDisappear:(BOOL)animated
{
    //block_all_calls = true;
 
    
    [super viewWillDisappear:animated];
    
    //showHeader = NO;
    selectedArrayIndex = 0;
    [[[CalculationDataManager sharedManager] unsupportedCalculationsArray] removeAllObjects];
}



/** ERRORS **/
- (void)dataManagerReturnedParametersErrors:(NSArray *)returnedArray
{
    [_calculationControl updateErrors:returnedArray];
    Error* noFormulaError;

    for (int i = 0 ; i <[_calculationControl getNoFormulaErrors].count ; i++) {
        
        noFormulaError = [[_calculationControl getNoFormulaErrors] objectAtIndex:i];
        [noFormulaErrorsArray addObject:noFormulaError];
    }
}

/** WARNINGS **/
- (void)dataManagerReturnedParametersWarnings:(NSArray *)returnedArray
{
    [_calculationControl updateWarnings:returnedArray];
    
    Warning* noFormulaWarning = [_calculationControl getNoFormulaWarning];
        
    if (noFormulaWarning != nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:noFormulaWarning.description
                                                       delegate:self
                                              cancelButtonTitle:GetLocalizedString(@"OK")
                                              otherButtonTitles:nil];
        alert.tag = 2;
        [alert show];
        [alert release];
    }
}

- (void)dataManagerReturnedParametersForCalculation:(NSArray *)dataManagerReturnedParametersForCalculation
{    
    [_calculationControl updateMissingParameters:dataManagerReturnedParametersForCalculation];
    [_calculationControl prefillValuesWithPreviousInputsForParameters];
    [_calculationControl prefillValuesWithDefaultsForParameters];
    
    if(noFormulaErrorsArray.count>0) {
        [self handleErrors];
    }
    
    [self prepareHeaderView];
    [self prepareFooterView];
    
    [self setSearchingModeEnd];
        
    [inputParametersTable reloadData];
}

-(NSString *)fixBR:(NSString *)inString
{
    return [inString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"];
}

- (void) updateSelectedCalculationsLabel
{
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    NSMutableString *title=[[[NSMutableString alloc] initWithString:@""] autorelease];;
    if (calculationDataManager.mutlipleCalculation) {
        
        //[title appendString: GetLocalizedString(@"Selected_calculation")];
        //[title appendString:@": "];
        
        for(int index=0; index<calculationDataManager.selectedCalculationsArray.count; index++) {
            
            Calculation *calculation =  [calculationDataManager.selectedCalculationsArray objectAtIndex:index];
            [title appendFormat:@"%@ ", calculation.presentation];
            
            if (index < calculationDataManager.selectedCalculationsArray.count-1 ) {
                
                [title appendFormat:@"| "];
            }
        }
    }  else {
        
        //[title appendString: GetLocalizedString(@"Selected_calc")];
        //[title appendString:@": "];
        [title appendFormat:@"%@ ", calculationDataManager.selectedCalculation.presentation];
        
    }
    
    selectedCalculationsLabel.text = title;
    [selectedCalculationsLabel sizeToFit];
    
    CGSize maximumLabelSize = CGSizeMake(300, 1000);
    CGSize expectedLabelSize = [title sizeWithFont:selectedCalculationsLabel.font constrainedToSize:maximumLabelSize lineBreakMode:selectedCalculationsLabel.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect newFrame = selectedCalculationsLabel.frame;
    newFrame.size.height = expectedLabelSize.height;
    selectedCalculationsLabel.frame = newFrame;

}

- (void) adjustHeaderView
{

    if (forceImage_iPhone.image.size.width < 20) {
        
        forceImage_iPhone.frame = CGRectZero;
        
        imageDisclaimer.frame = CGRectZero;
    }else {
        
        forceImage_iPhone.frame = CGRectMake(50, selectedCalculationsLabel.frame.size.height + selectedCalculationsLabel.frame.origin.y, 250, 150);
        
        imageDisclaimer.frame = CGRectMake(10, forceImage_iPhone.frame.size.height + forceImage_iPhone.frame.origin.y, 300, 30);

    }
    headerHeight = selectedCalculationsLabel.frame.size.height + forceImage_iPhone.frame.size.height + imageDisclaimer.frame.size.height+30;
    headerView.frame = CGRectMake(0, 0, 320, headerHeight);
}

- (void) prepareHeaderView
{
    
    if(headerView!= nil) {
        
        [self updateSelectedCalculationsLabel];
        [self adjustHeaderView];
        [self.inputParametersTable setTableHeaderView:headerView];
        return;
    }
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    NSString *selectedCalsColor = COLOR_SKF_BLUE;
    
    UILabel * headerLabel = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.opaque = NO;
    headerLabel.textColor = [UIColor colorWithHexString:COLOR_SKF_RED];
    headerLabel.highlightedTextColor = [UIColor colorWithHexString:COLOR_SKF_RED];
    headerLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_GROUPED_TABLE_HEADER];
    headerLabel.frame = CGRectMake(10.0, 0.0, 300.0, 25.0);
   
    NSMutableString *title=[[[NSMutableString alloc] initWithString:@""] autorelease];;
    if (calculationDataManager.mutlipleCalculation) {
        
        [title appendString: GetLocalizedString(@"Selected_calculation")];
        [title appendString:@": "];
            }
    else {
        
        [title appendString: GetLocalizedString(@"Selected_calc")];
        [title appendString:@": "];
        
        
    }
    headerLabel.text = title;

    
    selectedCalculationsLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    selectedCalculationsLabel.backgroundColor = [UIColor clearColor];
    selectedCalculationsLabel.opaque = NO;
    selectedCalculationsLabel.textColor = [UIColor colorWithHexString:selectedCalsColor];
    selectedCalculationsLabel.highlightedTextColor = [UIColor colorWithHexString:selectedCalsColor];
    selectedCalculationsLabel.font = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_GROUPED_CALCULATION_TITLE];
    selectedCalculationsLabel.frame = CGRectMake(10, headerLabel.frame.size.height + headerLabel.frame.origin.y, 300, 44);
    selectedCalculationsLabel.lineBreakMode = NSLineBreakByWordWrapping;
    selectedCalculationsLabel.numberOfLines = 0;
    
    [self updateSelectedCalculationsLabel];
    
    
    //if ((![selectedProduct.type isEqualToString:@"3_1"]) && (![selectedProduct.type isEqualToString:@"3_3a"]) && (![selectedProduct.type isEqualToString:@"3_3b"]) && (![selectedProduct.type isEqualToString:@"3_4"])) {
        
        //forceImage_iPhone = [[UIImageView alloc] initWithFrame:CGRectMake(50, selectedCalculationsLabel.frame.size.height + selectedCalculationsLabel.frame.origin.y, 250, 140)] ;
    
        forceImage_iPhone = [[UIImageView alloc] initWithFrame:CGRectZero];
        float scale = [UIScreen mainScreen].scale;
        CGSize imageViewSize = CGSizeMake(320, 280);
        CGSize imageSize = CGSizeMake(scale * (imageViewSize.width), scale * (imageViewSize.height));
    
    bool radial_load = false , axial_load = false;
    
    if([_calculationControl getParameterObject:@"20"] != Nil)
    {
        radial_load = true;
    }
    if ([_calculationControl getParameterObject:@"21"] != Nil)
    {
        axial_load = true;
    }
    
    
        NSString* imagePath =
        [NSString stringWithFormat:@"/Image?bearingType=%@&productId=%@&measurement=%@&calculation=1&withForce=true&width=%d&height=%d&withFa=%s&withFr=%s",
         selectedProduct.type,selectedProduct.productId,[calculationDataManager selectedMeasurement],
         (int)imageSize.width, (int)imageSize.height , (axial_load) ?"true":"false" , (radial_load) ? "true":"false" ];
        
        NSString* productImageUrl = [NSString stringWithFormat:@"%@%@", calculationDataManager.baseURL,imagePath];
        forceImage_iPhone.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:productImageUrl]]];
        forceImage_iPhone.contentMode = UIViewContentModeScaleAspectFit;
        forceImage_iPhone.autoresizingMask = YES;
    
    
        imageDisclaimer = [[UILabel alloc] initWithFrame:CGRectZero];
        imageDisclaimer.backgroundColor = [UIColor clearColor];
        imageDisclaimer.opaque = NO;
        imageDisclaimer.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_DISCLAIMER];
        imageDisclaimer.text = GetLocalizedString(@"ImageDisclaimer");
        imageDisclaimer.lineBreakMode = NSLineBreakByWordWrapping;
        imageDisclaimer.numberOfLines=0;
   // }
    
    headerView = [[UIView alloc] initWithFrame:CGRectZero];
    [self adjustHeaderView];
    [headerView addSubview:headerLabel];
    [headerView addSubview:selectedCalculationsLabel];
    [headerView addSubview:forceImage_iPhone];
    [headerView addSubview: imageDisclaimer];
    [self.inputParametersTable setTableHeaderView:headerView];
}



- (void) prepareFooterView
{
    if(footerView != nil)
    {
        [self.inputParametersTable setTableFooterView:footerView];
        return;
    }
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];

    UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.opaque = NO;
	headerLabel.textColor = [UIColor colorWithHexString:COLOR_SKF_RED];
	headerLabel.highlightedTextColor = [UIColor colorWithHexString:COLOR_SKF_RED];
	headerLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_GROUPED_TABLE_HEADER];
	headerLabel.frame = CGRectMake(10.0, 0.0, 300.0, 44.0);
    headerLabel.text = GetLocalizedString(@"Bearing_information");
    
    productDataImageView = [[UIImageView alloc] initWithFrame:CGRectMake(35, 20, 250, 150)];
    productDataImageView.contentMode = UIViewContentModeScaleAspectFit;
    productDataImageView.autoresizingMask = YES;

    float scale = [UIScreen mainScreen].scale;
    CGSize diaImageViewSize = CGSizeMake(320, 280);
    CGSize diaImageSize = CGSizeMake(scale * diaImageViewSize.width, scale * diaImageViewSize.height);
    
    NSString* diaImagePath = [NSString stringWithFormat:@"/Image?bearingType=%@&productId=%@&measurement=%@&withDia=true&width=%d&height=%d",
     selectedProduct.type, selectedProduct.productId,[calculationDataManager selectedMeasurement],
     (int)diaImageSize.width, (int)diaImageSize.height];
    
    NSString* diaProductImageUrl = [NSString stringWithFormat:@"%@%@", calculationDataManager.baseURL,diaImagePath];
    productDataImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:diaProductImageUrl]]];
    
  

    UILabel * disclaimerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	disclaimerLabel.backgroundColor = [UIColor clearColor];
	disclaimerLabel.opaque = NO;
	disclaimerLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_DISCLAIMER];
	disclaimerLabel.frame = CGRectMake(10, productDataImageView.frame.size.height + productDataImageView.frame.origin.y, 300, 30);
    disclaimerLabel.text = GetLocalizedString(@"ImageDisclaimer");
    disclaimerLabel.numberOfLines = 2;
    
    
    if (productDataImageView.image.size.width <20) {
        productDataImageView.frame = CGRectZero;
        disclaimerLabel.frame = CGRectZero;
    }
    // Bearing description
    UIWebView *descriptionView = [[UIWebView alloc] initWithFrame:CGRectZero];
    descriptionView.userInteractionEnabled = false;
    descriptionView.backgroundColor = [UIColor whiteColor];
    descriptionView.opaque = NO;
    descriptionView.dataDetectorTypes = UIDataDetectorTypeNone;
    
    // Check whether selecting bearing is SKF Explorer or not.
    NSMutableString *databaseString;
    if([selectedProduct.explorer isEqualToString:@"true"]) {
        databaseString = [NSMutableString stringWithFormat:@"%@* - ", selectedProduct.designation];
    }else {
        databaseString = [NSMutableString stringWithFormat:@"%@ - ", selectedProduct.designation];
    }
    
    [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@</b>: %@, ", [calculationDataManager getTranslationString:@"Type"], selectedProduct.typestext]];
    
    for (Property *property in selectedProduct.properties) {
        if (property.uom) {
            [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@ %@, ", property.presentation, property.value, property.uom]];
        } else{
            [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@, ", property.presentation, property.value]];
        }
    }
    
    if (databaseString.length) {
        [databaseString deleteCharactersInRange:NSMakeRange(databaseString.length-2, 2)];
        [descriptionView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, databaseString] baseURL:[NSURL URLWithString:@""]];
        UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_SUBTEXT];
        float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:databaseString] :descriptionFont :280 :NSLineBreakByWordWrapping];
        descriptionView.frame = CGRectMake(descriptionView.frame.origin.x, descriptionView.frame.origin.y, descriptionView.frame.size.width, descriptionHeight+40);
    }
    
   
    if(disclaimerLabel.frame.size.width>0) {
        descriptionView.frame = CGRectMake(10, disclaimerLabel.frame.size.height + disclaimerLabel.frame.origin.y+5, 300, 60);
    }else{
        descriptionView.frame = CGRectMake(10, headerLabel.frame.size.height + headerLabel.frame.origin.y+5, 300, 60);
    }
    
    
    
    // if explorer bearing
    UILabel *explorerLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    if ([calculationDataManager.selectedBearing.explorer isEqualToString:@"true"]) {
        
        explorerLabel.frame = CGRectMake(10, descriptionView.frame.size.height + descriptionView.frame.origin.y, 300, 20);
        explorerLabel.userInteractionEnabled = false;
        explorerLabel.backgroundColor = [UIColor clearColor];
        //explorerLabel.text = @"*SKF Explorer bearing ";
        explorerLabel.text =GetLocalizedString(@"Explorer_bearing");
        explorerLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_DISCLAIMER];
        explorerLabel.textAlignment = NSTextAlignmentLeft;
        explorerLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    }

    // View product details link
    UIButton *productLinkButton = [[UIButton alloc]initWithFrame:CGRectZero];
    
    [productLinkButton setTitle:GetLocalizedString(@"View_product_details") forState:UIControlStateNormal];
    NSString *selectedColor = COLOR_SKF_BLUE;
    
    productLinkButton.titleLabel.textColor = [UIColor colorWithHexString:selectedColor];
    
    productLinkButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    productLinkButton.backgroundColor = [UIColor clearColor];
    [productLinkButton setTitleColor:[UIColor colorWithHexString:selectedColor] forState:UIControlStateNormal];
    [productLinkButton setTitleColor:[UIColor colorWithHexString:selectedColor] forState:(UIControlStateSelected | UIControlStateHighlighted)];
    productLinkButton.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON_MEDIUM];
    
    [productLinkButton addTarget:self
                          action:@selector(productLinkButtonClicked:)
                forControlEvents:UIControlEventTouchUpInside];
    

    productLinkButton.frame = CGRectMake(10, descriptionView.frame.size.height + descriptionView.frame.origin.y+5, 300, 25);
    if(explorerLabel.frame.size.height)
    {
        productLinkButton.frame = CGRectMake(10, explorerLabel.frame.size.height+explorerLabel.frame.origin.y+5, 300, 25);
    }
    
    footerHeight = headerLabel.frame.size.height + productDataImageView.frame.size.height + disclaimerLabel.frame.size.height + descriptionView.frame.size.height + explorerLabel.frame.size.height + productLinkButton.frame.size.height+10;
    
    footerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320, footerHeight)];
    [footerView addSubview:headerLabel];
    [footerView addSubview:productDataImageView];
    [footerView addSubview: disclaimerLabel];
    [footerView addSubview: descriptionView];
    [footerView addSubview:explorerLabel];
    [footerView addSubview:productLinkButton];

    [descriptionView release];
    [headerLabel release];
    [disclaimerLabel release];
    
    [self.inputParametersTable setTableFooterView:footerView];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
      switch (section) {
        case 0:
          {
              UIView *customView = [[[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 300.0, 30.0)] autorelease];
              
              UILabel * headerLabel = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
              headerLabel.backgroundColor = [UIColor clearColor];
              headerLabel.opaque = NO;
              headerLabel.textColor = [UIColor colorWithHexString:COLOR_SKF_RED];
              headerLabel.highlightedTextColor = [UIColor colorWithHexString:COLOR_SKF_RED];
              headerLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_GROUPED_TABLE_HEADER];
              headerLabel.frame = CGRectMake(10.0, 0.0, 300.0, 30.0);
              headerLabel.text = GetLocalizedString(@"Enter_Application_Data");
              [customView addSubview:headerLabel];
              
              return customView;
          }
        
        default:
            break;
    }
    
	return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 30.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        ParameterMissing* parameterMissing = [_calculationControl getVisibleParameterAtIndex:indexPath.row];
        
        UIFont *nameFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
        UIFont *resultFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_SUBTEXT];
        UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_SUBTEXT];
        NSString *nameString;
        
        if (!parameterMissing.uom)
        {
            nameString = parameterMissing.presentation;
        }
        else
        {
            nameString = [NSString stringWithFormat:@"%@ [%@]", parameterMissing.presentation, parameterMissing.uom];
        }
        
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        NSString *lastInput = [calculationDataManager getPreviousInput:parameterMissing.name];
        
        float optionHeight;
        if (parameterMissing.families)
        {
            optionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:lastInput] :resultFont :80 :NSLineBreakByWordWrapping];
        }
        else
        {
            optionHeight = 44;
        }
        
        NSString *descriptionString;
        if (![parameterMissing.presentation isEqualToString:parameterMissing.description])
        {
            descriptionString = parameterMissing.description;
        }
        else
        {
            descriptionString = @"";
        }
        
        float nameHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:nameString] :nameFont :220 :NSLineBreakByWordWrapping];
        float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:descriptionString] :descriptionFont :200 :NSLineBreakByWordWrapping];
        
        //DLog(@"name:%f desc:%f option:%f total:%f", nameHeight, descriptionHeight, optionHeight, MAX(nameHeight+descriptionHeight+5, optionHeight));
        return MAX(nameHeight+descriptionHeight+5, optionHeight);
    }
    if (indexPath.section == 1) {
        UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
        
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        
        NSMutableString *databaseString = [NSMutableString stringWithFormat:@"%@ - ", selectedProduct.designation];
        [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@</b>: %@ ", [calculationDataManager getTranslationString:@"Type"], selectedProduct.typestext]];
        for (Property *property in selectedProduct.properties) {
            [databaseString appendString:[NSMutableString stringWithFormat:@"<br/><b>%@:</b> %@mm, ", property.presentation, property.value]];
        }
        
        float height = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:databaseString] :descriptionFont :280 :NSLineBreakByWordWrapping];
        return height+10;
    }
    else
        return 44.0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section)
    {
        case 0:
            [_calculationControl resetVisibleParameters];
            return [[_calculationControl getVisibleParameters] count];
            break;
        case 1:
            //return 1;
            break;
        default:
            break;
    }
    return 0;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSString *)acceptsEntryInTextField:(UITextField *)textField
{   /*
     UITableViewCell *parentCell = (UITableViewCell *)[textField superview];
     ParameterMissing *currentParameter =[_calculationControl getParameterByIdAsInt:parentCell.tag];
     return [_calculationControl validateParameterValue:currentParameter value:textField.text];
     */
    return nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
    
    UITableViewCell *cell = [self getCellFromTableView:inputParametersTable Sender:activeField];
    
    if(cell== nil) {
        buffer_tag = -1;
        return;}
    
    NSInteger tag = cell.tag;
    buffer_tag = tag;
     if ((tag == 20) || (tag == 21) || (tag ==35)) {
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    }
    
    inputParametersTable.contentInset = UIEdgeInsetsMake(0, 0, 240, 0);
    [inputParametersTable selectRowAtIndexPath:[inputParametersTable indexPathForCell:cell] animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    [cell setBackgroundColor:[UIColor whiteColor]];
    [_calculationControl resetMissingValueForParameterId:tag];
    
    
    
    
}


/*- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSString *NUMBERSPERIOD = @"1234567890.,-";
    NSString *NUMBERSPERIODNEW = @"1234567890.,";
    NSString *NUMBERS = @"1234567890";
    NSCharacterSet *cs;
    NSString *filtered;
    
    NSLog(@"Placeholder: %@", textField.placeholder);
    
    if(textField.placeholder != nil){
        
        if ((([textField.text rangeOfString:@"."].location == NSNotFound)) || (([textField.text rangeOfString:@","].location == NSNotFound)) ||
            (([textField.text rangeOfString:@"-"].location == NSNotFound)) ) {
            
            cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERSPERIOD] invertedSet];
            filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            
            NSLog(@"Filtered: %@", filtered);
            NSLog(@"textfield: %@", textField.text);
            NSLog(@"string: %@", string);

            NSString *fullText = textField.text;

            fullText = [fullText stringByAppendingString:filtered];
            
            NSLog(@"Fulltext: %@", fullText);
            
            //NSArray  *arrayOfStringDot = [fullText componentsSeparatedByString:@"."];
            
//            if ([arrayOfStringDot count]>=2) {
//                //return NO;
//                return nil;
//            }
            else {
                return YES;
                //return [string isEqualToString:filtered];
            }
            
            
            
            
            NSArray *sep = [fullText componentsSeparatedByString:@"."];
            if([sep count] >= 2)
            {
                NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
                return !([sepStr length]>1);
            }
            //return YES;
            
            
            

            return [string isEqualToString:filtered];
        }else{
            
            cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
            filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            return [string isEqualToString:filtered];
        }
        
    }else {
        
        if ((([textField.text rangeOfString:@"."].location == NSNotFound)) || (([textField.text rangeOfString:@","].location == NSNotFound))) {
            
            cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERSPERIODNEW] invertedSet];
            filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            return [string isEqualToString:filtered];
            
        }else {
            
            cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
            filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            return [string isEqualToString:filtered];
            
        }
        
    }
    // Period is in use
    cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
    filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [string isEqualToString:filtered];
    
}*/




-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
        
    if([string length]==0){
        return YES;
    }
    
    //Validate Character Entry
    NSCharacterSet *myCharSet;
    if (textField.placeholder != nil) {
                myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789,.-"];
    }else{
            myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789,."];
    }
    
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        
        
        if ([myCharSet characterIsMember:c]) {
            //now check if string already has 1 decimal mark
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
           // NSArray *sep = [newString componentsSeparatedByString:@"."];
            
            NSArray  *arrayOfStringDot = [newString componentsSeparatedByString:@"."];
            NSArray  *arrayOfStringComma = [newString componentsSeparatedByString:@","];
            NSArray  *arrayOfStringMinus = [newString componentsSeparatedByString:@"-"];
            
            if ([arrayOfStringDot count] > 2 || [arrayOfStringComma count] > 2 ||[arrayOfStringMinus count] > 2 ) return NO;
                return YES;
        }
        
    }
    
    return NO;


}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if(block_all_calls) return;
    
   // NSString *message = [self acceptsEntryInTextField:textField];
    
    UITableViewCell *parentCell  = [self getCellFromTableView:inputParametersTable Sender:textField];
    if(parentCell == nil ) {
        if( buffer_tag < 0) return;
        //int  tag = textField.tag;
        int  tag = buffer_tag;
        if(tag == 207 || tag == 226){
        
        [self editTextfieldData:textField :tag];
        }
        return;    }
    
    NSInteger tag = parentCell.tag;
   
    [self editTextfieldData:textField :tag];
   
}


-(void) editTextfieldData:(UITextField *) textField : (int) tag
{
    NSString *message = [self acceptsEntryInTextField:textField];
    
    
    if (tag == 207)//freq
    {
        
        if (textField != nil) {
            NSString *string = textField.text;
            
            if ([string rangeOfString:@","].location != NSNotFound) {
                NSString * newString = [string stringByReplacingOccurrencesOfString:@"," withString:@"."];
                freqVal = [newString doubleValue];
            }
            else
                freqVal   = [textField.text doubleValue];
        }
        
        double freq;
        
 if (freqVal <= 0) {
            
            UIAlertView *freqValue = [[UIAlertView alloc] initWithTitle:GetLocalizedString(@"Error")
                                                                 message:@"Please enter frequency value above zero."
                                                                delegate:self
                                                       cancelButtonTitle:GetLocalizedString(@"Ok")
                                                       otherButtonTitles:nil];
            [freqValue show];
            freqValue.tag = 40;
            [freqValue release];
            [textField resignFirstResponder];
           // return;
        }
        
         if(freqVal > 0) {
            
            stop_update = 1;
            freq  = 60.0/freqVal;
            // To roundup the decimal value
            NSNumberFormatter *format = [[NSNumberFormatter alloc]init];
            [format setNumberStyle:NSNumberFormatterDecimalStyle];
            [format setRoundingMode:NSNumberFormatterRoundHalfUp];
            [format setMaximumFractionDigits:3];
            NSString *newFreqDecimalValue = [format stringFromNumber:[NSNumber numberWithFloat:freq]];
            NSLog(@"%@",newFreqDecimalValue);
            
            [_calculationControl setValue:newFreqDecimalValue forId:226];
            NSString* parameterName = [_calculationControl nameForParameterId:226];
            CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
            [calculationDataManager setPreviousInput:parameterName withValue:newFreqDecimalValue];
            
        }else{
            NSString * f_7 = [_calculationControl valueForId:@"207"];
            [textField setText:f_7];
            //[textField becomeFirstResponder];
        }

        
    }
    
    if (tag == 226) { // time param
        if (textField !=nil) {
            NSString *string = textField.text;
            if ([string rangeOfString:@","].location != NSNotFound) {
                NSString * newString = [string stringByReplacingOccurrencesOfString:@"," withString:@"."];
                timeVal = [newString doubleValue];
            }
            else
                timeVal = [textField.text doubleValue];
            
        }
        
        double time;
 if (timeVal <= 0) {
            
            UIAlertView *timeValue = [[UIAlertView alloc] initWithTitle:GetLocalizedString(@"Error")
                                                                 message:@"Please enter oscillation time value above zero."
                                                                delegate:self
                                                       cancelButtonTitle:GetLocalizedString(@"Ok")
                                                       otherButtonTitles:nil];
            [timeValue show];
            timeValue.tag = 41;
            [timeValue release];
            [textField resignFirstResponder];
             //return;
            
            
        }
        if (timeVal > 0) {
            stop_update = 1;
            time  = 60.0/timeVal;
            //NSString * t_freq = @(time).stringValue;
            // To roundup the decimal value
            NSNumberFormatter *format = [[NSNumberFormatter alloc]init];
            [format setNumberStyle:NSNumberFormatterDecimalStyle];
            [format setRoundingMode:NSNumberFormatterRoundHalfUp];
            [format setMaximumFractionDigits:3];
            NSString *newTimeDecimalValue = [format stringFromNumber:[NSNumber numberWithFloat:time]];
            NSLog(@"%@",newTimeDecimalValue);
            
            
            [_calculationControl setValue:newTimeDecimalValue forId:207];
            NSString* parameterName = [_calculationControl nameForParameterId:207];
            CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
            [calculationDataManager setPreviousInput:parameterName withValue:newTimeDecimalValue];
            
        }else{
            NSString *t_7 = [_calculationControl valueForId:@"226"];
            [textField setText:t_7];
            //[textField becomeFirstResponder];

        }

    }
    
    
    if ([message length] == 0) {
        
        [_calculationControl setValue:textField.text forId:tag];
        NSString* parameterName = [_calculationControl nameForParameterId:tag];
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        [calculationDataManager setPreviousInput:parameterName withValue:textField.text];
        if(stop_update > 0)
        {
            [inputParametersTable reloadData];
         //   [inputParametersTable reloadInputViews];
            stop_update = -1;
           // [textField becomeFirstResponder];
            [textField resignFirstResponder];
            [self textFieldDidBeginEditing : textField ];
            
            
        }
        
    } else{
        
        textField.delegate = nil;
        [_calculationControl removeValueForId:tag];
        [textField setText:@""];
        [textField resignFirstResponder];
        currTextField = textField;
        
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        if ([self.navigationController.viewControllers containsObject:self]) {
            
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:[calculationDataManager getTranslationString:@"Error"]
                                                         message:NSLocalizedString(message, @"")
                                                        delegate:self
                                               cancelButtonTitle:[calculationDataManager getTranslationString:@"OK"] otherButtonTitles:nil];
            [av show];
            [av release];
            
        }
    }
    activeField = textField;
    
    
    inputParametersTable.contentInset = UIEdgeInsetsZero;
    
}

#pragma mark -
#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 2) {
        
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        

        if(!calculationDataManager.mutlipleCalculation || calculationDataManager.selectedCalculationsArray.count == noFormulaErrorsArray.count){
            [self.navigationController popViewControllerAnimated:YES];
            
        }else {

            //showHeader = YES;
            //showHeader = !showHeader;
//            if(objectsToDiscard != nil && objectsToDiscard.count == calculationDataManager.selectedCalculationsArray.count-1) {
//             
//                
//                calculationDataManager.mutlipleCalculation = false;
//            }
            
            
            [calculationDataManager.selectedCalculationsArray removeObjectsInArray:objectsToDiscard];
            //[inputParametersTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            [self.inputParametersTable beginUpdates];
            [self prepareHeaderView];
            [self.inputParametersTable endUpdates];
            
        }
                
    }else if (alertView.tag == 11) {
        
            if (buttonIndex == 1) {
            
            CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
            [calculationDataManager resetInputParameters];
            [_calculationControl resetInputParameters];
                
                

//                if ([parameterCoolingFactor.parameterId isEqualToString:@"97"]) {
//                    
//                    parameterFieldCoolingFactor.text = @"";
//                    
//
//                }
                
            [inputParametersTable reloadData];
        }
        
    }else {
        // gets called when the user dismisses the alert view
        
        currTextField.text = @""; // erase bad entry
        currTextField.delegate = self; // We want to listen for return events again
        [currTextField becomeFirstResponder];
        currTextField = nil;

    }
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*** As in iOS 7 we are not reusing the cell, we are using single Cellindentifier ***/
    /*static NSString *inputCellIdentifier = @"Cell1";
    static NSString *bearingCellIdentifier = @"Cell2";*/
    
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"%d_%d",indexPath.section,indexPath.row];
    
    UITableViewCell *cell;
    UIWebView *nameWebView;
    UIWebView *descriptionLabel;
    UITextField *parameterField;
    UISwitch* parameterSwitch;
    UIWebView *parameterLabel;
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    ParameterMissing* parameterMissing = [_calculationControl getVisibleParameterAtIndex:indexPath.row];
    
    switch (indexPath.section) {
        case 0:
        {
            //cell = [tableView dequeueReusableCellWithIdentifier:inputCellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                //cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:inputCellIdentifier] autorelease];
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                
                nameWebView = [[UIWebView alloc] initWithFrame:CGRectMake(15, 3, 200, 80)];
                nameWebView.userInteractionEnabled = TRUE;
                nameWebView.tag = 10010;
                nameWebView.backgroundColor = [UIColor clearColor];
                nameWebView.dataDetectorTypes = UIDataDetectorTypeLink;
                nameWebView.opaque = NO;
                nameWebView.delegate = self;
                nameWebView.scrollView.scrollEnabled = NO;
                nameWebView.scrollView.bounces = NO;
                [cell addSubview:nameWebView];
                [nameWebView release];
                
                descriptionLabel = [[UIWebView alloc] initWithFrame:CGRectMake(15, 25, 200, 12)];
                descriptionLabel.backgroundColor = [UIColor clearColor];
                descriptionLabel.userInteractionEnabled = false;
                descriptionLabel.opaque = NO;
                descriptionLabel.dataDetectorTypes = UIDataDetectorTypeNone;
                descriptionLabel.tag = 10020;
                [cell addSubview:descriptionLabel];
                [descriptionLabel release];
                
                parameterField = [[UITextField alloc] initWithFrame:CGRectMake(220, 7, 80, 32)];
                parameterField.borderStyle = UITextBorderStyleRoundedRect;
                parameterField.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
                parameterField.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_INPUT];
                parameterField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                parameterField.autocorrectionType = UITextAutocorrectionTypeNo;
                parameterField.backgroundColor = [UIColor whiteColor];
                parameterField.clearButtonMode = UITextFieldViewModeWhileEditing;
                //parameterField.keyboardType = UIKeyboardTypeDecimalPad;
                parameterField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                parameterField.returnKeyType = UIReturnKeyDone;
                parameterField.inputAccessoryView = keyboardToolBar;
                parameterField.clearButtonMode = UITextFieldViewModeWhileEditing;
                parameterField.textAlignment = NSTextAlignmentRight;
                parameterField.tag = 10100;
                parameterField.delegate = self;
                //[cell addSubview:parameterField];
                
                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
                    [cell.contentView addSubview:parameterField];
                }else {
                    [cell addSubview:parameterField];
                }
                [parameterField release];
                
                parameterSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(220, 15, 80, 32)];
                parameterSwitch.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                parameterSwitch.backgroundColor = [UIColor clearColor];
                parameterSwitch.tag = 10040;
                [cell addSubview:parameterSwitch];
                [parameterSwitch release];
                [parameterSwitch addTarget:self action:@selector(switchDidChangeValue:) forControlEvents:UIControlEventValueChanged];
                
                parameterLabel = [[UIWebView alloc] initWithFrame:CGRectMake(220, 7, 80, 120)];
                parameterLabel.backgroundColor = [UIColor clearColor];
                parameterLabel.userInteractionEnabled = false;
                parameterLabel.tag = 10030;
                parameterLabel.opaque = NO;
                parameterLabel.dataDetectorTypes = UIDataDetectorTypeNone;
                parameterLabel.hidden = true;
                [cell addSubview:parameterLabel];
                [parameterLabel release];
                
                if ([parameterMissing.parameterId isEqualToString:@"20"] || [parameterMissing.parameterId isEqualToString:@"21"] || [parameterMissing.parameterId isEqualToString:@"35"]) {
                    parameterField.placeholder = @" ";
                }
        }
            
           
           

            
/*** setting Lubrication Cleanness & lubrication - Oil & Grease **/

            //CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
            NSString *lastInput = [calculationDataManager getPreviousInput:parameterMissing.name];

            // To check relubrication calcualtion exist
            bool relubCalc_exist = false;
            for (Calculation *calculation in calculationDataManager.selectedCalculationsArray) {
                if ([calculation.calculationId isEqualToString:@"14"]) {
                    relubCalc_exist = true;
                    break;
                }
            }
            
            // To check Lubrication cleanness type selected
            if ([parameterMissing.parameterId isEqualToString:@"62"]) {
                if ([lastInput.description rangeOfString:@"ISO 4406"].location != NSNotFound) {
                    lubClean_oil = true;
                    lubClean_grease = false;
                }else {
                    lubClean_grease = true;
                    lubClean_oil = false;
                }
            }
           
            
            // To reset & remove the lubrication values
            NSLog(@"ParameterID: %@",parameterMissing.parameterId);
            if ([parameterMissing.parameterId isEqualToString:@"96"] || [parameterMissing.parameterId isEqualToString:@"227"] || [parameterMissing.parameterId isEqualToString:@"278"]) {
                if ((!relubCalc_exist) && (lubClean_oil) ) {
                    if ([lastInput.description isEqualToString:@"Grease"]) {
                        lastInput = nil;
                        [calculationDataManager setPreviousInput:parameterMissing.name withValue:@""];
                        [_calculationControl setValue:@"" forName:parameterMissing.name];
                    }
                }else if ((!relubCalc_exist) && (lubClean_grease)){
                    if (![lastInput.description isEqualToString:@"Grease"]) {
                        lastInput = nil;
                        [calculationDataManager setPreviousInput:parameterMissing.name withValue:@""];
                        [_calculationControl setValue:@"" forName:parameterMissing.name];
                    }
                }
            }
            
            // To remove oil option for Relubrication calculation
            if ((relubCalc_exist)) {
                if ([parameterMissing.parameterId isEqualToString:@"62"]) {
                    if ([lastInput.description rangeOfString:@"ISO 4406"].location != NSNotFound) {
                        lastInput = nil;
                        [calculationDataManager setPreviousInput:parameterMissing.name withValue:@""];
                        [_calculationControl setValue:@"" forName:parameterMissing.name];
                    }
                }
                
                if ([parameterMissing.parameterId isEqualToString:@"96"] || [parameterMissing.parameterId isEqualToString:@"227"] || [parameterMissing.parameterId isEqualToString:@"278"]) {
                    if (![lastInput.description isEqualToString:@"Grease"]) {
                        lastInput = nil;
                        [calculationDataManager setPreviousInput:parameterMissing.name withValue:@""];
                        [_calculationControl setValue:@"" forName:parameterMissing.name];

                    }
                }
            }
                    
            // End
            
            nameWebView = (UIWebView *)[cell viewWithTag:10010];
            descriptionLabel = (UIWebView *)[cell viewWithTag:10020];
            parameterField = (UITextField *)[cell viewWithTag:10100];
            parameterLabel = (UIWebView *)[cell viewWithTag:10030];
            parameterSwitch = (UISwitch *)[cell viewWithTag:10040];
                                    
            if([parameterMissing.parameterId isEqualToString:@"165"]) {
                parameterYbearingMinLoad = [_calculationControl getVisibleParameterAtIndex:indexPath.row];
            }
//            if ([parameterMissing.parameterId isEqualToString:@"97"]) {
//                
//                parameterCoolingFactor = [_calculationControl getVisibleParameterAtIndex:indexPath.row];
//            }
            // To allow -ve value for fr,fa & temp
            /*if ([parameterMissing.parameterId isEqualToString:@"20"]) {
                parameterNegativeValue = [_calculationControl getVisibleParameterAtIndex:indexPath.row];
            }*/
            
            //NSString *lastInput = [_calculationControl valueForId:parameterMissing.parameterId];
            
            BOOL typeIsCheckBox = [parameterMissing.dataType isEqualToString:@"0"];
            BOOL typeIsOptions = !typeIsCheckBox && parameterMissing.families;
            BOOL cellIsMissingInputValue = [_calculationControl isParameterIdStrMissingValue:parameterMissing.parameterId];
            BOOL cellHasInvalidInputValue = [_calculationControl hasParameterIdStrInvalidValue:parameterMissing.parameterId];
            
            
            if (cellIsMissingInputValue || cellHasInvalidInputValue) {
                [cell setBackgroundColor:[UIColor colorWithHexString:@"fff8e6"]];
            } else {
                [cell setBackgroundColor:[UIColor whiteColor]];
            }
            
//            if (lastInput !=nil) {
//                if (typeIsCheckBox){
//                    parameterSwitch.on = [lastInput isEqualToString:@"true"];
//                } else {
//                    parameterField.text = lastInput;
//  
//                    
//                    
//                }
//                [parameterLabel loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, lastInput] baseURL:[NSURL URLWithString:@""]];
//                
//            } else {
//                if (typeIsCheckBox){
//                    parameterSwitch.on = FALSE;
//                }  else {
//                    parameterField.text = @"";
//                }
//                [parameterLabel loadHTMLString:[NSString stringWithFormat:@"%@", cssString] baseURL:[NSURL URLWithString:@""]];
//            }
            
            
            NSString* currentValue = [_calculationControl valueForId:parameterMissing.parameterId];
            
            if (currentValue !=nil /* &&  ![parameterMissing.parameterId isEqualToString:@"226"] && ![parameterMissing.parameterId isEqualToString:@"207"]*/)  {
                
                parameterSwitch.on = [currentValue isEqualToString:@"true"];
                [parameterLabel loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, currentValue] baseURL:[NSURL URLWithString:@""]];
                parameterField.text = currentValue;
                
                
            }else /*if( ![parameterMissing.parameterId isEqualToString:@"226"] && ![parameterMissing.parameterId isEqualToString:@"207"])*/{
                parameterSwitch.on = FALSE;
                [parameterLabel loadHTMLString:[NSString stringWithFormat:@"%@", cssString] baseURL:[NSURL URLWithString:@""]];
                if ([parameterMissing.parameterId isEqualToString:@"97"]) {
                    parameterField.text = @"0.0";
                }
                else
                {
                    parameterField.text = @"";


                }
                
            }
            
            nameWebView.hidden = false;
            NSString *nameString;
            
            if (!parameterMissing.uom) {
                nameString = parameterMissing.presentation;
                [nameWebView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, parameterMissing.presentation] baseURL:[NSURL URLWithString:@""]];
            } else {
                nameString = [NSString stringWithFormat:@"%@ [%@]", parameterMissing.presentation, parameterMissing.uom];
                [nameWebView loadHTMLString:[NSString stringWithFormat:@"%@%@ [%@]", cssString, parameterMissing.presentation, parameterMissing.uom] baseURL:[NSURL URLWithString:@""]];
            }
            
            // Removing null object from the input parameters table
            if (![parameterMissing.presentation isEqualToString:parameterMissing.description])
            {
                if ([parameterMissing.description length] == 0) {
                    
                    descriptionLabel.hidden = true;
                }else {
                    
                    [descriptionLabel loadHTMLString:[NSString stringWithFormat:@"%@%@", cssStringGray, parameterMissing.description] baseURL:[NSURL URLWithString:@""]];
                    descriptionLabel.hidden = false;
                    
                }
            }
            else
            {
                [descriptionLabel loadHTMLString:[NSString stringWithFormat:@"%@%@", cssStringGray, @""] baseURL:[NSURL URLWithString:@""]];
            }
            
            if (typeIsCheckBox)
            {
                parameterField.hidden = true;
                parameterSwitch.hidden = false;
                parameterLabel.hidden = true;
                [cell setUserInteractionEnabled:YES];
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            }
            else if (typeIsOptions)
            {

// Hiding label text & input field for minimum load for YB/YAR bearing.
                
                if ([parameterMissing.parameterId isEqualToString:@"165"]) {
                    
                    nameWebView.hidden = true;
                    parameterField.hidden = true;
                    parameterLabel.hidden = true;
                    parameterSwitch.hidden = true;
                    [cell setUserInteractionEnabled:NO];
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                    
                }else {
                    
                    parameterField.hidden = true;
                    parameterSwitch.hidden = true;
                    parameterLabel.hidden = false;
                    [cell setUserInteractionEnabled:YES];
                    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
                }
            }
            else
            {
                if ([indexPathsForTextFields indexOfObject:indexPath] == NSNotFound)
                {
                    [indexPathsForTextFields addObject:indexPath];
                    cell.tag = [parameterMissing.parameterId intValue];
                    
                    // cell.contentView.tag = [parameterMissing.parameterId intValue];
                    
                    //NSLog(@"Cell tag===:%ld", (long)cell.tag);
                     //NSLog(@"indexPath.row===:%ld", (long)indexPath.row);
                }
                
                parameterField.hidden = false;
                parameterSwitch.hidden = true;
                parameterLabel.hidden = true;
                [cell setUserInteractionEnabled:YES];
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            }
            
           cell.tag = [parameterMissing.parameterId intValue];
            
           // cell.contentView.tag = [parameterMissing.parameterId intValue];
            
            //NSLog(@"Cell tag:%ld", (long)cell.tag);
            
            UIFont *nameFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
            UIFont *optionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_SUBTEXT];
            UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_SUBTEXT];
            
            float nameHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:nameString] :nameFont :220 :NSLineBreakByWordWrapping];
            float optionHeight;
            if (parameterMissing.families)
            {
                optionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:parameterField.text] :optionFont :80 :NSLineBreakByWordWrapping];
            }
            else
            {
                optionHeight = 44;
            }
            
            NSString *descriptionString;
            if (![parameterMissing.presentation isEqualToString:parameterMissing.description])
            {
                descriptionString = parameterMissing.description;
            }
            else
            {
                descriptionString = @"";
            }
            
            float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:descriptionString] :descriptionFont :200 :NSLineBreakByWordWrapping];
            
            float totalHeight = MAX(nameHeight+descriptionHeight+5, optionHeight);
            
            descriptionLabel.frame = CGRectMake(descriptionLabel.frame.origin.x, totalHeight-descriptionHeight-2, descriptionLabel.frame.size.width, descriptionHeight+100);
            parameterLabel.frame = CGRectMake(parameterLabel.frame.origin.x, parameterLabel.frame.origin.y, parameterLabel.frame.size.width, optionHeight);
            parameterLabel.center = CGPointMake(parameterLabel.center.x, totalHeight/2);
            parameterField.center = CGPointMake(parameterLabel.center.x, totalHeight/2);
            
            return cell;
            break;
        }
        case 1:
        {
            //cell = [tableView dequeueReusableCellWithIdentifier:bearingCellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

            
            
            UIWebView *descriptionView;
            
            if (cell == nil) {
                //cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:bearingCellIdentifier] autorelease];
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                descriptionView = [[UIWebView alloc] initWithFrame:CGRectMake(15, 3, 280, 180)];
                descriptionView.userInteractionEnabled = false;
                descriptionView.backgroundColor = [UIColor clearColor];
                descriptionView.opaque = NO;
                descriptionView.dataDetectorTypes = UIDataDetectorTypeNone;
                descriptionView.tag = 30;
                [cell addSubview:descriptionView];
                [descriptionView release];
            }
            
            descriptionView = (UIWebView *)[cell viewWithTag:30];
            
            [cell setUserInteractionEnabled:YES];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            //NSMutableString *databaseString = [NSMutableString stringWithFormat:@"%@ - ", selectedProduct.designation];

// Check whether selecting bearing is SKF Explorer or not.
          
            NSMutableString *databaseString;
            
            if([selectedProduct.explorer isEqualToString:@"true"]) {
                
                databaseString = [NSMutableString stringWithFormat:@"%@* - ", selectedProduct.designation];
                
            }else {
                
                databaseString = [NSMutableString stringWithFormat:@"%@ - ", selectedProduct.designation];
            }

            [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@</b>: %@, ", [calculationDataManager getTranslationString:@"Type"], selectedProduct.typestext]];
            
            for (Property *property in selectedProduct.properties) {
                if (property.uom)
                {
                    [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@ %@, ", property.presentation, property.value, property.uom]];
                }
                else
                {
                    [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@, ", property.presentation, property.value]];
                }
            }
            
            
            if ([databaseString length] != 0)
            {
                [databaseString deleteCharactersInRange:NSMakeRange([databaseString length]-2, 2)];
                
                [descriptionView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, databaseString] baseURL:[NSURL URLWithString:@""]];
                UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_SUBTEXT];
                float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:databaseString] :descriptionFont :280 :NSLineBreakByWordWrapping];
                descriptionView.frame = CGRectMake(descriptionView.frame.origin.x, descriptionView.frame.origin.y, descriptionView.frame.size.width, descriptionHeight+40);
            }
            

            
            return cell;
            break;
        }
            
        default:
            break;
    }
    return nil;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ParameterMissing *parameterMissing = [_calculationControl getVisibleParameterAtIndex:indexPath.row];
    
    BOOL typeIsCheckBox = [parameterMissing.dataType isEqualToString:@"0"];
    BOOL typeIsOptions = !typeIsCheckBox && parameterMissing.families;
    
    if (typeIsOptions) {
        
        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
        [cell setBackgroundColor:[UIColor whiteColor]];
        [_calculationControl resetMissingValueForParameterId:cell.tag];
        
        if ([parameterMissing.families count] == 1)
        {
            OptionSelectViewController_iPhone *optionController = [[OptionSelectViewController_iPhone alloc] initWithNibName:@"OptionSelectViewController" bundle:[NSBundle mainBundle]];
            optionController.optionsArray = parameterMissing.families;
            optionController.parameterName = parameterMissing.name;
            optionController.parameterPresentation = parameterMissing.presentation;
            
            optionController.parentIndexPath = indexPath;
            optionController.delegate = self;
            
            
            fiveinoneAppDelegate_iPhone *rootDelegate = (fiveinoneAppDelegate_iPhone *)[UIApplication sharedApplication].delegate;
            [rootDelegate.navigationController pushViewController:optionController animated: YES];
        }
        else
        {
            FamilySelectViewController_iPhone *optionController = [[FamilySelectViewController_iPhone alloc] initWithNibName:@"OptionSelectViewController" bundle:[NSBundle mainBundle]];
            
            optionController.optionsArray = parameterMissing.families;
            optionController.parameterName = parameterMissing.name;
            optionController.parameterPresentation = parameterMissing.presentation;
            
            optionController.parentIndexPath = indexPath;
            optionController.delegate = self;
            
            fiveinoneAppDelegate_iPhone *rootDelegate = (fiveinoneAppDelegate_iPhone *)[UIApplication sharedApplication].delegate;
            [rootDelegate.navigationController pushViewController:optionController animated: YES];
            
        }
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

- (void)selectedOption:(NSString *)selectedOption withIndex:(NSInteger)selectedIndex andName:(NSString *)name andIndexPath:(NSIndexPath *)indexPath {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager setPreviousInput:name withValue:selectedOption];
    [_calculationControl setValue:selectedOption forName:name];
    
    if ([name isEqualToString:@"grease"]) {
        
        [_calculationControl setValue:selectedOption forId:163];

    }
    
    fiveinoneAppDelegate_iPhone *rootDelegate = (fiveinoneAppDelegate_iPhone *)[UIApplication sharedApplication].delegate;
    [rootDelegate.navigationController popToViewController:self animated:YES];
    [inputParametersTable reloadData];
}


-(void) done {
    
    [[self.inputParametersTable superview] endEditing:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [self setCalculationButton:nil];

    keyboardToolBar = nil;
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager removeObserver:self];
    
    [indexPathsForTextFields removeAllObjects];
    [indexPathsForTextFields release];
    indexPathsForTextFields =nil;
    
    [noFormulaErrorsDictionary removeAllObjects];
    [noFormulaErrorsDictionary release];
    noFormulaErrorsDictionary = nil;
    
    [noFormulaErrorsArray removeAllObjects];
    [noFormulaErrorsArray release];
    noFormulaErrorsArray=nil;
    
    [objectsToDiscard removeAllObjects];
    [objectsToDiscard release];
    objectsToDiscard = nil;
    
    
    [selectedCalculationsLabel release];
    selectedCalculationsLabel = nil;
    
    [forceImage_iPhone release];
    forceImage_iPhone = nil;
    
    [imageDisclaimer release];
    imageDisclaimer = nil;
    
    [headerView release];
    headerView = nil;
    
    [productDataImageView release];
    productDataImageView = nil;
    
    [footerView release];
    footerView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    CalculationDataManager* calculatorDataManager = [CalculationDataManager sharedManager];
    [calculatorDataManager removeObserver:self];
    
    [_calculationControl release];
    [keyboardToolBar release];
    [calculationButton release];
    
    
    [super dealloc];
}

- (void)dataManagerReturnedTranslation:(BOOL)success {
    
    if (success){
        
        self.navigationItem.titleView = [TitleManager getTitle:GetLocalizedString(@"Parameters")];
        self.title = GetLocalizedString(@"Parameters");
        
        //[doCalculation setTitle:GetLocalizedString(@"Calculate") forState:UIControlStateNormal];
        
        [doCalculation setTitle:GetLocalizedString(@"Calculate") forState:UIControlStateNormal];
        [doCalculation setTitle:GetLocalizedString(@"Calculate") forState:UIControlStateDisabled];
        [doCalculation setTitle:GetLocalizedString(@"Calculate") forState:UIControlStateHighlighted];
        [doCalculation setTitle:GetLocalizedString(@"Calculate") forState:UIControlStateSelected];
                
        [doResetButton setTitle:@"Reset" forState:UIControlStateNormal];
        [doResetButton setTitle:@"Reset" forState:UIControlStateDisabled];
        [doResetButton setTitle:@"Reset" forState:UIControlStateHighlighted];
        [doResetButton setTitle:@"Reset" forState:UIControlStateSelected];
    }
}

- (void) productLinkButtonClicked:(id)sender {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    NSString *productLinkPath = [NSString stringWithFormat:@"http://www.skf.com/productcatalogue/prodlink.html?prodid=%@&lang=en&imperial=false",calculationDataManager.selectedBearing.productId];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: productLinkPath]];
    
}


-(void)sendNewRequest
{
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
//    NSLog(@"Calc count: %lu", (unsigned long)[calculationDataManager.selectedCalculationsArray count]);
//    
//    NSLog(calculationDataManager.mutlipleCalculation ? @"Yes" : @"No");
//    
//    
//    for (Calculation *calcID in calculationDataManager.selectedCalculationsArray) {
//        
//        NSLog(@"ID: %@",calcID.calculationId);
//        
//    }
//    NSLog(@"CalID: %@", calculationDataManager.selectedCalculation.calculationId);
//
    

    if(calculationDataManager.mutlipleCalculation == false) {
        
        [calculationDataManager getParametersForCalculation:calculationDataManager.selectedCalculation.calculationId andProductId:
         [selectedProduct productId] userData:self];
        

        
        }else if (calculationDataManager.mutlipleCalculation == true) {
        
        NSMutableString *selectedCalculationsId = [[[NSMutableString alloc] initWithString:@""] autorelease];
        
        int count =0;
        for (Calculation *calculation in calculationDataManager.selectedCalculationsArray) {
            
            [selectedCalculationsId appendString:calculation.calculationId];
            
            if (count++ < (calculationDataManager.selectedCalculationsArray.count-1)) {
                
                [selectedCalculationsId appendFormat:@","];
            }
        }
        
        [calculationDataManager getParametersForCalculation:selectedCalculationsId andProductId:[selectedProduct productId] userData:self];
    
        }
    
//    if (calculationDataManager.mutlipleCalculation && ([calculationDataManager.selectedCalculationsArray count] == 1)) {
//            
//            [calculationDataManager getParametersForCalculation:calculationDataManager.selectedCalculation.calculationId andProductId:
//             [selectedProduct productId] userData:self];
//            
//        }

    

}


-(void) handleErrors {
    
    [self RemoveUnsupportedCalculations];
    
    if (noFormulaErrorsArray.count == 1) {
        
        for (Error* noFormulaError in noFormulaErrorsArray){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:noFormulaError.description
                                                           delegate:self
                                                  cancelButtonTitle:GetLocalizedString(@"OK")
                                                  otherButtonTitles:nil];
            alert.tag = 2;
            [alert show];
            [alert release];
            
        }
    }
    else
    {
        NSMutableString *errorMessages = [[[NSMutableString alloc]init]autorelease];
        NSMutableString *alertTitle = [[[NSMutableString alloc]init]autorelease];
        
        for (Calculation* noFormulaCalc in objectsToDiscard) {
            
            [errorMessages appendString:noFormulaCalc.presentation];
            [errorMessages appendFormat:@"\n\n"];
        }
        
        alertTitle = [[NSMutableString alloc] initWithString:GetLocalizedString(@"The_following_calculations_are_not_possible")];
        [alertTitle appendFormat:@" %@",selectedProduct.designation];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle
                                                        message:errorMessages
                                                       delegate:self
                                              cancelButtonTitle:GetLocalizedString(@"OK")
                                              otherButtonTitles:nil];
        alert.tag = 2;
        [alert show];
        [alert release];
    }
    
}



-(void) RemoveUnsupportedCalculations {
    
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    if(objectsToDiscard == nil) {
        objectsToDiscard = [[NSMutableArray alloc]init];
    }
    else {
        [objectsToDiscard removeAllObjects];
    }
    
    for(Calculation *calc in calculationDataManager.selectedCalculationsArray) {
        for(Error *err in noFormulaErrorsArray) {
            NSString *calcName = [self getCalculationNameFromError:err.description];
            if ([calcName isEqualToString:calc.presentation]) {
                [objectsToDiscard addObject:calc];
                
                if ([calc.calculationId isEqualToString:@"14"]) {
                    
                    calculationDataManager.isRelubricationCalcExist = FALSE;
                    
                }
            }
            
            
            
        }
    }
    NSLog(calculationDataManager.isRelubricationCalcExist ? @"YES" : @"NO");

}

- (NSString *)getCalculationNameFromError:(NSString *)errString {
    
    NSMutableString *errorMessages = [[[NSMutableString alloc]init]autorelease];
    
    NSRange range = [errString rangeOfString:@"is" options:NSBackwardsSearch];
    [errorMessages appendString:[errString substringToIndex:range.location-1]];
    
    [errorMessages replaceOccurrencesOfString:@"Calculation " withString:@""options:NSCaseInsensitiveSearch
                                        range:NSMakeRange(0, [errorMessages length])];
    return errorMessages;
}

- (void)setSearchingModeStart
{
        
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    HUD = [[MBProgressHUD alloc] initWithFrame:CGRectMake(35, 100, 250, 250)];
    HUD.labelText = [calculationDataManager getTranslationString:@"Loading"];
    HUD.graceTime = 0.5f;
    HUD.taskInProgress = YES;
    HUD.allowsCancelation = NO;
    HUD.delegate = self;
    HUD.labelFont = [UIFont fontWithName:CHEVIN_BOLD size:20.f];
    [self.view addSubview:HUD];    
    [HUD show:YES];
    doCalculation.userInteractionEnabled = NO;
    doResetButton.userInteractionEnabled = NO;
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    
}

- (void)setSearchingModeEnd {
    
    [HUD hide:YES];
    HUD.taskInProgress = NO;
    [HUD removeFromSuperview];
    [HUD release];
    HUD = nil;
    [self.view setUserInteractionEnabled:YES];
    doCalculation.userInteractionEnabled = YES;
    doResetButton.userInteractionEnabled = YES;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    
    
    if (noFormulaErrorsArray.count == 0) {
    }    
}


-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;
{
    NSURL *requestURL =[ [ request URL ] retain ];    
    NSString *urlString = [requestURL absoluteString];
    NSURL *hyperlinkURL = nil;
    
    if ([urlString isEqualToString:@"file:///"] == false) {
        
        CalculationDataManager *calculateDataManager = [CalculationDataManager sharedManager];
        urlString = [urlString stringByReplacingOccurrencesOfString:@"file:///" withString:[NSString stringWithFormat:@"%@/",[calculateDataManager baseURL]]];
        NSLog(@"URL Path: %@",urlString);
        hyperlinkURL = [NSURL URLWithString:urlString];
        [self showHyperlinkView:hyperlinkURL];
    }
    [requestURL release ];
    return YES;
}


-(void) showHyperlinkView:(NSURL *) url{
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    UIWebView *webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    [webView loadRequest:request];
    webView.scalesPageToFit = YES;

        HyperlinkViewController *hyperlinkView = [[[HyperlinkViewController alloc]
                                                 initWithNibName:@"HyperlinkViewController" bundle:nil]autorelease];
        [hyperlinkView.view addSubview:webView];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration: 0.5];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
        [self.navigationController pushViewController:hyperlinkView animated:YES];
        [UIView commitAnimations];
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
    
    return YES;
}

- (void)popoverControllerDidDismissPopover: (UIPopoverController *)popoverController {
    
    NSLog(@"popover dismissed");
}

@end
