//
//  OptionSelectViewController.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/19/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "FamilySelectViewController_iPhone.h"
#import "OptionSelectViewController_iPhone.h"
#import "Family.h"
#import "Option.h"
#import "TitleManager.h"
#import "fiveinoneAppDelegate_iPhone.h"


@implementation FamilySelectViewController_iPhone

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [TitleManager getSKFLogo];
    
    
    /*** iOS 7 - TO remove the space of header ***/
    
    self.optionsTable.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.optionsTable.bounds.size.width, 0.01f)];
    
    /*** END ***/
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    Family *family = [optionsArray objectAtIndex:indexPath.section];
//    Option *option = [family.options objectAtIndex:indexPath.row];
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    bool relub_exists = false;
    for (Calculation *cal in calculationDataManager.selectedCalculationsArray){
        if ([cal.calculationId isEqualToString:@"14"]) {
            relub_exists = true;
            break;
        }
    }
    
    NSLog(relub_exists ? @"YES" : @"No");
    
        if (relub_exists && (indexPath.row !=2)) {
        
                    
           /* UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Warning!"
                                                              message:@"Relubrication interval calculation cannot be performed for oil lubrication."
                                                             delegate:self
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
                    
                [message show];
                [message release];*/
            
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
                
        }else {
    
    
    OptionSelectViewController_iPhone *optionController = [[OptionSelectViewController_iPhone alloc] initWithNibName:@"OptionSelectViewController" bundle:[NSBundle mainBundle]];
    
    optionController.optionsArray = optionsArray;
    optionController.parameterName = parameterName;
    optionController.familyNr = indexPath.row;
    
    optionController.parameterPresentation = parameterPresentation;
    
    
    optionController.parentIndexPath = parentIndexPath;
    
    optionController.delegate = delegate;
    
    fiveinoneAppDelegate_iPhone *rootDelegate = (fiveinoneAppDelegate_iPhone *)[UIApplication sharedApplication].delegate;
    [rootDelegate.navigationController pushViewController:optionController animated: YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        }
    
    
}

@end
