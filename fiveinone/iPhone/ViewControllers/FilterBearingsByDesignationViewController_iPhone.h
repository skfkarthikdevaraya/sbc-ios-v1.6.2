//
//  FilterBearingsByDesignationViewController.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/22/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FilterBearingsByDesignationViewController.h"

@interface FilterBearingsByDesignationViewController_iPhone : FilterBearingsByDesignationViewController {

    BOOL showingAlert;
    
}


@property (nonatomic, assign) BOOL showingAlert;
@property (nonatomic, assign) BOOL connected;




@end
