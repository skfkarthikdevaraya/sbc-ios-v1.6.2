//
//  FilterBearingsByDesignationViewController.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/22/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "FilterBearingsByDesignationViewController_iPhone.h"
#import "FilterBearingsByDesignationViewController.h"
#import "CalculateViewController.h"
#import "Product.h"
#import "FilterBearingsViewController.h"
#import "SelectCalculationViewController.h"
#import "SelectBearingViewController_iPhone.h"
#import "fiveinoneAppDelegate_iPhone.h"

@implementation FilterBearingsByDesignationViewController_iPhone

@synthesize connected;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:RKReachabilityDidChangeNotification
                                               object:nil];
    
    showingAlert = false;
    connected = true;
    
}

- (void)reachabilityChanged:(NSNotification*)notification {
    RKReachabilityObserver* observer = (RKReachabilityObserver*)[notification object];
    
    if (![observer isNetworkReachable])
    {
        // [self setSearchingModeEnd];
        connected = false;
        [self showingAlert];
        DLog(@"Disconnected");
    }
    if ([observer isNetworkReachable]) {
        connected = true;
        DLog(@"Connected");
    }
}



- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    if (!connected) {
        
        [calculationDataManager showAlertForNetworkConnection];
        return;
    
    }
    
    
    
    if ([searchBar.text length] >= 3) {
        
        

        
        NSString *searchType = @"";
        switch ([searchBar selectedScopeButtonIndex]) {
            case 0:
                searchType = @"designationStartsWith";
                break;
            case 1:
                searchType = @"designation";
                break;
            case 2:
                searchType = @"designationEndsWith";
                break;
            default:
                break;
        }
        
        NSLog(@"%@",searchBar.text);
        [calculationDataManager getProducts:[NSMutableDictionary dictionaryWithKeysAndObjects:searchType, searchBar.text, nil] userData:self];
        searchBar.showsScopeBar = NO;  
        
        [searchBar sizeToFit];  
        [searchBar setShowsCancelButton:NO animated:YES]; 
        
        [UIView beginAnimations:@"MyAnimation" context:nil];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:0.3]; 
        
        CGRect frame = parentViewContainer.frame;
        frame.origin.y += 44; 
        frame.size.height -= 44;
        
        CGRect toolbarFrame = segmentedToolBar.frame;
        toolbarFrame.origin.y += 44;
        
       // [filterBearingsNavigationController setNavigationBarHidden:NO animated:YES];
        
        parentViewContainer.frame = frame;
        segmentedToolBar.frame = toolbarFrame;
        
        
        UIView *disabledView = [self.view viewWithTag:200];
        
        [disabledView removeFromSuperview]; 
        
        [UIView commitAnimations];
        
        [searchBar resignFirstResponder];
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[calculationDataManager getTranslationString:@"Please_fill_in_at_least_3_characters."] delegate:self cancelButtonTitle:[calculationDataManager getTranslationString:@"OK"] otherButtonTitles:nil];
        
        [alert show];
        
        [alert release];
        
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
   
    searchBar.showsScopeBar = NO;  
    
    [searchBar sizeToFit];  
    [searchBar setShowsCancelButton:NO animated:YES]; 
    
    [UIView beginAnimations:@"MyAnimation" context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3]; // 5 seconds
    
    CGRect frame = parentViewContainer.frame;
    frame.origin.y += 44; // Move view down 100 pixels
    frame.size.height -= 44;
    
    CGRect toolbarFrame = segmentedToolBar.frame;
    toolbarFrame.origin.y += 44;
    
    parentViewContainer.frame = frame;
    segmentedToolBar.frame = toolbarFrame;
    
    UIView *disabledView = [self.view viewWithTag:200];
    
    [disabledView removeFromSuperview]; 
    
    [searchBar resignFirstResponder];
    [UIView commitAnimations];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CalculateViewController *calculateController = [[CalculateViewController alloc] initWithNibName:@"CalculateViewController" bundle:[NSBundle mainBundle]];
    
    CalculationDataManager *calculateDataManager = [CalculationDataManager sharedManager];
    [calculateDataManager setBearing:(Product *)[previousBearingsArray objectAtIndex:indexPath.row]];

    fiveinoneAppDelegate_iPhone *rootDelegate = (fiveinoneAppDelegate_iPhone *)[UIApplication sharedApplication].delegate;
    [rootDelegate.navigationController pushViewController:calculateController animated:YES];
     
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [calculateController release];
    calculateController = nil;    
}

- (void)calculationDataManager:(CalculationDataManager *)calculationDataManager returnedArray:(NSArray *)returnedArray {
    BOOL requestByThisView = calculationDataManager.lastUserData == self;
    
    if (requestByThisView){
        foundBearingsArray = [[NSMutableArray alloc] init];
    
        SelectBearingViewController_iPhone *bearingController = [[SelectBearingViewController_iPhone alloc] initWithNibName:@"SelectBearingViewController" bundle:[NSBundle mainBundle]];
    
        for (int i = 0; i < [returnedArray count]; i++) {
            Product *result = [returnedArray objectAtIndex:i];
            
            if (result.properties.count > 0) {
                
                [foundBearingsArray addObject:result];
            }
        }
    
        bearingController.foundBearingsArray = foundBearingsArray;

        fiveinoneAppDelegate_iPhone *rootDelegate = (fiveinoneAppDelegate_iPhone *)[UIApplication sharedApplication].delegate;
        [rootDelegate.navigationController pushViewController:bearingController animated: YES];
        [bearingController release];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end
