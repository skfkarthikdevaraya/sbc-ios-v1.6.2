//
//  FilterBearingsByDimensionsViewController.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/22/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "FilterBearingsByDimensionsViewController_iPhone.h"
#import "SelectBearingTypeViewController_iPhone.h"
#import "SelectBearingViewController_iPhone.h"
#import "CalculationDataManager.h"
#import "fiveinoneAppDelegate_iPhone.h"

@implementation FilterBearingsByDimensionsViewController_iPhone

@synthesize managingViewController;
//@synthesize connected;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGSize rect =   [[UIScreen mainScreen] bounds].size;
      if(rect.height > 500)
      {
          filterBearingsButton.frame = CGRectMake(self.view.frame.size.width/2 - 280/2 , 385, 280, 37);
          CGRect newFrame = CGRectMake(0, 370, 320, 1);
         // newFrame.;
          
          [lineView setFrame:newFrame]
          ;
      }
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    keyboardToolBar = [[UIToolbar alloc] init];
    [keyboardToolBar setBarStyle:UIBarStyleBlackTranslucent];
    [keyboardToolBar sizeToFit];
    
    UIBarButtonItem *prevButton =[[UIBarButtonItem alloc] initWithTitle:[calculationDataManager getTranslationString:@"Prev"] style:UIBarButtonItemStyleBordered target:self action:@selector(prevButton:)];
    UIBarButtonItem *nextButton =[[UIBarButtonItem alloc] initWithTitle:[calculationDataManager getTranslationString:@"Next"] style:UIBarButtonItemStyleBordered target:self action:@selector(nextButton:)];
    UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *doneButton =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done)];
    
    NSArray *itemsArray = [NSArray arrayWithObjects:prevButton, nextButton, flexButton, doneButton, nil];
    
    [flexButton release];
    [doneButton release];
    [nextButton release];
    [prevButton release];
    [keyboardToolBar setItems:itemsArray];
    
    boreDiameterMinInput.inputAccessoryView = keyboardToolBar;
    boreDiameterMaxInput.inputAccessoryView = keyboardToolBar; 
    outerDiameterMinInput.inputAccessoryView = keyboardToolBar;
    outerDiameterMaxInput.inputAccessoryView = keyboardToolBar;
    
    

//    if ((int)[[UIScreen mainScreen] bounds].size.height == 568)
//    {
//        
//        CGRect newFrame = self.view.frame;
//        
//        newFrame.size.width = 320;
//        newFrame.size.height = 600;
//        [self.view setFrame:newFrame];
//        
//        [self setContentSizeForViewInPopover:CGSizeMake(320, 600)];
//        
//        [scrollView setFrame:CGRectMake(0, 0, 320, 600)];
//        filterBearingsButton.backgroundColor = [UIColor brownColor];
//        [filterBearingsButton setFrame:CGRectMake(20, 358, 280, 37)];
//    }
    
}

//- (void)reachabilityChanged:(NSNotification*)notification {
//    RKReachabilityObserver* observer = (RKReachabilityObserver*)[notification object];
//    
//    if (![observer isNetworkReachable])
//    {
//        
//        
//        // [self setSearchingModeEnd];
//        connected = false;
//        [self showingAlert];
//        DLog(@"Disconnected");
//    }
//    if ([observer isNetworkReachable]) {
//        connected = true;
//        DLog(@"Connected");
//    }
//}


- (void)moveKeyboard:(int)height
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= (height + 44);
    CGPoint origin = activeField.frame.origin;
    origin.y -= scrollView.contentOffset.y;
    if (!CGRectContainsPoint(aRect, origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-(aRect.size.height)); 
        [scrollView setContentOffset:scrollPoint animated:YES];
    }
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
//    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
//
//    if (!connected) {
//        
//        [calculationDataManager showAlertForNetworkConnection];
//        return;
//        
//    }
    
    
    activeField = textField;
    [self moveKeyboard:260+44];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *NUMBERSPERIOD = @"1234567890.,";
    NSString *NUMBERS = @"1234567890";
    NSCharacterSet *cs;
    NSString *filtered;
    
    // Check for period
    if (([textField.text rangeOfString:@"."].location == NSNotFound) && ([textField.text rangeOfString:@","].location == NSNotFound))
    {
        cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERSPERIOD] invertedSet];
        filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    
    // Period is in use
    cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
    filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [string isEqualToString:filtered];
}


- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [self moveKeyboard:kbSize.height+44];
}


- (void)nextButton:(id)sender
{
    if (activeField == outerDiameterMinInput) {
        [outerDiameterMaxInput becomeFirstResponder];
    } else if (activeField == outerDiameterMaxInput) {
        [boreDiameterMinInput becomeFirstResponder];
    } else if (activeField == boreDiameterMinInput) {
        [boreDiameterMaxInput becomeFirstResponder];
    } else if (activeField == boreDiameterMaxInput) {
        [outerDiameterMinInput becomeFirstResponder];
    }
    [self moveKeyboard:260+44];
}

- (void)prevButton:(id)sender
{    
    if (activeField == outerDiameterMinInput) {
        [boreDiameterMaxInput becomeFirstResponder];
    } else if (activeField == boreDiameterMaxInput) {
        [boreDiameterMinInput becomeFirstResponder];
    } else if (activeField == boreDiameterMinInput) {
        [outerDiameterMaxInput becomeFirstResponder];
    } else if (activeField == outerDiameterMaxInput) {
        [outerDiameterMinInput becomeFirstResponder];
    }
    [self moveKeyboard:260+44];
}


- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    

}

@end
