//
//  FilterBearingsViewController_iPhone.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/22/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterBearingsViewController.h"

@interface 	FilterBearingsViewController_iPhone : FilterBearingsViewController  

-(IBAction)segmentedControlIndexChanged:(id)sender;

@end
