//
//  FilterBearingsViewController_iPhone.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/22/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "FilterBearingsViewController_iPhone.h"
#import "FilterBearingsByDesignationViewController_iPhone.h"
#import "FilterBearingsByDimensionsViewController_iPhone.h"
#import "FilterBearingsViewController.h"
#import "fiveinoneAppDelegate_iPhone.h"
#import "TitleManager.h"

@implementation FilterBearingsViewController_iPhone




- (void)cancelFilterBearings:(id)sender {
//    [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) viewWillAppear:(BOOL)animated
{
    
        CGSize result = [[UIScreen mainScreen] bounds].size;
    
        if(result.height > 500)
        {
            CGRect newFrame = self.view.frame;
            newFrame.size.height = 500;
            [self.viewContainer setFrame:newFrame];
        }
        //filterBearingsButton.frame = CGRectMake(160 , 365, 380, 37);
    
    //self.view.backgroundColor = [UIColor redColor];
    
    /////////// added for the greyout of the tab bar when pressing back from i/p param page
    
    [self dataManagerReturnedTranslation:YES];
    
    for(int i =  0 ; i < segmentedControl.subviews.count ; i++)
    {
        [[self.segmentedControl.subviews objectAtIndex:i] setTintColor:nil];
        [[self.segmentedControl.subviews objectAtIndex:i] setTintColor:[UIColor colorWithHexString:@"156bc0"]];
    }
   
    ////////////////
    
    
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [currentViewController viewWillAppear:animated];  
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [currentViewController viewWillDisappear:animated];  
    [super viewWillDisappear:animated];
}


-(void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [TitleManager getSKFLogo];
    
    FilterBearingsByDesignationViewController_iPhone *filterBearingsByDesignationViewController = [[FilterBearingsByDesignationViewController_iPhone alloc] initWithNibName:@"FilterBearingsByDesignationViewController" bundle:[NSBundle mainBundle]];
    
    filterBearingsByDesignationViewController.segmentedToolBar = segmentedToolBar;
    //filterBearingsByDesignationViewController.filterBearingsNavigationController = filterBearingsNavigationController;
    filterBearingsByDesignationViewController.parentViewContainer = viewContainer;
    //filterBearingsByDesignationViewController.parentNavigationController = parentNavigationController;
    
    currentViewController = filterBearingsByDesignationViewController;
    
    [self.viewContainer addSubview:filterBearingsByDesignationViewController.view];
    
}

-(IBAction)segmentedControlIndexChanged:(id)sender
{
    
    switch (self.segmentedControl.selectedSegmentIndex) {      
        case 0:
        {
            FilterBearingsByDesignationViewController_iPhone *filterBearingsByDesignationViewController = [[FilterBearingsByDesignationViewController_iPhone alloc] initWithNibName:@"FilterBearingsByDesignationViewController" bundle:[NSBundle mainBundle]];
            
            filterBearingsByDesignationViewController.segmentedToolBar = segmentedToolBar;
            filterBearingsByDesignationViewController.parentViewContainer = viewContainer;

            //filterBearingsByDesignationViewController.parentNavigationController = parentNavigationController;
            
            [currentViewController.view removeFromSuperview];
            

            currentViewController = filterBearingsByDesignationViewController;
            
            [self.viewContainer addSubview:filterBearingsByDesignationViewController.view];
 
            break; 
        }     
        case 1:
        {

            FilterBearingsByDimensionsViewController_iPhone *filterBearingsByDimensionsViewController = [[FilterBearingsByDimensionsViewController_iPhone alloc] initWithNibName:@"FilterBearingsByDimensionsViewController" bundle:[NSBundle mainBundle]];
            
            //filterBearingsByDimensionsViewController.filterBearingsNavigationController = filterBearingsNavigationController;
            //filterBearingsByDimensionsViewController.parentNavigationController = parentNavigationController;
            
            [currentViewController.view removeFromSuperview];
            

            currentViewController = filterBearingsByDimensionsViewController;

            filterBearingsByDimensionsViewController.parentNavigationController = self.navigationController;
           

            [self.viewContainer addSubview:filterBearingsByDimensionsViewController.view];

            break; 
        }
        default:
            break;
    }
}


 
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [currentViewController viewDidAppear:animated];
}

@end
