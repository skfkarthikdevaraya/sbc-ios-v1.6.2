//
//  HyperlinkViewController.m
//  fiveinone
//
//  Created by Admin on 09/07/13.
//  Copyright (c) 2013 HiQ. All rights reserved.
//

#import "HyperlinkViewController.h"

@interface HyperlinkViewController ()

@end

@implementation HyperlinkViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (floor(NSFoundationVersionNumber)> NSFoundationVersionNumber_iOS_6_1) {
        
        //iOS 7 - Navigation bg & text color change in option screen
        NSString *navigationBarTintColor = NAVIGATION_BACKGROUND_COLOR;
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:navigationBarTintColor];
        self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
        self.navigationController.navigationBar.translucent = NO;
        [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    }
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
