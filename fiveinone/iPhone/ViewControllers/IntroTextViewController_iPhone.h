//
//  IntroTextViewController_iPhone.h
//  fiveinone
//
//  Created by Admin on 22/11/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IntroTextViewController.h"

@interface IntroTextViewController_iPhone : IntroTextViewController  {
    
    UIButton *checkbox;
    BOOL checkboxSelected;
    BOOL showingAlert;
}
@property (nonatomic, retain) IBOutlet UIButton *checkbox;
@property (nonatomic, assign) BOOL showingAlert;
@property (nonatomic, assign) BOOL connected;
@property (nonatomic, assign) BOOL checkboxSelected;

-(IBAction)buttonNextClicked:(id)sender;
-(IBAction)ButtonCheckboxSelected:(id)sender;

@end
