//
//  IntroTextViewController_iPhone.m
//  fiveinone
//
//  Created by Admin on 22/11/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import "IntroTextViewController_iPhone.h"
#import "SelectCalculationViewController.h"
#import "CalculationDataManager.h"
#import "MBProgressHUD.h"
#import "MainViewController_iPad.h"

@implementation IntroTextViewController_iPhone

@synthesize checkbox;
@synthesize checkboxSelected;
@synthesize connected;
@synthesize showingAlert;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setSearchingModeStart];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:RKReachabilityDidChangeNotification
                                               object:nil];
    showingAlert = false;
    connected = true;
}


- (void)reachabilityChanged:(NSNotification*)notification {
    RKReachabilityObserver* observer = (RKReachabilityObserver*)[notification object];
    
    if (![observer isNetworkReachable]) {
        connected = false;
        [self showingAlert];
        DLog(@"Disconnected");
    }
    if ([observer isNetworkReachable]) {
        connected = true;
        DLog(@"Connected");
    }
}


-(void)ButtonCheckboxSelected:(id)sender {
    
    [checkbox setBackgroundImage:[UIImage imageNamed:@"checked2.png"] forState:UIControlStateSelected];
    [checkbox setBackgroundImage:[UIImage imageNamed:@"checked2.png"] forState:UIControlStateHighlighted];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults boolForKey:@"DONOTSHOW"] == NO) {
        [userDefaults setBool:YES forKey:@"DONOTSHOW"];
    } else {
        [userDefaults setBool:NO forKey:@"DONOTSHOW"];
    }
    
    [userDefaults synchronize];
    checkboxSelected = !checkboxSelected;
    [checkbox setSelected:checkboxSelected];
}


-(IBAction)buttonNextClicked:(id)sender {

    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    if (!connected) {
        [calculationDataManager showAlertForNetworkConnection];
        return;
    }
    [self dismissViewControllerAnimated:(YES) completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.preferredContentSize = CGSizeMake(320, 480);
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}


- (void)dealloc {
    [super dealloc];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)setSearchingModeStart {
    
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    HUD = [[MBProgressHUD alloc] initWithFrame:CGRectMake(35, 100, 250, 250)];
    HUD.labelText = [calculationDataManager getTranslationString:@"Loading"];
    HUD.graceTime = 0.5f;
    HUD.taskInProgress = YES;
    HUD.allowsCancelation = YES;
    HUD.delegate = self;
    HUD.labelFont = [UIFont fontWithName:CHEVIN_BOLD size:20.f];
    [self.view addSubview:HUD];
    [HUD show:YES];
}

@end
