//
//  LegalInformation.h
//  DialSet
//
//  Created by Martin Lannsjö on 9/16/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LegalInformation : UIViewController{

    UIButton* btnAgree;
    UITextView* legalText;
    UIToolbar* toolBar;
    UISegmentedControl* segmentedControl;
    NSArray* legalHeaders;
}


@property (nonatomic,retain) IBOutlet UIButton *btnAgree;
@property (nonatomic,retain) IBOutlet UITextView *legalText;
@property (nonatomic,retain) IBOutlet UIToolbar *toolBar;

@property (nonatomic, assign) BOOL withoutAgree;


- (void) pickOne:(id)sender;
- (IBAction)buttonAgreeClicked:(id)sender;

@end
