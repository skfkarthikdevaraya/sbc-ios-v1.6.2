//
//  LegalInformation.m
//  DialSet
//
//  Created by Martin Lannsjö on 9/16/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "LegalInformation.h"
#import "SettingsView.h"
#import <QuartzCore/QuartzCore.h>
#import "CalculationDataManager.h"

@implementation LegalInformation
@synthesize btnAgree, legalText, toolBar;


-(void)dealloc{
    [btnAgree release];
    [legalText release];
    [legalHeaders release];
    [toolBar release];
	[super dealloc];
}

- (IBAction)buttonAgreeClicked:(id)sender
{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setValue:@"NO" forKey:@"firstrun"];
    [prefs synchronize];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
    [self.navigationController popViewControllerAnimated:YES];
    [UIView commitAnimations];
    
}

- (void) pickOne:(id)sender{
    UISegmentedControl *segmentedControlr = (UISegmentedControl *)sender;
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    switch ([segmentedControlr selectedSegmentIndex]) {
        case 0:
            legalText.text = [calculationDataManager getTranslationString:@"LEGAL.OWNERSHIP_TEXT"]; 
            break;
        case 1:
            legalText.text = [calculationDataManager getTranslationString:@"LEGAL.PRIVACY_TEXT"]; 
            break;
        case 2:
            legalText.text = [calculationDataManager getTranslationString:@"LEGAL.TERMS_TEXT"]; 
            break;
        default:
            break;
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)changeUISegmentFont:(UIView*) aView selectedText:(NSString*)text
{
	NSString* typeName = NSStringFromClass ([aView class]);
    
	if ([typeName compare:@"UISegmentLabel" options:NSLiteralSearch] == NSOrderedSame) 
    {
		UILabel* label = (UILabel*)aView;
        if ([label.text isEqualToString:text])
        {
            label.textColor = [UIColor colorWithHexString:TEXT_COLOR_WHITE];
            label.shadowColor = [UIColor colorWithWhite:1.0 alpha:0];
        }
        else
        {
            label.textColor = [UIColor colorWithHexString:TEXT_COLOR_WHITE];
            label.shadowColor = [UIColor colorWithWhite:1.0 alpha:0];
        }
        
		[label setTextAlignment:UITextAlignmentCenter];
		[label setFont:[UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_SEGMENTED_CONTROL]];
        
	}
    
	NSArray* subs = [aView subviews];
	NSEnumerator* iter = [subs objectEnumerator];
	UIView* subView;
    
	while (subView = [iter nextObject]) 
    {
		[self changeUISegmentFont:subView selectedText:text];
	}
}

- (void)didChangeSegmentControl:(UISegmentedControl *)control {
        
    NSUInteger i = control.selectedSegmentIndex;
    NSString* selectedText = [legalHeaders objectAtIndex:i];
    [self changeUISegmentFont:control selectedText:selectedText];
}


- (void)initializeSegmentedControl {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    legalHeaders= [[NSArray alloc] 
                            initWithObjects:
                            [calculationDataManager getTranslationString:@"Terms_&_Conditions"],
                            [calculationDataManager getTranslationString:@"Privacy_Policy"], 
                           [calculationDataManager getTranslationString:@"Ownership"], nil];
    
	segmentedControl = [[UISegmentedControl alloc] initWithItems:legalHeaders];
    segmentedControl.frame = CGRectMake(10, 6, 300, 30);
	segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    [segmentedControl addTarget:self action:@selector(didChangeSegmentControl:) forControlEvents:UIControlEventValueChanged];

    
    [segmentedControl setWidth:130 forSegmentAtIndex:0];
    [segmentedControl setWidth:90 forSegmentAtIndex:1];
    [segmentedControl setWidth:80 forSegmentAtIndex:2];
    
    [segmentedControl addTarget:self
	                     action:@selector(pickOne:)
	           forControlEvents:UIControlEventValueChanged];
    
    toolBar.frame = CGRectMake(0, -2, 320, 40);
    
    segmentedControl.selectedSegmentIndex = 0;
    
    segmentedControl.tintColor =  [UIColor colorWithHexString:SEGMENTED_CONTROL_TINT];
    
    
    legalText.text = [calculationDataManager getTranslationString:@"LEGAL.OWNERSHIP_TEXT"];
    
    [self changeUISegmentFont:segmentedControl selectedText:[legalHeaders objectAtIndex:0]];
    
    [toolBar addSubview:segmentedControl];
    [segmentedControl release];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   
    //DialSetAppDelegate *appDelegate = (DialSetAppDelegate*)[[UIApplication sharedApplication] delegate];
    if (true)
    {
        self.navigationItem.hidesBackButton = YES;
    }
    else
    {
        btnAgree.hidden = YES;
        // Make frame a bit higher
        CGRect rect = legalText.frame;
        rect.size.height = rect.size.height + 55;
        legalText.frame = rect;
    }
    

}

- (void)gotTranslation:(id)sender
{
    [self initializeSegmentedControl];
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [self initializeSegmentedControl];

    UIColor *background = [[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"main_background_grey.png"]] autorelease];
    self.view.backgroundColor = background;
    
    NSString* title = NSLocalizedString(@"SETTINGS.LEGAL", nil);
    //self.navigationItem.titleView = [TitleManager getTitle:title]; 
             
    //self.navigationItem.rightBarButtonItem = [TitleManager getSKFLogo];
    
    btnAgree.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
    
    legalText.layer.cornerRadius = 8;
    legalText.layer.masksToBounds = YES;
    legalText.opaque = NO;
    legalText.font = [UIFont fontWithName:CHEVIN_MEDIUM size:14.0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gotTranslation:)
                                                 name:@"kGotTranslation"
                                               object:nil];
    

}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
