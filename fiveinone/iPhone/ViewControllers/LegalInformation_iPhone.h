//
//  LegalInformation.h
//  DialSet
//
//  Created by Martin Lannsjö on 9/16/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LegalInformation.h"

@interface LegalInformation_iPhone : LegalInformation {
    BOOL _fromSettingsView;
}

@property (nonatomic, readonly) BOOL _fromSettingsView;
@property (nonatomic, retain) IBOutlet UIView *lineView_legal;

- (IBAction)buttonAgreeClicked:(id)sender;

@end
