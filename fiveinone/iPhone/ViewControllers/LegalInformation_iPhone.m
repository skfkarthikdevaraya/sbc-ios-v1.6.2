//
//  LegalInformation.m
//  DialSet
//
//  Created by Martin Lannsjö on 9/16/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "LegalInformation_iPhone.h"

@implementation LegalInformation_iPhone

@synthesize _fromSettingsView;


- (IBAction)buttonAgreeClicked:(id)sender
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setValue:@"NO" forKey:@"firstrun"];
    [prefs synchronize];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
    [self.navigationController popViewControllerAnimated:YES];
    [UIView commitAnimations];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.preferredContentSize = CGSizeMake(320, 480);
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if([[prefs objectForKey:@"firstrun"] isEqualToString:@"NO"]) {
        [self.lineView_legal setHidden:YES];
    }
    
    if (_fromSettingsView){
            [self.navigationController setNavigationBarHidden:YES animated:animated];
            _fromSettingsView = FALSE;
    }
}

- (void)dealloc {
    [_lineView_legal release];
    [super dealloc];
}
@end
