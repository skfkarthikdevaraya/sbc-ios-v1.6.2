//
//  OptionSelectViewController.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/19/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "OptionSelectViewController_iPhone.h"
#import "Family.h"
#import "Option.h"
#import "TitleManager.h"
#import "ParameterMissing.h"

@implementation OptionSelectViewController_iPhone
    bool  lubClean_grease = false;
    bool  lubClean_parameter = false;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _calculationControl = [[CalculationControl alloc] init];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [TitleManager getSKFLogo];
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    Family *family = [optionsArray objectAtIndex:familyNr];
    Option *option = [family.options objectAtIndex:indexPath.row];
    
    
    // To check Lubrication cleanness parameter
    if ([parameterName isEqualToString:@"oil_system"]) {
        lubClean_parameter = true;
    }
    
    
   // To check relubrication calculation
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    bool relub_exists = false;
    for (Calculation *cal in calculationDataManager.selectedCalculationsArray){
        if ([cal.calculationId isEqualToString:@"14"]) {
            relub_exists = true;
            break;
        }
        
        
        
        if ([cal.calculationId isEqualToString:@"5"] && (!calculationDataManager.mutlipleCalculation)) {
            relub_exists = false;
            lubClean_parameter = false;
            break;
        }
    }

    
   
    
    
    NSLog(lubClean_parameter ? @"Yes" : @"No");
    NSLog(relub_exists ? @"Yes" : @"No");
    NSLog(calculationDataManager.mutlipleCalculation ? @"Yes" : @"No");
    NSLog(calculationDataManager.isRelubricationCalcExist ? @"Yes" : @"No");

    
    // To check Lubrication cleaniness value
        if ( [family.familyId isEqualToString:@"2"] && [family.name isEqualToString:@"Grease lubrication"]) {
            lubClean_grease = true;
        }else if(([family.familyId isEqualToString:@"0"] && [family.name isEqualToString:@"Circulation oil with on-line filter"]) ||
                 ([family.familyId isEqualToString:@"1"] && [family.name isEqualToString:@"Oil lubricant without filter or off-line filter"]) ){
            lubClean_grease = false;
        }
    NSLog(lubClean_grease ? @"Yes" : @"No");

    
    // To show alert for non supported lubricant
    if ((!calculationDataManager.isRelubricationCalcExist) && (lubClean_grease) && (calculationDataManager.mutlipleCalculation)) {
        if (([family.name isEqualToString:@"Lubrication"]) && (![option.optionId isEqualToString:@"0"])) {
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
            return;
        }
    }
    
    if ((!calculationDataManager.isRelubricationCalcExist) && (!lubClean_grease) && (lubClean_parameter) && (calculationDataManager.mutlipleCalculation)) {
        if (([family.name isEqualToString:@"Lubrication"]) && ([option.optionId isEqualToString:@"0"])) {
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
            return;
        }
    }
    
    
    if ((calculationDataManager.isRelubricationCalcExist) && ([family.name isEqualToString:@"Lubrication"]) && (![option.optionId isEqualToString:@"0"])) {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        return;
    }else   {
    
    [self.delegate selectedOption:option.presentation withIndex:indexPath.row andName:parameterName andIndexPath:parentIndexPath];
   // [tableView deselectRowAtIndexPath:indexPath animated:YES];
    /*** iOS 7 - Commented this code to avoid crash issues after returning from the selecting option ***/
    //[self.navigationController popViewControllerAnimated:YES];
        
    }
}

#pragma mark AlertView Delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag ==1) {
        return;
    }
}


@end
