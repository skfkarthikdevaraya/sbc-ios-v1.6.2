//
//  ReportsViewController.h
//  fiveinone
//
//  Created by Admin on 27/11/13.
//  Copyright (c) 2013 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuickLook/QuickLook.h>
#import "DirectoryWatcher.h"

@interface ReportsViewController : UITableViewController<QLPreviewControllerDataSource, DirectoryWatcherDelegate,QLPreviewControllerDelegate,UIDocumentInteractionControllerDelegate>
{

    UIViewController    *baseViewController;

}



@property (nonatomic,retain) UIViewController    *baseViewController;


@end