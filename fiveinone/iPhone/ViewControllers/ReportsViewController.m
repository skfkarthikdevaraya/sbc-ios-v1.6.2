//
//  ReportsViewController.m
//  fiveinone
//
//  Created by Admin on 27/11/13.
//  Copyright (c) 2013 HiQ. All rights reserved.
//

#import "ReportsViewController.h"
#import "TitleManager.h"
#import "CalculationDataManager.h"
#import "MainViewController_iPad.h"

#define kRowHeight 58.0f

@interface ReportsViewController ()

@property (nonatomic, strong) DirectoryWatcher *docWatcher;
@property (nonatomic, strong) NSMutableArray *documentURLs;
@property (nonatomic, strong) UIDocumentInteractionController *docInteractionController;



@end



@implementation ReportsViewController


@synthesize baseViewController;
@synthesize docWatcher;
@synthesize docInteractionController;
@synthesize documentURLs;



- (void)setupDocumentControllerWithURL:(NSURL *)url

{
    
    if (self.docInteractionController == nil)
        
    {
        
        self.docInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        
        self.docInteractionController.delegate = self;
        
    }
    
    else
        
    {
        
        self.docInteractionController.URL = url;
        
    }
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
     //self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // start monitoring the document directory…
    
    //self.baseViewController = nil;

    self.navigationItem.rightBarButtonItem = [TitleManager getSKFLogo];

    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];

    self.title = [calculationDataManager getTranslationString:@"Reports"];

    NSString *skfBlueColor = NAVIGATION_BACKGROUND_COLOR;
    [[UINavigationBar appearanceWhenContainedIn:[QLPreviewController class], nil] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearanceWhenContainedIn:[QLPreviewController class], nil] setBarTintColor:[UIColor colorWithHexString:skfBlueColor]];

    
    /*if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        
    }else{
        
        [[UINavigationBar appearanceWhenContainedIn:[QLPreviewController class], nil] setTintColor:[UIColor whiteColor]];
        
        [[UINavigationBar appearanceWhenContainedIn:[QLPreviewController class], nil] setBarTintColor:[UIColor colorWithHexString:skfBlueColor]];
        NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON2], NSFontAttributeName,
                                                   [UIColor whiteColor],NSForegroundColorAttributeName,
                                                   //[UIColor whiteColor], UITextAttributeTextShadowColor,
                                                   [NSValue valueWithUIOffset:UIOffsetMake(-1, 0)], NSShadowAttributeName, nil];
        [[UINavigationBar appearanceWhenContainedIn:[QLPreviewController class], nil] setTitleTextAttributes:navbarTitleTextAttributes];
    }*/


    
    self.docWatcher = [DirectoryWatcher watchFolderWithPath:[self applicationDocumentsDirectory] delegate:self];
    
    self.documentURLs = [NSMutableArray array];
    
    // scan for existing documents
    
    [self directoryDidChange:docWatcher];
    
}

-(void) viewDidUnload
{
    self.documentURLs = nil;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        // Return the number of sections.
    return 1;

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    
    // To reverse the tableview odrer
    NSURL *fileURL = [self.documentURLs objectAtIndex:indexPath.row];
    [self setupDocumentControllerWithURL:fileURL];
    
        

    /*NSArray* reversed = [[self.documentURLs reverseObjectEnumerator] allObjects];
        NSURL *fileURL = [reversed objectAtIndex:indexPath.row];
    [self setupDocumentControllerWithURL:fileURL];*/

    
    

    cell.textLabel.text = [[fileURL path] lastPathComponent];
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    NSString *skfbluecolor = SKF_BLUE_COLOR;
    cell.textLabel.textColor = [UIColor colorWithHexString:skfbluecolor];
    
    
    
    
    

    NSInteger iconCount = [self.docInteractionController.icons count];
    
    if (iconCount > 0) {
        
        cell.imageView.image = [self.docInteractionController.icons objectAtIndex:iconCount - 1];
    }
    
    NSString *fileURLString = [self.docInteractionController.URL path];
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:fileURLString error:nil];
    NSInteger fileSize = [[fileAttributes objectForKey:NSFileSize] intValue];
    NSString *fileSizeStr = [NSByteCountFormatter stringFromByteCount:fileSize countStyle:NSByteCountFormatterCountStyleFile];
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@", fileSizeStr, self.docInteractionController.UTI];
    cell.detailTextLabel.textColor = [UIColor grayColor];

   
    return cell;
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    return kRowHeight;
    
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source

       /* NSString *folderPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:indexPath.row];
        NSError *error = nil;
        for (NSString *file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:&error]) {
            [[NSFileManager defaultManager] removeItemAtPath:[folderPath stringByAppendingPathComponent:file] error:&error];
        }
        */
        
        NSURL *fileURL = [self.documentURLs objectAtIndex:indexPath.row];
        NSError *error = nil;
        
        
        [[NSFileManager defaultManager] removeItemAtURL:fileURL error:&error];
        
        [self.documentURLs removeObjectAtIndex:indexPath.row];
        
        
        
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [tableView reloadData];
    }
    /*else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    } */
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSURL *fileURL = [self.documentURLs objectAtIndex:indexPath.row];
    [self setupDocumentControllerWithURL:fileURL];
    [self.docInteractionController presentPreviewAnimated:YES];
    
    
    /*QLPreviewController *previewController = [[QLPreviewController alloc] init];
    previewController.dataSource = self;
    previewController.delegate = self;
    
     
    // start previewing the document at the current section index
    
    previewController.currentPreviewItemIndex = indexPath.row;
    
    if(baseViewController)
    {
        [self presentModalViewController:previewController animated:YES];
    }
    else
    {
        [[self navigationController] pushViewController:previewController animated:YES];
    }*/
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return self.documentURLs.count;
}







#pragma mark - UIDocumentInteractionControllerDelegate



- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)interactionController

{
    
    //if(baseViewController != nil)
        //return  baseViewController;
    
    return self;

    
}


- (void) documentInteractionControllerWillBeginPreview: (UIDocumentInteractionController *) controller
{
    if(baseViewController==nil){
        return;
    }
    
    if ([baseViewController isKindOfClass:[MainViewController_iPad class]]) {
        
        MainViewController_iPad *iPadController = (MainViewController_iPad*) baseViewController;
        
        
        if (floor(NSFoundationVersionNumber)<= NSFoundationVersionNumber_iOS_7_1) {
            
            [iPadController dismissReportViewPopover];
        }else {
            
            [iPadController presentationController];
        }

        

    }

}

- (void)documentInteractionControllerDidEndPreview:(UIDocumentInteractionController *)controller
{
   
}


#pragma mark - QLPreviewControllerDelegate
- (void)previewControllerWillDismiss:(QLPreviewController *)controller
{
    [self.tableView reloadData];
}

- (void)previewControllerDidDismiss:(QLPreviewController *)controller

{
    
    // if the preview dismissed (done button touched), use this method to post-process previews
    
    //[self.tableView reloadData];
    
    
    
}


#pragma mark - QLPreviewControllerDataSource



// Returns the number of items that the preview controller should preview

- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)previewController

{
    
    return self.documentURLs.count;;
    
}


// returns the item that the preview controller should preview

- (id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx

{
    
    NSURL *fileURL = [self.documentURLs objectAtIndex:idx];
    
    return fileURL;
    
}



#pragma mark - File system support

- (NSString *)applicationDocumentsDirectory {
    
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}



- (void)directoryDidChange:(DirectoryWatcher *)folderWatcher
{
    [self.documentURLs removeAllObjects];    // clear out the old docs and start over
    
    NSString *documentsDirectoryPath = [self applicationDocumentsDirectory];
    NSArray *documentsDirectoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectoryPath error:NULL];
    
    for (NSString* curFileName in [documentsDirectoryContents objectEnumerator]){
        
        NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:curFileName];
        
        NSURL *fileURL = [NSURL fileURLWithPath:filePath];

        BOOL isDirectory;
        
        [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
        
        // proceed to add the document URL to our list (ignore the "Inbox" folder)
        
        if (!(isDirectory && [curFileName isEqualToString:@"Inbox"])) {
            
            [self.documentURLs  addObject  :fileURL];
            
        }
        
    }
    
    
    NSMutableArray *reversedArray = [NSMutableArray arrayWithCapacity:self.documentURLs.count];
    for (id object in self.documentURLs.reverseObjectEnumerator)
    {
        [reversedArray addObject:object];
    }
    self.documentURLs = reversedArray ;
    [self.tableView reloadData];
    
  //  [self.tableView reloadData];
    
}


@end
