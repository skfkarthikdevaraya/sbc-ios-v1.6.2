//
//  results.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/15/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <QuickLook/QuickLook.h>
#import "CalculationDataManager.h"
#import "ReportsViewController.h"



#import "GAITrackedViewController.h"

@interface ResultsViewController : GAITrackedViewController <CalculationDataManagerDelegate,UITableViewDataSource,UITableViewDelegate, UIAlertViewDelegate, MBProgressHUDDelegate, UIWebViewDelegate, QLPreviewControllerDataSource,QLPreviewControllerDelegate, UIDocumentInteractionControllerDelegate> {

//@interface ResultsViewController : UIViewController <CalculationDataManagerDelegate,UITableViewDataSource,UITableViewDelegate, UIAlertViewDelegate, MBProgressHUDDelegate, UIWebViewDelegate, QLPreviewControllerDataSource,QLPreviewControllerDelegate, UIDocumentInteractionControllerDelegate> {

    
    NSDictionary *params;
    UIActivityIndicatorView *activityIndicator;
    NSMutableArray *resultsParameterArray;
    NSMutableArray *parameterAvailableArray;
    NSMutableArray *errorsArray;
    NSMutableArray *warningsArray;
    NSMutableArray *messagesArray;
    NSMutableArray *arrayWithCalcID;
    NSMutableArray *resultCountArray;
    NSMutableArray *resultArray;
    NSMutableArray *parameterDatabaseArray;
    NSArray *commonParams;
    NSString *cssString;
    NSString *cssStringGray;
    UITableView *resultsTable;
    NSString *reportId;
    BOOL showingAlert;
    int calculationsCount;
    NSString *whereInter;
    
    NSString *lastReportFileName;
    ReportsViewController *reportsViewController;
    BOOL hideIntermediate;
   
}

@property (nonatomic, assign) BOOL showingAlert;
@property (nonatomic, assign) BOOL connected;
@property (nonatomic, assign) int calculationsCount;

@property (nonatomic, retain) NSDictionary *params;
@property (nonatomic, retain) NSString *whereInter;
@property (nonatomic, retain) NSString *cssString;
@property (nonatomic, retain) NSString *cssStringGray;
@property (nonatomic, retain) NSString *cssStringRed;
@property (nonatomic, retain) IBOutlet UITableView *resultsTable;
@property (retain, nonatomic) IBOutlet UIButton *homeButton;
@property (retain, nonatomic) IBOutlet UIImageView *topBarImageView;
@property(nonatomic, retain)  NSMutableArray *parameterAvailableArray;
@property (nonatomic, retain) NSMutableArray *resultsParameterArray;
@property (nonatomic, retain) NSMutableArray *parameterDatabaseArray;
@property (nonatomic, retain) NSMutableArray *errorsArray;
@property (nonatomic, retain) NSMutableArray *warningsArray;
@property (nonatomic, retain) NSMutableArray *messagesArray;

@property (retain, nonatomic) NSMutableArray *arrayWithCalcID;
@property (retain, nonatomic) NSArray *commonParams;
@property (retain, nonatomic) NSMutableArray *resultCountArray;
@property (nonatomic, retain) NSMutableArray *resultArray;
@property (nonatomic, retain) MBProgressHUD *HUD;
@property(nonatomic,retain)     NSString *lastReportFileName;
@property(nonatomic,retain)     ReportsViewController *reportsViewController;
@property (nonatomic, assign) BOOL hideIntermediate;


/*** Report Generation **/
//@property (nonatomic, retain) NSURL *url;
@property (nonatomic, retain) NSString *reportId;
@property (nonatomic, retain) IBOutlet UIButton *saveButton;
//@property (nonatomic, retain) IBOutlet UIButton *printButton;
//@property (nonatomic, retain) IBOutlet UIButton *emailButton;
@property (retain, nonatomic) IBOutlet UIButton *reportButton;

-(IBAction)saveReport:(id)sender;
//-(IBAction)PrintReport:(id)sender;
//-(IBAction)sendReportToEmail:(id)sender;
-(IBAction)reportButtonClicked:(id)sender;


/*** ***/


- (void)updateMissingParameters:(NSArray*)nextParameterAvailableArray;
- (void)copyUniqueParameters: (NSArray *)newParameterAvailableArray;
- (void)copyUniqueWarnings: (NSArray *)newWarningsAvailableArray;
- (void)copyUniqueErrors: (NSArray *)newErrorsAvailableArray;
- (IBAction)homeButtonClicked:(id)sender;
- (void) productLinkButtonClicked:(id)sender;
- (void) showHyperlinkView:(NSURL *) url;
- (void)sendNewRequest;
- (void) sendGAdispatch:(NSString *) val;

@end
