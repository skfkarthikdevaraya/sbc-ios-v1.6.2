//
//  results.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/15/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "ResultsViewController.h"
#import "CalculationDataManager.h"
#import "ParameterResult.h"
#import "ParameterDatabase.h"
#import "ParameterAvailable.h"
#import "CalculationResult.h"
#import "Family.h"
#import "Option.h"
#import "Warning.h"
#import "Error.h"
#import "Message.h"
#import "TitleManager.h"
#import "NSString+StripHTML.h"
#import "CalculationControl.h"
#import "ParameterMissing.h"
#import "Product.h"
#import "CalculationControl.h"
#import "MBProgressHUD.h"
#import "HyperlinkViewController.h"
#import "ReportsViewController.h"
#import "Property.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"

static int selectedIndex = 0;
static int count = 0;

@implementation ResultsViewController

#define TBL_SEC_ERRORS 0
#define TBL_SEC_RESULTS 1
#define TBL_SEC_WARNINGS 2
#define TBL_SEC_MESSAGES 3
#define TBL_SEC_INPUT_PARAMETERS 4
#define TBL_SEC_PRODUCT_DATA 5

@synthesize params;
@synthesize cssString;
@synthesize cssStringGray;
@synthesize cssStringRed;
@synthesize resultsTable;
@synthesize homeButton;
@synthesize topBarImageView;
@synthesize arrayWithCalcID;
@synthesize commonParams;
@synthesize resultCountArray;
@synthesize resultArray;
@synthesize parameterAvailableArray;
@synthesize HUD;

@synthesize connected;
@synthesize calculationsCount;
@synthesize whereInter;
@synthesize reportId;
@synthesize saveButton;
@synthesize lastReportFileName;
//@synthesize printButton;
//@synthesize emailButton;
//@synthesize url;

@synthesize  resultsParameterArray;
@synthesize  parameterDatabaseArray;
@synthesize  errorsArray;
@synthesize  warningsArray;
@synthesize  messagesArray;
@synthesize hideIntermediate;

bool intermediate_result = false;
bool intermediate_result_composite = false;
int what_plain_brearing = 0;

UILabel *textLabel_intr;
UIView *myview_intr;
UIImageView *imgView_intr;
UIImageView *imgView_tintr;



-(float) calculateHeightOfTextFromWidth:(NSString*)text: (UIFont*)withFont: (float)width :(UILineBreakMode)lineBreakMode
{
    
    NSString *textAdd = [text stringByAppendingString:@"MM"];
    [text retain];
    [withFont retain];
    CGSize suggestedSize = [textAdd sizeWithFont:withFont constrainedToSize:CGSizeMake(width, FLT_MAX) lineBreakMode:lineBreakMode];
    CGSize lineHeightSize = [textAdd sizeWithFont:withFont];
    
    float height = 0;
    if (![text isEqualToString:@""])
    {
        CGFloat rows = suggestedSize.height / lineHeightSize.height;
        height = rows * (lineHeightSize.height + 5);
        height += 5;
    }
    [text release];
    [withFont release];
    
    return height;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"SBC_iPhone - Result";
    
    calculationsCount=0;
        
    [self setSearchingModeStart];
    
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
   
    
    NSString* product_id = calculationDataManager.selectedBearing.designation;
    [self sendGAdispatch: product_id];
    
    
    NSString* bearingtype = calculationDataManager.selectedBearing.typestext;
    [self sendGAdispatch: bearingtype];
    
    
    
    for(int i= 0; i < [calculationDataManager.selectedCalculationsArray count] ; i++)
    {
        NSString *text = [[calculationDataManager.selectedCalculationsArray objectAtIndex:i] presentation];
    [self sendGAdispatch:text];
        
    }
     resultCountArray  = [[NSMutableArray alloc] init];
    resultArray = [[NSMutableArray alloc] init];
    self.navigationItem.rightBarButtonItem = [TitleManager getSKFLogo];
    
    [calculationDataManager addObserver:self];
    [calculationDataManager initTranslation:self];
    
    cssString = @"<html> \n"
    "<head> \n"
    "<style type=\"text/css\"> \n"
    "body {font-family: \"SKF Chevin Medium\"; font-size: 12; margin:4px 0px;background-color:transparent; color:#333333; word-wrap:break-word;}\n"
    "</style> \n"
    "</head> \n";
    
    cssStringGray = @"<html> \n"
    "<head> \n"
    "<style type=\"text/css\"> \n"
    "body {font-family: \"SKF Chevin Medium\"; font-size: 12; margin:4px 0px;background-color:transparent; color:#808080; word-wrap:break-word;}\n"
    "</style> \n"
    "</head> \n";
    
    cssStringRed = @"<html> \n"
    "<head> \n"
    "<style type=\"text/css\"> \n"
    "body {font-family: \"SKF Chevin Medium\"; font-size: 11; margin:4px 0px;background-color:transparent; color:#333333; word-wrap:break-word;}\n"
    "</style> \n"
    "</head> \n";
    
    
    
    //homeButton.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON];
    //homeButton.titleLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    
    topBarImageView.transform = CGAffineTransformMakeRotation(M_PI);
    
    [resultsTable setBackgroundView:nil];
    [resultsTable setBackgroundColor:[UIColor clearColor]];
    [resultsTable setOpaque:NO];
    
    [resultsTable reloadData];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:RKReachabilityDidChangeNotification
                                               object:nil];
    
    showingAlert = false;
    connected = true;
    hideIntermediate = true;
    what_plain_brearing = 0;
    for (Calculation *cal in calculationDataManager.selectedCalculationsArray)
    {
        if ([cal.calculationId isEqualToString:@"17"] ) {
            intermediate_result = true;
            what_plain_brearing = 1;
            break;
        }
        else if([cal.calculationId isEqualToString:@"23"])
        {
            intermediate_result_composite = true;
            what_plain_brearing = 2;
            break;
        }
        else{
            intermediate_result = false;
            intermediate_result_composite = false;
            what_plain_brearing = 0;
        }
   }
  //  NSLog(@" .....%d ... %d ",intermediate_result , intermediate_result_composite);
    
    [self sendNewRequest];
}


- (void)reachabilityChanged:(NSNotification*)notification {
    RKReachabilityObserver* observer = (RKReachabilityObserver*)[notification object];
    
    if (![observer isNetworkReachable])
    {
        [self setSearchingModeEnd];
        connected = false;
        [self showingAlert];
        DLog(@"Disconnected");
    }
    if ([observer isNetworkReachable]) {
        connected = true;
        DLog(@"Connected");
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    for (id key in params)
    {
        ALog(@"key: %@, value: %@", key, [params objectForKey:key]);
    }
    
    if(what_plain_brearing == 1)
        intermediate_result = true;
    else if (what_plain_brearing == 2)
        intermediate_result_composite = true;
    
    [resultsTable reloadData];
    //[self sendNewRequest];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
}

- (IBAction)homeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*- (void)copyUniqueParameters: (NSMutableArray *)newParameterAvailableArray {
 
 NSMutableArray *newParamAddedArray = [[NSMutableArray alloc]init];
 bool isAvailable = false;
 
 for (ParameterAvailable *newParam in newParameterAvailableArray) {
 
 isAvailable = false;
 
 for (ParameterAvailable *oldParam in parameterAvailableArray) {
 
 if([newParam.parameterAvailableId isEqualToString:oldParam.parameterAvailableId]) {
 isAvailable = true;
 break;
 }
 
 }
 if(isAvailable == false) {
 
 [newParamAddedArray addObject:newParam];
 }
 }
 
 parameterAvailableArray = [[parameterAvailableArray arrayByAddingObjectsFromArray:newParamAddedArray] mutableCopy];
 [newParamAddedArray release];
 }*/

- (void)copyUniqueParameters: (NSArray *)newParameterAvailableArray {
    
    if(parameterAvailableArray == nil)
    {
        parameterAvailableArray = [newParameterAvailableArray copy];
        return;
    }
    
    
    NSMutableArray *newParamAddedArray = [[NSMutableArray alloc]init];
    bool isAvailable = false;
    
    for (ParameterAvailable *newParam in newParameterAvailableArray) {
        
        isAvailable = false;
        
        for (ParameterAvailable *oldParam in parameterAvailableArray) {
            
            if([newParam.parameterAvailableId isEqualToString:oldParam.parameterAvailableId]) {
                isAvailable = true;
                break;
            }
            
        }
        if(isAvailable == false) {
            
            [newParamAddedArray addObject:newParam];
        }
    }
    
    
    parameterAvailableArray = [[parameterAvailableArray arrayByAddingObjectsFromArray:newParamAddedArray] copy];
    
    
    [newParamAddedArray release];
}


- (void)copyUniqueDatabaseParameters: (NSArray *)newDatabaseParamsArray {
    
    if(parameterDatabaseArray == nil)
    {
        parameterDatabaseArray = [newDatabaseParamsArray copy];
        return;
    }
    
    NSMutableArray *newParamAddedArray = [[NSMutableArray alloc]init];
    bool isAvailable = false;
    
    for (ParameterDatabase *newParam in newDatabaseParamsArray) {
        
        isAvailable = false;
        
        for (ParameterDatabase *oldParam in parameterDatabaseArray) {
            
            if([newParam.parameterDatabaseId isEqualToString:oldParam.parameterDatabaseId]) {
                isAvailable = true;
                break;
            }
            
        }
        if(isAvailable == false) {
            
            [newParamAddedArray addObject:newParam];
        }
    }
    
    parameterDatabaseArray = [[parameterDatabaseArray arrayByAddingObjectsFromArray:newParamAddedArray] copy];
    
    [newParamAddedArray release];
}

/*- (void)copyUniqueWarnings: (NSMutableArray *)newWarningsAvailableArray {
 
 NSMutableArray *newWarningAddedArray = [[NSMutableArray alloc]init];
 bool isAvailable = true;
 
 for (Warning *newWarning in newWarningsAvailableArray) {
 
 isAvailable = false;
 
 for (Warning *oldWarning in warningsArray) {
 
 if([newWarning.description isEqualToString:oldWarning.description]) {
 isAvailable = true;
 break;
 }
 }
 if(isAvailable == false) {
 
 [newWarningAddedArray addObject:newWarning];
 }
 }
 
 warningsArray = [[warningsArray arrayByAddingObjectsFromArray:newWarningAddedArray] mutableCopy];
 [newWarningAddedArray release];
 }*/


- (void)copyUniqueWarnings: (NSArray *)newWarningsAvailableArray {
    
    if(warningsArray == nil)
    {
        warningsArray = [newWarningsAvailableArray copy];
        return;
    }
    
    NSMutableArray *newWarningAddedArray = [[NSMutableArray alloc]init];
    bool isAvailable = true;
    
    for (Warning *newWarning in newWarningsAvailableArray) {
        
        isAvailable = false;
        
        for (Warning *oldWarning in warningsArray) {
            
            if([newWarning.description isEqualToString:oldWarning.description]) {
                isAvailable = true;
                break;
            }
        }
        if(isAvailable == false) {
            
            [newWarningAddedArray addObject:newWarning];
        }
    }
    
    
    warningsArray = [[warningsArray arrayByAddingObjectsFromArray:newWarningAddedArray] copy];
    
    
    [newWarningAddedArray release];
}


/*- (void)copyUniqueErrors: (NSMutableArray *)newErrorsAvailableArray {
 
 NSMutableArray *newErrorAddedArray = [[NSMutableArray alloc]init];
 bool isAvailable = true;
 
 for (Error *newError in newErrorsAvailableArray) {
 
 isAvailable = false;
 
 for (Error *oldError in errorsArray) {
 
 if([newError.description isEqualToString:oldError.description]) {
 isAvailable = true;
 break;
 }
 }
 if(isAvailable == false) {
 
 [newErrorAddedArray addObject:newError];
 }
 }
 
 errorsArray = [[errorsArray arrayByAddingObjectsFromArray:newErrorAddedArray] mutableCopy];
 [newErrorAddedArray release];
 }*/


- (void)copyUniqueErrors: (NSArray *)newErrorsAvailableArray {
    
    if(errorsArray == nil)
    {
        errorsArray = [newErrorsAvailableArray copy];
        return;
    }
    
    
    NSMutableArray *newErrorAddedArray = [[NSMutableArray alloc]init];
    bool isAvailable = true;
    
    for (Error *newError in newErrorsAvailableArray) {
        
        isAvailable = false;
        
        for (Error *oldError in errorsArray) {
            
            if([newError.description isEqualToString:oldError.description]) {
                isAvailable = true;
                break;
            }
        }
        if(isAvailable == false) {
            
            [newErrorAddedArray addObject:newError];
        }
    }
    
    
    errorsArray = [[errorsArray arrayByAddingObjectsFromArray:newErrorAddedArray] copy];
    
    
    [newErrorAddedArray release];
}

- (void)copyUniqueMessages: (NSArray *)newMessagesAvailableArray {
    
    if(messagesArray == nil)
    {
        messagesArray = [newMessagesAvailableArray copy];
        return;
    }
    
    
    NSMutableArray *newMessagesAddedArray = [[NSMutableArray alloc]init];
    bool isAvailable = true;
    
    for (Warning *newWarning in newMessagesAvailableArray) {
        
        isAvailable = false;
        
        for (Warning *oldWarning in messagesArray) {
            
            if([newWarning.description isEqualToString:oldWarning.description]) {
                isAvailable = true;
                break;
            }
        }
        if(isAvailable == false) {
            
            [newMessagesAddedArray addObject:newWarning];
        }
    }
    
    messagesArray = [[messagesArray arrayByAddingObjectsFromArray:newMessagesAddedArray] copy];
    
    
    [newMessagesAddedArray release];
}


/*
 // Modified for MultipleCalc - Result screen - iPhone
 - (void)dataManagerReturnedCalculationDictionary:(NSDictionary *)returnedDictionary
 {
 CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
 if(calculationDataManager.mutlipleCalculation) {
 NSArray *array = [returnedDictionary objectForKey:@"results"];
 if(array != nil && array.count) {
 NSString *text = [[calculationDataManager.selectedCalculationsArray objectAtIndex:calculationsCount] presentation];
 [resultArray addObject:text];
 }
 
 } else {
 
 [resultArray addObject:calculationDataManager.selectedCalculation.presentation];
 }
 
 //count++;
 
 [resultArray addObjectsFromArray:[returnedDictionary objectForKey:@"results"]];
 
 if(calculationsCount == 0)
 {
 resultsParameterArray = [[returnedDictionary objectForKey:@"results"] copy];
 parameterAvailableArray = [[returnedDictionary objectForKey:@"availablesResults"] copy];
 parameterDatabaseArray = [[returnedDictionary objectForKey:@"databaseResults"] copy];
 errorsArray = [[returnedDictionary objectForKey:@"errorResults"] copy];
 warningsArray = [[returnedDictionary objectForKey:@"warningResults"] copy];
 messagesArray = [[returnedDictionary objectForKey:@"messageResults"] copy];
 
 UILabel *resultBearingTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 20)];
 resultBearingTitle.text = [NSString stringWithFormat:@"%@", calculationDataManager.selectedBearing.designation];
 resultBearingTitle.textAlignment = UITextAlignmentRight;
 resultBearingTitle.backgroundColor = [UIColor clearColor];
 resultBearingTitle.font = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_BUTTON_LARGE];
 resultBearingTitle.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
 
 
 [self.view addSubview:resultBearingTitle];
 // [self.view addSubview:resultCalculationTitle];
 
 [resultBearingTitle release];
 
 }
 else
 {
 resultsParameterArray = [[resultsParameterArray arrayByAddingObjectsFromArray:[returnedDictionary objectForKey:@"results"]] mutableCopy];
 
 //messagesArray = [[messagesArray arrayByAddingObjectsFromArray:[returnedDictionary objectForKey:@"messageResults"]]mutableCopy];
 
 [self copyUniqueWarnings:[returnedDictionary objectForKey:@"warningResults"]];
 
 [self copyUniqueErrors:[returnedDictionary objectForKey:@"errorResults"]];
 
 [self copyUniqueParameters:[returnedDictionary objectForKey:@"availablesResults"]];
 }
 
 calculationsCount++;
 int actualCalculationsCount =0;
 if(calculationDataManager.mutlipleCalculation) {
 actualCalculationsCount = calculationDataManager.selectedCalculationsArray.count-calculationDataManager.unsupportedCalculationsArray.count;
 if(calculationsCount < actualCalculationsCount) {
 [self sendNewRequest];
 }
 }
 
 if(!calculationDataManager.mutlipleCalculation || (calculationsCount == actualCalculationsCount)) {
 [resultsTable reloadData];
 calculationsCount=0;
 [self setSearchingModeEnd];
 }
 
 
 for (int i = 0; i < [resultsParameterArray count]; i++) {
 ParameterResult *result = [resultsParameterArray objectAtIndex:i];
 ALog(@"%@ %@",result.name, result.value);
 }
 
 
 
 
 }
 */


// SK - Modified for MultipleCalc - Result screen - iPhone
- (void)dataManagerReturnedCalculationDictionary:(NSDictionary *)returnedDictionary
{
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    //UILabel *resultBearingTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 20)];
    UILabel *resultBearingTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 15, 300, 20)];
    resultBearingTitle.text = [NSString stringWithFormat:@"%@", calculationDataManager.selectedBearing.designation];
    //resultBearingTitle.textAlignment = UITextAlignmentRight;
    
    resultBearingTitle.textAlignment = NSTextAlignmentLeft;
    resultBearingTitle.backgroundColor = [UIColor clearColor];
    resultBearingTitle.font = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_BUTTON_LARGE];
    resultBearingTitle.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    
    [self.view addSubview:resultBearingTitle];
    [resultBearingTitle release];
    
    
    NSArray *calculationResults = [returnedDictionary objectForKey:@"results"];
    if(calculationResults != nil && calculationResults.count) {
        
        for(Calculation *calc in calculationResults)
        {
            reportId = calc.reportId;
            
            [resultArray addObject:calc.name];
            [resultArray addObjectsFromArray:calc.resultParams];
            
            resultsParameterArray = [[resultsParameterArray arrayByAddingObjectsFromArray:calc.resultParams] mutableCopy];
            
            if(calc.warnings)
            {
                [self copyUniqueWarnings:[calc.warnings copy] ];
            }
            
            if(calc.errors)
            {
                [self copyUniqueErrors:[calc.errors copy ]];
            }
            
            if(calc.messages)
            {
                [self copyUniqueMessages:[calc.messages copy]];
            }
            
            if(calc.availableParams)
            {
                [self copyUniqueParameters:[calc.availableParams copy ]];
            }
            
            if(calc.resultDatabaseParams)
            {
                [self copyUniqueDatabaseParameters:[calc.resultDatabaseParams copy ]];
                
            }
            
        }
    }
    
    for(int i = 1 ; i < resultArray.count-1 ; i++)
    {
        ParameterResult *newresult = [resultArray objectAtIndex:i];
    
        if(hideIntermediate && [newresult.description rangeOfString:@"rating life"].location != NSNotFound)
        {
            whereInter = newresult.parameterId;
        }
        
    }
    
    
    [resultsTable reloadData];
    
    [self setSearchingModeEnd];
    
   // [self.view setUserInteractionEnabled:YES];
    
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == TBL_SEC_ERRORS && [errorsArray count] == 0)
        return [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
    if (section == TBL_SEC_WARNINGS && [warningsArray count] == 0)
        return [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
    if (section == TBL_SEC_MESSAGES && [messagesArray count] == 0)
        return [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
    
	// create the parent view that will hold header Label
	UIView* customView = [[[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 44.0)] autorelease];
    
	// create the button object
	UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.opaque = NO;
	headerLabel.textColor = [UIColor colorWithHexString:COLOR_SKF_RED];
	headerLabel.highlightedTextColor = [UIColor colorWithHexString:COLOR_SKF_RED];
	headerLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_GROUPED_TABLE_HEADER];
	headerLabel.frame = CGRectMake(10.0, 0.0, 300.0, 44.0);
    
	// If you want to align the header text as centered
	// headerLabel.frame = CGRectMake(150.0, 0.0, 300.0, 44.0);
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    switch (section) {
        case TBL_SEC_RESULTS:
            //headerLabel.text = [calculationDataManager getTranslationString:@"Results"];
            if (calculationDataManager.mutlipleCalculation == TRUE) {
                
                headerLabel.text = [calculationDataManager getTranslationString:@"Results"];
                
            }else if (calculationDataManager.mutlipleCalculation == FALSE) {
                
                headerLabel.text = @"Result";
            }
            
            break;
        case TBL_SEC_ERRORS:
            headerLabel.text = [calculationDataManager getTranslationString:@"Errors"];
            break;
        case TBL_SEC_WARNINGS:
            headerLabel.text = [calculationDataManager getTranslationString:@"Warnings"];
            break;
        case TBL_SEC_MESSAGES:
            headerLabel.text = [calculationDataManager getTranslationString:@"Messages"];
            break;
        case TBL_SEC_PRODUCT_DATA:
            headerLabel.text = [calculationDataManager getTranslationString:@"Bearing_information"];
            
           // NSString *selectedColor = COLOR_SKF_BLUE;
            
            
            /** Disaplying Product Data Image in Result screen */
            
            [customView setFrame:CGRectMake(10.0, 10.0, 300, 400)];
            
            UIImageView *ProdDataImage_iPhone= [[[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 300, 250)] autorelease];
            
            float scale1 = [UIScreen mainScreen].scale;
            CGSize diaImageViewSize = CGSizeMake(320, 280);
            CGSize diaImageSize = CGSizeMake(scale1 * (diaImageViewSize.width - 1), scale1 * (diaImageViewSize.height - 1));
            
            NSMutableString* diaImagePath =
            [NSMutableString stringWithFormat:@"/Image?bearingType=%@&productId=%@&measurement=%@&withDia=true&width=%d&height=%d",
             calculationDataManager.selectedBearing.type, calculationDataManager.selectedBearing.productId,[calculationDataManager selectedMeasurement],
             (int)diaImageSize.width, (int)diaImageSize.height];
            
            NSMutableString* diaProductImageUrl = [NSMutableString stringWithFormat:@"%@%@", calculationDataManager.baseURL,diaImagePath];
            ProdDataImage_iPhone.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:diaProductImageUrl]]];
            ProdDataImage_iPhone.contentMode = UIViewContentModeScaleAspectFit;
            [customView addSubview:ProdDataImage_iPhone];
            
            /** Displaying image desclaimer text for force action image */
            
            //            UIWebView *imageDesclaimer = [[[UIWebView alloc] initWithFrame:CGRectMake(10, ProdDataImage_iPhone.frame.size.height-20, 300, 40)]autorelease];
            //
            //            [imageDesclaimer loadHTMLString:[NSString stringWithFormat:@"%@%@", cssStringRed, [self fixBR:[calculationDataManager getTranslationString:@"ImageDisclaimer"]]] baseURL:[NSURL URLWithString:@""]];
            //
            //            imageDesclaimer.scrollView.scrollEnabled = NO;
            //            [imageDesclaimer setBackgroundColor:[UIColor clearColor]];
            //            [imageDesclaimer setOpaque:NO];
            //
            //            [customView addSubview:imageDesclaimer];
            //
            //            /** View product details link */
            //
            //            UIButton *productLinkButton = [[[UIButton alloc]initWithFrame:CGRectMake(10, ProdDataImage_iPhone.frame.size.height + imageDesclaimer.frame.size.height-30, 300, 44)]autorelease];
            //
            //            //[productLinkButton setTitle:@"View product details" forState:UIControlStateNormal];
            //
            //            [productLinkButton setTitle:GetLocalizedString(@"View_product_details") forState:UIControlStateNormal];
            //
            //            productLinkButton.titleLabel.textColor = [UIColor colorWithHexString:selectedColor];
            //
            //            productLinkButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            //            productLinkButton.backgroundColor = [UIColor clearColor];
            //            [productLinkButton setTitleColor:[UIColor colorWithHexString:selectedColor] forState:UIControlStateNormal];
            //            [productLinkButton setTitleColor:[UIColor colorWithHexString:selectedColor] forState:(UIControlStateSelected | UIControlStateHighlighted)];
            //            productLinkButton.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:IPAD_FONT_SIZE_FOR_BEARING_VALUES];
            //
            //            [productLinkButton addTarget:self
            //                                     action:@selector(productLinkButtonClicked:)
            //                                    forControlEvents:UIControlEventTouchUpInside];
            //
            //            [customView addSubview:productLinkButton];
            //
            //            if ([calculationDataManager.selectedBearing.explorer isEqualToString:@"true"]) {
            //
            //           UILabel *explorerLabel = [[[UILabel alloc]initWithFrame:CGRectMake(10, ProdDataImage_iPhone.frame.size.height + productLinkButton.frame.size.height+0, 280, 20)]autorelease];
            //            explorerLabel.userInteractionEnabled = false;
            //            explorerLabel.backgroundColor = [UIColor clearColor];
            //            //explorerLabel.text = @"*SKF Explorer bearing ";
            //
            //                explorerLabel.text =GetLocalizedString(@"Explorer_bearing");
            //
            //
            //            explorerLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_SUBTEXT2];
            //            explorerLabel.textAlignment = UITextAlignmentLeft;
            //            explorerLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
            //            [customView addSubview:explorerLabel];
            //
            //            }
            
            break;
        case TBL_SEC_INPUT_PARAMETERS:
            headerLabel.text = [calculationDataManager getTranslationString:@"Input_parameters"];
            break;
        default:
            break;
    }
    
	[customView addSubview:headerLabel];
    [headerLabel release];
    
    return customView;
}

- (void) productLinkButtonClicked:(id)sender {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    NSMutableString *productLinkPath = [NSMutableString stringWithFormat:@"http://www.skf.com/productcatalogue/prodlink.html?prodid=%@&lang=en&imperial=false",calculationDataManager.selectedBearing.productId];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: productLinkPath]];
    
}

-(NSString *)fixBR:(NSString *)inString
{
    return [inString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if( indexPath.row > 0 && indexPath.row < resultArray.count && (intermediate_result || intermediate_result_composite))
    {
         ParameterResult *newresult = [resultArray objectAtIndex:indexPath.row];
         if (hideIntermediate && [newresult.description rangeOfString:@"rating life"].location == NSNotFound && [newresult.description rangeOfString:@"Frictional"].location == NSNotFound)
        {
             return 0.0;
        }

       
//        if([newresult.presentation isEqualToString:@"G<sub>Nmax</sub>"])// &&  [newresult.parameterId isEqualToString:@"193"])
//            return 100.0;
    }
   
       
          
    UIFont *myFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
    
    switch (indexPath.section) {
        case TBL_SEC_RESULTS:
        {
            if (![[[[resultArray objectAtIndex:indexPath.row] class] description] isEqualToString:@"ParameterResult"])
            {
                return 44.0;
                
            }
            
            ParameterResult *parameterResult = [resultArray objectAtIndex:indexPath.row];
            
            UIFont *nameFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
            UIFont *resultFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
            UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
            NSString *nameString;
            
            if (!parameterResult.uom)
            {
                nameString = parameterResult.presentation;
            }
            else
            {
                nameString = [NSString stringWithFormat:@"%@ [%@]", parameterResult.presentation, parameterResult.uom];
            }
            
          float nameHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:nameString] :nameFont :200 :NSLineBreakByWordWrapping];
            float resultHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:parameterResult.value] :resultFont :100 :NSLineBreakByWordWrapping];
            float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:parameterResult.description] :descriptionFont :230 :NSLineBreakByWordWrapping];
            
	    if(intermediate_result || intermediate_result_composite)
        {
            ParameterResult *newresult = [resultArray objectAtIndex:indexPath.row];
            if([newresult.parameterId isEqualToString:whereInter] && intermediate_result)
                return MAX(nameHeight+descriptionHeight+45, resultHeight);
            else if([newresult.parameterId isEqualToString:whereInter] && intermediate_result_composite)
                return MAX(nameHeight+descriptionHeight+45, resultHeight);
            else
                return MAX(nameHeight+descriptionHeight+5, resultHeight);
        }
         //   return MAX(nameHeight+descriptionHeight+35, resultHeight);
	    else
	    return MAX(nameHeight+descriptionHeight+5, resultHeight);
            break;
            
        }
        case TBL_SEC_ERRORS:
        {
            Error *error = [errorsArray objectAtIndex:indexPath.row];
            float height = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:error.description]  :myFont :280 :NSLineBreakByWordWrapping];
            return height+10;
            break;
        }
        case TBL_SEC_WARNINGS:
        {
            Warning *warning = [warningsArray objectAtIndex:indexPath.row];
            float height = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:warning.description] :myFont :280 :NSLineBreakByWordWrapping];
            //return height+10;
            return height+20;
            
            break;
        }
        case TBL_SEC_MESSAGES:
        {
            Message *message = [messagesArray objectAtIndex:indexPath.row];
            float height = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:message.description] :myFont :280 :NSLineBreakByWordWrapping];
            return height+10;
            break;
        }
        case TBL_SEC_PRODUCT_DATA:
        {
            UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
            CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
            Product *product = [calculationDataManager selectedBearing];
            
            NSMutableString *databaseString = [NSMutableString stringWithFormat:@"%@ - ", product.designation];
            
          /*  for (ParameterDatabase *paramDb in parameterDatabaseArray) {
                
                if (paramDb.uom)
                {
                    [databaseString appendString:[NSMutableString stringWithFormat:@"%@:: %@ %@, ", paramDb.presentation, paramDb.value, paramDb.uom]];
                }
                else
                {
                    [databaseString appendString:[NSMutableString stringWithFormat:@"%@: %@, ", paramDb.presentation, paramDb.value]];
                }
            }
            
            if ([databaseString length] != 0)
            {
                [databaseString deleteCharactersInRange:NSMakeRange([databaseString length]-2, 2)];
            }
            
            float height = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:databaseString] :descriptionFont :280 :NSLineBreakByWordWrapping]+10;*/
            
            
            for (Property *property in product.properties) {
                if (property.uom)
                {
                    [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@ %@, ", property.presentation, property.value, property.uom]];
                }
                else
                {
                    [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@, ", property.presentation, property.value]];
                }
            }
            
            
            if ([databaseString length] != 0)
            {
                [databaseString deleteCharactersInRange:NSMakeRange([databaseString length]-2, 2)];
                
            }

            float height = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:databaseString] :descriptionFont :280 :NSLineBreakByWordWrapping]+10;

            
            return height+10;
            break;
            
        }
        case TBL_SEC_INPUT_PARAMETERS:
        {
            UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
            NSMutableString *availableString = [NSMutableString stringWithString:@""];
            
            for (ParameterAvailable *paramAvail in parameterAvailableArray) {
                
                if (![paramAvail.parameterAvailableId isEqualToString:@"-1"])
                {
                    if (paramAvail.uom)
                    {
                        if (paramAvail.families)
                        {
                            NSString *optionName = nil;
                            for (Family *family in paramAvail.families) {
                                for (Option *option in family.options) {
                                    if ([option.optionId isEqualToString:paramAvail.value]) {
                                        optionName = option.presentation;
                                    }
                                }
                            }
                            [availableString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@ %@, ", paramAvail.presentation, optionName, paramAvail.uom]];                        }
                        else
                        {
                            [availableString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@ %@, ", paramAvail.presentation, paramAvail.value, paramAvail.uom]];
                        }
                    }
                    else
                    {
                        if (paramAvail.families)
                        {
                            {
                                NSString *optionName = nil;
                                for (Family *family in paramAvail.families) {
                                    for (Option *option in family.options) {
                                        if ([option.optionId isEqualToString:paramAvail.value]) {
                                            optionName = option.presentation;
                                        }
                                    }
                                }
                                
                                [availableString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@, ", paramAvail.presentation, optionName]];
                            }
                        }
                        else
                        {
                            [availableString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@, ", paramAvail.presentation, paramAvail.value]];
                        }
                    }
                }
            }
            if ([availableString length] != 0)
            {
                [availableString deleteCharactersInRange:NSMakeRange([availableString length]-2, 2)];
            }
            float height = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:availableString] :descriptionFont :280 :NSLineBreakByWordWrapping];
            return height+20;
            break;
        }
        default:
            return 44.0;
            break;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == TBL_SEC_ERRORS && [errorsArray count] == 0)
        return 1.0;
    if (section == TBL_SEC_WARNINGS && [warningsArray count] == 0)
        return 1.0;
    if (section == TBL_SEC_MESSAGES && [messagesArray count] == 0)
        return 1.0;
    if (section == TBL_SEC_PRODUCT_DATA ){
        
        //return 230.0;
        
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        Product *product = [calculationDataManager selectedBearing];
        
        
        //if (([product.type isEqualToString:@"3_1"]) || ([product.type isEqualToString:@"3_3a"]) || ([product.type isEqualToString:@"3_3b"]) || ([product.type isEqualToString:@"3_4"])) {
            
            if([self isPlainBearingType:product]) {

            
            
            //return 60.0;
            return 44.0;
            
        }else {
            
            return 230.0;
        }
        
    }
    else
        return 44.0;
}

//-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 5.0;
//}


-(BOOL) isPlainBearingType: (Product*)product
{
    if (([product.type caseInsensitiveCompare:@"3_3a"]== NSOrderedSame) || ([product.type caseInsensitiveCompare:@"3_3b"]== NSOrderedSame) || [product.type isEqualToString:@"3_1"] || [product.type isEqualToString:@"3_4"])
        
    {
        return YES;
    }
    
    return NO;
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == TBL_SEC_PRODUCT_DATA ){
        
        // return 100.0;
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        Product *product = [calculationDataManager selectedBearing];
        
       // if (([product.type isEqualToString:@"3_1"]) || ([product.type isEqualToString:@"3_3a"]) || ([product.type isEqualToString:@"3_3b"]) || ([product.type isEqualToString:@"3_4"])) {
            
            //return 50.0;
        
        if([self isPlainBearingType:product]) {
            return 44.0;
            
        }else {
            
            return 110.0;
        }
        
        
    }else
        
        
        //return 5.0;
        return 5.0;
}


-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    //return [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
    
    UIView *footerView = [[[UIView alloc]init]autorelease];
    UILabel *explorerLabel;
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    if (section == TBL_SEC_PRODUCT_DATA ){
        
        /*** SKF Explorer text */
        
        UIButton *productLinkButton = [[[UIButton alloc]initWithFrame:CGRectMake(10, 10, 300, 30)]autorelease];
        
        if ([calculationDataManager.selectedBearing.explorer isEqualToString:@"true"]) {
            
            explorerLabel = [[[UILabel alloc]initWithFrame:CGRectMake(10, 10, 280, 20)]autorelease];
            explorerLabel.userInteractionEnabled = false;
            explorerLabel.backgroundColor = [UIColor clearColor];
            //explorerLabel.text = @"*SKF Explorer bearing ";
            explorerLabel.text =GetLocalizedString(@"Explorer_bearing");
            explorerLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_SUBTEXT2];
            explorerLabel.textAlignment = NSTextAlignmentLeft;
            explorerLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
            [footerView addSubview:explorerLabel];
            
            productLinkButton.frame = CGRectMake(10, explorerLabel.frame.size.height+explorerLabel.frame.origin.y, 300, 30);
            
            
        }
        /** View product details link */
        
        
        //        UIButton *productLinkButton = [[[UIButton alloc]initWithFrame:CGRectMake(10, explorerLabel.frame.size.height+explorerLabel.frame.origin.y, 300, 30)]autorelease];
        
        [productLinkButton setTitle:GetLocalizedString(@"View_product_details") forState:UIControlStateNormal];
        NSString *selectedColor = COLOR_SKF_BLUE;
        
        productLinkButton.titleLabel.textColor = [UIColor colorWithHexString:selectedColor];
        
        productLinkButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        productLinkButton.backgroundColor = [UIColor clearColor];
        [productLinkButton setTitleColor:[UIColor colorWithHexString:selectedColor] forState:UIControlStateNormal];
        [productLinkButton setTitleColor:[UIColor colorWithHexString:selectedColor] forState:(UIControlStateSelected | UIControlStateHighlighted)];
        productLinkButton.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:IPAD_FONT_SIZE_FOR_BEARING_VALUES];
        
        [productLinkButton addTarget:self
                              action:@selector(productLinkButtonClicked:)
                    forControlEvents:UIControlEventTouchUpInside];
        
        [footerView addSubview:productLinkButton];
        
        
        /** Image desclaimer text  - Result screen */
        
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        Product *product = [calculationDataManager selectedBearing];
        
        //if ((![product.type isEqualToString:@"3_1"]) && (![product.type isEqualToString:@"3_3a"]) && (![product.type isEqualToString:@"3_3b"]) && (![product.type isEqualToString:@"3_4"])) {
        
        if([self isPlainBearingType:product] == NO) {

        
            UIWebView *imageDesclaimer = [[[UIWebView alloc] initWithFrame:CGRectMake(10, productLinkButton.frame.size.height+productLinkButton.frame.origin.y, 300, 40)]autorelease];
            
            [imageDesclaimer loadHTMLString:[NSString stringWithFormat:@"%@%@", cssStringRed, [self fixBR:[calculationDataManager getTranslationString:@"ImageDisclaimer"]]] baseURL:[NSURL URLWithString:@""]];
            
            imageDesclaimer.scrollView.scrollEnabled = NO;
            [imageDesclaimer setBackgroundColor:[UIColor clearColor]];
            [imageDesclaimer setOpaque:NO];
            
            [footerView addSubview:imageDesclaimer];
            
            footerView.frame = CGRectMake(10, 10, 300, imageDesclaimer.frame.size.height+imageDesclaimer.frame.origin.y);
            
        }
        
        
        [self.resultsTable.tableFooterView addSubview:footerView];
        
        
    } else {
        return [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
    }
    
    return footerView;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section)
    {
        case TBL_SEC_RESULTS:
            return resultArray.count;
            break;
        case TBL_SEC_ERRORS:
            return [errorsArray count];
            break;
        case TBL_SEC_WARNINGS:
            return [warningsArray count];
            break;
        case TBL_SEC_MESSAGES:
            return [messagesArray count];
            break;
        case TBL_SEC_PRODUCT_DATA:
            return 1;
        case TBL_SEC_INPUT_PARAMETERS:
            return 1;
        default:
            break;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier1 = @"Cell1";
    static NSString *CellIdentifier2 = @"Cell2";
    static NSString *CellIdentifier3 = @"Cell3";
    
    UIWebView *nameWebView;
    UIWebView *descriptionLabel;
    UILabel *resultLabel;
    UIWebView *descriptionView;
    UITableViewCell *cell;

    
    if (indexPath.section == TBL_SEC_RESULTS)
    {
        if (![[[[resultArray objectAtIndex:indexPath.row] class] description] isEqualToString:@"ParameterResult"])
        {
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
            if (cell == nil)
            {
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier3] autorelease];
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.text = [resultArray objectAtIndex:indexPath.row];
            cell.textLabel.textColor = [UIColor colorWithHexString:COLOR_SKF_RED];
            cell.textLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:17];

            return cell;
        }
        
        
           cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        if (cell == nil )
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] autorelease];
            
            

            //nameWebView = [[UIWebView alloc] initWithFrame:CGRectMake(20, 5, 200, 80)];
            nameWebView = [[UIWebView alloc] initWithFrame:CGRectMake(20, 5, 200, 60)];
            //nameWebView.userInteractionEnabled = false;
            nameWebView.backgroundColor = [UIColor clearColor];
            nameWebView.opaque = NO;
            nameWebView.dataDetectorTypes = UIDataDetectorTypeLink;
            nameWebView.delegate = self;
            nameWebView.tag = 10;
            [cell addSubview:nameWebView];
            [nameWebView release];
            
            
            descriptionLabel= [[UIWebView alloc] initWithFrame:CGRectMake(20, 28, 230, 12)];
            descriptionLabel.backgroundColor = [UIColor clearColor];
            descriptionLabel.userInteractionEnabled = false;
            descriptionLabel.opaque = NO;
            descriptionLabel.dataDetectorTypes = UIDataDetectorTypeNone;
            descriptionLabel.tag = 20;
            [cell addSubview:descriptionLabel];
            [descriptionLabel release];
            
            resultLabel = [[UILabel alloc] initWithFrame:CGRectMake(200, 7, 100, 32)];
            resultLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
            resultLabel.textAlignment = NSTextAlignmentRight;
            resultLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
            resultLabel.backgroundColor = [UIColor clearColor];
            resultLabel.tag = 100;
            
            [cell addSubview:resultLabel];
            [resultLabel release];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            
        }
        
        CGFloat height = [tableView rectForRowAtIndexPath:indexPath].size.height;
        
        
        if(height == 0) // hidden cells for intermediate
        {
            cell.hidden = YES;
            return cell;
        }
        
        for (UIView* tempView in cell.contentView.subviews) {
            if(tempView.tag == 555)
            {
                [tempView removeFromSuperview];
            }
        }
        
        
 
       
        for (UIView* tempView in cell.subviews) {
            if(tempView.tag == 555)
            {
                [tempView removeFromSuperview];
            }
        }

        if(indexPath.row < resultArray.count && indexPath.row > 0 && what_plain_brearing == 1)
        {
            ParameterResult *newresult = [resultArray objectAtIndex:indexPath.row];
            if([newresult.parameterId isEqualToString:whereInter] )
            {
                int x = 0;
                for (UIView* tempView in cell.subviews) {
                    
                   if(tempView.tag == 555) {
                       break;
                   }
                   x++;
                  //  NSLog(@". x = %d  ... count = %d" , x,cell.subviews.count);
                   if( x == cell.subviews.count) {
                       intermediate_result = true;
                   }
                }
            }
           
            if ( intermediate_result  && [newresult.parameterId isEqualToString:whereInter]) {
                if(textLabel_intr == nil)
                {
                textLabel_intr = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 300, 25)];
                textLabel_intr.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
                textLabel_intr.textAlignment = NSTextAlignmentLeft;
                textLabel_intr.textColor = [UIColor colorWithHexString:COLOR_SKF_RED];
                // textLabel_intr.backgroundColor = [UIColor greenColor];//[UIColor clearColor];
                textLabel_intr.backgroundColor = [UIColor clearColor];
                textLabel_intr.tag = 153;
                }
                textLabel_intr.userInteractionEnabled = YES;
             //   textLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"red_disclosure_indicator.png"]];
               
                
                textLabel_intr.text = @"Intermediate Results";
                if(myview_intr == nil)
                {
                    myview_intr = [[UIView alloc] initWithFrame:CGRectMake(-1, 60, 600, 83)];//(-1, 60, 600, 83)
                    myview_intr.tag = 555;
                    myview_intr.layer.borderWidth = 0.0f;
                  //  myview_intr.backgroundColor = [UIColor blueColor];
                    myview_intr.layer.borderColor = [UIColor whiteColor].CGColor;
                }
                [myview_intr addSubview:textLabel_intr];
                myview_intr.userInteractionEnabled = YES;

                
                
                if(imgView_intr == nil)
                {
                    imgView_intr = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"red_disclosure_indicator.png"]];
                    imgView_intr.frame = CGRectMake(200, 16, 8 , 12);
                }
                
                if(imgView_tintr == nil)
                {
                    imgView_tintr = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"red_disclosure_indicator.png"]];
                    imgView_tintr.frame = CGRectMake(200, 16, 8 , 12);
                    CGAffineTransform transform;
                    transform = CGAffineTransformRotate(imgView_tintr.transform, 33);
                    imgView_tintr.transform = transform;

                }
                
                if(!hideIntermediate)
                {
                    [myview_intr addSubview:imgView_tintr];
                    [imgView_intr removeFromSuperview];
                }
                else
                {
                    [myview_intr addSubview:imgView_intr];
                    [imgView_tintr removeFromSuperview];
                }
                
               // [cell setBackgroundColor:[UIColor blackColor]];
                   [cell.contentView addSubview:myview_intr];
               // intermediate_result = false;
                
                UITapGestureRecognizer *tapgesture  = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                              action:@selector(didtaplabel:)];
                [myview_intr addGestureRecognizer:tapgesture];
                [tapgesture release];
                
               // [cell.contentView bringSubviewToFront:myview_intr];

            }
        }
        else if(indexPath.row < resultArray.count && indexPath.row > 0 && what_plain_brearing == 2)
        {
            ParameterResult *newresult = [resultArray objectAtIndex:indexPath.row];
            
            //first ele after all rating life param
            //Max. basic rating life for regular relubrication
            //Ratio of the axial to the radial load
//            NSLog(@".....paramid... %@", newresult.presentation);
            
            if([newresult.parameterId isEqualToString:whereInter]  )
            {
                 intermediate_result_composite = true;
            }
            
            if ( intermediate_result_composite  && [newresult.parameterId isEqualToString:whereInter] && [newresult.presentation isEqualToString:@"G<sub>h</sub>"])
            {
                 NSLog(@".....%d ... ",indexPath.row);
                
                
                if(myview_intr == nil)
                {
                    myview_intr = [[UIView alloc] initWithFrame:CGRectMake(-1, 60, 600, 83)];
                    myview_intr.tag = 555;
                    myview_intr.layer.borderWidth = 0.5f;
                    myview_intr.layer.borderColor = [UIColor grayColor].CGColor;
                                   }
                
                if(textLabel_intr == nil)
                {
                    textLabel_intr = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 300, 25)];
                    textLabel_intr.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
                    textLabel_intr.textAlignment = NSTextAlignmentLeft;
                    textLabel_intr.textColor = [UIColor colorWithHexString:COLOR_SKF_RED];
                    textLabel_intr.backgroundColor = [UIColor clearColor];
                    textLabel_intr.tag = 155;
                    
                }
                textLabel_intr.userInteractionEnabled = YES;
                textLabel_intr.text = @"Intermediate Results";
                [myview_intr addSubview:textLabel_intr];
                UITapGestureRecognizer *tapgesture  = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                              action:@selector(didtaplabel:)];
                
               // tapgesture.numberOfTapsRequired = 1;
                
                [textLabel_intr addGestureRecognizer:tapgesture];
                
                
                [tapgesture release];
                
                if(imgView_intr == nil)
                {
                    imgView_intr = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"red_disclosure_indicator.png"]];
                    imgView_intr.frame = CGRectMake(200, 16, 8 , 12);
                }
                
                if(imgView_tintr == nil)
                {
                    imgView_tintr = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"red_disclosure_indicator.png"]];
                    imgView_tintr.frame = CGRectMake(200, 16, 8 , 12);
                    CGAffineTransform transform;
                    transform = CGAffineTransformRotate(imgView_tintr.transform, 33);
                    imgView_tintr.transform = transform;
                    
                }
                
                if(!hideIntermediate)
                {
                    [myview_intr addSubview:imgView_tintr];
                    [imgView_intr removeFromSuperview];
                }
                else
                {
                    [myview_intr addSubview:imgView_intr];
                    [imgView_tintr removeFromSuperview];
                }
                
                [cell.contentView addSubview:myview_intr];
                
                intermediate_result_composite = false;
           }
        }
        
        
       
        nameWebView = (UIWebView *)[cell viewWithTag:10];
        descriptionLabel = (UIWebView *)[cell viewWithTag:20];
        resultLabel = (UILabel *)[cell viewWithTag:100];
        // Set up the cell...
        ParameterResult *parameterResult = [resultArray objectAtIndex:indexPath.row];
        

        
        
        NSString *nameString;
        
        if (!parameterResult.uom) {
            nameString = parameterResult.presentation;
            [nameWebView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, parameterResult.presentation] baseURL:[NSURL URLWithString:@""]];
        } else {
            nameString = [NSString stringWithFormat:@"%@ [%@]", parameterResult.presentation, parameterResult.uom];
            [nameWebView loadHTMLString:[NSString stringWithFormat:@"%@%@ [%@]", cssString, parameterResult.presentation, parameterResult.uom] baseURL:[NSURL URLWithString:@""]];
        }
        
        [descriptionLabel loadHTMLString:[NSString stringWithFormat:@"%@%@", cssStringGray, parameterResult.description] baseURL:[NSURL URLWithString:@""]];
        
        NSLog(@"%@", parameterResult.description);
        
        resultLabel.text = parameterResult.value;
        
        UIFont *nameFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
        UIFont *resultFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
        UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
        
        float nameHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:nameString] :nameFont :200 :NSLineBreakByWordWrapping];
        float resultHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:parameterResult.value] :resultFont :100 :NSLineBreakByWordWrapping];
        float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:parameterResult.description] :descriptionFont :230 :NSLineBreakByWordWrapping];
        
        float totalHeight = MAX(nameHeight+descriptionHeight+5, resultHeight);
        
        descriptionLabel.frame = CGRectMake(descriptionLabel.frame.origin.x, totalHeight-descriptionHeight-2, descriptionLabel.frame.size.width, descriptionHeight);
        resultLabel.frame = CGRectMake(resultLabel.frame.origin.x, resultLabel.frame.origin.y, resultLabel.frame.size.width, resultHeight+10);
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            descriptionView = [[UIWebView alloc] initWithFrame:CGRectMake(20, 7, 280, 80)];
            // descriptionView.userInteractionEnabled = false;
            descriptionView.backgroundColor = [UIColor clearColor];
            descriptionView.opaque = NO;
            descriptionView.delegate = self;
            descriptionView.dataDetectorTypes = UIDataDetectorTypeLink;
            descriptionView.tag = 30;
            [cell addSubview:descriptionView];
            [descriptionView release];
        }
        
        descriptionView = (UIWebView *)[cell viewWithTag:30];
        //  ParameterMissing* parameterMissing = [CalculationControl getVisibleParameterAtIndex:indexPath.row];
        
        
        // Set up the cell...
        
        switch (indexPath.section) {
            case TBL_SEC_ERRORS:
            {
                Error *error = [errorsArray objectAtIndex:indexPath.row];
                [descriptionView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, error.description] baseURL:[NSURL URLWithString:@""]];
                
                UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
                float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:error.description] :descriptionFont :280 :NSLineBreakByWordWrapping];
                descriptionView.frame = CGRectMake(descriptionView.frame.origin.x, descriptionView.frame.origin.y, descriptionView.frame.size.width, descriptionHeight+10);
                break;
            }
            case TBL_SEC_WARNINGS:
            {
                Warning *warning = [warningsArray objectAtIndex:indexPath.row];
                [descriptionView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, warning.description] baseURL:[NSURL URLWithString:@""]];
                
                UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
                float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:warning.description] :descriptionFont :280 :NSLineBreakByWordWrapping];
                descriptionView.frame = CGRectMake(descriptionView.frame.origin.x, descriptionView.frame.origin.y, descriptionView.frame.size.width, descriptionHeight+10);
                break;
            }
            case TBL_SEC_MESSAGES:
            {
                Message *message = [messagesArray objectAtIndex:indexPath.row];
                [descriptionView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, message.description] baseURL:[NSURL URLWithString:@""]];
                
                UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:14];
                float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:message.description] :descriptionFont :280 :NSLineBreakByWordWrapping];
                descriptionView.frame = CGRectMake(descriptionView.frame.origin.x, descriptionView.frame.origin.y, descriptionView.frame.size.width, descriptionHeight+10);
                break;
            }
            case TBL_SEC_PRODUCT_DATA:
            {
                
                CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
                Product *product = [calculationDataManager selectedBearing];
                
                NSMutableString *databaseString;
                
                // Check whether selecting bearing is SKF Explorer or not.
                if([product.explorer isEqualToString:@"true"]) {
                    
                    databaseString = [NSMutableString stringWithFormat:@"%@* - ", product.designation];
                    
                }else {
                    
                    databaseString = [NSMutableString stringWithFormat:@"%@ - ", product.designation];
                }
                
                [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@</b>: %@, ", [calculationDataManager getTranslationString:@"Type"], calculationDataManager.selectedBearing.typestext]];
              

                for (Property *property in product.properties) {
                    if (property.uom)
                    {
                        [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@ %@, ", property.presentation, property.value, property.uom]];
                    }
                    else
                    {
                        [databaseString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@, ", property.presentation, property.value]];
                    }
                }

                
 
                if ([databaseString length] != 0)
                {
                    [databaseString deleteCharactersInRange:NSMakeRange([databaseString length]-2, 2)];
                    
                    [descriptionView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, databaseString] baseURL:[NSURL URLWithString:@""]];
                    UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
                    float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:databaseString] :descriptionFont :280 :NSLineBreakByWordWrapping];
                    descriptionView.frame = CGRectMake(descriptionView.frame.origin.x, descriptionView.frame.origin.y, descriptionView.frame.size.width, descriptionHeight+ 10);
                }
                
                break;
            }
            case TBL_SEC_INPUT_PARAMETERS:
            {
                // int counts = 0;
                
                NSMutableString *availableString = [NSMutableString stringWithString:@""];
                for (ParameterAvailable *paramAvail in parameterAvailableArray) {
                    
                    if (![paramAvail.parameterAvailableId isEqualToString:@"-1"])
                    {
                        if (paramAvail.uom)
                        {
                            if (paramAvail.families)
                            {
                                NSString *optionName = nil;
                                for (Family *family in paramAvail.families) {
                                    for (Option *option in family.options) {
                                        if ([option.optionId isEqualToString:paramAvail.value]) {
                                            optionName = option.presentation;
                                        }
                                    }
                                }
                                [availableString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@ %@, ", paramAvail.presentation, optionName, paramAvail.uom]];
                            }
                            else
                            {
                                [availableString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@ %@, ", paramAvail.presentation, paramAvail.value, paramAvail.uom]];
                            }
                        }
                        else
                        {
                            if (paramAvail.families)
                            {
                                NSString *optionName = nil;
                                for (Family *family in paramAvail.families) {
                                    for (Option *option in family.options) {
                                        if ([option.optionId isEqualToString:paramAvail.value]) {
                                            optionName = option.presentation;
                                        }
                                    }
                                }
                                
                                if ([paramAvail.parameterAvailableId isEqualToString:@"165"]) {
                                    
                                    // Removing input param for NRB minimum load for multiple calculation
                                    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
                                    
                                    if (calculationDataManager.mutlipleCalculation) {
                                        
                                        [availableString appendString:[NSMutableString stringWithFormat:@""]];
                                        
                                    }else {
                                        
                                        [availableString appendString:[NSMutableString stringWithFormat:@"%@, ", paramAvail.description]];
                                    }
                                    
                                    // [availableString appendString:[NSMutableString stringWithFormat:@"%@", paramAvail.description]];
                                    
                                } else {
                                    
                                    [availableString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@, ", paramAvail.presentation, optionName]];
                                    
                                }
                                
                            } else {
                                
                                [availableString appendString:[NSMutableString stringWithFormat:@"<b>%@:</b> %@, ", paramAvail.presentation, paramAvail.value]];
                                
                            }
                        }
                    }
                }
                if ([availableString length] != 0)
                {
                    
                    // To show input param for minimum load cals in input param section: loa to load.
                    [availableString deleteCharactersInRange:NSMakeRange([availableString length]-2, 2)];
                    
                    [descriptionView loadHTMLString:[NSString stringWithFormat:@"%@%@", cssString, availableString] baseURL:[NSURL URLWithString:@""]];
                    
                    UIFont *descriptionFont = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
                    float descriptionHeight = [self calculateHeightOfTextFromWidth:[NSString stringByStrippingHTML:availableString] :descriptionFont :280 :NSLineBreakByWordWrapping];
                    descriptionView.frame = CGRectMake(descriptionView.frame.origin.x, descriptionView.frame.origin.y, descriptionView.frame.size.width, descriptionHeight+15);
                }
                
                break;
            }
            default:
                break;
        }
    }
    
    return cell;
}





-(void)didtaplabel:(UITapGestureRecognizer *) tapgesture
{
    
    hideIntermediate = !hideIntermediate;
    if(what_plain_brearing == 1)
        intermediate_result = true;
    else if (what_plain_brearing == 2)
        intermediate_result_composite = true;
    
    [resultsTable reloadData];
    
    NSLog(@" the intermediated result has been touched");
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)viewDidUnload
{
    [self setHomeButton:nil];
    [self setTopBarImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager removeObserver:self];
    
    [reportsViewController release];
    reportsViewController = nil;
    
    count = 0;
    selectedIndex = 0;
    [resultArray removeAllObjects];
    resultArray = nil;
    [textLabel_intr release];
    [imgView_intr release];
    [imgView_tintr release];
    [myview_intr release];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    CalculationDataManager* calculatorDataManager = [CalculationDataManager sharedManager];
    [calculatorDataManager removeObserver:self];
    
    [homeButton release];
    [topBarImageView release];
    [super dealloc];
}

- (void)dataManagerReturnedTranslation:(BOOL)success{
    if (success){
        CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
        self.navigationItem.titleView = [TitleManager getTitle:[calculationDataManager getTranslationString:@"Results"]];
        self.title = [calculationDataManager getTranslationString:@"Results"];
        [homeButton setTitle:[calculationDataManager getTranslationString:@"Home"] forState:UIControlStateNormal];
        [homeButton setTitle:[calculationDataManager getTranslationString:@"Home"] forState:UIControlStateDisabled];
        [homeButton setTitle:[calculationDataManager getTranslationString:@"Home"] forState:UIControlStateSelected];
        [homeButton setTitle:[calculationDataManager getTranslationString:@"Home"] forState:UIControlStateHighlighted];
    }
}


/*-(void)sendNewRequest
 {
 
 if (!connected) {
 
 [self setSearchingModeEnd];
 return;
 }
 
 NSString *calcID=nil;
 CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
 
 
 if(!calculationDataManager.mutlipleCalculation) {
 calcID = calculationDataManager.selectedCalculation.calculationId;
 
 }
 else {
 calcID = [[calculationDataManager.selectedCalculationsArray objectAtIndex:calculationsCount] calculationId];
 }
 
 if ([calcID isEqualToString:@"5"]) {                                             // Sending H=-1 for Radiolubrication - Oil jet
 NSString *value =  [params valueForKey:@"radioLubrication"];
 if([value isEqualToString:@"3"]) {
 [params setValue:@"-1" forKeyPath:@"H"];
 }
 }
 
 [calculationDataManager doCalculation:calcID withParameters:params userData:self];
 
 }*/



-(void)sendNewRequest
{
    
    if (!connected) {
        
        [self setSearchingModeEnd];
        return;
    }
    
    NSString *calcID=nil;
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    
    if(!calculationDataManager.mutlipleCalculation) {
        calcID = calculationDataManager.selectedCalculation.calculationId;
        
    }
    else {
        
        NSMutableString *selectedCalculationsId = [[[NSMutableString alloc] initWithString:@""] autorelease];
        
        int count =0;
        for (Calculation *calculation in calculationDataManager.selectedCalculationsArray) {
            
            [selectedCalculationsId appendString:calculation.calculationId];
            
            if (count++ < (calculationDataManager.selectedCalculationsArray.count-1)) {
                
                [selectedCalculationsId appendFormat:@","];
            }
        }
        
        
        //calcID = [[calculationDataManager.selectedCalculationsArray objectAtIndex:calculationsCount] calculationId];
        calcID = selectedCalculationsId;
        
        
    }
    
    /*  if ([calcID isEqualToString:@"5"]) {                                             // Sending H=-1 for Radiolubrication - Oil jet
     NSString *value =  [params valueForKey:@"radioLubrication"];
     if([value isEqualToString:@"3"]) {
     [params setValue:@"-1" forKeyPath:@"H"];
     }
     }*/
    
    // Sending H=-1 for Radiolubrication - Oil jet
    if ([calcID rangeOfString:@"5"].location != NSNotFound) {
        NSString *value =  [params valueForKey:@"radioLubrication"];
        if([value isEqualToString:@"3"]) {
            [params setValue:@"-1" forKeyPath:@"H"];
        }
    }
    
    [params setValue:@"1" forKey:@"mobile"];
    
    [calculationDataManager doCalculation:calcID withParameters:params userData:self];
    
//    [self.view setUserInteractionEnabled:NO];
    
}

-(void) showAlertForNetworkConnection {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[calculationDataManager getTranslationString:@"Network_error!"]
                                                    message:[calculationDataManager getTranslationString:@"This_application_need_a_network_connection."]
                                                   delegate:nil
                                          cancelButtonTitle:[calculationDataManager getTranslationString:@"OK"]
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
    
    
}
- (void)setSearchingModeStart {
    
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    HUD = [[MBProgressHUD alloc] initWithFrame:CGRectMake(35, 100, 250, 250)];
    HUD.labelText = [calculationDataManager getTranslationString:@"Loading"];
    HUD.graceTime = 0.5f;
    HUD.taskInProgress = YES;
    HUD.allowsCancelation = YES;
    HUD.delegate = self;
    HUD.labelFont = [UIFont fontWithName:CHEVIN_BOLD size:20.f];
    
    [self.view setUserInteractionEnabled:NO];
    //self.navigationItem.hidesBackButton = YES;
    
    [self.view addSubview:HUD];
    [HUD show:YES];
    
}


- (void)setSearchingModeStartForPDFReport {
    
    HUD = [[MBProgressHUD alloc] initWithFrame:CGRectMake(35, 100, 250, 250)];
    HUD.labelText = @"Please wait";
    HUD.graceTime = 0.5f;
    HUD.taskInProgress = YES;
    HUD.allowsCancelation = NO;
    HUD.delegate = self;
    HUD.labelFont = [UIFont fontWithName:CHEVIN_BOLD size:20.f];
    
    [self.view setUserInteractionEnabled:NO];
    [self.view addSubview:HUD];
    [HUD show:YES];
    
}
- (void)setSearchingModeEnd {
    
    [HUD hide:YES];
    HUD.taskInProgress = NO;
    [HUD removeFromSuperview];
    
    [self.view setUserInteractionEnabled:YES];
    //self.navigationItem.hidesBackButton = NO;
    
    [HUD release];
    HUD = nil;
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;
{
    NSURL *requestURL =[ [ request URL ] retain ];
    NSString *urlString = [requestURL absoluteString];
    NSURL *hyperlinkURL = nil;
    
    if ([urlString isEqualToString:@"file:///"] == false) {
        
        CalculationDataManager *calculateDataManager = [CalculationDataManager sharedManager];
        urlString = [urlString stringByReplacingOccurrencesOfString:@"file:///" withString:[NSString stringWithFormat:@"%@/",[calculateDataManager baseURL]]];
        NSLog(@"URL Path: %@",urlString);
        hyperlinkURL = [NSURL URLWithString:urlString];
        [self showHyperlinkView:hyperlinkURL];
    }
    [requestURL release ];
    return YES;
}

-(void) showHyperlinkView:(NSURL *) url{
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    UIWebView *webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    [webView loadRequest:request];
    webView.scalesPageToFit = YES;
    
    HyperlinkViewController *hyperlinkView = [[[HyperlinkViewController alloc]
                                               initWithNibName:@"HyperlinkViewController" bundle:nil]autorelease];
    [hyperlinkView.view addSubview:webView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
    [self.navigationController pushViewController:hyperlinkView animated:YES];
    [UIView commitAnimations];
}


/*** PDF Report Generation - Save | Print | Email ***/

#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1 && buttonIndex == 1) {
        
        NSURL *url = [NSURL fileURLWithPath:[[self documentDirectoryPathForResource] stringByAppendingPathComponent:lastReportFileName]];
        UIDocumentInteractionController *interactionController = [self setupControllerWithURL:url usingDelegate:self];
        
        NSString *navigationBarTintColoriOS7 = NAVIGATION_BACKGROUND_COLOR;
        [[UINavigationBar appearanceWhenContainedIn:[QLPreviewController class], nil] setTintColor:[UIColor whiteColor]];
        [[UINavigationBar appearanceWhenContainedIn:[QLPreviewController class], nil] setBarTintColor:[UIColor colorWithHexString:navigationBarTintColoriOS7]];

        
        // To change navigation bgcolor, title color, back button color
        /*if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
            
        }else {

        NSString *navigationBarTintColoriOS7 = NAVIGATION_BACKGROUND_COLOR;
        [[UINavigationBar appearanceWhenContainedIn:[QLPreviewController class], nil] setTintColor:[UIColor whiteColor]];
        [[UINavigationBar appearanceWhenContainedIn:[QLPreviewController class], nil] setBarTintColor:[UIColor colorWithHexString:navigationBarTintColoriOS7]];
        NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_BUTTON2], NSFontAttributeName,
                                                   [UIColor whiteColor],NSForegroundColorAttributeName,
                                                   //[UIColor whiteColor], UITextAttributeTextShadowColor,
                                                   [NSValue valueWithUIOffset:UIOffsetMake(-1, 0)], NSShadowAttributeName, nil];
        [[UINavigationBar appearanceWhenContainedIn:[QLPreviewController class], nil] setTitleTextAttributes:navbarTitleTextAttributes];
    }*/
        [interactionController presentPreviewAnimated:YES];
        
    }
}

- (NSInteger) numberOfPreviewItemsInPreviewController: (QLPreviewController *) controller {
    
    return 1;
}

- (id <QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index {
    
    NSURL *url = [NSURL fileURLWithPath:[[self documentDirectoryPathForResource] stringByAppendingPathComponent:lastReportFileName]];
    return url;
}



-(IBAction)saveReport:(id)sender {
    
    NSMutableDictionary *event =
    [[GAIDictionaryBuilder createEventWithCategory:@"UI"
                                            action:@"buttonPress"
                                             label:@"savereport-iPhone"
                                             value:nil] build];
    [[GAI sharedInstance].defaultTracker send:event];
    [[GAI sharedInstance] dispatch];
    
    
    if (!connected) {
        
        [self showAlertForNetworkConnection];
        return;
    }
    
    [self setSearchingModeStartForPDFReport];

    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // Add code here to do background processing
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        NSString *reportPath = @"/GenerateReport?reportId=";
        NSString *pdfReportId = reportId;
        NSString *mobile = @"&mobile=1";
        NSString *pdfReportAdress = [NSString stringWithFormat:@"%@%@%@%@",calculationDataManager.baseURL,reportPath,pdfReportId,mobile];
        NSLog(@"URL: %@",pdfReportAdress);
        NSURL *url = [NSURL URLWithString:pdfReportAdress];
        NSData *myData = [NSData dataWithContentsOfURL:url];
        NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
        req.timeoutInterval = 100000;
        NSLog(@"Data: %lu",(unsigned long)[myData length]);

        //make a file name to write the data to using the documents directory:
        NSDate *currentDateTime = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd_HH.mm.ss"];
        NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
        NSLog(@"%@", dateInStringFormated);
        [dateFormatter release];
        
        NSString *pdfReportName = @"SBC_Report_";
        NSString *fileFormat = @".pdf";
        NSString *fileName = [NSString stringWithFormat:@"%@%@%@",pdfReportName,dateInStringFormated,fileFormat];
        lastReportFileName = [fileName copy];
        NSLog(@"Pdf file name: %@",lastReportFileName);
        
        BOOL success = [myData writeToFile:[[self documentDirectoryPathForResource] stringByAppendingPathComponent:lastReportFileName] atomically:YES];
        
                dispatch_async( dispatch_get_main_queue(), ^{
            // Add code here to update the UI/send notifications based on the
            // results of the background processing
            
                    if (success) {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[calculationDataManager getTranslationString:@"Report_Downloaded"] message:[calculationDataManager getTranslationString:@"View_Report"]  delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
                        alert.tag = 1;
                        [alert show];
                        [alert release];
                    }
                    else {
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[calculationDataManager getTranslationString:@"Download_Error"] message:[calculationDataManager getTranslationString:@"Download_Report_Error"]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alert show];
                        [alert release];
                    }
                    
                    [self setSearchingModeEnd];
        });
    });
    
}

-(NSString*)documentDirectoryPathForResource {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask,
                                                         YES);
    return  [paths lastObject];
    
}

-(IBAction)reportButtonClicked:(id)sender {
    
    if(reportsViewController==nil) {
        
        reportsViewController = [[ReportsViewController alloc] initWithNibName:@"ReportsViewController" bundle:[NSBundle mainBundle]];
    }
    [self.navigationController pushViewController:reportsViewController animated:YES];
    
}

- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL
                                               usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    
    UIDocumentInteractionController *interactionController =
    [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    return interactionController;
    
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)interactionController {
    
    return self;
}

-(void)sendGAdispatch : (NSString *)val{
    NSMutableDictionary *event =
    [[GAIDictionaryBuilder createEventWithCategory:@"UI"
                                            action:nil
                                             label:val
                                             value:nil] build];
    [[GAI sharedInstance].defaultTracker send:event];
    [[GAI sharedInstance] dispatch];
    
    
}

/*** END ***/



@end
