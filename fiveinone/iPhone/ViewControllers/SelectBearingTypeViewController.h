//
//  SelectBearingTypeViewController.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/28/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculationDataManager.h"
#import "MBProgressHUD.h"



@interface SelectBearingTypeViewController : UIViewController <CalculationDataManagerDelegate, MBProgressHUDDelegate> {
    NSMutableArray *bearingTypesArray;
    UITableView *bearingTypesListTable;
    
}


@property (nonatomic,retain) IBOutlet UITableView *bearingTypesListTable;
@property (nonatomic, retain) NSMutableArray *bearingTypesArray;

@property (nonatomic, retain) MBProgressHUD *HUD;


@end


