//
//  SelectBearingTypeViewController.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/28/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "SelectBearingTypeViewController.h"
#import "CalculationDataManager.h"
#import "BearingType.h"
#import "TitleManager.h"


@implementation SelectBearingTypeViewController

@synthesize bearingTypesListTable;
@synthesize bearingTypesArray;
@synthesize HUD;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc{
    CalculationDataManager* calculatorDataManager = [CalculationDataManager sharedManager];
    [calculatorDataManager removeObserver:self];

    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //self.navigationItem.title = @"Bearing type";
    
    
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    [self setSearchingModeStart];

    [calculationDataManager addObserver:self];
    [calculationDataManager getAllBearingTypes:self];
    [calculationDataManager initTranslation:self];
    
    [bearingTypesListTable setBackgroundView:nil];
    [bearingTypesListTable setBackgroundColor:[UIColor clearColor]];
    [bearingTypesListTable setOpaque:NO];
    
    /*** iOS 7 - TO remove the space of header ***/
    
    self.bearingTypesListTable.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.bearingTypesListTable.bounds.size.width, 0.01f)];
    
    /*** END ***/
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager removeObserver:self];
 
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dataManagerReturnedBearingTypes:(NSArray *)returnedArray{
    
    [self setSearchingModeEnd];
    
    bearingTypesArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [returnedArray count]; i++) {
        BearingType *result = [returnedArray objectAtIndex:i];
        [bearingTypesArray addObject:result];
    }

    [bearingTypesListTable reloadData];

    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    [calculationDataManager cancelAllRequests];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [bearingTypesArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UILabel *nameLabel;
    UIImageView *iconImageView;
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 14, 230, 20)];
        nameLabel.userInteractionEnabled = false;
        nameLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
        nameLabel.backgroundColor = [UIColor clearColor];
        nameLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
        nameLabel.tag = 20;
        
        [cell addSubview:nameLabel];
        [nameLabel release];
        
        UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 2, 50, 40)];

        iconImageView.opaque = YES;
        iconImageView.tag = 40;
        iconImageView.hidden = NO; 

        [cell addSubview:iconImageView];
        [iconImageView release];
        
    }
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    nameLabel = (UILabel *)[cell viewWithTag:20];
    iconImageView = (UIImageView *)[cell viewWithTag:40];

    NSString *iconUrl = [[bearingTypesArray objectAtIndex:indexPath.row] iconUrl];
    NSString *urlString = [NSString stringWithFormat: @"%@/%@", calculationDataManager.baseURL, iconUrl];
    
    NSURL *url = [NSURL URLWithString: urlString];
    UIImage *image = [[UIImage imageWithData: [NSData dataWithContentsOfURL: url]] retain];
    
    iconImageView.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
    [iconImageView setImage:image];
    [image release];
    NSString *cellValue = [[bearingTypesArray objectAtIndex:indexPath.row] presentation];
    nameLabel.text = cellValue;
            
    BearingType *selectedBearingType = [calculationDataManager selectedBearingType];
    
    if ([[selectedBearingType presentation] isEqualToString:nameLabel.text])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)dataManagerReturnedTranslation:(BOOL)success{
    
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];

    if (success){
        
        self.navigationItem.titleView = [TitleManager getTitle:[calculationDataManager getTranslationString:@"Bearing_type"]];
     
    }
}
- (void)setSearchingModeStart {
    
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    
    HUD = [[MBProgressHUD alloc] initWithFrame:CGRectMake(35, 100, 250, 250)];
    HUD.labelText = [calculationDataManager getTranslationString:@"Searching"];
    HUD.graceTime = 0.5f;
    HUD.taskInProgress = YES;
    HUD.allowsCancelation = YES;
    HUD.delegate = self;
    HUD.labelFont = [UIFont fontWithName:CHEVIN_BOLD size:20.f];
    [self.view addSubview:HUD];
    [HUD show:YES];

    // [self.view setUserInteractionEnabled:NO];
}
- (void)setSearchingModeEnd {
    
    [HUD hide:YES];
    HUD.taskInProgress = NO;
    [HUD removeFromSuperview];
    [self.view setUserInteractionEnabled:YES];
    [HUD release];
    HUD = nil;
    
}



@end
