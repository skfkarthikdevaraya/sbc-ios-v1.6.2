//
//  SelectBearingTypeViewController_iPhone.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/20/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectBearingTypeViewController.h"
#import "fiveinoneAppDelegate_iPhone.h"
#import "MBProgressHUD.h"


@interface SelectBearingTypeViewController_iPhone : SelectBearingTypeViewController {

UINavigationController *filterBearingsNavigationController;

}

@property (nonatomic,retain) UINavigationController *filterBearingsNavigationController;

@end
