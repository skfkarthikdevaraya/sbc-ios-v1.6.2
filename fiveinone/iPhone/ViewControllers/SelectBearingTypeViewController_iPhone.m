//
//  SelectBearingTypeViewController_iPhone.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 10/20/11.
//  Copyright (c) 2011 HiQ. All rights reserved.
//

#import "SelectBearingTypeViewController_iPhone.h"
#import "TitleManager.h"

@implementation SelectBearingTypeViewController_iPhone

@synthesize filterBearingsNavigationController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setContentSizeForViewInPopover:CGSizeMake(320, 416)];
}


#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [TitleManager getSKFLogo];
    
    
    }


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager setBearingType:[bearingTypesArray objectAtIndex:indexPath.row]];
    
    fiveinoneAppDelegate_iPhone *rootDelegate = (fiveinoneAppDelegate_iPhone *)[UIApplication sharedApplication].delegate;
    
    [rootDelegate.navigationController popViewControllerAnimated:YES];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
  
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
