//
//  selectBearing.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/15/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "SelectBearingViewController_iPhone.h"
#import "CalculateViewController.h"
#import "fiveinoneAppDelegate_iPhone.h"
#import "TitleManager.h"
#import "CalculationDataManager.h"


@implementation SelectBearingViewController_iPhone

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"main_background_grey.png"]];
    
    self.view.backgroundColor = [UIColor whiteColor];


    self.navigationItem.rightBarButtonItem = [TitleManager getSKFLogo];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CalculateViewController *calculateController = [[CalculateViewController alloc] initWithNibName:@"CalculateViewController" bundle:[NSBundle mainBundle]];
    
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    calculateController.selectedDesignation = cell.textLabel.text;
    calculateController.selectedProduct = (Product *)[foundBearingsArray objectAtIndex:indexPath.row];
    
    CalculationDataManager *calculateDataManager = [CalculationDataManager sharedManager];
    
    [calculateDataManager setBearing:(Product *)[foundBearingsArray objectAtIndex:indexPath.row]];
    
    
    fiveinoneAppDelegate_iPhone *rootDelegate = (fiveinoneAppDelegate_iPhone *)[UIApplication sharedApplication].delegate;
    [rootDelegate.navigationController pushViewController:calculateController animated:YES];

    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [calculateController release];
    calculateController = nil;    
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    [bearingsListTable reloadData];

    
}

//self.contentSizeForViewInPopover = CGSizeMake(320, 155); // sized for a 3-row UITableView

@end
