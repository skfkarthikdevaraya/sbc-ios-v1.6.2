//
//  selectCalculationView.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/15/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculationDataManager.h"
#import "TSAlertView.h"
#import "MBProgressHUD.h"
#import "ReportsViewController.h"

#import "GAITrackedViewController.h"

//@interface SelectCalculationViewController : UIViewController <CalculationDataManagerDelegate, TSAlertViewDelegate, MBProgressHUDDelegate> {
    
@interface SelectCalculationViewController : GAITrackedViewController <CalculationDataManagerDelegate, TSAlertViewDelegate, MBProgressHUDDelegate> {

    NSMutableArray *calculationTypesArray;
    UITableView *theTableView;
    UILabel *appTitleLabel;
    UILabel *appShortDescLabel;
    Calculation *currentCalculation;
    
    // Changing navigation bar - Header
    UIView* _headerView;
    BOOL _showHeaderView;
    
    UIButton *checkboxButton;           // Checkbox in calculation tableview
    BOOL isCheckboxSelected ;
    
    UIButton *selectAllcheckbox;        // Checkbox for selecting all rolling bearing caluculations
    BOOL selectAllBoxChecked;
    
    UIButton *selectAllPBCBox;        // Checkbox for selecting all plain bearing caluculations
    BOOL selectAllPBCBoxChecked;

    
    int selectedCheckboxCount;
    BOOL showingAlert;
    
    NSMutableArray *plainBearingCalcArray;
    int selectedPlainBearingCalc;
    
    UIButton *reportsButton;
    UIButton *proceedButton;
    
    BOOL showAlertPlainBearing;
    BOOL showAlertRollingBearing;

    UIButton *calculationCheckbox;
    
    ReportsViewController *reportsViewController;
    NSMutableArray *selectedRowsArray;
    int rollingBearingCalculations;
    int plainBearingCalculations;
    
    
    
}
@property (nonatomic, assign) BOOL showingAlert;
@property (nonatomic, assign) BOOL connected;
@property (nonatomic, retain) IBOutlet UITableView *theTableView;
@property (nonatomic, retain) NSMutableArray *calculationTypesArray;
@property (nonatomic, retain) IBOutlet UILabel *appTitleLabel;
@property (nonatomic, retain) IBOutlet UILabel *appShortDescLabel;
@property (nonatomic, retain) IBOutlet UIButton *settingsButton;
@property (nonatomic, retain) Calculation *currentCalculation;
@property (nonatomic, retain) NSString* lastUsedLanguage;
@property (nonatomic, retain) NSMutableDictionary *cellDictionery;      // Cell Dectionary for selected calc
@property (nonatomic, retain)  UIView *headerView;                      // Changing navigation bar - Header
@property (nonatomic, assign) BOOL showHeaderView;
@property (nonatomic, retain) UIButton *checkboxButton;
@property (nonatomic, assign) BOOL isCheckboxSelected;
@property (nonatomic, retain) UIButton *selectAllcheckbox;
@property (nonatomic, assign) BOOL selectAllBoxChecked;
@property(nonatomic, assign) int selectedCheckboxCount;
@property (nonatomic, retain) MBProgressHUD *HUD;
@property (nonatomic, retain) NSMutableArray *plainBearingCalcArray;
@property(nonatomic,retain)  IBOutlet   UIButton *reportsButton;
@property (nonatomic, retain) IBOutlet UIButton *proceedButton;
@property (nonatomic, assign) BOOL showAlertPlainBearing;
@property (nonatomic, assign) BOOL showAlertRollingBearing;
@property(nonatomic, retain) UIButton *calculationCheckbox;
@property(nonatomic,retain) ReportsViewController *reportsViewController;
@property (nonatomic, retain) IBOutlet UIView *lineView_info;

-(IBAction)nextButtonToAppData:(id)sender;
-(void) selectAllCalculations:(id)sender;
-(void)showInformation:(id)sender;
-(IBAction)reportsButtonClicked:(id)sender;
-(void)checkboxSelected:(id)sender;


@end
