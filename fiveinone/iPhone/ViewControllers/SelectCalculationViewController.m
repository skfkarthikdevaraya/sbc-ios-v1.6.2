//
//  selectCalculationView.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/15/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "SelectCalculationViewController.h"
#import "SelectBearingViewController.h"
#import "Calculation.h"
#import "FilterBearingsViewController_iPhone.h"
#import "LegalInformation_iPhone.h"
#import "SettingsView.h"
#import "TSAlertView.h"
#import "SettingsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "IntroTextViewController_iPhone.h"
#import "CalculationDataManager.h"

#import "TitleManager.h"
#import "MBProgressHUD.h"
#import "ReportsViewController.h"




//static BOOL allSelected = NO;
#define kNoOfPlainBearingCalculations 3
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@implementation SelectCalculationViewController

@synthesize theTableView;
@synthesize calculationTypesArray;
@synthesize appShortDescLabel;
@synthesize appTitleLabel;
@synthesize currentCalculation;
@synthesize settingsButton;
@synthesize lastUsedLanguage;
@synthesize headerView = _headerView;
@synthesize proceedButton;
@synthesize checkboxButton;
@synthesize isCheckboxSelected ;
@synthesize selectAllcheckbox;
@synthesize selectAllBoxChecked;
@synthesize selectedCheckboxCount;
@synthesize cellDictionery;
@synthesize HUD;
@synthesize connected;
@synthesize plainBearingCalcArray;
@synthesize reportsButton;
@synthesize showAlertPlainBearing;
@synthesize showAlertRollingBearing;
@synthesize calculationCheckbox;



- (void)showInformation:(id)sender {
    SettingsViewController *settingsView = [[[SettingsViewController alloc]
                                             initWithNibName:@"SettingsViewController" bundle:nil]autorelease];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
    [self.navigationController pushViewController:settingsView animated:YES];
    [UIView commitAnimations];    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


/** For navigation bar - Header */
- (BOOL)showHeaderView {
    return _showHeaderView;
} 


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setSearchingModeStart];
       
    self.screenName = @"SBC_iPhone - Main screen";      // For google analytics  - screen name
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    NSString *navcolor = TITLE_TINT_COLOR;
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithHexString:navcolor];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // do nothing
    }
    else {
        NSString *navBgColoriOS7 = NAVIGATION_BACKGROUND_COLOR;
        
        //set bar color
        [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithHexString:navBgColoriOS7]];
        //optional, i don't want my bar to be translucent
        [self.navigationController.navigationBar setTranslucent:NO];  // Also to avoid overlapping in all screens like search bar, table view,etc..
        //set title and title color
        self.navigationItem.titleView = [TitleManager getTitle:[calculationDataManager getTranslationString:@"select_calculation"]];
        self.title = [calculationDataManager getTranslationString:@"select_calculation"];
        [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName]];
        //set back button color
        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
        //set back button arrow color
        [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    }

    
    
    //set logo
    self.navigationItem.rightBarButtonItem = [TitleManager getSKFLogo];
    // set info button
    /*UIButton* infoIcon = [UIButton buttonWithType:UIButtonTypeInfoLight];
    infoIcon.frame = CGRectMake(20,37,25,25);
    infoIcon.tintColor = [UIColor whiteColor];
    [infoIcon addTarget:self action:@selector(showInformation:)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:infoIcon];
    [self.navigationItem setLeftBarButtonItem:modalButton animated:YES];
    [modalButton release];*/

    
    UIButton *infoIcon = [UIButton buttonWithType:UIButtonTypeInfoDark];
    [infoIcon addTarget:self action:@selector(showInformation:)forControlEvents:UIControlEventTouchUpInside];
    //how View" forState:UIControlStateNormal];
    infoIcon.frame = CGRectMake(20,37,25,25);
    UIImage * buttonImage = [UIImage imageNamed:@"infoButton.png"];
    [infoIcon setImage:buttonImage forState:UIControlStateNormal];
    UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:infoIcon];
    [self.navigationItem setLeftBarButtonItem:modalButton animated:YES];
    [modalButton release];
    
    

    
    /**  Don's Show Again checkbox implemention */
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL dontShow= [userDefaults boolForKey:@"DONOTSHOW"];
    if(dontShow == NO)
    {
        IntroTextViewController_iPhone *intro = [[[IntroTextViewController_iPhone alloc] initWithNibName:@"IntroTextViewController_iPhone" bundle:nil] autorelease];
        [self presentModalViewController:intro animated:YES];
    }
    
    /*******************/
    
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    calculationTypesArray = [[NSMutableArray alloc] init];
    plainBearingCalcArray = [[NSMutableArray alloc] init];
    
    selectedRowsArray = [[NSMutableArray alloc] init];
    
    rollingBearingCalculations=0;
    plainBearingCalculations=0;
    
    [calculationDataManager initTranslation:self];
    [calculationDataManager addObserver:self];
    //[calculationDataManager getAllCalculations:self];
    
    
    //TODO Make better update way
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // NSLog(@"Application Version: %@ Build No: %@ Build Date: %@",version,buildNo,buildDate);
    //self.navigationItem.title = @"Calc type";
    
    appTitleLabel.font      = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_APPTITLE];
    appTitleLabel.textColor = [UIColor colorWithRed:218.0/255.0 green:30.0/255.0 blue:60.0/255.0 alpha:1];
    appTitleLabel.text = @"";
    
    appShortDescLabel.font       = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_APPVERSION];
    appShortDescLabel.textColor  = [UIColor colorWithHexString:@"5e5e5e"];
    appShortDescLabel.text = @"";


    /** Commented as mainpage redesigned*/
//    CGRect rect = CGRectMake(
//                             infoButton.frame.origin.x - 8,
//                             infoButton.frame.origin.y - 8,
//                             infoButton.frame.size.width + 16,
//                             infoButton.frame.size.height + 16);
//    [infoButton setFrame:rect];

    
    if (![[prefs valueForKey:@"firstrun"] isEqualToString:@"NO"])
    {
        
        LegalInformation_iPhone *view = [[[LegalInformation_iPhone alloc] 
                                   initWithNibName:@"LegalInformation" 
                                   bundle:nil] autorelease];
        
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration: 0.5];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
        [self.navigationController pushViewController:view animated:NO];
        [UIView commitAnimations];
        
    }
    
    self.cellDictionery  = [[NSMutableDictionary alloc] initWithCapacity:(calculationTypesArray.count+plainBearingCalcArray.count)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:RKReachabilityDidChangeNotification
                                               object:nil];
    
    showingAlert = false;
    connected = true;
    
    selectedPlainBearingCalc = 0;

    
    
    /*** iOS 7 - TO remove the space of header ***/
    
    self.theTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.theTableView.bounds.size.width, 0.01f)];
    
    /*** END ***/
    
}


- (void)reachabilityChanged:(NSNotification*)notification {
    RKReachabilityObserver* observer = (RKReachabilityObserver*)[notification object];
    
    if (![observer isNetworkReachable])
    {
        [self setSearchingModeEnd];
        connected = false;
        [self showingAlert];
        DLog(@"Disconnected");
    }
    if ([observer isNetworkReachable]) {
        connected = true;
        DLog(@"Connected");
    }
}

/***** Proceed Button Action ******/
/*
-(IBAction)nextButtonToAppData:(id)sender {
    
    NSArray *keys = [self.cellDictionery allKeys];
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    keys = [keys sortedArrayUsingSelector:@selector(compare:)];
    
    for (NSIndexPath *object in keys )
    {
        if ([cellDictionery objectForKey:object] == [NSNumber numberWithBool:YES])
        {
            if(object.section==0)
            {
                [indexPaths addObject: [calculationTypesArray objectAtIndex:object.row]];
                 NSLog(@"index:%d, calculationID:%@, presentation:%@",object.row, [[calculationTypesArray objectAtIndex:object.row] calculationId], [[calculationTypesArray objectAtIndex:object.row] presentation]);
            }
            else if(object.section==1)
            {
                [indexPaths addObject: [plainBearingCalcArray objectAtIndex:object.row]];
            }
        }
    }
    
    if ([indexPaths count]== 0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:GetLocalizedString(@"Please_select_calculations_to_proceed") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];

        alert.tag = 101;
        [alert show];
        [alert release];
        
    }
    else{
        
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
//        [calculationDataManager.selectedCalculationsArray removeAllObjects];
//        [calculationDataManager.selectedCalculationsArray addObjectsFromArray: indexPaths];
        
        if(indexPaths.count > 1) {
            
            [calculationDataManager.selectedCalculationsArray removeAllObjects];
            [calculationDataManager.selectedCalculationsArray addObjectsFromArray: indexPaths];
            calculationDataManager.mutlipleCalculation = true;
        }
        else {
            calculationDataManager.selectedCalculation = (Calculation *)[indexPaths objectAtIndex:0];
            calculationDataManager.mutlipleCalculation = false;
        }

        FilterBearingsViewController_iPhone *filterBearingsViewController = [[FilterBearingsViewController_iPhone alloc] initWithNibName:@"FilterBearingsViewController" bundle:nil];
        
        [self.navigationController pushViewController:filterBearingsViewController animated:YES];
        [filterBearingsViewController release];
    }
}*/

-(IBAction)nextButtonToAppData:(id)sender {
    
    if (selectedRowsArray.count == 0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:GetLocalizedString(@"Please_select_calculations_to_proceed") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        alert.tag = 101;
        [alert show];
        [alert release];
        return;
    }

    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager.selectedCalculationsArray removeAllObjects];

    for (NSIndexPath *indexPath in selectedRowsArray )
    {
        if(indexPath.section==0)
        {
            [calculationDataManager.selectedCalculationsArray addObject: [calculationTypesArray objectAtIndex:indexPath.row]];
            
        }
        else if(indexPath.section==1)
        {
            [calculationDataManager.selectedCalculationsArray addObject: [plainBearingCalcArray objectAtIndex:indexPath.row]];
        }
    }
    
    if(selectedRowsArray.count > 1) {
        calculationDataManager.mutlipleCalculation = true;
    }
    else {
        calculationDataManager.selectedCalculation = (Calculation *)[calculationDataManager.selectedCalculationsArray objectAtIndex:0];
        calculationDataManager.mutlipleCalculation = false;
    }
    
    FilterBearingsViewController_iPhone *filterBearingsViewController = [[FilterBearingsViewController_iPhone alloc] initWithNibName:@"FilterBearingsViewController" bundle:nil];
    
    [self.navigationController pushViewController:filterBearingsViewController animated:YES];
    [filterBearingsViewController release];
}


- (void) viewWillAppear:(BOOL)animated
{
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    BOOL languageHasChanged = ![self.lastUsedLanguage isEqualToString:[calculationDataManager getUserSelectedLanguage]];
    
    if (languageHasChanged){
        //[calculationDataManager getAllCalculations:self];
    }
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
   
    /*NSString *navigationBarTintColor = NAVIGATION_TINT_COLOR;
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithHexString:navigationBarTintColor]];*/
    

    
    

    [super viewWillAppear:animated];
}


- (void)dataManagerReturnedTranslation:(BOOL)success
{
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    if (success) {
        
        self.navigationItem.titleView = [TitleManager getTitle:[calculationDataManager getTranslationString:@"select_calculation"]];
        self.title = [calculationDataManager getTranslationString:@"select_calculation"];
        
        appTitleLabel.text = [calculationDataManager getTranslationString:@"Application_name"];
        appShortDescLabel.text = [calculationDataManager getTranslationString:@"Application_short_description"];
        
    }
    
    [calculationDataManager getAllCalculations:self];
    
   // [self setSearchingModeEnd];

}

- (void)dataManagerReturnedCalculations:(NSArray *)returnedArray
{
    
    
    // Select all Label
    /*UILabel *selectAll = [[UILabel alloc]initWithFrame:CGRectMake(215.0, 15.0, 69.0, 21.0)];
    selectAll.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_MESSAGES];
    selectAll.textColor = [UIColor colorWithHexString:LABEL_COLOR_LT_BLACK];
    selectAll.backgroundColor = [UIColor clearColor];
    [selectAll setText:GetLocalizedString(@"Select_all")];
    [self.view addSubview:selectAll];
    
    // Select all Checkbox
     
     selectAllcheckbox = [[[UIButton alloc] initWithFrame:CGRectMake(260,4,44,44)]autorelease];
     [selectAllcheckbox setBackgroundImage:[UIImage imageNamed:@"check2.png"]  forState:UIControlStateNormal];
     [selectAllcheckbox setBackgroundImage:[UIImage imageNamed:@"checked2.png"] forState:UIControlStateSelected];
     [selectAllcheckbox setBackgroundImage:[UIImage imageNamed:@"checked2.png"] forState:UIControlStateHighlighted];
     [selectAllcheckbox setBackgroundImage:[UIImage imageNamed:@"checked2.png"] forState:UIControlStateReserved];
     selectAllcheckbox.adjustsImageWhenHighlighted=YES;
     [selectAllcheckbox addTarget:self action:@selector(selectAllCalculations:) forControlEvents: UIControlEventTouchUpInside];
     [self.view addSubview:selectAllcheckbox];
    */

    [proceedButton setHidden:NO];
   // proceedButton.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];

    
    [calculationTypesArray removeAllObjects];
    [plainBearingCalcArray removeAllObjects];
    
    // To hide shaft & housing tolerance calculation
   /* for (int i = 0; i < [returnedArray count]; i++) {
        Calculation *result = [returnedArray objectAtIndex:i];
        [calculationTypesArray addObject:result];
    }*/
    
    /*for (int i = 0; i < [returnedArray count]; i++) {
        
        Calculation *result = [returnedArray objectAtIndex:i];
        
        if ([result.calculationId isEqualToString:@"24"]) {
            
            [calculationTypesArray removeObject:result];
        }else {

            [calculationTypesArray addObject:result];
        }
        
        
    }*/
    
    for (Calculation *calc in returnedArray) {
        if ([calc.calculationId isEqualToString:@"24"]) {
            
            // do nothing
        }else {
            
            
            // Add plain bearing calculations to the array
            if([calc.calculationId isEqualToString:@"17"] || [calc.calculationId isEqualToString:@"22"] || [calc.calculationId isEqualToString:@"23"])
            {
                [plainBearingCalcArray addObject:calc];
            }
            else
            {
                [calculationTypesArray addObject:calc];

            }

        }
        
        
    }

    
    
    [theTableView reloadData];

    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    self.lastUsedLanguage = [calculationDataManager getUserSelectedLanguage];
    [self setSearchingModeEnd];
}
/*
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSString *sectionTitle = @"Just a title";
    
    // Create label with section title
    UILabel *label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(0, 0, 284, 23);
    label.textColor = [UIColor blackColor];
    label.font = [UIFont fontWithName:@"Helvetica" size:14];
    label.text = sectionTitle;
    label.backgroundColor = [UIColor clearColor];
    
    // Create header view and add label as a subview
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
    [view autorelease];
    [view addSubview:label];
    
    return view;
}*/

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.opaque = NO;
	headerLabel.textColor = [UIColor colorWithHexString:COLOR_SKF_RED];
	headerLabel.highlightedTextColor = [UIColor colorWithHexString:COLOR_SKF_RED];
	headerLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_GROUPED_TABLE_HEADER];
    headerLabel.frame = CGRectMake(20, 10, 320, 24);
    headerLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    
    UIView *headerView = [[UIView alloc] init];
    [headerView addSubview:headerLabel];
    
    
    if(section==0){
        
        if(selectAllcheckbox==nil){
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
                
                selectAllcheckbox = [[UIButton alloc] initWithFrame:CGRectMake(275,2,44,44)];
                
            }
            else {
                
                selectAllcheckbox = [[UIButton alloc] initWithFrame:CGRectMake(275,2,44,44)];
                
            }
            
            [selectAllcheckbox setBackgroundImage:[UIImage imageNamed:@"check2.png"]  forState:UIControlStateNormal];
            [selectAllcheckbox setBackgroundImage:[UIImage imageNamed:@"checked2.png"] forState:UIControlStateSelected];
            [selectAllcheckbox setBackgroundImage:[UIImage imageNamed:@"checked2.png"] forState:UIControlStateHighlighted];
            
            //selectAllcheckbox.adjustsImageWhenHighlighted=YES;
            
            [selectAllcheckbox addTarget:self action:@selector(selectAllRBC:) forControlEvents:UIControlEventTouchUpInside];
            
            
        }
        
        if (plainBearingCalculations==0) {
            [selectAllcheckbox setEnabled:TRUE];
        }
        else {
            [selectAllcheckbox setEnabled:FALSE];
        }

        [headerView addSubview:selectAllcheckbox];
        
        
    }
    
    if(section==1){
        
        if(selectAllPBCBox == nil) {
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
                
                selectAllPBCBox = [[UIButton alloc] initWithFrame:CGRectMake(275,2,44,44)];
                
            }
            else {
                
                selectAllPBCBox = [[UIButton alloc] initWithFrame:CGRectMake(275,2,44,44)];
                
            }
            
            [selectAllPBCBox setBackgroundImage:[UIImage imageNamed:@"check2.png"]  forState:UIControlStateNormal];
            [selectAllPBCBox setBackgroundImage:[UIImage imageNamed:@"checked2.png"] forState:UIControlStateSelected];
            [selectAllPBCBox setBackgroundImage:[UIImage imageNamed:@"checked2.png"] forState:UIControlStateHighlighted];
            
            
            //selectAllPBCBox.adjustsImageWhenHighlighted=YES;
            [selectAllPBCBox addTarget:self action:@selector(selectAllPBC:) forControlEvents: UIControlEventTouchUpInside];
            
        }
        
        if (rollingBearingCalculations==0) {
            [selectAllPBCBox setEnabled:TRUE];
        }
        else {
            [selectAllPBCBox setEnabled:FALSE];
        }

        [headerView addSubview:selectAllPBCBox];
    }

    
    return headerView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 44.0;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];

    switch (section)
    {
        case 0:
            //sectionName = NSLocalizedString(@"mySectionName", @"mySectionName");
            //sectionName = @"Rolling bearing calculations";
            
            sectionName =[calculationDataManager getTranslationString:@"Rolling_Bearing_Calc"];
            break;
        case 1:
            sectionName =[calculationDataManager getTranslationString:@"Plain_Bearing_Calc"];
            break;
            // ...
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(section==0)
    {
        return calculationTypesArray.count;
    }
    else if(section==1)
    {
        return plainBearingCalcArray.count;
    }
    
    return 0;
    
}

/*
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UILabel *presentationLabel;
    UILabel *descriptionLabel;
    UIButton *calculationInfo;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        presentationLabel = [[UILabel alloc] initWithFrame:CGRectMake(44, 17, 235, 20)];
        presentationLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
        presentationLabel.backgroundColor = [UIColor clearColor];
        presentationLabel.tag = 10015;
        [cell addSubview:presentationLabel];
        [presentationLabel release];
        
        
        calculationInfo = [UIButton buttonWithType:UIButtonTypeInfoDark];
        calculationInfo.frame = CGRectMake(20,17,16,16);
        
        // Change the existing touch area by 40 pixels in each direction
        // Move the x/y starting coordinates so the button remains in the same location
        CGRect rect = CGRectMake(
                                 calculationInfo.frame.origin.x - 8,
                                 calculationInfo.frame.origin.y - 8,
                                 calculationInfo.frame.size.width + 16,
                                 calculationInfo.frame.size.height + 16);
        [calculationInfo setFrame:rect];
        

        [calculationInfo addTarget:self 
                         action:@selector(showCalculationInfoPopup:)
                         forControlEvents:UIControlEventTouchUpInside];
        
        //calculationInfo.tag = indexPath.row;
        
        [cell addSubview:calculationInfo];

    }

    descriptionLabel = (UILabel *)[cell viewWithTag:10020];
    presentationLabel = (UILabel *)[cell viewWithTag:10015];
    
    // add calculationInfo.tag here so that tag is updated with the row number for the right calculation info.
    NSArray * subViewsInCell = [cell subviews];
    for(UIView *view in subViewsInCell)
    {
        if([view isKindOfClass:[UIButton class]])
        {
            UIButton* buttonInfo = (UIButton*)view;
            if(buttonInfo.buttonType == UIButtonTypeInfoDark)
            {
                buttonInfo.tag= indexPath.row;
            }
        }
            
    }

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    Calculation *calc = [calculationTypesArray objectAtIndex:indexPath.row];
    
    
  
    
    
    NSString *cellValue = calc.presentation;
    NSString *calcLevel = calc.level;

    if ([calcLevel isEqualToString:@"0"]) {
        presentationLabel.font       = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_TABLE_LVL0];
    } else {
        presentationLabel.font       = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_LVL1];
    }
    
    presentationLabel.text = cellValue;
    descriptionLabel.text = calc.description;
    
     //Checkbox in Calculation TableView
    
    BOOL checked = [[cellDictionery objectForKey:indexPath] boolValue];
    UIImage *image = (checked) ? [UIImage   imageNamed:@"checked2.png"] : [UIImage imageNamed:@"check2.png"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    CGRect frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    CGRect frame = CGRectMake(0.0, 0.0, 44, 44);
    button.frame = frame;
    

    [button setBackgroundImage:image forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(checkButtonTapped:event:)  forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [UIColor clearColor];
    cell.accessoryView = button;
    
    
    
    return cell;
}
*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //static NSString *CellIdentifier = @"Cell";
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"%d_%d",indexPath.section,indexPath.row];
    
    UILabel *presentationLabel;
    UILabel *descriptionLabel;
    UIButton *calculationInfo;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        presentationLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 17, 235, 20)];
        presentationLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
        presentationLabel.backgroundColor = [UIColor clearColor];
        presentationLabel.tag = 10015;
        [cell addSubview:presentationLabel];
        [presentationLabel release];
        
        
        /*calculationInfo = [UIButton buttonWithType:UIButtonTypeInfoDark];
        calculationInfo.frame = CGRectMake(20,17,16,16);
        CGRect rect = CGRectMake(
                                 calculationInfo.frame.origin.x - 8,
                                 calculationInfo.frame.origin.y - 8,
                                 calculationInfo.frame.size.width + 16,
                                 calculationInfo.frame.size.height + 16);
        [calculationInfo setFrame:rect];
        [calculationInfo addTarget:self action:@selector(showCalculationInfoPopup:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:calculationInfo];*/
        
        
        
        calculationInfo = [UIButton buttonWithType:UIButtonTypeInfoDark];
        calculationInfo.frame = CGRectMake(20,15,20,20);
        UIImage * buttonImage = [UIImage imageNamed:@"infoButton_blue.png"];
        [calculationInfo setImage:buttonImage forState:UIControlStateNormal];

       
        [calculationInfo addTarget:self action:@selector(showCalculationInfoPopup:) forControlEvents:UIControlEventTouchUpInside];
        calculationInfo.exclusiveTouch = YES;
        [cell addSubview:calculationInfo];
        
        
        /*CGRect rect = CGRectMake(
                                 calculationInfo.frame.origin.x - 8,
                                 calculationInfo.frame.origin.y - 8,
                                 calculationInfo.frame.size.width + 16,
                                 calculationInfo.frame.size.height + 16);
        [calculationInfo setFrame:rect];*/
        
        
    }
    
    
    descriptionLabel = (UILabel *)[cell viewWithTag:10020];
    presentationLabel = (UILabel *)[cell viewWithTag:10015];
    
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    if([selectedRowsArray containsObject:indexPath]){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    
    Calculation *calc = nil;
    if(indexPath.section==-0)
    {
        calc = [calculationTypesArray objectAtIndex:indexPath.row];
    }
    else if (indexPath.section==1)
    {
        calc = [plainBearingCalcArray objectAtIndex:indexPath.row];
    }
    
    NSString *cellValue = calc.presentation;
    NSString *calcLevel = calc.level;
    
    if ([calcLevel isEqualToString:@"0"]) {
        presentationLabel.font       = [UIFont fontWithName:CHEVIN_BOLD size:FONT_SIZE_FOR_TABLE_LVL0];
    } else {
        presentationLabel.font       = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_LVL1];
    }
    
    presentationLabel.text = cellValue;
    descriptionLabel.text = calc.description;
    
    
    if(indexPath.section==0) {
        if (plainBearingCalculations==0) {
            [self setInteraction:TRUE forCell:cell];
        }
        else {
            [self setInteraction:FALSE forCell:cell];
        }
        
    }
    else if(indexPath.section==1) {
        if (rollingBearingCalculations==0) {
            [self setInteraction:TRUE forCell:cell];
        }
        else {
            [self setInteraction:FALSE forCell:cell];
        
        }
    }


 /*   // Checkbox in Calculation TableView
    [cell setUserInteractionEnabled:TRUE];
  
    if([self isPlainBearingCalculation:calc.calculationId])
    {
        if(selectAllBoxChecked || selectedCheckboxCount)
        {
            [cell setUserInteractionEnabled:FALSE];
            [cellDictionery setObject:[NSNumber numberWithBool:FALSE] forKey:indexPath];
        }
        
    }
    else if(selectedPlainBearingCalc>0)
    {
        [cell setUserInteractionEnabled:FALSE];
        [cellDictionery setObject:[NSNumber numberWithBool:FALSE] forKey:indexPath];
    }
    
    
    BOOL checked = [[cellDictionery objectForKey:indexPath] boolValue];
    UIImage *image = (checked) ? [UIImage   imageNamed:@"checked2.png"] : [UIImage imageNamed:@"check2.png"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    //    CGRect frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    CGRect frame = CGRectMake(0.0, 0.0, 44, 44);
    button.frame = frame;
    
    
    [button setBackgroundImage:image forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(checkButtonTapped:event:)  forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [UIColor clearColor];
    cell.accessoryView = button;
    	
    
    */
    
    return cell;
}

/*
-(BOOL) isPlainBearingCalculation:(NSString *)calcId
{
    
    if([calcId isEqualToString:@"17"] || [calcId isEqualToString:@"22"] || [calcId isEqualToString:@"23"])
    {
        return YES;
    }
    
    return NO;
}

-(void) disabletOtherCalculations:(BOOL)enableUserInteraction
{
    for (int i = 0; i<[theTableView numberOfRowsInSection:0]; i++)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
    
        UITableViewCell *cell = [theTableView cellForRowAtIndexPath:indexPath];
        UIButton *button = (UIButton *)cell.accessoryView;
        [button setBackgroundImage:[UIImage imageNamed:@"check2.png"] forState:UIControlStateNormal];
        [cell setUserInteractionEnabled:enableUserInteraction];
        [cellDictionery setObject:[NSNumber numberWithBool:FALSE] forKey:indexPath];
    }
    
    selectedCheckboxCount = 0;
}*/
/*
-(void) disablePlainBearingCalculations:(BOOL)enableUserInteraction
{
    for (int i = 0; i<[theTableView numberOfRowsInSection:1]; i++)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:1];
        
        UITableViewCell *cell = [theTableView cellForRowAtIndexPath:indexPath];
        UIButton *button = (UIButton *)cell.accessoryView;
        [button setBackgroundImage:[UIImage imageNamed:@"check2.png"] forState:UIControlStateNormal];
        [cell setUserInteractionEnabled:enableUserInteraction];
        [cellDictionery setObject:[NSNumber numberWithBool:FALSE] forKey:indexPath];
        
    }
    
}
*/

-(void)setInteraction:(BOOL)enableUserInteraction forCell:(UITableViewCell*)cell
{
    //[cell setUserInteractionEnabled:enableUserInteraction];
    
    UILabel *descriptionLabel = (UILabel *)[cell viewWithTag:10020];
    UILabel *presentationLabel = (UILabel *)[cell viewWithTag:10015];
    descriptionLabel.enabled = enableUserInteraction;
    presentationLabel.enabled = enableUserInteraction;

}

-(void) enableRollingBearingCalculations:(BOOL)enableUserInteraction
{
    for (int i = 0; i<[theTableView numberOfRowsInSection:0]; i++)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        UITableViewCell *cell = [theTableView cellForRowAtIndexPath:indexPath];
        [self setInteraction:enableUserInteraction forCell:cell];
        
    }
    
}

-(void) enablePlainBearingCalculations:(BOOL)enableUserInteraction
{
    for (int i = 0; i<[theTableView numberOfRowsInSection:1]; i++)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:1];
        UITableViewCell *cell = [theTableView cellForRowAtIndexPath:indexPath];
       [self setInteraction:enableUserInteraction forCell:cell];
    }
    
}


/****** Select/Deselect all Checkbox *****/
/*-(void)selectAllCalculations:(id)sender {
    
    selectAllBoxChecked = !selectAllBoxChecked;
    [selectAllcheckbox setSelected:selectAllBoxChecked];
        
    if (allSelected)
    {
        for (int i = 0; i<[theTableView numberOfRowsInSection:0]; i++)
        {
            if ([[cellDictionery objectForKey:[NSIndexPath indexPathForRow:i inSection:0]] boolValue]) {
                
                [self tableView:theTableView accessoryButtonTappedForRowWithIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                
            }
        }
        allSelected = NO;
    }
    else
    {
        for (int i = 0; i<[theTableView numberOfRowsInSection:0]; i++)
        {
            if (![[cellDictionery objectForKey:[NSIndexPath indexPathForRow:i inSection:0]] boolValue]) {
                [self tableView:theTableView accessoryButtonTappedForRowWithIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            }
        }
        allSelected = YES;
    }
}
 */

/*
-(void)selectAllCalculations:(id)sender {
    
    selectAllBoxChecked = !selectAllBoxChecked;
    [selectAllcheckbox setSelected:selectAllBoxChecked];
    
    if ([self allCheckboxesSelected])
    {
        for (int i = 0; i<[theTableView numberOfRowsInSection:0]; i++)
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            if ([[cellDictionery objectForKey:indexPath] boolValue]) {
                
                [self tableView:theTableView accessoryButtonTappedForRowWithIndexPath:indexPath];
                
            }
        }
        
    }
    else
    {
        for (int i = 0; i<[theTableView numberOfRowsInSection:0]; i++)
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            if (![[cellDictionery objectForKey:indexPath] boolValue]) {
                [self tableView:theTableView accessoryButtonTappedForRowWithIndexPath:indexPath];
            }
           
        }
    }
    
    
}*/

-(void)selectAllRBC:(id)sender {
    
    if(!selectAllBoxChecked) {
        selectAllBoxChecked = !selectAllBoxChecked;
        [selectAllcheckbox setSelected:selectAllBoxChecked];
        
        [selectedRowsArray removeAllObjects];
        
        NSUInteger numberOfRowsInSection = [self.theTableView numberOfRowsInSection:0];
        for (NSUInteger r = 0; r < numberOfRowsInSection; ++r) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:r inSection:0];
            [selectedRowsArray addObject:indexPath];
        }
        
        rollingBearingCalculations = [selectedRowsArray count];
        [self.theTableView reloadData];
        
    }
    else {
        
        selectAllBoxChecked = !selectAllBoxChecked;
        [selectAllcheckbox setSelected:selectAllBoxChecked];
        
        [selectedRowsArray removeAllObjects];
        rollingBearingCalculations = 0;
        [self.theTableView reloadData];
        
    }
}


-(void)selectAllPBC:(id)sender {
    
    if(!selectAllPBCBoxChecked) {
        selectAllPBCBoxChecked = !selectAllPBCBoxChecked;
        [selectAllPBCBox setSelected:selectAllPBCBoxChecked];
        
        [selectedRowsArray removeAllObjects];
        
        NSUInteger numberOfRowsInSection = [self.theTableView numberOfRowsInSection:1];
        for (NSUInteger r = 0; r < numberOfRowsInSection; ++r) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:r inSection:1];
            [selectedRowsArray addObject:indexPath];
        }
        
        plainBearingCalculations = [selectedRowsArray count];
        [self.theTableView reloadData];
        
    }
    else {
        
        selectAllPBCBoxChecked = !selectAllPBCBoxChecked;
        [selectAllPBCBox setSelected:selectAllPBCBoxChecked];
        
        [selectedRowsArray removeAllObjects];
        plainBearingCalculations = 0;
        [self.theTableView reloadData];
        
    }
}

/*
- (BOOL)allCheckboxesSelected
{
    if(selectedCheckboxCount == [theTableView numberOfRowsInSection:0])
    {
        return YES;
    }
    
    return NO;
}



- (void)checkButtonTapped:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.theTableView];
    NSIndexPath *indexPath = [self.theTableView indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil)
    {
        [self tableView: self.theTableView accessoryButtonTappedForRowWithIndexPath: indexPath];
    }
}*/
/*- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    
    BOOL checked = [[cellDictionery objectForKey:indexPath] boolValue];

    if (checked)
    {
        selectedCheckboxCount--;
        
        if (selectAllBoxChecked)
        {
            
            [selectAllcheckbox setSelected:NO];
            allSelected = NO;
        }
        
    }else {
        selectedCheckboxCount++;
        if (selectedCheckboxCount == [theTableView numberOfRowsInSection:0]) {
            
            [selectAllcheckbox setSelected:YES];
            allSelected = YES;
            selectAllBoxChecked = YES;
        }
    }
    
    [cellDictionery setObject:[NSNumber numberWithBool:!checked] forKey:indexPath];
    UITableViewCell *cell = [theTableView cellForRowAtIndexPath:indexPath];
    UIButton *button = (UIButton *)cell.accessoryView;
    
    UIImage *newImage = (checked) ? [UIImage imageNamed:@"check2.png"] : [UIImage imageNamed:@"checked2.png"];
    [button setBackgroundImage:newImage forState:UIControlStateNormal];


}*/

/* version2
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    BOOL checked = [[cellDictionery objectForKey:indexPath] boolValue];
    
    UITableViewCell *cell = [theTableView cellForRowAtIndexPath:indexPath];
    
    UIButton *button = (UIButton *)cell.accessoryView;
    
    Calculation *calc = [calculationTypesArray objectAtIndex:indexPath.row];
    
    
    if(calc && [self isPlainBearingCalculation:calc.calculationId])
    {
        if (selectAllBoxChecked)
        {
            [cell setUserInteractionEnabled:false];
            checked = !checked;
            if(selectedPlainBearingCalc > 0)
            {
                --selectedPlainBearingCalc;
            }
        }
        else
        {
            [cell setUserInteractionEnabled:TRUE];
            if(checked)
            {
                --selectedPlainBearingCalc;
                if(selectedPlainBearingCalc==0)
                {
                    [self disabletOtherCalculations:TRUE];
                }
                
            }
            else
            {
                ++selectedPlainBearingCalc;
                if(selectedPlainBearingCalc==1)
                {
                    [self disabletOtherCalculations:FALSE];
                }
            }
            
        }
    }
    else
    {
        if (checked)
        {
            selectedCheckboxCount--;
            
            if (selectAllBoxChecked)
            {
                [selectAllcheckbox setSelected:NO];
                selectAllBoxChecked = NO;
            }
            
            if(selectedCheckboxCount==0)
            {
                [self disablePlainBearingCalculations:TRUE];
            }
            
        }else {
            selectedCheckboxCount++;
            
            if ([self allCheckboxesSelected]) {
                
                [selectAllcheckbox setSelected:YES];
                selectAllBoxChecked = YES;
            }
            
            if(selectedCheckboxCount==1)
            {
                [self disablePlainBearingCalculations:FALSE];
            }
            
        }
        
    }
    
    
    
    
    
    
    
    [cellDictionery setObject:[NSNumber numberWithBool:!checked] forKey:indexPath];
    
    UIImage *newImage = (checked) ? [UIImage imageNamed:@"check2.png"] : [UIImage imageNamed:@"checked2.png"];
    [button setBackgroundImage:newImage forState:UIControlStateNormal];
    
 
}
 */

/*
int lastusedsection = -1;

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    
    BOOL checked = [[cellDictionery objectForKey:indexPath] boolValue];
    
    UITableViewCell *cell = [theTableView cellForRowAtIndexPath:indexPath];
    
    UIButton *button = (UIButton *)cell.accessoryView;
    
    if(indexPath.section==0) {
        
        if(selectedPlainBearingCalc !=0) {
            
            [selectAllcheckbox setUserInteractionEnabled: NO];
            [selectAllcheckbox setSelected:NO];
            selectAllBoxChecked = NO;

            return;
        }
        
        if (checked) {
            
            selectedCheckboxCount--;
            
            if (selectAllBoxChecked) {
                [selectAllcheckbox setSelected:NO];
                selectAllBoxChecked = NO;
            }
            
            if(selectedCheckboxCount==0){
                
                [self disablePlainBearingCalculations:TRUE];
            }
            
        }else {
            
            showAlertRollingBearing = TRUE;
            showAlertPlainBearing = FALSE;
            selectedCheckboxCount++;
            
            if ([self allCheckboxesSelected]) {
                
                [selectAllcheckbox setSelected:YES];
                selectAllBoxChecked = YES;
            }
            
            //if(selectedCheckboxCount==1)
            
            if (lastusedsection != 100 && (lastusedsection == 2 || lastusedsection < 0)) {
            
                if(selectedPlainBearingCalc ==0) {
                
                
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Selected Rolling bearing calculation."
                                                                        message:@"So Plain bearing calculations can't be selected"
                                                                       delegate:self
                                                              cancelButtonTitle:@"Don't show again"
                                                              otherButtonTitles:@"OK", nil];
                    alertView.tag = 110;
                    [alertView show];
                    [alertView release];
                    
               

                if(lastusedsection != 100){
                    
                    lastusedsection = 1;
                }

                
                [self disablePlainBearingCalculations:FALSE];
                    
                    
                }
            }
            
            
        }

    }
    else if(indexPath.section==1)
    {
        if(!checked){

            showAlertRollingBearing = FALSE;
            showAlertPlainBearing = TRUE;
            
            if (lastusedsection != 100 && (lastusedsection == 1 || lastusedsection < 0)) {
        
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Selected Plain bearing calculation."
                                                                message:@"So Rolling bearing calculations can't be selected"
                                                               delegate:self
                                                      cancelButtonTitle:@"Don't show again"
                                                      otherButtonTitles:@"OK", nil];
                alert.tag = 111;
                [alert show];
                [alert release];
                
                if(lastusedsection != 100){
                    lastusedsection = 2;
                }

                [selectAllcheckbox setSelected:NO];
                [selectAllcheckbox setUserInteractionEnabled:false];
                [self disabletOtherCalculations:FALSE]; // used on true
            }
            
            ++selectedPlainBearingCalc;
            //if(selectedPlainBearingCalc==1)
        }
        else {
    
            --selectedPlainBearingCalc;
            if(selectedPlainBearingCalc==0)
            {
                [selectAllcheckbox setSelected:NO];
                [selectAllcheckbox setUserInteractionEnabled:true];
                [self disabletOtherCalculations:TRUE];
            }
            
        }
    }
    
    
    [cellDictionery setObject:[NSNumber numberWithBool:!checked] forKey:indexPath];
    
    UIImage *newImage = (checked) ? [UIImage imageNamed:@"check2.png"] : [UIImage imageNamed:@"checked2.png"];
    [button setBackgroundImage:newImage forState:UIControlStateNormal];
    
    
}


-(void)checkboxSelected:(id)sender{
    
    
    if([calculationCheckbox isSelected]==YES)
    {
        [calculationCheckbox setSelected:NO];
    }
    else{
        [calculationCheckbox setSelected:YES];
    }
    
}

*/


- (IBAction)showCalculationInfoPopup:(UIButton *)sender {
    
    
    UIView *view = sender.superview;
    while (view && ![view isKindOfClass:[UITableViewCell self]]) view = view.superview;
    
    UITableViewCell *cell = (UITableViewCell *)view;
    NSIndexPath *indexPath  = [self.theTableView indexPathForCell:cell];
    
    Calculation *calc = nil;
    if(indexPath.section==0)
    {
        calc = [calculationTypesArray objectAtIndex:indexPath.row];
    }
    else if (indexPath.section==1)
    {
        calc = [plainBearingCalcArray objectAtIndex:indexPath.row];
    }
    
    currentCalculation = calc;
    
    
    currentCalculation = calc;
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:calc.presentation
                                                    message:calc.description
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:nil];
    
    if (calc.calculationUrl != nil) {
        [alert addButtonWithTitle:[calculationDataManager getTranslationString:@"More_info"]];
    }
    [alert addButtonWithTitle:[calculationDataManager getTranslationString:@"Close"]];
    alert.delegate  = self;
    [alert show];
    
    
    
}

- (void)alertView:(TSAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 101)
    {
        return;
    }
    if ((alertView.tag == 110 && buttonIndex == 0) || (alertView.tag == 111 && buttonIndex == 0)) {
        
            // Don't show alert for plain & rolling bearings
        NSUserDefaults *calcUserDefaults = [NSUserDefaults standardUserDefaults];
        if ([calcUserDefaults boolForKey:@"alertDisplayed"] == NO) {
            
            [calcUserDefaults setBool:YES forKey:@"alertDisplayed"];
            //lastusedsection = 100;
            
        } else {
            
            [calcUserDefaults setBool:NO forKey:@"alertDisplayed"];
        }
        
        [calcUserDefaults synchronize];
        
    }
    
    if (currentCalculation.calculationUrl != nil) {
        switch (buttonIndex) {
            case 0:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: currentCalculation.calculationUrl]];
                break;
            case 1:
                break;
                
            default:
                break;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellText = [[calculationTypesArray objectAtIndex:indexPath.row] presentation];;
    UIFont *cellFont = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
    CGSize constraintSize = CGSizeMake(MAXFLOAT, MAXFLOAT);
    CGSize labelSize = [cellText sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
    
    return labelSize.height + 25;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    /** Modified for MultipleCalc */
    
//    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
//    calculationDataManager.selectedCalculation = (Calculation *)[calculationTypesArray objectAtIndex:indexPath.row];
//    
//    FilterBearingsViewController_iPhone *filterBearingsViewController = [[FilterBearingsViewController_iPhone alloc] initWithNibName:@"FilterBearingsViewController" bundle:nil];
//    
//    [self.navigationController pushViewController:filterBearingsViewController animated:YES];
//    [filterBearingsViewController release];
    
    //[self tableView: self.theTableView accessoryButtonTappedForRowWithIndexPath: indexPath];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if(cell.accessoryType == UITableViewCellAccessoryNone) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [selectedRowsArray addObject:indexPath];
        
        if(indexPath.section==0) {
            if (rollingBearingCalculations==0) {
                [self enablePlainBearingCalculations:FALSE];
                [selectAllPBCBox setEnabled:FALSE];
            }
            
            ++rollingBearingCalculations;
            
            if(selectedRowsArray.count == calculationTypesArray.count) {
                selectAllBoxChecked = YES;
                [selectAllcheckbox setSelected:selectAllBoxChecked];
            }

            
        }
        else if(indexPath.section==1) {
            if (plainBearingCalculations==0) {
                [self enableRollingBearingCalculations:FALSE];
                [self.selectAllcheckbox setEnabled:FALSE];
            }
            
            ++plainBearingCalculations;
            
            if(selectedRowsArray.count == plainBearingCalcArray.count) {
                selectAllPBCBoxChecked = YES;
                [selectAllPBCBox setSelected:selectAllPBCBoxChecked];
            }
        }
        
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
        [selectedRowsArray removeObject:indexPath];
        
        if(indexPath.section==0) {
            --rollingBearingCalculations;
            if (rollingBearingCalculations==0) {
                [self enablePlainBearingCalculations:TRUE];
                [selectAllPBCBox setEnabled:TRUE];
            }
            
            if(selectedRowsArray.count < calculationTypesArray.count) {
                selectAllBoxChecked = NO;
                [selectAllcheckbox setSelected:selectAllBoxChecked];
            }

        }
        else if(indexPath.section==1) {
            --plainBearingCalculations;
            if (plainBearingCalculations==0) {
                [self enableRollingBearingCalculations:TRUE];
                [self.selectAllcheckbox setEnabled:TRUE];
            }
            
            if(selectedRowsArray.count < plainBearingCalcArray.count) {
                selectAllPBCBoxChecked = NO;
                [selectAllPBCBox setSelected:selectAllPBCBoxChecked];
            }
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (void)viewDidUnload
{
    //[self setInfoButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager removeObserver:self];
    
    [reportsViewController release];
    reportsViewController = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    CalculationDataManager* calculatorDataManager = [CalculationDataManager sharedManager];
    [calculatorDataManager removeObserver:self];

   // [infoButton release];
    [super dealloc];
}

- (void)setSearchingModeStart {
    
    CalculationDataManager* calculationDataManager = [CalculationDataManager sharedManager];
    
    HUD = [[MBProgressHUD alloc] initWithFrame:CGRectMake(35, 75, 250, 250)];
    HUD.labelText = [calculationDataManager getTranslationString:@"Searching"];
    HUD.graceTime = 0.1;
    HUD.taskInProgress = YES;
    HUD.allowsCancelation = YES;
    HUD.delegate = self;
    HUD.labelFont = [UIFont fontWithName:CHEVIN_BOLD size:20.f];
    [self.view addSubview:HUD];
    //[self.view setUserInteractionEnabled:NO];
    proceedButton.userInteractionEnabled = NO;
    

    [HUD show:YES];
    
}
- (void)setSearchingModeEnd {
    
    [HUD hide:YES];
    HUD.taskInProgress = NO;
    [HUD removeFromSuperview];
    [self.view setUserInteractionEnabled:YES];
    proceedButton.userInteractionEnabled = YES;

    [HUD release];
    HUD = nil;
}

-(IBAction)reportsButtonClicked:(id)sender{
   
    if(reportsViewController==nil)
    {
        reportsViewController = [[ReportsViewController alloc] initWithNibName:@"ReportsViewController" bundle:[NSBundle mainBundle]];
    }
    
    [self.navigationController pushViewController:reportsViewController animated:YES];
   
    
}


- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // rows in section 0 should not be selectable if rows in section 1 is selected
    if ( indexPath.section == 0 && plainBearingCalculations>0)
    {
        return nil;
    }
   
    // rows in section 0 should not be selectable if rows in section 1 is selected
    if ( indexPath.section == 1 && rollingBearingCalculations>0)
    {
        return nil;
    }
    
    // By default, allow row to be selected
    return indexPath;
}



@end
