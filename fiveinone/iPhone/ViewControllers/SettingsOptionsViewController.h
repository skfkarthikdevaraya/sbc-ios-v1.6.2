//
//  SettingsOptionsViewController.h
//  fiveinone
//
//  Created by Tommy Eriksson on 4/5/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SettingsOptionsDelegate
@required
-(void)didSelectOption:(NSInteger)index withTag:(NSInteger)tag;
@end

@interface SettingsOptionsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain) IBOutlet UITableView* tableView;
@property (nonatomic, retain) NSArray* possibleValues;
@property (nonatomic) NSInteger selectedIndex;
@property (nonatomic, assign) id<SettingsOptionsDelegate> delegate;

@property (nonatomic) NSInteger tag;

@property(nonatomic,retain) NSIndexPath *selectedIndexPath;



@end
