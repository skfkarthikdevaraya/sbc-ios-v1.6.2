//
//  SettingsOptionsViewController.m
//  fiveinone
//
//  Created by Tommy Eriksson on 4/5/12.
//  Copyright (c) 2012 HiQ. All rights reserved.
//

#import "SettingsOptionsViewController.h"
#import "TitleManager.h"
#import "CalculationDataManager.h"
#import "SettingsOptionsViewController.h"
#import "SettingsViewController.h"

@implementation SettingsOptionsViewController

@synthesize possibleValues;
@synthesize selectedIndex;
@synthesize delegate;
@synthesize tag;
@synthesize tableView;

@synthesize selectedIndexPath;


- (id) init
{
    self = [super init];
    if (!self) return nil;
        return self;
}

-(void)dealloc
{
    self.possibleValues = nil;
    self.delegate = nil;
    [super dealloc];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.backgroundView = nil;
    
    self.navigationItem.rightBarButtonItem = [TitleManager getSKFLogo];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(yesMeasurmentNotification:) name:@"YesMeasurmentNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noMeasurmentNotification:) name:@"NoMeasurmentNotification" object:nil];


    /*** iOS 7 - TO remove the space of header ***/
    
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 0.01f)];
    
    /*** END ***/
    
  }



- (void)viewDidUnload
{
    [super viewDidUnload];
    self.tableView.backgroundView = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.preferredContentSize = CGSizeMake(320, [tableView contentSize].height);
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    /*NSString *navigationBarTintColor = NAVIGATION_TEXT_COLOR;
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithHexString:navigationBarTintColor]];*/
    
    self.navigationItem.titleView = [TitleManager getTitle:self.title];
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    self.preferredContentSize = CGSizeMake(320, [tableView contentSize].height);
    [super viewDidAppear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [possibleValues count];
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        cell.textLabel.userInteractionEnabled = false;
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
        cell.textLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
    }
    
    cell.textLabel.text = [possibleValues objectAtIndex:indexPath.row];
    
    if (indexPath.row == selectedIndex){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;        
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndex = indexPath.row;
    
    self.selectedIndexPath = indexPath;
    
    //[aTableView reloadData];
    
   /* UITableViewCell *cell = [aTableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;

    [delegate didSelectOption:indexPath.row withTag:tag];
    // [self.navigationController popViewControllerAnimated:TRUE];*/
    NSLog(@"Selected Row:,Tag: %ld,%ld",(long)indexPath.row,(long)tag);
    NSLog(@"Selected Row: %ld",(long)indexPath.row);
    NSLog(@"Tag: %ld",(long)tag);
    

    
     [delegate didSelectOption:indexPath.row withTag:tag];


   
}


- (void) yesMeasurmentNotification:(NSNotification *) notification
{
    [self.tableView reloadData];

    UITableViewCell *cell = [self.tableView  cellForRowAtIndexPath:self.selectedIndexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;

}

- (void) noMeasurmentNotification:(NSNotification *) notification
{
   // [self.tableView reloadData];

//    UITableViewCell *cell = [self.tableView  cellForRowAtIndexPath:self.selectedIndexPath];
//    cell.accessoryType = UITableViewCellAccessoryNone;
    [tableView deselectRowAtIndexPath:self.selectedIndexPath animated:YES];
    
}

@end

