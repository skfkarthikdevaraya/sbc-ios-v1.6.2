//
//  Settings.h
//  DialSet
//
//  Created by Martin Lannsjö on 9/7/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    LANGUAGE,
    UNIT,
    LEGAL
} SettingsMenu;

@interface SettingsView : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UIButton *btnSave;
    IBOutlet UITableView *tblSettings;
}

@property (nonatomic,retain) UIButton *btnSave;
@property (nonatomic,retain) UITableView *tblSettings;
@property (nonatomic,retain) IBOutlet UILabel *lblDialSet;
@property (nonatomic,retain) IBOutlet UILabel *lblDialSet2;
@property (nonatomic,retain) IBOutlet UILabel *lblVersion;

- (IBAction)buttonClicked:(id)sender;
@end
