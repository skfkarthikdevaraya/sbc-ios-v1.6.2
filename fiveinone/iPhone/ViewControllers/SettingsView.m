//
//  Settings.m
//  DialSet
//
//  Created by Martin Lannsjö on 9/7/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "SettingsView.h"
#import "LegalInformation.h"

@implementation SettingsView

@synthesize tblSettings, btnSave, lblDialSet, lblVersion, lblDialSet2;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tblSettings reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    tblSettings.backgroundColor = [UIColor clearColor];
    
    NSString *red_color = COLOR_SKF_RED;
    NSString *grey_color = TEXT_COLOR_GREY;
    lblDialSet.text = @"DialSet";
    lblDialSet2.text = @"Re-lubrication Calculation Program";
    lblVersion.text = [self getVersion];
    lblDialSet.font = [UIFont fontWithName:CHEVIN_MEDIUM size:30];
    lblDialSet2.font = [UIFont fontWithName:CHEVIN_MEDIUM size:14];
    lblVersion.font = [UIFont fontWithName:CHEVIN_MEDIUM size:12];
    lblDialSet.textColor = [UIColor colorWithHexString:red_color];
    lblDialSet2.textColor = [UIColor colorWithHexString:grey_color];
    lblVersion.textColor = [UIColor colorWithHexString:grey_color];
    
    UIImage *button_up_image = [UIImage imageNamed:@"button_up.png"];
    btnSave.titleLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
    NSString* textColor = TEXT_COLOR_GREY;
    [btnSave setTitleColor:[UIColor colorWithHexString:textColor] forState:UIControlStateNormal];
    [btnSave setTitleShadowColor:[UIColor clearColor] forState:UIControlStateNormal];
    [btnSave setBackgroundImage:button_up_image forState:UIControlStateNormal];
    self.navigationItem.hidesBackButton = YES;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)buttonClicked:(id)sender{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
    [self.navigationController popToRootViewControllerAnimated:YES];
    [UIView commitAnimations];
}

-(void)dealloc{
    [btnSave release];
    [tblSettings release];
	[super dealloc];
}

- (NSString*) getVersion {
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    return [NSString stringWithFormat:@"Version: %@", version];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    if (indexPath.row == LANGUAGE) {
        NSString* cellIdentifier = @"uiTableViewCell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier] autorelease];
        }
        
        NSString* title = NSLocalizedString(@"SETTINGS.LANGUAGE", nil);
        cell.textLabel.text = title;
    } else if (indexPath.row == UNIT){
        
        NSString* cellIdentifier = @"uiTableViewCell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier] autorelease];
        }
        
        NSString* title = NSLocalizedString(@"SETTINGS.UNIT_SYSTEM", nil);
        cell.textLabel.text = title;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    }else{
        NSString* cellIdentifier = @"uiTableViewCell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier] autorelease];
        }
        
        NSString* title = NSLocalizedString(@"SETTINGS.LEGAL", nil);
        cell.textLabel.text = title;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    cell.textLabel.font       = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
    cell.detailTextLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
    return cell;
    
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == LANGUAGE) { }
    if (indexPath.row == UNIT) { }
    if (indexPath.row == LEGAL) {
        LegalInformation *view = [[[LegalInformation alloc]  initWithNibName:@"LegalInformation"  bundle:nil] autorelease];
        [self.navigationController pushViewController:view animated:YES]; 
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
}

- (void) didSelectValue:(NSString *)selectedKey identifier:(NSString *)identifier{

    if ([identifier  isEqual: @"SETTINGS.UNIT_SYSTEM"]){
        //appDelegate.unit  = selectedKey;
    }else if ([identifier  isEqual: @"SETTINGS.LANGUAGE"]){
        //appDelegate.language = selectedKey;
    }
}

@end
