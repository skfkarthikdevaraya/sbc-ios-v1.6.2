//
//  settingsViewController.h
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/13/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "CalculationDataManager.h"
#import "SettingsOptionsViewController.h"

@interface SettingsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, CalculationDataManagerDelegate, SettingsOptionsDelegate, MFMailComposeViewControllerDelegate>
{
    NSArray* _languages;
    NSArray* _measurements;
    
    UIView* _headerView;
    BOOL _showHeaderView;
    
    NSInteger selectedIndex;

    BOOL isChangeMeasurment;
    
    
}

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutlet UILabel *appTitleLabel;
@property (nonatomic, retain) IBOutlet UILabel *appShortDescLabel;
@property (nonatomic, retain) IBOutlet UIView *headerView;
@property (nonatomic, assign) BOOL showHeaderView;
@property (nonatomic, assign) BOOL isChangeMeasurment;


@end