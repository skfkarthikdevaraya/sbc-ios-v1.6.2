//
//  settingsViewController.m
//  fiveinone
//
//  Created by Kristoffer Nilsson on 9/13/11.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsOptionsViewController.h"
#import "LegalInformation_iPhone.h"
#import "TitleManager.h"

@implementation SettingsViewController

@synthesize tableView;
@synthesize appTitleLabel;
@synthesize appShortDescLabel;
@synthesize headerView = _headerView;
@synthesize isChangeMeasurment;

#define CELL_LANG_TAG 10
#define CELL_MEASURE_TAG 20
#define CELL_FEEDBACK_TAG 30
#define CELL_LEGAL_TAG 40
#define CTRL_NAME_TAG  110
#define CTRL_VALUE_TAG 120
#define TAG_LANG_SETTING 10
#define TAG_MEASURE_SETTING 20

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _showHeaderView = TRUE;
    }
    return self;
}

-(void)dealloc
{
    [_measurements release];
    _measurements = nil;
    [_languages release];
    _languages = nil;
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager removeObserver:self];
    [super dealloc];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (BOOL)showHeaderView{
    return _showHeaderView;
}

- (void) setShowHeaderView:(BOOL)showHeaderView{
    if (_showHeaderView != showHeaderView){
        _showHeaderView = showHeaderView;
        
        if (showHeaderView){
            [_headerView setHidden:FALSE];
            self.tableView.frame = CGRectOffset(self.tableView.frame, 0, self.headerView.frame.size.height);
        } else {
            [_headerView setHidden:TRUE];
            self.tableView.frame = CGRectOffset(self.tableView.frame, 0, -self.headerView.frame.size.height);
        }
    }
}

- (void)viewDidLoad
{    
    [super viewDidLoad];
    
    // iOS 7 - To remove Table view header title space ***/
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 0.01f)];
    
    self.tableView.backgroundView = nil;
    self.navigationItem.rightBarButtonItem = [TitleManager getSKFLogo];
    
    if (_showHeaderView){
        self.navigationItem.leftBarButtonItem = 
        [[[UIBarButtonItem alloc] initWithTitle:GetLocalizedString(@"Done")
                                         style:UIBarButtonItemStylePlain
                                        target:self 
                                        action:@selector(dismissSettings:)] autorelease];
    }
    
    appShortDescLabel.text = @"";
    appTitleLabel.text = @"";
    
    if (!_showHeaderView){
        [_headerView setHidden:TRUE];
        self.tableView.frame = CGRectOffset(self.tableView.frame, 0, -self.headerView.frame.size.height);       
    }

    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager initTranslation:self];
    [calculationDataManager addObserver:self];
    [calculationDataManager getLanguages:self];
    [calculationDataManager getMeasurements:self];
    appTitleLabel.font           = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_APPTITLE];
    appTitleLabel.textColor      = [UIColor colorWithRed:218.0/255.0 green:30.0/255.0 blue:60.0/255.0 alpha:1];
    appShortDescLabel.font       = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_APPVERSION];
    appShortDescLabel.textColor  = [UIColor colorWithHexString:@"5e5e5e"];
    isChangeMeasurment = NO;
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
    [calculationDataManager removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.preferredContentSize = CGSizeMake(320, tableView.contentSize.height);
    [self forcePopoverSize];
}

- (void) forcePopoverSize {
    CGSize currentSetSizeForPopover = self.preferredContentSize;
    CGSize fakeMomentarySize = CGSizeMake(currentSetSizeForPopover.width - 1.0f, currentSetSizeForPopover.height - 1.0f);
    self.preferredContentSize = fakeMomentarySize;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.preferredContentSize = CGSizeMake(320, tableView.contentSize.height);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)dismissSettings:(id)sender
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
    [self.navigationController popViewControllerAnimated:YES];
    [UIView commitAnimations];

    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];  
    // Force reload of translation from server side.
    [calculationDataManager getTranslation:self];
}

- (void)didSelectOption:(NSInteger)index withTag:(NSInteger)tag
{
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];    
    
    if (tag == TAG_LANG_SETTING){
        /*[calculationDataManager setUserSelectedLanguageWithIndex:index];
        [calculationDataManager getLanguages:self];
        [calculationDataManager getTranslation:self];*/
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
    if (tag == TAG_MEASURE_SETTING){
            if ([calculationDataManager getUserSelectedMeasurementAsIndex] != index) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unit System"
                                                        message:GetLocalizedString(@"Unit_change_measurment")
                                                        delegate:self
                                                        cancelButtonTitle:GetLocalizedString(@"No")
                                                        otherButtonTitles:GetLocalizedString(@"Yes"), nil];
                alert.alertViewStyle = UIAlertActionStyleDefault;
                alert.tag = 3;
                [alert show];
                [alert release];
            }
        selectedIndex = index;
    }
    [calculationDataManager resetInputParameters];
    [tableView reloadData];
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 3) {
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        if (buttonIndex == 1) {
            NSMutableDictionary *sendParams = [NSMutableDictionary dictionary];
            [calculationDataManager setUserSelectedMeasurementWithIndex:selectedIndex];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"YesMeasurmentNotification" object:self];
            [calculationDataManager getUpdatedProducts:sendParams userData:self];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else if (buttonIndex == 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NoMeasurmentNotification" object:self];
        }
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfSectionsInTableView:(NSInteger)section
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
    int numberOfRows = (_languages != nil ? 1 : 0);
    numberOfRows += (_measurements != nil ? 1 : 0);
    numberOfRows += 1; /* Send feedback to SKF */
    numberOfRows += 1; /* Show legal inforation */;
    
    return numberOfRows;
}

#define ROW_ID_LANGUAGE    0
#define ROW_ID_MEASUREMENT 1
#define ROW_ID_FEEDBACK    2
#define ROW_ID_LEAGAL      3

- (int)rowNumberForRowId:(int)rowId{
    int rowNumLang = _languages != nil ? 0 : -1;
    int rowNumMeasure = _measurements != nil ? rowNumLang + 1 : -1;
    int rowNumFeedback = MAX(rowNumLang, rowNumMeasure) + 1;
    int rowNumLeagal = rowNumFeedback + 1;
    
    switch (rowId) {
        case ROW_ID_LANGUAGE:
            return rowNumLang;
            break;
        case ROW_ID_MEASUREMENT:
            return rowNumMeasure;
            break;
        case ROW_ID_FEEDBACK:
            return rowNumFeedback;
            break;
        case ROW_ID_LEAGAL:
            return rowNumLeagal;
            break;
            
        default:
            return -1;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"SettingsCell";
    UITableViewCell* cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];

    UILabel* nameLabel = nil;
    UILabel* parametersLabel = nil;
    
    CGRect centerNameLabelRect = CGRectMake(20, 12, 280, 20);
    CGRect topMostNameLabelRect = CGRectMake(20, 6, 280, 20);
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 6, 280, 20)];
        nameLabel.userInteractionEnabled = false;
        nameLabel.backgroundColor = [UIColor clearColor];
        nameLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE];
        nameLabel.textColor = [UIColor colorWithHexString:TEXT_COLOR_LT_BLACK];
        nameLabel.tag = CTRL_NAME_TAG;        
        [cell addSubview:nameLabel];
        [nameLabel release];
        
        parametersLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 26, 280, 12)];
        parametersLabel.textColor = [UIColor grayColor];
        parametersLabel.tag = CTRL_VALUE_TAG;
        parametersLabel.font = [UIFont fontWithName:CHEVIN_MEDIUM size:FONT_SIZE_FOR_TABLE_SUBTEXT];
        parametersLabel.backgroundColor = [UIColor clearColor];
        [cell addSubview:parametersLabel];
        [parametersLabel release];
    }
    
    nameLabel = (UILabel *)[cell viewWithTag:CTRL_NAME_TAG];
    parametersLabel = (UILabel *)[cell viewWithTag:CTRL_VALUE_TAG];
    
    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];    
    
    BOOL isLangugeRow = indexPath.row == [self rowNumberForRowId:ROW_ID_LANGUAGE];
    BOOL isMeasurementsRow = indexPath.row == [self rowNumberForRowId:ROW_ID_MEASUREMENT];
    BOOL isFeedbackRow = indexPath.row == [self rowNumberForRowId:ROW_ID_FEEDBACK];
    BOOL isLegalRow = indexPath.row == [self rowNumberForRowId:ROW_ID_LEAGAL];
    
    if (isLangugeRow){
        nameLabel.text = GetLocalizedString(@"Language");
        nameLabel.frame = topMostNameLabelRect;
        parametersLabel.text = [calculationDataManager getUserSelectedLanguageAsDescription];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.tag = CELL_LANG_TAG;
    } else if (isMeasurementsRow){
        nameLabel.text = GetLocalizedString(@"select_unit_system");
        nameLabel.frame = topMostNameLabelRect;
        parametersLabel.text = [calculationDataManager getUserSelectedMeasurementAsDescription];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.tag = CELL_MEASURE_TAG;
    } else if (isFeedbackRow) {
        nameLabel.text = GetLocalizedString(@"SendFeedbackToSKF");
        nameLabel.frame = centerNameLabelRect;
        parametersLabel.text = @"";
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.tag = CELL_FEEDBACK_TAG;
    } else if (isLegalRow) {
        nameLabel.text = GetLocalizedString(@"Show_legal");
        nameLabel.frame = centerNameLabelRect;
        parametersLabel.text = @"";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.tag = CELL_LEGAL_TAG;
    }
    [cell setSelectionStyle:UITableViewCellSeparatorStyleNone];
        self.tableView.separatorColor = [UIColor lightGrayColor];

    return cell;
}

-(void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger tag = [aTableView cellForRowAtIndexPath:indexPath].tag;
    BOOL isLanguageSelect = tag == CELL_LANG_TAG;
    BOOL isMeaurementSelect = tag == CELL_MEASURE_TAG;
    BOOL isFeedbackSelect = tag == CELL_FEEDBACK_TAG;
    BOOL isLegalSelect = tag == CELL_LEGAL_TAG;

    CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];    

    SettingsOptionsViewController* optionsViewController = [[SettingsOptionsViewController alloc] initWithNibName:@"SettingsOptionsViewController" bundle:nil];
    optionsViewController.delegate = self;
    
    if (isLanguageSelect){
        optionsViewController.title = GetLocalizedString(@"Language");
        optionsViewController.possibleValues = [calculationDataManager getLanguageValues];
        optionsViewController.selectedIndex = [calculationDataManager getUserSelectedLanguageAsIndex];
        optionsViewController.tag = TAG_LANG_SETTING;
    }
    
    if (isMeaurementSelect){
        optionsViewController.title = GetLocalizedString(@"select_unit_system");
        optionsViewController.possibleValues = [calculationDataManager getMeasurementValues];
        optionsViewController.selectedIndex = [calculationDataManager getUserSelectedMeasurementAsIndex];
        optionsViewController.tag = TAG_MEASURE_SETTING;
    }
    
    if (isLegalSelect){
        LegalInformation_iPhone* legal = [[[LegalInformation_iPhone alloc] initWithNibName:@"LegalInformation" bundle:nil] autorelease];
        legal.withoutAgree = TRUE;
        [self.navigationController pushViewController:legal animated:TRUE];
    }
    
    if (isFeedbackSelect){
        [self sendMail];
    }
    
    if (isLanguageSelect || isMeaurementSelect){
        [self.navigationController pushViewController:optionsViewController animated:TRUE];
    }
    
    [aTableView deselectRowAtIndexPath:indexPath animated:FALSE];
    [optionsViewController release];
}

- (void)dataManagerReturnedTranslation:(BOOL)success
{
    if (success)
    {
        self.navigationItem.titleView = [TitleManager getTitle:GetLocalizedString(@"Settings")];
        
        if (_showHeaderView){
            self.navigationItem.leftBarButtonItem = 
            [[UIBarButtonItem alloc] initWithTitle:GetLocalizedString(@"Done") 
                                             style:UIBarButtonItemStylePlain
                                            target:self 
                                            action:@selector(dismissSettings:)]; 
        }

        appTitleLabel.text = GetLocalizedString(@"Application_name");
        appShortDescLabel.text = GetLocalizedString(@"Application_short_description");
        self.title = GetLocalizedString(@"Settings");
        
        [tableView reloadData];
        self.preferredContentSize = CGSizeMake(320, tableView.contentSize.height);
    }
}

-(void)dataManagerReturnedLangauges:(BOOL)success
{
    if (success){
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        _languages = calculationDataManager.languageArray; 
        [_languages retain];
        [tableView reloadData];
        self.preferredContentSize = CGSizeMake(320, tableView.contentSize.height);
    }    
}

-(void)dataManagerReturnedMeasurements:(BOOL)success
{
    if (success){
        CalculationDataManager *calculationDataManager = [CalculationDataManager sharedManager];
        _measurements = calculationDataManager.measurementArray; 
        [_measurements retain];
        [tableView reloadData];
        self.preferredContentSize = CGSizeMake(320, tableView.contentSize.height);
    }        
}

- (IBAction)sendMail
{
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:[NSString stringWithFormat:@"%@ %@", GetLocalizedString(@"Application_name"), GetLocalizedString(@"feedback")]];
        NSArray *toRecipients = [NSArray arrayWithObject:GetLocalizedString(@"infoEmail")];
        [mailer setToRecipients:toRecipients];
        NSString* emailBody = [NSString stringWithFormat:@"\n\n\n%@ %@", GetLocalizedString(@"This_is_a_comment_about_application"), GetLocalizedString(@"Application_name")];
        [mailer setMessageBody:emailBody isHTML:NO];
        //[self presentModalViewController:mailer animated:YES];
        [self presentViewController:mailer animated:YES completion:nil];
        [mailer release];
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:GetLocalizedString(@"Error")
                                                        message:GetLocalizedString(@"noMailer.")
                                                       delegate:nil
                                              cancelButtonTitle:GetLocalizedString(@"OK")
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    //[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}




@end
