//
//  fiveinoneAppDelegate_iPhone.h
//  fiveinone
//
//  Created by Erik Lindberg on 2011-08-19.
//  Copyright 2011 HiQ. All rights reserved.
//

#import "fiveinoneAppDelegate.h"

@interface fiveinoneAppDelegate_iPhone : fiveinoneAppDelegate

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;

@end
