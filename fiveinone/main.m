//
//  main.m
//  fiveinone
//
//  Created by Erik Lindberg on 2011-08-19.
//  Copyright 2011 HiQ. All rights reserved.
//

#import <UIKit/UIKit.h>


int main(int argc, char *argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}



